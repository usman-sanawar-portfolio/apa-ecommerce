﻿using DRS.APA.Domain.Core.Domain.Common;


namespace ServiceManagement.Domain.Core.Domain
{
    public class Region : AuditableEntity
    {
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RegionId { get; set; }

        public string RegionName { get; set; }
        public string Description { get; set; }
      
        //[ForeignKey("OrganizationId")]
        //public int OrganizationId { get; set; }
        //public Organization Organization { get; set; }
       // public ICollection<Country> Countries { get; set; }
    }
}
