﻿using DRS.APA.Core.Domain;
using System.Collections.Generic;
using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class ProductCategory : AuditableEntity
    {
       public short ProductCategoryId { get; set; }
       public string Name { get; set; }
       public short? ParentCategoryId { get; set; }
       public ProductCategory ParentCategory { get; set; }    
       public string ThumbPath { get; set; }
       public string MetaKeywords { get; set; }
        /// <summary>
        /// Gets or sets the meta description
        /// </summary>
        public string MetaDescription { get; set; }
        /// <summary>
        /// Gets or sets the meta title
        /// </summary>
        public string MetaTitle { get; set; }
        public bool VisibleOnMainMenu { get; set; }
        public string UrlSeo { get; set; }
        /// <summary>
        /// Category code
        /// Retrofit code= ret
        /// Operator code= op
        /// Barrier code= br
        /// </summary>
        public string Code { get; set; }
        public ICollection<ProductCategoriesJunction> ProductCategoriesJunction { get; set; } = new List<ProductCategoriesJunction>();
        public ICollection<HomePageCollection> HomePageCollection { get; set; } = new List<HomePageCollection>();

    }
}
