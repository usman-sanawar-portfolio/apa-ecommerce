﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;
namespace DRS.APA.Domain.Core.Domain.MasterData

{
    public class ReturnReason : AuditableEntity
    {
        public int ReturnReasonId { get; set; }
        public string Name { get; set; }
    }
}
