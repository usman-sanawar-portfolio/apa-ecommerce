﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Domain.MasterData
{
    public class CategoryCode
    {
        public int CategoryCodeId { get; set; }
        public string ProductName { get; set; }
        public string Code { get; set; }
    }
}
