﻿using DRS.APA.Domain.Core.Domain.Common;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class FileLibrary : AuditableEntity
    {
        public short FileLibraryId { get; set; }

        public string Name { get; set; }

        public string FileName { get; set; } 
        public string Type { get; set; }

    }
}
