﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData

{
    public class OrderIdentifier : AuditableEntity
    {      
        public int OrderIdentifierId { get; set; }
    }
}
