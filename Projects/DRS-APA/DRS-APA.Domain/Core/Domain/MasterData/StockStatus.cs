﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;
namespace DRS.APA.Domain.Core.Domain.MasterData

{
    public class StockStatus : AuditableEntity
    {
        public int StockStatusId { get; set; }
        public string Name { get; set; }
        public ICollection<Product> Products { get; set; } = new List<Product>();


    }
}
