using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Text;
namespace DRS.APA.Domain.Core.Domain.MasterData

{
    public class OrderStatus : AuditableEntity
    {
        public int OrderStatusId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        /// <summary>
        /// Fixed types>Order,SaleOrder
        /// </summary>
        public string Type { get; set; }
        public ICollection<SaleOrder> SaleOrder { get; set; } = new List<SaleOrder>();
        public ICollection<Orders> Orders { get; set; } = new List<Orders>();


    }
}
