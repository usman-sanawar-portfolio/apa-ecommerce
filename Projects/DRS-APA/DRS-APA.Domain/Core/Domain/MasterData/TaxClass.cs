using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData

{
    public class TaxClass : AuditableEntity
    {      
        public int TaxClassId { get; set; }
        public string TaxClassTitle { get; set; }
        public string Description { get; set; }
        public ICollection<Product> Products { get; set; } = new List<Product>();
        public ICollection<TaxRate> TaxRates { get; set; } = new List<TaxRate>();    
        public ICollection<SaleOrderLines> SaleOrderLines{ get; set; } = new List<SaleOrderLines>();
        public ICollection<OrderLines> OrderLines { get; set; } = new List<OrderLines>();
        public ICollection<ApplicableTaxOnShipment> ApplicableTaxOnShipment { get; set; } = new List<ApplicableTaxOnShipment>();

    }
}
