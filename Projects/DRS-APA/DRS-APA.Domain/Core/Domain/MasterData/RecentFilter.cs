﻿using System;


namespace ShopApp.Core.Models
{

    [Serializable()]
    public class RecentFilter
    {
        public string Name { get; set; }
        public string Value { get; set; }
        

    }
}

