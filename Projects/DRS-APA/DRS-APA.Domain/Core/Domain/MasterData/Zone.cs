﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData

{
    public class Zone : AuditableEntity
    {      
        public int ZoneId { get; set; }
        public string ZoneName { get; set; }
        public string ZoneCode { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }

        public ICollection<ZoneToGeoZone> ZoneToGeoZone { get; set; } = new List<ZoneToGeoZone>();

    }
}
