﻿using DRS.APA.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DRS.APA.Domain.Core.Domain.Common;


namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class Collection : AuditableEntity
    {
        
        public int CollectionId { get; set; }
        public string  Name { get; set; }
        public string ThumbPath { get; set; }
        public string MetaKeywords { get; set; }
        /// <summary>
        /// Gets or sets the meta description
        /// </summary>
        public string MetaDescription { get; set; }
        /// <summary>
        /// Gets or sets the meta title
        /// </summary>
        public string MetaTitle { get; set; }

        /// <summary>
        /// URL or Handle
        /// </summary>
        public string UrlSeo { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the entity is published
        /// </summary>
        public bool? Published { get; set; }

        public DateTime? PublishedDate { get; set; }
        public byte? Popular { get; set; }

        public ICollection<CollectionProduct> ProductCollections { get; set; } = new List<CollectionProduct>();
    }
}