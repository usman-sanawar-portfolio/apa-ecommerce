﻿using System.Collections.Generic;
using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.Repair;
using DRS.APA.Domain.Core.Domain.Schedule;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class ResourceGroup : AuditableEntity
    {
        public int ResourceGroupId { get; set; }
        public string Name { get; set; }
        public ICollection<Resource> Resources { get; set; } = new List<Resource>();

        public ICollection<Booking> Bookings { get; set; } = new List<Booking>();



    }
}
