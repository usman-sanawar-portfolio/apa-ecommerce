﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class FavProduct
    {
        public Product Product { get; set; }
       // public ApplicationUser User { get; set; }

        [Key]
        [Column(Order = 1)]
        public int ProductId { get; set; }

        [Key]
        [Column(Order = 2)]
        public string UserId { get; set; }

        public DateTime? AddedOn { get; set; }


    }
}
