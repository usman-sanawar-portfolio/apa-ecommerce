﻿using System.Collections.Generic;
using DRS.APA.Core.Domain;
using DRS.APA.Domain.Core.Domain.Common;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class DeliveryMethod : AuditableEntity
    {
        public short DeliveryMethodId { get; set; }       

        public string Name { get; set; }

        public string ThumbPath { get; set; }



    }
}
