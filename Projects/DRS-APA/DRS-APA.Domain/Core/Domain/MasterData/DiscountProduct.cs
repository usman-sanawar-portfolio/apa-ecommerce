﻿using DRS.APA.Core.Domain;
using DRS.APA.Domain.Core.Domain.Common;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class DiscountProduct : AuditableEntity
    { 
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public short DiscountId { get; set; }
        public Discount Discount { get; set; }  
    }
}