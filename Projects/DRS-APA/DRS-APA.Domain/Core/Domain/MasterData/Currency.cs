﻿using DRS.APA.Core.Domain;
using DRS.APA.Domain.Core.Domain.Common;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class Currency : AuditableEntity
    {
        public short CurrencyId { get; set; }

        public string  Name { get; set; }

        public string CurrencyCode { get; set; }

    }
}