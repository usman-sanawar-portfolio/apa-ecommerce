﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;
namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class Organization : AuditableEntity
    {
        public short OrganizationId { get; set; }       
        //public short Id { get; set; }

        public string Name { get; set; }

        public string Website { get; set; }

        public string BusinessNature { get; set; }

        public int? NoOfEmployees { get; set; }

        public string EstablishedYear { get; set; }
        public string description { get; set; }
        
        public string Logo { get; set; }
        public string OfficeTiming { get; set; }
        public string Code { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Youtube { get; set; }
        public string Twitter { get; set; }


        public ICollection<Location> Locations { get; set; } = new List<Location>();
    }
}