﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Domain.MasterData
{
    public class ProductCategoriesJunction: AuditableEntity
    {
        public int ProductId { get; set; }
        public Product Product { get; set; }

        public short ProductCategoryId { get; set; }
        public ProductCategory ProductCategory { get; set; }
    }
}
