﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
   public class Store:AuditableEntity
{
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public string StoreOwner { get; set; }
        public string Address { get; set; }
        public string GeoCode { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Image { get; set; }
        public string OpeningTimes { get; set; }
        public string Comment { get; set; }
        public string MetaTitle { get; set; }
        public string MetaTagDescription { get; set; }
        public string MetaTagKeywords { get; set; }
        public int? CountryId { get; set; }
        public Country Country { get; set; }
        public ICollection<Product> Products { get; set; } = new List<Product>();
    }
}
