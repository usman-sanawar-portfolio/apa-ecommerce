﻿using DRS.APA.Domain.Core.Domain.Common;


namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class Attachment : AuditableEntity
    {

        public int AttachmentId { get; set; }
        /// <summary>
        /// sub-folder  i.e Model etc
        /// </summary>
        public string Type { get; set; }       

        public int? ProductId { get; set; }
        public Product Product { get; set; }


        public string Name { get; set; }
        public bool? IsVisible { get; set; }
        public string Title { get; set; }


    }
}
