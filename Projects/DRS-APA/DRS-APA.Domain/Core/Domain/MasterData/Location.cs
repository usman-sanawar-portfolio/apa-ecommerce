﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class Location : AuditableEntity
    {
        public short LocationId { get; set; }       

        public short OrganizationId { get; set; }
        public Organization Organization { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Type { get; set; }// would be used for Type (Corporate/Branch/Sales Office/Plant)

        public string PhoneNo { get; set; }
        public string PhoneNo2 { get; set; }

        public string WhatsappNo { get; set; }

        public string FaxNo { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string PostCode { get; set; }

        public double? Lat { get; set; }

        public double? Lng { get; set; }

        public string Email { get; set; }
        public bool? IsDefault { get; set; }

    }

}
