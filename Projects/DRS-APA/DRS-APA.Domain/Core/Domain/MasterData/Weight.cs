﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class Weight : AuditableEntity
    {
        public int WeightId { get; set; }
        public string WeightTitle { get; set; }
        public string WeightUnit { get; set; }
        public decimal? WeightValue { get; set; }
        public ICollection<Product> Products { get; set; } = new List<Product>();

    }
}
