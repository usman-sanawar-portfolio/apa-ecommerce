﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class Manufacturer : AuditableEntity
    {
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
        public string SEOKeyword { get; set; }
        public int? SortOrder { get; set; }
        public ICollection<Product> Products { get; set; } = new List<Product>();

    }
}
