﻿using DRS.APA.Core.Domain;
using DRS.APA.Domain.Core.Domain.Common;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class CollectionProduct : AuditableEntity
    {

        public int ProductId { get; set; }
        public Product Product { get; set; }

        public int CollectionId { get; set; }
        public Collection Collection { get; set; }

        public bool? IsFeatured { get; set; }
        /// <summary>
        ///  order set in the collection form
        /// </summary>
        public byte? Popular { get; set; }

     
       
        
    }
}