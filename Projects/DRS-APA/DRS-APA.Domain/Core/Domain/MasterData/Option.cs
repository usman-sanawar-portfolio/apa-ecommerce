using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData

{
    public class Option : AuditableEntity
    {
        public int OptionId { get; set; }
        public string OptionName { get; set; }
        public int? SortOrder { get; set; }
        public int OptionTypeId { get; set; }
        public OptionType OptionType { get; set; }
        public ICollection<OptionValue> OptionValues { get; set; } = new List<OptionValue>();
        public ICollection<ProductOptionCombination> ProductOptionCombination
         { get; set; } = new List<ProductOptionCombination>();

    //Test
    public ICollection<SaleOrderLineProductOptionCombinations> SaleOrderLineProductOptionCombinations
        { get; set; } = new List<SaleOrderLineProductOptionCombinations>();

      public ICollection<OrderLineProductOptionCombinations> OrderLineProductOptionCombinations
    { get; set; } = new List<OrderLineProductOptionCombinations>();
    }
}
