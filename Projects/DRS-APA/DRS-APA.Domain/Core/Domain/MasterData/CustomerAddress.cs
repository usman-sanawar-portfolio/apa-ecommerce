﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Domain.MasterData
{
    public class CustomerAddress:AuditableEntity
    {
        public int CustomerAddressId { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string Street { get; set; }

        public string CompanyName { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Country { get; set; }

        public string ZipCode { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
        /// <summary>
        /// Billing ; Shipping
        /// </summary>
        public string AddressType { get; set; }
    }
}
