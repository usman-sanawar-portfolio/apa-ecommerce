﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Domain.MasterData
{
    public class RewardPoints : AuditableEntity
    {
        public int RewardPointsId { get; set; }
        /// <summary>
        /// Points to get reward
        /// </summary>
        public int Points { get; set; }
        public int? ProductId { get; set; }
        public Product Product { get; set; }
        //public ICollection<RewardPointCustomerGroup> RewardPointCustomerGroup
        //{ get; set; } = new List<RewardPointCustomerGroup>();
    }
}
