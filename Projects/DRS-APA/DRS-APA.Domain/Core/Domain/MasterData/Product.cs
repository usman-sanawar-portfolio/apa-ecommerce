using System;
using System.Collections.Generic;
using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class Product : AuditableEntity
    {
        public int ProductId { get; set; }
        /// <summary>
        /// Rich Text
        /// </summary>
        public string ProductName { get; set; }
        public string Description { get; set; }
        public string SeoH1 { get; set; }
        public string SeoH2 { get; set; }
        public string SeoH3 { get; set; }
        public string ImageTitle { get; set; }
        public string MetaTagTitle { get; set; }
        public string MetaTagDescription { get; set; }
        public string MetaTagKeywords { get; set; }
        public string NewTabName { get; set; }
        /// <summary>
        /// Rich text
        /// </summary>
        public string NewTabContent { get; set; }
        /// <summary>
        /// comma separated tags
        /// </summary>
        public string ProductTags { get; set; }

        public ICollection<ProductPhoto> ProductPhotos { get; set; }
            = new List<ProductPhoto>();//One-to-Many Relationship
        public string Model { get; set; }
        /// <summary>
        /// stock keeping unit
        /// </summary>
        public string SKU { get; set; }
        /// <summary>
        /// Universal Product Code
        /// </summary>
        public string UPC { get; set; }
        /// <summary>
        /// European Article Number
        /// </summary>
        public string EAN { get; set; }
        /// <summary>
        /// Japanese Article Number
        /// </summary>
        public string JAN { get; set; }
        /// <summary>
        /// Internation standard book number
        /// </summary>
        public string ISBN { get; set; }
        /// <summary>
        /// Manufacturer part number
        /// </summary>
        public string MPN { get; set; }
        public string Location { get; set; }
        public decimal Price { get; set; }
        public int TaxClassId { get; set; }
        public TaxClass TaxClass { get; set; }
        public int Quantity { get; set; }
        /// <summary>
        /// Min Ordered Qty
        /// </summary>
        public int? MinimumQuantity { get; set; }
        public bool SubtractStock { get; set; }
        public int StockStatusId { get; set; }
        public StockStatus StockStatus { get; set; }
        public bool RequiresShipping { get; set; }
        public string SeoUrl { get; set; }
        public DateTime? DateAvailable { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public int? LengthUnitId { get; set; }
        public Length LengthUnit { get; set; }
        public string Weight { get; set; }
        public int? WeightUnitId { get; set; }
        public Weight WeightUnit { get; set; }
        //status field will come from auditable entity, isActive
        public int? SortOrder { get; set; }

        public bool IsDeleted { get; set; }
        public int? ManufacturerId { get; set; }

        public Manufacturer Manufacturer { get; set; }

        public string image { get; set; }
        public ICollection<ProductCategoriesJunction> ProductCategoriesJunction
        { get; set; } = new List<ProductCategoriesJunction>(); //Here will be the List of categories; many to many table relation

        //  stores; one to many relation
        public int StoreId { get; set; }
        public Store Store { get; set; }

        public bool? UKBarrierDeliveryPrices { get; set; }
        public bool? UKFreeDeliverPrices { get; set; }
        public bool? UKParcelDelivery { get; set; }
        public bool? UKSpecialDeliveryPrices { get; set; }

        public ICollection<ProductOptions> ProductOptions
        { get; set; } = new List<ProductOptions>(); //Product Options Tab here

        public ICollection<DiscountProduct> DiscountProducts
        { get; set; } = new List<DiscountProduct>();//: Discount Tab

        public ICollection<Review> Reviews
        { get; set; } = new List<Review>();

        public ICollection<SaleOrderLines> SaleOrderLines { get; set; } = new List<SaleOrderLines>();
        public ICollection<OrderLines> OrderLines { get; set; } = new List<OrderLines>();
        public ICollection<Attachment> Attachments { get; set; } = new List<Attachment>();

    }
}
