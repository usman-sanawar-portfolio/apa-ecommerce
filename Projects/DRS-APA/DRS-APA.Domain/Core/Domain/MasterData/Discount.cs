using System;
using System.Collections.Generic;
using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    /// <summary>
    /// Represents a discount
    /// </summary>

    public  class Discount :AuditableEntity
    {

        public short DiscountId { get; set; }
        /// <summary>
        /// Gets or sets the name
        /// </summary>
		public string Name { get; set; }

        /// <summary>
        /// AssignedToOrderTotal, AssignedToProducts, AssignedToShipping
        /// </summary>
		public string DiscountType { get; set; }

  //      /// <summary>
  //      /// Gets or sets a value indicating whether to use percentage
  //      /// </summary>
		//public bool UsePercentage { get; set; }

        /// <summary>
        /// Gets or sets the discount percentage
        /// </summary>
		public float DiscountPercentage { get; set; }

  //      /// <summary>
  //      /// Gets or sets the discount amount
  //      /// </summary>

        public ICollection<DiscountCustomerGroup> DiscountCustomerGroups { get; set; } = new List<DiscountCustomerGroup>();

        public ICollection<DiscountProduct> DiscountProducts { get; set; } = new List<DiscountProduct>();
        public ICollection<SaleOrderLines> SaleOrderLines { get; set; } = new List<SaleOrderLines>();
        public ICollection<OrderLines> OrderLines { get; set; } = new List<OrderLines>();


    }
}
