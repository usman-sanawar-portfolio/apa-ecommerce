﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Domain.MasterData
{
    public class Special : AuditableEntity
    {
        public int SpecialId { get; set; }
        public string Priority { get; set; }
        public decimal Price { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int CustomerGroupId { get; set; }
        public CustomerGroup CustomerGroup { get; set; }
        //public int ProductId { get; set; }
        //public Product Product { get; set; }
    }
}
