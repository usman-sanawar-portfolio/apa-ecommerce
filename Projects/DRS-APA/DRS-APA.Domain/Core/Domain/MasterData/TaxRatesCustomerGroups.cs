﻿using System;
using System.Collections.Generic;
using DRS.APA.Domain.Core.Domain.Common;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    

    public class TaxRatesCustomerGroups : AuditableEntity
    {
        public int TaxRateId { get; set; }
        public TaxRate TaxRate { get; set; }

        public int CustomerGroupId { get; set; }
        public CustomerGroup CustomerGroup { get; set; }
    }
}