﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class CustomerGroup : AuditableEntity
    {
        public int CustomerGroupId { get; set; }
        public string CustomerGroupName { get; set; }
        public string Description { get; set; }
        public bool ApproveNewCustomer { get; set; }
        public int? SortOrder { get; set; }
        public bool IsDeleted { get; set; }
        public bool DisplayOnSite { get; set; }
        public ICollection<Customer> Customers { get; set; } = new List<Customer>();
        public ICollection<TaxRatesCustomerGroups> TaxRatesCustomerGroups { get; set; } = new List<TaxRatesCustomerGroups>();
        public ICollection<DiscountCustomerGroup> DiscountCustomerGroups { get; set; } = new List<DiscountCustomerGroup>();


    }
}
