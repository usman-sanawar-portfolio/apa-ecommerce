﻿

namespace ShopApp.Core.Models
{
	/// <summary>
	/// Represents a payment method
	/// </summary>
	public partial class PaymentMethod 
	{
		/// <summary>
		/// Gets or sets the payment method system name
		/// </summary>
		public string PaymentMethodSystemName { get; set; }

		/// <summary>
		/// Gets or sets the full description
		/// </summary>
		public string FullDescription { get; set; }
	}
}
