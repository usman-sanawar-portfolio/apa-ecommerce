﻿using DRS.APA.Core.Domain;
using System;
using DRS.APA.Domain.Core.Domain.Common;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class ProductPhoto : AuditableEntity
    {
        public int ProductPhotoId { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }

        public short? Order { get; set; }

        public bool? IsPrimary { get; set; }

        public int ProductId { get; set; }
        public Product Product { get; set; }

    }
}