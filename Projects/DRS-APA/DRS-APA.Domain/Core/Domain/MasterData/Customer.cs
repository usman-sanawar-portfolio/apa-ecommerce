using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using System.Collections.Generic;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class Customer : AuditableEntity
    {
        public int CustomerId { get; set; }
        /// <summary>
        /// Autodoor fields
        /// </summary>
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsEmailVerified { get; set; }
        public string ActivationCode { get; set; }
        public string TaxNumber { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public bool Newsletter { get; set; }
        public bool Status { get; set; }
        public bool Approved { get; set; }
        public bool Safe { get; set; }
        public string Gender { get; set; }
        public int CustomerGroupId { get; set; }
        public CustomerGroup CustomerGroup { get; set; }
        public ICollection<CustomerAddress> CustomerAddress { get; set; } = new List<CustomerAddress>();
        public ICollection<SaleOrder> SaleOrders { get; set; } = new List<SaleOrder>();
        public ICollection<Orders> Orders { get; set; } = new List<Orders>();
        public ICollection<Blog> Blog { get; set; } = new List<Blog>();
        public ICollection<BlogComment> BlogCommentAuthor { get; set; } = new List<BlogComment>();
        public ICollection<Review> Review { get; set; } = new List<Review>();
    }
}
