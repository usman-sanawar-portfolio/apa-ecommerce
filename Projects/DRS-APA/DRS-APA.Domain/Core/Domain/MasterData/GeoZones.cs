﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class GeoZones : AuditableEntity
    {
        public int GeoZonesId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<TaxRate> TaxRates { get; set; } = new List<TaxRate>();
        public ICollection<EU_CourierCharges> EU_CourierCharges { get; set; } = new List<EU_CourierCharges>();
        public ICollection<ZoneToGeoZone> ZoneToGeoZone { get; set; } = new List<ZoneToGeoZone>();


    }
}
