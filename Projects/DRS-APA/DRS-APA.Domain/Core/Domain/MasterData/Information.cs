﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData

{
    public class Information : AuditableEntity
    {      
        public int InformationId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string MetaTagDescription { get; set; }
        public string MetaTagKeyword { get; set; }
        public string SeoTitle { get; set; }
        public string Tags { get; set; }
        public string SeoKeywords { get; set; }
        public bool? Bottom { get; set; }
        public int? SortOrder { get; set; }

    }
}
