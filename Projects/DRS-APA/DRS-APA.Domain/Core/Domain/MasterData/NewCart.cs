﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Domain.Core.Shared;

//using MvcSalesApp.SharedKernel.Settings;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
  //public class NewCart : INewCart
  public class NewCart 
    {
    private NewCart(string sourceUrl, string customerCookie)
    {
      if (Uri.IsWellFormedUriString(sourceUrl, UriKind.Absolute))
      {
        SourceUrl = sourceUrl;
      }
      else
      {
        SourceUrl = "";
      }

      CustomerCookie = customerCookie;
      CartItems = new List<CartItem>();
    }

      public NewCart()
      {
           // CartItems = new Collection<CartItem>();
        }
    public int CartId { get; set; }
    public string CartCookie { get; private set; }
    public DateTime Initialized { get; private set; }
    public DateTime Expires { get; private set; }
    public string SourceUrl { get; private set; }
    public string CustomerId { get; private set; }
    //public ApplicationUser Customer { get; set; }

    //[InverseProperty("Cart")]
    //public ICollection<CartItem> CartItems { get; }
    public ICollection<CartItem> CartItems { get; set; } = new List<CartItem>();
        //TODO: decide if I should store this property in the database or not
        public string CustomerCookie { get; private set; }

    public static NewCart CreateCartFromProductSelection
      (string sourceUrl, string customerCookie, int productId, int quantity,
        decimal displayedPrice)
    {
      var cart = new NewCart(sourceUrl, customerCookie);
      cart.InitializeCart();
      cart.InsertNewCartItem(productId, quantity, displayedPrice);
      return cart;
    }

    private void InsertNewCartItem(int productId, int quantity, decimal displayedPrice)
    {
      CartItems.Add(CartItem.Create(productId, quantity, displayedPrice, CartCookie));
    }

    private void InitializeCart()
    {
      Initialized = DateTime.Now;
            //Expires = Initialized.Add(ShoppingCartSettings.CookieExpiration);
            Expires = Initialized.AddDays(1);
       //     Expires = Initialized.Add(ShoppingCartSettings.CookieExpiration);
            CartCookie = Guid.NewGuid().ToString();
    }

    public void CustomerFound(string customerId)
    {
      CustomerId = customerId;
    }
  }
}