using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;
 

namespace DRS.APA.Microserivce.Domain.Core.Domain.MasterData
{
    public class ProductOptionCombination : AuditableEntity
    {
        public int ProductOptionCombinationId { get; set; }
        public int ProductOptionsId { get; set; }
        public ProductOptions ProductOptions { get; set; }

      public int OptionId { get; set; }
        public Option Option { get; set; }

        /// <summary>
        /// Selected Option Value Id
        /// </summary>
        public int? OptionValueId { get; set; }
        public OptionValue OptionValue { get; set; }
    }
}
