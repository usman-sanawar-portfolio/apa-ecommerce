using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Domain.MasterData
{
    public class ProductOptions : AuditableEntity
    {
        public int ProductOptionsId { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public string OptionModel { get; set; }
        public int OptionQuantity { get; set; }
        public bool SubtractStock { get; set; }
        /// <summary>
        /// PriceParam ( a dropdown with fixed 3 values ) = plus,minus,equal i.e. if price param is selected as equal, so price shown
        /// in Field "Price" will be shown to end-user. If plus is selected then ProductPrice+OptionPrice will be  
        ///  shown to end-user. If minus is selected then ProductPrice-OptionPrice will be show to end-user.
        /// </summary>
        public string PriceParam { get; set; }
        public decimal OptionPrice { get; set; }
        /// <summary>
        /// Weight Parameters Equal, Greater Than, Less Than
        /// </summary>
        public string WeightParam { get; set; }
        public string OptionWeight { get; set; }
        public string Images { get; set; }
          
    public ICollection<ProductOptionCombination> ProductOptionCombination
        { get; set; } = new List<ProductOptionCombination>();
        
    }
}

