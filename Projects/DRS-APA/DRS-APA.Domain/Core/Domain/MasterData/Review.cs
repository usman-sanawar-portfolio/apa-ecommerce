using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Domain.MasterData
{
    public class Review : AuditableEntity
    {
        public int ReviewId { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public short Rating { get; set; }
        public string Text { get; set; }
        public int? CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}
