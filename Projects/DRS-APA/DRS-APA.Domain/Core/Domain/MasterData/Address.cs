﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class Address 
    {
        public string FirstName { get; set; }
       
        public string LastName { get; set; }
       
        public string Phone { get; set; }
        
        public String Street { get;  set; }
        
        public string CompanyName { get; set; }
       
        public String City { get;  set; }
       
        public String State { get;  set; }
       
        public String Country { get;  set; }
        
        public String ZipCode { get;  set; }

        public double? Latitude { get; set; }
        
        public double? Longitude { get; set; }
        
        private Address() { }

     

    }
}
