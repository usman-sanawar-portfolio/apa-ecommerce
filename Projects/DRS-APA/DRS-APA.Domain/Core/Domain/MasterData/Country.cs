﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using System;
using System.Collections.Generic;
using System.Text;
namespace DRS.APA.Domain.Core.Domain.MasterData

{
    public class Country : AuditableEntity
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string ISOCode2 { get; set; }
        public string ISOCode3 { get; set; }
        public bool PostcodeRequired { get; set; }

        public ICollection<Zone> Zones { get; set; } = new List<Zone>();
        public ICollection<Store> Stores { get; set; } = new List<Store>();

        public ICollection<ZoneToGeoZone> ZoneToGeoZone { get; set; } = new List<ZoneToGeoZone>();
    }
}
