﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Microserivce.Domain.Core.Domain.MasterData
{
    public class ZoneToGeoZone : AuditableEntity
    {
        public int ZoneToGeoZoneId { get; set; }
        public int? CountryId { get; set; }
        public Country Country { get; set; }
        public int? ZoneId { get; set; }
        public Zone Zone { get; set; }
        public int? GeoZonesId { get; set; }
        public GeoZones GeoZones { get; set; }
    }
}
