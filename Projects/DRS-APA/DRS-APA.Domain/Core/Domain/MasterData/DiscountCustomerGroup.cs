using System;
using System.Collections.Generic;
using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    /// <summary>
    /// Represents a discount
    /// </summary>

    public  class DiscountCustomerGroup :AuditableEntity
    {
        public short DiscountId { get; set; }
        public Discount Discount { get; set; }
        public int CustomerGroupId { get; set; }
        public CustomerGroup CustomerGroup { get; set; }

    }
}
