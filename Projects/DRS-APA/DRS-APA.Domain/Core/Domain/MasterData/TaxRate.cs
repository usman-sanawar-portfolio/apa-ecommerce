﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData

{
    public class TaxRate : AuditableEntity
    {      
        public int TaxRateId { get; set; }
        public string TaxRateName { get; set; }
        public decimal Rate { get; set; }
        public string TaxRateCode { get; set; }
        public int? GeoZonesId { get; set; }
        public GeoZones GeoZones { get; set; }
        public int? TaxClassId { get; set; }
        public TaxClass TaxClass { get; set; }

        public ICollection<CustomerGroup> CustomerGroups { get; set; } = new List<CustomerGroup>();
        public ICollection<TaxRatesCustomerGroups> TaxRatesCustomerGroups { get; set; } = new List<TaxRatesCustomerGroups>();
    }
}
