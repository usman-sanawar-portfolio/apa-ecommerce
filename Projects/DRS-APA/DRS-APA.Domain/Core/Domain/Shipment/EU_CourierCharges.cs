﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Microserivce.Domain.Core.Domain.Shipment
{
    public class EU_CourierCharges : AuditableEntity
    {
        public int EU_CourierChargesId { get; set; }
        public decimal First20KgPrice { get; set; }
        public decimal AdditionalPerKgPrice { get; set; }
        public string TransitTimes { get; set; }
        public int GeoZonesId { get; set; }
        public GeoZones GeoZones { get; set; } //one to many relation        
    }
}
