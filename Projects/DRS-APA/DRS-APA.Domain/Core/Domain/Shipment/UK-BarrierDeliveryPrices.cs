﻿using DRS.APA.Domain.Core.Domain.Common;
namespace DRS.APA.Microserivce.Domain.Core.Domain.Shipment
{
    public class UK_BarrierDeliveryPrices : AuditableEntity
    {
        public int UK_BarrierDeliveryPricesId { get; set; }
        public int Qty { get; set; }
        public decimal Price { get; set; }
        /// <summary>
        /// Price + PerAdditionalQtyPrice ( if Qty is in between i.e. Qty 2 ; in case of 1 and 3 qty availibility)
        /// </summary>
        public decimal PerAdditionalQtyPrice { get; set; }
        public string Notes { get; set; }
    }
}
