﻿using DRS.APA.Domain.Core.Domain.Common;
namespace DRS.APA.Microserivce.Domain.Core.Domain.Shipment
{
    public class UK_FreeDeliveryPrices : AuditableEntity
    {
        public int UK_FreeDeliveryPricesId { get; set; }
        public decimal FreeOrderStartsFrom { get; set; }
        public string Notes { get; set; }
    }
}
