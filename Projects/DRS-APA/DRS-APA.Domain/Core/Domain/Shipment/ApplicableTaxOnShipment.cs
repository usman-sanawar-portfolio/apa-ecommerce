using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Microserivce.Domain.Core.Domain.Shipment
{
  public class ApplicableTaxOnShipment : AuditableEntity
  {
    public int ApplicableTaxOnShipmentId { get; set; }
    public int TaxClassId { get; set; }
    public TaxClass TaxClass { get; set; }

  }
}
