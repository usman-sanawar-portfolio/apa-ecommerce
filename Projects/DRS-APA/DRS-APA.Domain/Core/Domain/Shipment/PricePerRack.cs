﻿using DRS.APA.Domain.Core.Domain.Common;
namespace DRS.APA.Microserivce.Domain.Core.Domain.Shipment
{
    public class PricePerRack : AuditableEntity
    {
        public int PricePerRackId { get; set; }
        public decimal Price { get; set; }
        public string Notes { get; set; }
    }
}
