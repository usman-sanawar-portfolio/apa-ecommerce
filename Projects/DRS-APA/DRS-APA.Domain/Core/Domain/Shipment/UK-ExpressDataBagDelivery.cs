﻿using DRS.APA.Domain.Core.Domain.Common;
namespace DRS.APA.Microserivce.Domain.Core.Domain.Shipment
{
    public class UK_ExpressDataBagDelivery : AuditableEntity
    {
        public int UK_ExpressDataBagDeliveryId { get; set; }
        public string Duration { get; set; }
        public decimal Price { get; set; }
        /// <summary>
        /// weight in kg
        /// </summary>
        public int DataBagStandardWeight { get; set; }
        public decimal AdditionalPricePerKg { get; set; }
        public string Notes { get; set; }
    }
}
