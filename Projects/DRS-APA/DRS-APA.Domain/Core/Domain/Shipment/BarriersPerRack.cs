﻿using DRS.APA.Domain.Core.Domain.Common;
namespace DRS.APA.Microserivce.Domain.Core.Domain.Shipment
{
    public class BarriersPerRack : AuditableEntity
    {
        public int BarriersPerRackId { get; set; }
        /// <summary>
        /// Number of barriers fit on one Rack.
        /// </summary>
        public int BarrierPerRackQty { get; set; }
        /// <summary>
        /// Price per rack, this is additional price of a rack
        /// </summary>
        public decimal PricePerRack { get; set; }
    }
}
