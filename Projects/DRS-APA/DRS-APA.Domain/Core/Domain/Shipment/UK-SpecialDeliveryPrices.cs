﻿using DRS.APA.Domain.Core.Domain.Common;
namespace DRS.APA.Microserivce.Domain.Core.Domain.Shipment
{
    public class UK_SpecialDeliveryPrices : AuditableEntity
    {
        public int UK_SpecialDeliveryPricesId { get; set; }
        public decimal RetrofitDeliveryCost { get; set; }
        public decimal OperatorDeliveryCost { get; set; }
        public string Notes { get; set; }
    }
}
