﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Domain.Website
{
    public class WebMenu : AuditableEntity
    {
        public short WebMenuId { get; set; }
        public string WebMenuTitle { get; set; }
        /// <summary>
        /// If Has Sub Menu then WebPagePage will be ignored 
        /// else get web page and its components
        /// </summary>
        public bool HasSubMenu { get; set; }
        public short? WebPageId { get; set; }
        public WebPage WebPage { get; set; }
        public short Order { get; set; }
        public ICollection<WebSubMenu> WebSubMenu { get; set; } = new List<WebSubMenu>();
        
    }
}
