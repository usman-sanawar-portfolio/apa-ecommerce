﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Domain.Website
{
    public class WebCard : AuditableEntity
    {
        public short WebCardId { get; set; }
        public short WebComponentId { get; set; }
        public WebComponent WebComponent { get; set; }
        public string Icon { get; set; }
        public string MainHeading { get; set; }//Card's top heading
        public string SubHeading { get; set; }
        public short Order { get; set; }
    }
}
