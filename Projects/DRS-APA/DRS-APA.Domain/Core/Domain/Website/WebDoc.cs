﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Domain.Website
{
    public class WebDoc : AuditableEntity
    {
        public short WebDocId { get; set; }
        public short WebComponentId { get; set; }
        public WebComponent WebComponent { get; set; }
        //public WebDocData WebDocData { get; set; }
        public string Header { get; set; }//text above document
        public string Icon { get; set; }//display icon
        public string DownloadLink { get; set; }
        public short Order { get; set; }
    }
}
