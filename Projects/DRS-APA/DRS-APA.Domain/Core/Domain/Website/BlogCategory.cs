﻿using DRS.APA.Domain.Core.Domain.Common;
using System.Collections.Generic;

namespace DRS.APA.Microserivce.Domain.Core.Domain.Website
{
    public class BlogCategory : AuditableEntity
    {
        public int BlogCategoryId { get; set; }
        public string CategoryName { get; set; }
        public ICollection<Blog> Blog { get; set; } = new List<Blog>();
    }
}
