using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;
using System;
using System.Collections.Generic;

namespace DRS.APA.Microserivce.Domain.Core.Domain.Website
{
    public class Blog : AuditableEntity
    {
        public int BlogId { get; set; }
        public string BlogTitle { get; set; }
        /// <summary>
        /// HTML - Rich Editor Text
        /// </summary>
        public string Content { get; set; }
        public bool IsApproved { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string SeoH1 { get; set; }
        public string SeoH2 { get; set; }
        public string SeoH3 { get; set; }
        public string MetaTagTitle { get; set; }
        public string MetaTagDescription { get; set; }
        public string MetaTagKeywords { get; set; }
        public string Image { get; set; }
        public int BlogCategoryId { get; set; }
        public BlogCategory BlogCategory { get; set; }
        public int? CustomerId { get; set; }
        public Customer Customer { get; set; }

        public ICollection<BlogComment> BlogComment { get; set; } = new List<BlogComment>();
    }
}
