﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Domain.Website
{
    public class WebSubMenu : AuditableEntity
    {
        public short WebSubMenuId { get; set; }
        public string WebSubMenuTitle { get; set; }
        public short WebMenuId { get; set; }
        public WebMenu WebMenu { get; set; }
        public short? WebPageId { get; set; }
        public WebPage WebPage { get; set; }
        public short Order { get; set; }
    }
}
