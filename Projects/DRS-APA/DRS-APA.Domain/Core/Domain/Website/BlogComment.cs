using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;
using System.ComponentModel.DataAnnotations.Schema;

namespace DRS.APA.Microserivce.Domain.Core.Domain.Website
{
    public class BlogComment : AuditableEntity
    {
        public int BlogCommentId { get; set; }
        public int BlogId { get; set; }
        public Blog Blog { get; set; }
        public string Comment { get; set; }
        public int? CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}
