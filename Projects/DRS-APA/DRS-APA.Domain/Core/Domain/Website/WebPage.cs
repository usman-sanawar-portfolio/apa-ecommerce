﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DRS.APAAPA.Microserivce.Domain.Core.Domain.Website
{
    public class WebPage : AuditableEntity
    {
        public short WebPageId { get; set; }
        public string WebPageTitle { get; set; }
        public string Slug { get; set; }
        public WebMenu WebMenu { get; set; }
        
        public ICollection<WebPageComponent> WebPageComponents { get; set; } = new List<WebPageComponent>();
    }
}
