﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Domain.Website
{
    public class WebHtml : AuditableEntity
    {
        public short WebHtmlId { get; set; }
        public short WebComponentId { get; set; }
        public WebComponent WebComponent { get; set; }
        public string HtmlData { get; set; }
        /// <summary>
        ///1=full, 1/2(half of the parent container) ,1/3 of parent containder ,1/4 of parent container
        /// </summary>
        public string Size { get; set; }
        public short Order { get; set; }
    }
}
