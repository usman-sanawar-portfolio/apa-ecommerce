﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Domain.Website
{
    public class WebCarousalPhotos : AuditableEntity
    {
        public int WebCarousalPhotosId { get; set; }
        public string PhotoURL { get; set; }
        public int WebCarousalId { get; set; }
        public WebCarousal WebCarousal { get; set; }
    }
}
