﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Domain.Website
{
    public class HomePageCollection : AuditableEntity
    {
        public int HomePageCollectionId { get; set; }
        public short ProductCategoryId { get; set; }
        public ProductCategory ProductCategory { get; set; }
    }
}
