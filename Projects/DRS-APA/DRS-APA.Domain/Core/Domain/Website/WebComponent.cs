﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Domain.Website
{
    public class WebComponent : AuditableEntity
    {
        public short WebComponentId { get; set; }
        public string Title { get; set; }
        /// <summary>
        /// photo,card,video,html,doc
        /// </summary>
        public string Type { get; set; }
        public ICollection<WebPageComponent> WebPageComponents { get; set; } = new List<WebPageComponent>();

        public ICollection<WebVideo> WebVideos { get; set; } = new List<WebVideo>();
        public ICollection<WebDoc> WebDocs { get; set; } = new List<WebDoc>();
        public ICollection<WebCard> WebCards { get; set; } = new List<WebCard>();
        public ICollection<WebPhoto> WebPhotos { get; set; } = new List<WebPhoto>();
        public ICollection<WebHtml> WebHtmls { get; set; } = new List<WebHtml>();
    }
}
