﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Domain.Website
{
    public class WebPageComponent : AuditableEntity
    {
        public short WebPageId { get; set; }
        public short WebComponentId { get; set; }

        public WebPage WebPage { get; set; }
        public WebComponent WebComponent { get; set; }
        public short Order { get; set; }

    }
}
