﻿using DRS.APA.Domain.Core.Domain.Common;

namespace DRS.APA.Domain.Core.Domain.Repair
{
   public class AccountSetting:AuditableEntity
    {
        public int AccountSettingId { get; set; }
        /// <summary>
        /// YesNo, List, Text
        /// </summary>
        public string Type { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Text, Yes/No, or List etc
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Heading i.e. Jobs on device etc
        /// </summary>
        public string Module { get; set; }
       
        public int AccountId { get; set; }
        public Account Account { get; set; }

    }
}
