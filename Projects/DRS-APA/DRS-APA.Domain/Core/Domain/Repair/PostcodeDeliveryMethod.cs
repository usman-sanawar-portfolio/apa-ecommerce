﻿using DRS.APA.Core.Domain;
using DRS.APA.Domain.Core.Domain.Common;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class PostcodeDeliveryMethod : AuditableEntity
    {
        public int PostcodeId { get; set; }
        public Postcode Postcode { get; set; }
        
        public short DeliveryMethodId { get; set; }
        public DeliveryMethod DeliveryMethod { get; set; }

     
    }
}
