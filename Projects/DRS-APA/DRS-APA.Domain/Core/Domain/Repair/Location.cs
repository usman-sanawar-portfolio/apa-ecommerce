﻿using System.Collections.Generic;
using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.Shop;
using DRS.APA.Domain.Core.Domain.Schedule;

namespace DRS.APA.Domain.Core.Domain.Repair
{
    public class Location : AuditableEntity
    {
        public short LocationId { get; set; }
        public int AccountId { get; set; }
        public Account Account { get; set; }

        public string Name { get; set; }
        public string LocationAddress { get; set; }
        public string Type { get; set; }// would be used for Type (Corporate/Branch/Sales Office/Plant)
        public string PhoneNo { get; set; }
        public string FaxNo { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public ICollection<SalesOrder> SalesOrders { get; set; } = new List<SalesOrder>();

        public ICollection<Schedule.Schedule> Schedules { get; set; } = new List<Schedule.Schedule>();


    }

}
