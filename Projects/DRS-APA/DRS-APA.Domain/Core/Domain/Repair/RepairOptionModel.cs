﻿using DRS.APA.Core.Domain;
using DRS.APA.Domain.Core.Domain.Common;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class RepairOptionModel : AuditableEntity
    {
        public short RepairOptionId { get; set; }
        public RepairOption RepairOption { get; set; }
        
        public int ModelId { get; set; }
        public Model Model { get; set; }

        public string Remarks { get; set; }

        public float? UnitPrice { get; set; }

        //public short? DeliveryMethodId { get; set; }

        //public DeliveryMethod DeliveryMethodEligible { get; set; }
    }
}
