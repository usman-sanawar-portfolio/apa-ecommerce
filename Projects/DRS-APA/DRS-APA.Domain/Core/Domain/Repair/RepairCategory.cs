﻿using System.Collections.Generic;
using DRS.APA.Core.Domain;
using DRS.APA.Domain.Core.Domain.Common;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class RepairCategory : AuditableEntity
    {
        public short RepairCategoryId { get; set; }

        public string Name { get; set; }

        public string Remarks { get; set; }

        public string ImagePath { get; set; }

        public ICollection<RepairOption> RepairOptions { get; set; } = new List<RepairOption>();
        public ICollection<Schedule.Schedule> Schedules { get; set; } = new List<Schedule.Schedule>();

    }
}