﻿using System.Collections.Generic;
using System.Security.AccessControl;
using DRS.APA.Core.Domain;
using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.Shop;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class RepairOption : AuditableEntity
    {
        public short RepairOptionId { get; set; }

        public short RepairCategoryId { get; set; }
        public RepairCategory RepairCategory { get; set; }

        //public short? DeliveryMethodId { get; set; }
        //public DeliveryMethod DeliveryMethodEligible { get; set; }

        public string Name { get; set; }
        /// <summary>
        /// general price
        /// </summary>
        public float? UnitPrice { get; set; }

        public bool Common { get; set; }

        public string ImagePath { get; set; }

        public string Remarks { get; set; }

        public ICollection<RepairOptionModel> RepairOptionModels { get; set; } = new List<RepairOptionModel>();
        public ICollection<LineItem> LineItems { get; set; } = new List<LineItem>();

    }
}
