﻿using System.Collections.Generic;
using DRS.APA.Domain.Core.Domain.Common;

namespace DRS.APA.Domain.Core.Domain.Repair
{
    public class Account : AuditableEntity
    {
        public int AccountId { get; set; }

        public string Name { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2{ get; set; }

        public string AddressLine3 { get; set; }

        public string FooterText { get; set; }


        public string Logo { get; set; }

        public ICollection<AccountSetting> AccountSettings { get; set; } = new List<AccountSetting>();

    }
}
