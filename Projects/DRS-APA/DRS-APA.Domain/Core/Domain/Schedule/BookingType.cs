﻿using System.Collections.Generic;
using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.Shop;

namespace DRS.APA.Domain.Core.Domain.Schedule
{
  public class BookingType : AuditableEntity
    {
    public short BookingTypeId { get; set; }
    public string Name { get; set; }
    public string Code { get; set; }
    /// <summary>
    /// Takes time in hours
    /// </summary>
    public int? Duration { get; set; }

    public ICollection<Booking> Bookings { get; set; } = new List<Booking>();

    }
}