using System;
using System.Collections.Generic;
using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Repair;
//using AppointmentScheduling.Core.Model.Events;

namespace DRS.APA.Domain.Core.Domain.Schedule
{
    public class Schedule : AuditableEntity
    {
        public int ScheduleId { get; set; }

        public short? LocationId { get; set; }
        public Location Location { get; set; }

        public short? RepairCategoryId { get; set; }
        public RepairCategory RepairCategory { get; set; }

        // not persisted
        //public DateTimeRange DateRange { get;  set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int Duration()
        {
            return (End - Start).Hours;
        }

        /// <summary>
        /// Daily, Weekly, Monthly
        /// </summary>
        public string ScheduleType { get; set; }

        /// <summary>
        /// Slot duration in hours
        /// </summary>
        public short SlotDuration { get; set; }

        /// <summary>
        /// resource in number per slot
        /// </summary>
        public short ResourcePerSlot { get; set; }

        public ICollection<Booking> Bookings { get; set; } = new List<Booking>();


        public bool Overlaps(DateTimeRange dateTimeRange)
        {
            return this.Start < dateTimeRange.End &&
                   this.End > dateTimeRange.Start;
        }

        //private void MarkConflictingBookings()
        //{
        //    foreach (var appointment in _appointments)
        //    {
        //        var potentiallyConflictingBookings = _appointments
        //            .Where(a => a.PatientId == appointment.PatientId &&
        //            a.TimeRange.Overlaps(appointment.TimeRange) &&
        //            a.Id != appointment.Id &&
        //            a.State != TrackingState.Deleted).ToList();

        //        potentiallyConflictingBookings.ForEach(a => a.IsPotentiallyConflicting = true);

        //        appointment.IsPotentiallyConflicting = potentiallyConflictingBookings.Any();
        //    }
        //}

    }
}