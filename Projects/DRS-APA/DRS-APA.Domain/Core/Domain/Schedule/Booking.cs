using System;
using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Repair;
using DRS.APA.Domain.Core.Domain.Shop;

namespace DRS.APA.Domain.Core.Domain.Schedule
{
    public class Booking : AuditableEntity
    {
        public int BookingId { get; set; }

        public int ScheduleId { get;  set; }
        public Schedule Schedule { get; set; }

        //public int CustomerId { get;  set; }  //get from Sale Order
        //public Customer Customer { get; set; }

        public int? SalesOrderId { get; set; }  
        public SalesOrder SalesOrder { get; set; }

        public DateTime BookingDate { get; set; }

        public int? ResourceId { get;  set; }
        public Resource AllocatedResourceAllocatedResource { get; set; }

        public short? BookingTypeId { get;  set; }
        public BookingType BookingType { get; set; }

        //public DateTimeRange TimeRange { get; set; }

        public TimeSpan StartTime { get;  set; }
        public TimeSpan EndTime { get;  set; }
    
        public string Title { get; set; }

        //#region More Properties
        public DateTime? DateTimeConfirmed { get; set; }

        public bool Confirmed { get; set; }

        public string Remarks { get; set; }
     
        public bool IsPotentiallyConflicting { get; set; }

        public bool Overlaps(DateTimeRange dateTimeRange)
        {
            var start = this.BookingDate.Add(this.StartTime);// + this.StartTime;

            var end = this.BookingDate.Add(this.EndTime);;// + EndTime;

            return start < dateTimeRange.End &&
                   end > dateTimeRange.Start;
        }

        // #endregion

        // Factory method for creation
        //public static Booking Create(Guid scheduleId, 
        //    int clientId, int patientId, 
        //    int roomId, DateTime startTime, DateTime endTime, 
        //    int appointmentTypeId, int? doctorId, string title)
        //{
        //    Guard.ForLessEqualZero(clientId, "clientId");
        //    Guard.ForLessEqualZero(patientId, "patientId");
        //    Guard.ForLessEqualZero(roomId, "roomId");
        //    Guard.ForLessEqualZero(appointmentTypeId, "appointmentTypeId");
        //    Guard.ForNullOrEmpty(title, "title");
        //    var appointment = new Booking(Guid.NewGuid());
        //    appointment.ScheduleId = scheduleId;
        //    appointment.PatientId = patientId;
        //    appointment.ClientId = clientId;
        //    appointment.RoomId = roomId;
        //    appointment.TimeRange = new DateTimeRange(startTime, endTime);
        //    appointment.BookingTypeId = appointmentTypeId;
        //    appointment.DoctorId = doctorId ?? 1;
        //    appointment.Title = title;
        //    return appointment;
        //}

        //DateTime date = ...;
        //TimeSpan time = ...;

        //DateTime result = date + time;
    }
}