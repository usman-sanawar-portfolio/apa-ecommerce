using System;
using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;
//using ShopApp.Core.Shared;

namespace DRS.APA.Domain.Core.Domain.Shop
{
    public class LineItem: AuditableEntity
    {
        public int LineItemId { get; set; }
        
        public int SalesOrderId { get; set; }
        public SalesOrder SalesOrder { get; set; }

        public int ProductId { get; set; }
        public Product Product { get; set; }

        public short? RepairOptionId { get; set; }
        public RepairOption RepairOption { get; set; }

        public int Quantity { get; private set; }

        public decimal UnitPrice { get; set; }

        public float? UnitPriceDiscount { get; set; }

        public decimal LineTotal
        {
            get
            {
                if (UnitPrice != null)
                {
                        return Quantity * (UnitPrice);
                }
                return 0;
            }
        }

        //public int? ShipmentId { get; set; }

        public void AdjustQuantity(int quantity)
        {
            Quantity = quantity;
        }

        public LineItem()
        {
            // required by EF
        }

        public LineItem(int productId,short? repairOptionId, decimal unitPrice, int quantity)
        {
            ProductId = productId;
            RepairOptionId = repairOptionId;
            Quantity = quantity;
            UnitPrice = unitPrice;
        }
    }
}