﻿using System;
using System.Collections.Generic;
using System.Linq;
using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Repair;
using DRS.APA.Domain.Core.Domain.Schedule;

////using ShopApp.Core.Enums;

namespace DRS.APA.Domain.Core.Domain.Shop
{
    public class SalesOrder:AuditableEntity
    {
        public int SalesOrderId { get; set; }

        public short LocationId { get; set; }
        public Location Location { get; set; }

        public string SalesOrderNumber { get; set; }

        public DateTime OrderDate { get; set; }

        public DateTime? DueDate { get; set; }

        public bool? OnlineOrder { get; set; }

        public string Reference { get; set; }

        public string Comment { get; set; }

        /// <summary>
        /// Draft, Paid, Closed
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Pending, Partially Paid, Paid, Refunded, Authorized, Void, Cancelled,Fulfilled
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Fulfilled, None, Partial, Not Shipped
        /// </summary>
        public string FulfillmentStatus { get; set; }

        public string PaymentGateway { get; set; }

        /// <summary>
        /// Customer,Fraud, Inventory, Payment Declined,Other
        /// </summary>
        public string CancelReason { get; set; }

        public string CurrentCustomerStatus { get; set; }

        public short? DeliveryMethodId { get; set; }
        public DeliveryMethod DeliveryMethod { get; set; }

        public string PaymentMethod { get; set; }

        public float? CustomerDiscount { get; set; }

        public float? PromoDiscount { get; set; }

        public int CustomerId { get; set; }
        public Customer Customer { get; set; }

        public Address ShippingAddress { get; set; }

        public Address BillingAddress { get; set; }

        public string CurrencyCode { get; set; }

        public string BrowserIp { get; set; }

        public Decimal? SubTotal { get; set; }

        public ICollection<LineItem> LineItems { get; set; } = new List<LineItem>();

        public bool Validate()
        {
            if (LineItems.Any())
            {
                return true;
            }

            return false;
        }

        public decimal Total()
        {
            var total = 0m;
            foreach (var item in LineItems)
            {
                total += item.UnitPrice * item.Quantity;
            }

            return total;
        }

        public ICollection<Booking> Bookings { get; set; } = new List<Booking>();

        //public void ApplyCustomerStatusDiscount()
        //{
        //    var status = CustomerStatus.New;
        //    if (_customer != null)
        //        status = _customer.Status;
        //    switch (status)
        //    {
        //        case CustomerStatus.Silver:
        //            CustomerDiscount = CustomerDiscounts.SilverDiscount;
        //            break;

        //        case CustomerStatus.Gold:
        //            CustomerDiscount = CustomerDiscounts.GoldDiscount;
        //            break;

        //        case CustomerStatus.Platinum:
        //            CustomerDiscount = CustomerDiscounts.PlatinumDiscount;
        //            break;

        //        default:
        //        {
        //            CustomerDiscount = 0;
        //            break;
        //        }
        //    }
        //}
    }
}