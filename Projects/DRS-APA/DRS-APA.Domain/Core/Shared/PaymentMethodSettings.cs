﻿namespace DRS.APA.Domain.Core.Shared
{
    public static class PaymentMethodSettings
    {
        public static string PaymentMethod { get; private set; }

        public static void PaymentMethodSetter(string paymentMethod)
        {
            PaymentMethod = paymentMethod;
        }
    }
}
