﻿using System;

namespace DRS.APA.Domain.Core.Shared
{
    public static class ShoppingCartSettings
    {

        public static TimeSpan CookieExpiration { get; private set; }

        public static void ShouldBeProtectedCookieExpirationSetter(TimeSpan expiration)
        {
            CookieExpiration = expiration;
        }
    }
}


