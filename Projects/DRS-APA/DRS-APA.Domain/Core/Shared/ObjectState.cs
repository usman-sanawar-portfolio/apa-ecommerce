﻿namespace DRS.APA.Domain.Core.Shared
{
  public enum ObjectState
  {
    Unchanged = 0,
    Added = 1,
    Modified = 2,
    Deleted = 3
  }
}