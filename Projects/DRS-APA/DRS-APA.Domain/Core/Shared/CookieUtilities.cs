﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;



namespace DRS.APA.Domain.Core.Shared
{
    public class CookieUtilities
    {
        private static IHttpContextAccessor _contextAccessor;

        public CookieUtilities(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
        }

        public static bool BuildCartCookie(string createdCartCookie, DateTime cartCookieExpires)
        {
            // read the cookie
            
            //var legacyCookie = _contextAccessor.HttpContext.Request.Cookies["DRS.APAcartcookie"];
            // var cookiename = legacyCookie["DRS.APAcartcookiename"];

            //if (legacyCookie.ToString() != "")
            //{
            //    return false;
            //}

            // write the cookie

            _contextAccessor.HttpContext.Response.Cookies.Append(
                "DRS.APAcartcookie",
                createdCartCookie,
                new CookieOptions()
                {
                    Expires = DateTime.Now.AddMinutes(10),
                    // Marking the cookie as essential
                    IsEssential = true
                });


            return true;
        }
        public static void RemoveCartCookie(string createdCartCookie)
        {
            // read the cookie
            var legacyCookie = _contextAccessor.HttpContext.Request.Cookies["DRS.APAcartcookie"];
           // var cookiename = legacyCookie["DRS.APAcartcookiename"];



            if (legacyCookie.ToString() != "")
            {
                _contextAccessor.HttpContext.Response.Cookies.Append(
                    "DRS.APAcartcookiename",
                    "",
                    new CookieOptions()
                    {
                        Expires = DateTime.Now.AddDays(-100),
                        // Marking the cookie as essential
                        IsEssential = true
                    });

            }

        }

        public static string CreateCookie(DateTime expiresDateTime)
        {
            return $"cName=SalesAppCookie|cGuid={Guid.NewGuid()}|cExpires={expiresDateTime.ToUniversalTime()}";
        }

        //public static string CookieValue()
        //{
        //    if ()
        //    {
                
        //    }
        //}

        public static bool IsCartCookie(string cookie)
        {
            var crumbs = cookie.Split(Convert.ToChar("|"));
            if (crumbs[0] != "cName=SalesAppCookie") return false;
            if (!crumbs[1].StartsWith("cGuid=")) return false;
            var guidCheck = crumbs[1].Substring("cGuid=".Length + 1);
            Guid guidResult;
            if (!Guid.TryParse(guidCheck, out guidResult)) return false;
            if (!crumbs[2].StartsWith("cExpires=")) return false;
            return true;
        }

        //{
        //  StringBuilder sb = new StringBuilder();
        //  // Get cookie from the current request.
        //  HttpCookie cookie = Request.Cookies.Get("DateCookieExample");

        //  // Check if cookie exists in the current request.
        //  if (cookie == null) {
        //    sb.Append("Cookie was not received from the client. ");
        //    sb.Append("Creating cookie to add to the response. <br/>");
        //    // Create cookie.
        //    cookie = new HttpCookie("DateCookieExample");
        //    // Set value of cookie to current date time.
        //    cookie.Value = DateTime.Now.ToString();
        //    // Set cookie to expire in 10 minutes.
        //    cookie.Expires = DateTime.Now.AddMinutes(10d);
        //    // Insert the cookie in the current HttpResponse.
        //    Response.Cookies.Add(cookie);
        //  }
        //  else {
        //    sb.Append("Cookie retrieved from client. <br/>");
        //    sb.Append("Cookie Name: " + cookie.Name + "<br/>");
        //    sb.Append("Cookie Value: " + cookie.Value + "<br/>");
        //    sb.Append("Cookie Expiration Date: " +
        //        cookie.Expires.ToString() + "<br/>");
        //  }
        //  Label1.Text = sb.ToString();
        //}
    }
}
