﻿namespace DRS.APA.Domain.Core.Shared
{
    public static class ShippingSettings
    {
        public static decimal ShippingRate { get; private set; }

        public static void ShippingRateSetter(decimal shippingrate)
        {
            ShippingRate = shippingrate;
        }

    }
}
