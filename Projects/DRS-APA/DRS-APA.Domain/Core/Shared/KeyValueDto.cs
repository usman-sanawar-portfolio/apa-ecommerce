﻿using System;
using System.Collections.Generic;
using System.Text;


namespace DRS.APA.Domain.Core.Shared
{
    public class KeyValueDto
    {
        public short? ProductId { get; set; }

        public string ProductName { get; set; }
    }
}
