﻿using System;

namespace DRS.APA.Domain.Core.Specifications
{

    public class BookingFilterSpecification : BaseSpecification<Booking>
    {
        public BookingFilterSpecification(int? scheduleId, DateTime? bookingDateFrom, DateTime? bookingDateTo)
            : base(i => (!scheduleId.HasValue || i.SalesOrderId == scheduleId) &&

            (!bookingDateFrom.HasValue || i.BookingDate >= bookingDateFrom) &&

            (!bookingDateTo.HasValue || i.BookingDate <= bookingDateTo))
            
        {
        }
    }
}
