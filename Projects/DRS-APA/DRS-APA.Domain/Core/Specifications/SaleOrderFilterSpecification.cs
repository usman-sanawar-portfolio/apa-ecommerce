using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using System;

namespace DRS.APA.Domain.Core.Specifications
{
    public class SaleOrderFilterSpecification : BaseSpecification<SaleOrder>
    {
        public SaleOrderFilterSpecification(string SaleIdentifier, bool? isOnlineOrder, bool? isPaymentOnline,
                string onlinePaymentId, DateTime? saleOrderDate, bool? isCancelled, int? CustomerId,
                bool? isActive, DateTime? orderDueDate = null)
                : base(i =>
                (string.IsNullOrEmpty(SaleIdentifier) || i.SaleIdentifier.Contains(SaleIdentifier)) &&
                (!isOnlineOrder == null || i.IsOnlineOrder == isOnlineOrder) &&
                (!isPaymentOnline == null || i.IsPaymentOnline == isPaymentOnline) &&
                (string.IsNullOrEmpty(onlinePaymentId) || i.OnlinePaymentId.Contains(onlinePaymentId)) &&
                (!saleOrderDate.HasValue || i.SaleOrderDate.Date == saleOrderDate.Value.Date) &&
                (!CustomerId.HasValue || i.CustomerId == CustomerId) &&
                (!orderDueDate.HasValue || i.OrderDueDate.Value.Date == orderDueDate.Value.Date) &&
                (!isCancelled == null || i.IsCancelled == isCancelled) &&
                (!isActive == null || i.IsActive == isActive)

                )
        {
            AddInclude(c => c.SaleOrderAddress);
            AddInclude(x => x.OrderStatus);
            AddInclude(s => s.Customers);
        }
    }
}
