﻿using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Specifications
{
    public class ProductFilterSpecification : BaseSpecification<Product>
    {
        public ProductFilterSpecification(string productName, string sku, decimal? price, decimal? fromPrice, decimal? ToPrice, string model, bool? isDeleted)
            : base(i =>
            (string.IsNullOrEmpty(productName) || i.ProductName.Contains(productName)) &&
            (string.IsNullOrEmpty(sku) || i.SKU.Contains(sku)) &&
            (!price.HasValue || i.Price == price) 
             &&
            (!isDeleted.HasValue || i.IsDeleted == isDeleted) &&
             (!fromPrice.HasValue || i.Price >= fromPrice&&i.Price<=ToPrice) &&
            (string.IsNullOrWhiteSpace(model) || i.Model.Contains(model))
            //&&
            //(manufacturerId.Length >0 || i.ManufacturerId == manufacturerId)
            )
        {
            AddInclude(s => s.ProductPhotos);
            AddInclude(s => s.Manufacturer);
            AddInclude(s => s.ProductCategoriesJunction);
               
                 AddInclude(m => m.Reviews);
            AddInclude(x => x.TaxClass.TaxRates);
            AddInclude(x => x.StockStatus);
            AddInclude(x => x.LengthUnit);
            AddInclude(x => x.WeightUnit);
         
            AddInclude(x => x.Store);
        }
    }
}
