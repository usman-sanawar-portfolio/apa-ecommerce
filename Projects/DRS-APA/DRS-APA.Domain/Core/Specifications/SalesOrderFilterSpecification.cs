﻿using System;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Shop;

namespace DRS.APA.Domain.Core.Specifications
{

    public class SalesOrderFilterSpecification : BaseSpecification<SalesOrder>
    {
        public SalesOrderFilterSpecification(int? customerId, short? deliveryMethodId, DateTime? dateFrom, DateTime? dateTo)
            : base(i => (!customerId.HasValue || i.CustomerId == customerId) &&

            (!deliveryMethodId.HasValue || i.DeliveryMethodId == deliveryMethodId) &&
            
            (!dateFrom.HasValue || i.OrderDate >= dateFrom) &&

            (!dateTo.HasValue || i.OrderDate <= dateTo))
        { }
    }
}
