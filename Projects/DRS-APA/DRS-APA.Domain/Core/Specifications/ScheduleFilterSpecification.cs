﻿using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Schedule;

namespace DRS.APA.Domain.Core.Specifications
{

    public class ScheduleFilterSpecification : BaseSpecification<Schedule>
    {
        public ScheduleFilterSpecification(int? repairCategoryId, short? locationId)
            : base(i => (!repairCategoryId.HasValue || i.RepairCategoryId == repairCategoryId) &&

            (!locationId.HasValue || i.LocationId == locationId))
        {
        }
    }
}
