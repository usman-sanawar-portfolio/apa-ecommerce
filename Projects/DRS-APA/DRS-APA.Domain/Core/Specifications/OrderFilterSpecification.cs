using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using System;

namespace DRS.APA.Domain.Core.Specifications
{
    public class OrderFilterSpecification : BaseSpecification<Orders>
    {
        public OrderFilterSpecification(string OrderIdentifier, bool? isOnlineOrder, bool? isPaymentOnline,
            string onlinePaymentId, DateTime? orderDate, bool? isCancelled, string CustomerName,  
            bool? isActive, string orderStatusName, DateTime? orderDueDate = null)
            : base(i =>
            (string.IsNullOrEmpty(OrderIdentifier) || i.OrderIdentifier.Contains(OrderIdentifier)) &&
            (!isOnlineOrder == null || i.IsOnlineOrder == isOnlineOrder) &&
            (!isPaymentOnline == null || i.IsPaymentOnline == isPaymentOnline) &&
            (string.IsNullOrEmpty(onlinePaymentId) || i.OnlinePaymentId.Contains(onlinePaymentId)) &&
            (!orderDate.HasValue || i.OrderDate.Date == orderDate.Value.Date) &&
            (!orderDueDate.HasValue || i.OrderDueDate.Value.Date == orderDueDate.Value.Date) &&
            (string.IsNullOrEmpty(orderStatusName) || i.OrderStatus.Name.Contains(orderStatusName)) &&
            (string.IsNullOrEmpty(CustomerName) || i.Customers.FirstName.Contains(CustomerName)
            || i.Customers.LastName.Contains(CustomerName)) &&
            (!isCancelled == null || i.IsCancelled == isCancelled) &&
            (!isActive == null || i.IsActive == isActive) 
            )
        {
            AddInclude(c => c.OrderAddress);
            AddInclude(c => c.OrderStatus);
            AddInclude(c => c.Customers);
        }
    }
}
