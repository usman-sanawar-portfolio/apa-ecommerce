﻿

using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Specifications
{

    public class AttachmentFilterSpecification : BaseSpecification<Attachment>
    {
        public AttachmentFilterSpecification(  int? productId,bool? isVisible)
            : base(i => (!productId.HasValue || i.ProductId == productId)&&
            (!isVisible.HasValue || i.IsVisible == isVisible)
                )
        {
            AddInclude(c => c.Product);
     
        }
    }
}
