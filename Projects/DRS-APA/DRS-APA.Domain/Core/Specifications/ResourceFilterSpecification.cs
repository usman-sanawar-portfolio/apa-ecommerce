﻿using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Repair;

namespace DRS.APA.Domain.Core.Specifications
{

    public class ResourceFilterSpecification: BaseSpecification<Resource>
    {
        public ResourceFilterSpecification(int? resourceGroupId)
            : base(i => (!resourceGroupId.HasValue || i.ResourceGroupId == resourceGroupId)  

                )
        {
        }
    }
}
