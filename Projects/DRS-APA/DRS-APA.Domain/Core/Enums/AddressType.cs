namespace DRS.APA.Core.Domain.Enums
{
  public enum AddressType
    {
        Billing = 1,
        ShippingPrimary = 2,
        ShippingSecondary = 3
    }
}
