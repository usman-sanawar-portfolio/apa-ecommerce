﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Domain.Core.Enums
{
    public enum EmployeeType
    {
        Employee=1,
        Contractor=2
    }
}
