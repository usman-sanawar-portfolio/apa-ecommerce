﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Domain.Core.Enums
{
    public enum ContractType
    {
        FixedTermAppointment = 1,
        TemporaryAppointment=2
    }
}
