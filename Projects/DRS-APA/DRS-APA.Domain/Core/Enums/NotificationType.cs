﻿namespace DRS.APA.Core.Domain.Enums
{
    public enum NotificationType
    {
        AbsenceRegistered = 1,
        LeaveRequestCreated = 2,
        LeaveRequestApproved = 3,
        LeaveRequestCancelled = 4,
        ResignRequestCreated = 5,
        ResignRequestApproved = 6,       
        AdvApproved = 7,
        AdvCancelled = 8,
        LoanCancelled = 9,
        LoanApproved = 10,
        LoanRequestCreated = 11,
        LoanRequestApproved =12,
        LoanRequestRejected = 13,
        LeaveRequestClosed = 14,
        
    }
}
