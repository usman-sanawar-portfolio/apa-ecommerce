using System;
using System.Collections.Generic;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Repair;
//using AppointmentScheduling.Core.Model.Events;

namespace DRS.APA.Domain.Core.Dtos.Schedule
{
    public class WeeklyScheduleDto
    {
        public string FullDate { get; set; }

        public int ScheduleId { get; set; }
    
        public DateTime Day { get; set; } //Start Date

        public bool IsActive { get; set; }

    }
    public class SlotDto
    {
        public DateTime FullDate { get; set; }

        public int ScheduleId { get; set; }

        public string SlotDesc { get; set; } //Start Date

        public bool IsBooked { get; set; }

    }
}