﻿namespace DRS.APA.Domain.Core.Dtos.Schedule
{
    public class BookingTypeDto
    {
        public short? BookingTypeId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        /// <summary>
        /// Takes time in hours
        /// </summary>
        public int? Duration { get; set; }

        public bool? IsActive { get; set; }
    }
}