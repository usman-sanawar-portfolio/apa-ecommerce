using System;
using DRS.APA.Domain.Core.Domain.Repair;
using DRS.APA.Domain.Core.Domain.Shop;

namespace DRS.APA.Domain.Core.Dtos.Schedule
{
    public class BookingDto
    {
        public int? BookingId { get; set; }

        public int ScheduleId { get;  set; }
       
        public int? SalesOrderId { get; set; }  

        public DateTime BookingDate { get;  set; }

        public int? ResourceId { get;  set; }
        public string AllocatedResourceName { get; set; }

        public short? BookingTypeId { get;  set; }
        public string BookingTypeName { get; set; }
    
        public TimeSpan StartTime { get;  set; }

        public TimeSpan EndTime { get;  set; }
    
        public string Title { get; set; }

        public DateTime? DateTimeConfirmed { get; set; }
        public bool Confirmed { get; set; }
     
        public bool IsPotentiallyConflicting { get; set; }

        public bool? IsActive { get; set; }

    }
}