using System;
using System.Collections.Generic;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Repair;
//using AppointmentScheduling.Core.Model.Events;

namespace DRS.APA.Domain.Core.Dtos.Schedule
{
    public class ScheduleDto
    {
        public int? ScheduleId { get; set; }

        public int? LocationId { get; set; }
        public string LocationName { get; set; }

        public int? RepairCategoryId { get; set; }
        public string RepairCategoryName { get; set; }
    
        public DateTime Start { get; set; } //Start Date

        public DateTime End { get; set; } // End Date
        public int Duration()
        {
            return (End - Start).Days;
        }

        /// <summary>
        /// Daily, Weekly, Monthly
        /// </summary>
        public string ScheduleType { get; set; }

        /// <summary>
        /// Slot duration in hours
        /// </summary>
        public short SlotDuration { get; set; }

        /// <summary>
        /// resource in number per slot
        /// </summary>
        public short ResourcePerSlot { get; set; }

        public bool? IsActive { get; set; }

    }
}