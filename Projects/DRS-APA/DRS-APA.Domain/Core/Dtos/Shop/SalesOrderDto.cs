﻿using System;
using System.Collections.Generic;
using System.Linq;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Repair;
using DRS.APA.Domain.Core.Domain.Shop;

////using ShopApp.Core.Enums;

namespace DRS.APA.Domain.Core.Dtos.Shop
{
    public class SalesOrderDto
    {
        public int? SalesOrderId { get; set; }

        public short LocationId { get; set; }
        public string LocationName { get; set; }

        public string SalesOrderNumber { get; set; }

        public DateTime OrderDate { get; set; }

        public DateTime? DueDate { get; set; }

        public bool OnlineOrder { get; set; }

        public string Reference { get; set; }

        public string Comment { get; set; }
        /// <summary>
        /// Draft, Paid, Closed
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Pending, Partially Paid, Paid, Refunded, Authorized, Void, Cancelled,Fulfilled
        /// </summary>
        public string State { get; set; }
        /// <summary>
        /// Fulfilled, None, Partial, Not Shipped
        /// </summary>
        public string FulfillmentStatus { get; set; }

        public string PaymentGateway { get; set; }

        /// <summary>
        /// Customer,Fraud, Inventory, Payment Declined,Other
        /// </summary>
        public string CancelReason { get; set; }

        public string CurrentCustomerStatus { get; set; }

        public int? DeliveryMethodId { get; set; }
        public string DeliveryMethodName { get; set; }

        public string PaymentMethod { get; set; }

        public float? CustomerDiscount { get; set; }

        public float? PromoDiscount { get; set; }

        public int CustomerId { get; set; }
        public string CustomerName { get; set; }

        public Address ShippingAddress { get; set; }

        public Address BillingAddress { get; set; }

        public string CurrencyCode { get; set; }

        public string BrowserIp { get; set; }

        public double SubTotal { get; set; }

        public ICollection<LineItemDto> LineItems { get; set; } = new List<LineItemDto>();

        public bool? IsActive { get; set; }
      
    }
}