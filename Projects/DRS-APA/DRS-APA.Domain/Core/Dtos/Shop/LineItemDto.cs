using DRS.APA.Domain.Core.Domain.MasterData;

//using ShopApp.Core.Shared;

namespace DRS.APA.Domain.Core.Dtos.Shop
{
    public class LineItemDto
    {
        public int? Id { get; set; }

        public int SalesOrderId { get; set; }
        public string SalesOrderNumber{ get; set; }

        public int ProductId { get; set; }
        public string ProductName { get; set; }

        public int? RepairOptionId { get; set; }
        public string RepairOptionName { get; set; }

        public int Quantity { get; set; }

        public decimal UnitPrice { get; set; }

        public float? UnitPriceDiscount { get; set; }

        public bool? IsActive { get; set; }
    }
}