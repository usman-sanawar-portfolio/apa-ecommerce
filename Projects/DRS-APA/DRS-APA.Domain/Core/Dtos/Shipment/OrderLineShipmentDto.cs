﻿using System.Collections.Generic;
namespace DRS.APA.Microserivce.Domain.Core.Dtos.Shipment
{
    public class OrderLineShipmentDto
    {
        public int UK_ParcelDeliveryId { get; set; }
        public string AdditionalNotes { get; set; }
    }
}
