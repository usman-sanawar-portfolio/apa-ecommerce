using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Shipment
{
  public class UK_ExpressDataBagDeliveryDto
  {
    public int UK_ExpressDataBagDeliveryId { get; set; }
    public string Duration { get; set; }
    public decimal Price { get; set; }
    public int DataBagStandardWeight { get; set; }
    public decimal AdditionalPricePerKg { get; set; }
    public string Notes { get; set; }
    public bool? IsActive { get; set; }
  }
}

