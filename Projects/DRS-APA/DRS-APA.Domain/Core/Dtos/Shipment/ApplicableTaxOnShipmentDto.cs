

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Shipment
{
    public class ApplicableTaxOnShipmentDto
  {
    public int ApplicableTaxOnShipmentId { get; set; }
    public int TaxClassId { get; set; }
    public string TaxClassTitle { get; set; }
  public bool? IsActive { get; set; }
    }
}
