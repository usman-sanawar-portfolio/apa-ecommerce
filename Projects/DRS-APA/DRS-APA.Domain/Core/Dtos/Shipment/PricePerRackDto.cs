﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Shipment
{
    public class PricePerRackDto
    {
        public int PricePerRackId { get; set; }
        public decimal Price { get; set; }
        public string Notes { get; set; }
        public bool? IsActive { get; set; }
    }
}
