﻿using DRS.APA.Domain.Core.Domain.Common;
namespace DRS.APA.Microserivce.Domain.Core.Dtos.Shipment
{
    public class UK_SpecialDeliveryPricesDto
    {
        public int? UK_SpecialDeliveryPricesId { get; set; }
        public decimal RetrofitDeliveryCost { get; set; }
        public decimal OperatorDeliveryCost { get; set; }
        public string Notes { get; set; }
        public bool? IsActive { get; set; }

    }
}
