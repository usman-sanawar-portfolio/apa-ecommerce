﻿

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Shipment
{
    public class EU_CourierChargesDto
    {
        public int? EU_CourierChargesId { get; set; }
        public decimal First20KgPrice { get; set; }
        public decimal AdditionalPerKgPrice { get; set; }
        public string TransitTimes { get; set; }
        public int GeoZonesId { get; set; }
        public string GeoZonesName { get; set; } //one to many relation    
        public bool? IsActive { get; set; }

    }
}
