﻿using System.Collections.Generic;
namespace DRS.APA.Microserivce.Domain.Core.Dtos.Shipment
{
    public class UK_AllShipmentDto
    {
        public IEnumerable<BarriersPerRackDto> BarriersPerRack { get; set; } = new List<BarriersPerRackDto>();
        //public IEnumerable<PricePerRackDto> PricePerRack { get; set; } = new List<PricePerRackDto>();
        //public IEnumerable<UK_ExpressDataBagDeliveryDto> UK_ExpressDataBagDelivery { get; set; } = new List<UK_ExpressDataBagDeliveryDto>();
        public IEnumerable<UK_FreeDeliveryPricesDto> UK_FreeDeliveryPrices { get; set; } = new List<UK_FreeDeliveryPricesDto>();
        public IEnumerable<UK_ParcelDeliveryDto> UK_ParcelDelivery { get; set; } = new List<UK_ParcelDeliveryDto>();
        public IEnumerable<UK_SpecialDeliveryPricesDto> UK_SpecialDeliveryPrices { get; set; } = new List<UK_SpecialDeliveryPricesDto>();
        public IEnumerable<UK_BarrierDeliveryPricesDto> UK_BarrierDeliveryPrices { get; set; } = new List<UK_BarrierDeliveryPricesDto>();
    }
}
