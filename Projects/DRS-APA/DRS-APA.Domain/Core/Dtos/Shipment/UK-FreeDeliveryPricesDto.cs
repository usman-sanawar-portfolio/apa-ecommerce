﻿using DRS.APA.Domain.Core.Domain.Common;
namespace DRS.APA.Microserivce.Domain.Core.Dtos.Shipment
{
    public class UK_FreeDeliveryPricesDto 
    {
        public int? UK_FreeDeliveryPricesId { get; set; }
        public decimal FreeOrderStartsFrom { get; set; }
        public string Notes { get; set; }
        public bool? IsActive { get; set; }

    }
}
