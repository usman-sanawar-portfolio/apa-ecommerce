﻿namespace DRS.APA.Microserivce.Domain.Core.Dtos.Shipment
{
    public class UK_ParcelDeliveryDto
    {
        public int? UK_ParcelDeliveryId { get; set; }
        public string Duration { get; set; }
        public decimal PricePerCustomKg { get; set; }
        public int CustomKgMin { get; set; }
        public int CustomKgMax { get; set; }

        /// <summary>
        /// Price for products above CustomKgMax
        /// </summary>
        public decimal PricePerCustomKg2 { get; set; }
        public int CustomKgMin2 { get; set; }
        public int CustomKgMax2 { get; set; }
        /// <summary>
        /// AdditionalPerKg applicable in case of Product Kg>CustomeKgMax2
        /// </summary>

        public decimal AdditionalPerKg { get; set; }
        public string Notes { get; set; }
        public bool? IsActive { get; set; }

    }
}
