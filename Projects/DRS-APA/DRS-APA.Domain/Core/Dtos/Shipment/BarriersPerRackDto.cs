using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Shipment
{
    public class BarriersPerRackDto
    {
        public int BarriersPerRackId { get; set; }
        /// <summary>
        /// How many barriers fit on per Rack i.e. 6 barriers/rack
        /// </summary>
        public int BarrierPerRackQty { get; set; }
        /// <summary>
        /// Price per rack, this is additional price of a rack
        /// </summary>
        public decimal PricePerRack { get; set; }
        public bool? IsActive { get; set; }
    }
}
