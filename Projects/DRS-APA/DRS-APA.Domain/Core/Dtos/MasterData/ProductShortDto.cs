﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.MasterData
{
    public class ProductShortDto
    {
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public string image { get; set; }
        public decimal Price { get; set; }

    }
}
