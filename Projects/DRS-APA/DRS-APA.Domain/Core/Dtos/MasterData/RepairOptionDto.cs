﻿namespace DRS.APA.Domain.Core.Dtos.MasterData
{
    public class RepairOptionDto
    {
        public short? RepairOptionId { get; set; }

        public short RepairCategoryId { get; set; }
        public string RepairCategoryName { get; set; }

        public int ProductId { get; set; }
        public string ProductName { get; set; }

        public short? DeliveryMethodId { get; set; }
        public string DeliveryMethodEligibleName { get; set; }

        public string Name { get; set; }

        public float? UnitPrice { get; set; }

        public bool Common { get; set; }

        public string Remarks { get; set; }

        public string ImagePath { get; set; }

        public bool? IsActive { get; set; }

    }
}
