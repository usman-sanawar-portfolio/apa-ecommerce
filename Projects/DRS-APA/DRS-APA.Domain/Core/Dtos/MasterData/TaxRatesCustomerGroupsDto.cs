﻿using System;
using System.Collections.Generic;
using DRS.APA.Domain.Core.Domain.Common;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class TaxRatesCustomerGroupsDto
    {
        public int TaxRateId { get; set; }
        public string TaxRateName { get; set; }

        public int CustomerGroupId { get; set; }
        public string CustomerGroupName { get; set; }


    }
}