﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Dtos.MasterData
{
    public class DiscountProductDto
    { 
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public short DiscountId { get; set; }
        public string DiscountName { get; set; }
        public DiscountDto Discount { get; set; }
        public bool? IsActive { get; set; }

    }
}