﻿using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Dtos.MasterData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class GeoZonesDto
    {
        public int? GeoZonesId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<ZoneToGeoZoneDto> ZoneToGeoZone { get; set; } = new List<ZoneToGeoZoneDto>();

        //public int ZonesId { get; set; }
        //public string ZoneName { get; set; }
        //public int CountryId { get; set; }
        //public string CountryName { get; set; }

    }
}
