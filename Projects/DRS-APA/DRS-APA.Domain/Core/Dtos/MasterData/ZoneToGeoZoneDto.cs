﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.MasterData
{
    public class ZoneToGeoZoneDto
    {
        public int ZoneToGeoZoneId { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public int? ZoneId { get; set; }
        public string ZoneName { get; set; }
        public int? GeoZonesId { get; set; }
        public string GeoZonesName { get; set; }
        public bool? IsActive { get; set; }
    }
}
