﻿namespace DRS.APA.Domain.Core.Dtos.MasterData
{
    public class ProductPhotoDto
    {
        public int? ProductPhotoId { get; set; }

        public int ProductId { get; set; }
        public string ProductName { get; set; }

        public string Type { get; set; }

        public string Name { get; set; }

        public short? Order { get; set; }

        public bool? IsPrimary { get; set; }

        public bool? IsActive { get; set; }

    }
}
