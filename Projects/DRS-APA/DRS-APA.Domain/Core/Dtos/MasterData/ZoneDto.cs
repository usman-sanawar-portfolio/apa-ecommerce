﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Domain.Core.Dtos.MasterData
{
    public class ZoneDto
    {
        public int? ZoneId { get; set; }
        public string ZoneName { get; set; }
        public string ZoneCode { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public bool? IsActive { get; set; }

    }
}
