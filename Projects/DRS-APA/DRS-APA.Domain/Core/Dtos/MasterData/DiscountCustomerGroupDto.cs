using System;
using System.Collections.Generic;
using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    /// <summary>
    /// Represents a discount
    /// </summary>

    public  class DiscountCustomerGroupDto
    {
        public short DiscountId { get; set; }
        public string DiscountName { get; set; }
        public int CustomerGroupId { get; set; }
        public string CustomerGroupName { get; set; }
        public bool? IsActive { get; set; }

    }
}
