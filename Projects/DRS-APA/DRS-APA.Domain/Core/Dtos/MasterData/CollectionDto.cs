﻿using System;

namespace DRS.APA.Domain.Core.Dtos.MasterData
{
    public class CollectionDto
    {        
        public int? CollectionId { get; set; }
        public string  Name { get; set; }
        public string  Code { get; set; }

        /// <summary>
        /// Text Area
        /// </summary>
        public string Description { get; set; }

        public string ThumbPath { get; set; }
        
        public string MetaKeywords { get; set; }
        /// <summary>
        /// Gets or sets the meta description
        /// </summary>
        public string MetaDescription { get; set; }
        /// <summary>
        /// Gets or sets the meta title
        /// </summary>
        public string MetaTitle { get; set; }

        /// <summary>
        /// URL or Handle
        /// </summary>
        public string UrlSeo { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the entity is published
        /// </summary>
        /// 
        public bool? Published { get; set; }

        public DateTime? PublishedDate { get; set; }
        /// <summary>
        /// Order
        /// </summary>
        public byte? Popular { get; set; }
        /// <summary>
        /// List Hiighest Price, Lowest Price, Newest, Oldest, Best Selling, Manually
        /// </summary>
        public string Sort { get; set; }

    }
}