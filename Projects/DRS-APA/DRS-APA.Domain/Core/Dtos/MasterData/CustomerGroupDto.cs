﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class CustomerGroupDto
    {
        public int? CustomerGroupId { get; set; }
        public string CustomerGroupName { get; set; }
        public string Description { get; set; }
        public bool ApproveNewCustomer { get; set; }
        public int? SortOrder { get; set; }
        public bool IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public bool DisplayOnSite { get; set; }

        public ICollection<TaxRatesCustomerGroupsDto> TaxRatesCustomerGroups { get; set; } = new List<TaxRatesCustomerGroupsDto>();
        public ICollection<DiscountCustomerGroupDto> DiscountCustomerGroups { get; set; } = new List<DiscountCustomerGroupDto>();

    }
}
