﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
   public class LengthDto
{
        public int? LengthId { get; set; }
        public string LengthTitle { get; set; }
        public string LengthUnit { get; set; }
        public decimal LengthValue { get; set; }
        public bool? IsActive { get; set; }
    }
}
