﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.MasterData
{
    public class RewardPointsDto
    {
        public int? RewardPointsId { get; set; }
        /// <summary>
        /// Points to get reward
        /// </summary>
        public int Points { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public ICollection<RewardPointCustomerGroupDto> RewardPointCustomerGroup
        { get; set; } = new List<RewardPointCustomerGroupDto>();
        public bool? IsActive { get; set; }

    }
}
