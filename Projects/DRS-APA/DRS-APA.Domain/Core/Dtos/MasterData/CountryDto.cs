﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData

{
    public class CountryDto
    {
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public string ISOCode2 { get; set; }
        public string ISOCode3 { get; set; }
        public bool PostcodeRequired { get; set; }

        //public int? GeoZoneId { get; set; }
        //public string GeoZonesName { get; set; }
        public bool? IsActive { get; set; }
    }
}
