﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRS.APA.Domain.Core.Dtos.MasterData
{
    public class CartProductDto
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public bool Add { get; set; }

        public string memebercookie { get; set; }
    }
}
