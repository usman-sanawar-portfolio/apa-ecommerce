﻿namespace DRS.APA.Domain.Core.Dtos.MasterData
{
    public class RepairCategoryDto
    {
        public short? RepairCategoryId { get; set; }

        public string Name { get; set; }

        public string Remarks { get; set; }

        public string ImagePath { get; set; }

        public bool? IsActive { get; set; }

    }
}
