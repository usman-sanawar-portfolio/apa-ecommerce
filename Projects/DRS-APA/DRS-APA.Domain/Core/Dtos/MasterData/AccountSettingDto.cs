﻿namespace DRS.APA.Domain.Core.Dtos.MasterData
{
   public class AccountSettingDto
    {
        public int? AccountSettingId { get; set; }
        /// <summary>
        /// YesNo, List
        /// </summary>
        public string Type { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// SELECT LIST  = Text, Yes/No, or List (comma separated values i.e. a,b,c)etc
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// SELECT LIST  = Heading i.e. Accounts, Repair Service, Shopping Cart, Reservations, Blog, Rent Service
        /// </summary>
        public string Module { get; set; }
       
        public int AccountId { get; set; }

        public bool? IsActive { get; set; }

    }
}
