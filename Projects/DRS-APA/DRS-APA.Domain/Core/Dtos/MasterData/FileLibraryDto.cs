﻿namespace DRS.APA.Domain.Core.Dtos.MasterData
{ 
    public class FileLibraryDto
    {
        public short? FileLibraryId { get; set; }

        public string Name { get; set; }

        public string FileName { get; set; }
        public string Type { get; set; }

        public bool? IsActive { get; set; }


    }
}
