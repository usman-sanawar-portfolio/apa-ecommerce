﻿using DRS.APA.Core.Domain;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class DeliveryMethodDto 
    {
        public short? DeliveryMethodId { get; set; }       

        public string Name { get; set; }

        public bool? IsActive { get; set; }



    }
}
