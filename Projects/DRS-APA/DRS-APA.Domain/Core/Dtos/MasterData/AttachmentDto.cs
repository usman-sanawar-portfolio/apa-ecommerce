﻿namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class AttachmentDto
    {

        public int? AttachmentId { get; set; }
        /// <summary>
        /// sub-folder  i.e Model etc
        /// </summary>
        public string Type { get; set; }

        public int? ProductId { get; set; }
        public string ProductName { get; set; }


        public string Name { get; set; }
        public bool? IsVisible { get; set; }
        public string Title { get; set; }
        public bool? IsActive { get; set; }
      


    }
}
