﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Domain.Core.Dtos
{
    public class DocumentTypeDto
    {
        public short? DocumentTypeId { get; set; }

        public string Name { get; set; }

        public string Notes { get; set; }

        public string Label { get; set; }
        /// <summary>
        /// For Automatic Reference
        /// </summary>
        public string ReferencePrefix { get; set; }

        public string ReferenceFormat { get; set; }

        public int? NextNumber { get; set; }

        public bool? IsActive { get; set; }
    }
}
