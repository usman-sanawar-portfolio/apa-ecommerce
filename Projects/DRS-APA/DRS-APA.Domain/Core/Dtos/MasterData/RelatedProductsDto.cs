using DRS.APA.Domain.Core.Dtos.MasterData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.MasterData
{
    public class RelatedProductsDto
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public ICollection<ProductPhotoDto> ProductPhotos { get; set; }
           = new List<ProductPhotoDto>();
        public string image { get; set; }
        public decimal Price { get; set; }
        public int? ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
        public int? TotalReviewsCount { get; set; }
        public int? TotalRating { get; set; }
        public bool IsDeleted { get; set; }

    }
}
