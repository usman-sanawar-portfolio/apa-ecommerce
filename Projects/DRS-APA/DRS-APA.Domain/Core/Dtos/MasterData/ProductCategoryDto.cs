﻿using System.Collections.Generic;
using DRS.APA.Domain.Core.Dtos.MasterData;


namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class ProductCategoryDto
    {
       public short? ProductCategoryId { get; set; }
       public string Name { get; set; }
       public short? ParentCategoryId { get; set; }
       public string ParentCategoryName { get; set; }
       public string ThumbPath { get; set; }
       public string MetaKeywords { get; set; }
        /// <summary>
        /// Gets or sets the meta description
        /// </summary>
        public string MetaDescription { get; set; }
        /// <summary>
        /// Gets or sets the meta title
        /// </summary>
        public string MetaTitle { get; set; }
        public bool VisibleOnMainMenu { get; set; }
        public string UrlSeo { get; set; }
        public bool? IsActive { get; set; }
        /// <summary>
        /// Category code
        /// Retrofit code= ret
        /// Operator code= op
        /// Barrier code= br
        /// </summary>
        public string Code { get; set; }
        public ICollection<ProductCategoryDto> SubCategories { get; set; } = new List<ProductCategoryDto>();
    }
}
