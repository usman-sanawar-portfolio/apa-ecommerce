using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData

{
    public class TaxRateDto 
    {      
        public int? TaxRateId { get; set; }
        public string TaxRateName { get; set; }
        public decimal Rate { get; set; }
        public string TaxRateCode { get; set; }
        public int? GeoZonesId { get; set; }
        public string GeoZonesName { get; set; }
        public int? TaxClassId { get; set; }
        public string TaxClassName { get; set; }

        public bool? IsActive { get; set; }
        public ICollection<TaxRatesCustomerGroupsDto> TaxRatesCustomerGroups { get; set; } = new List<TaxRatesCustomerGroupsDto>();


        //public ICollection<CustomerGroup> CustomerGroups { get; set; } = new List<CustomerGroup>();
    }
}
