using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData

{
    public class TaxClassDto 
    {      
        public int? TaxClassId { get; set; }
        public string TaxClassTitle { get; set; }
        public string Description { get; set; }

        public bool? IsActive { get; set; }

        public ICollection<TaxRateDto> TaxRates { get; set; } = new List<TaxRateDto>();

    }
}
