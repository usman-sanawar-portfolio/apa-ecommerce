﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.MasterData
{
    public class ProductCategoriesJunctionDto
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }

        public int ProductCategoryId { get; set; }
        public string ProductCategoryName { get; set; }
        public string CategoryCode { get; set; }
        public bool? IsActive { get; set; }

    }
}
