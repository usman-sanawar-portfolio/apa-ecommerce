﻿namespace DRS.APA.Domain.Core.Dtos.MasterData
{
    public class AccountDto
    {
        public int? AccountId { get; set; }

        public string Name { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2{ get; set; }

        public string AddressLine3 { get; set; }

        public string JobFooterText { get; set; }

        public string Logo { get; set; }

        public bool? IsActive { get; set; }

    }
}
