﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;
namespace DRS.APA.Domain.Core.Domain.MasterData

{
    public class ReturnActionDto
    {
        public int? ReturnActionId { get; set; }
        public string Name { get; set; }
        public bool? IsActive { get; set; }

    }
}
