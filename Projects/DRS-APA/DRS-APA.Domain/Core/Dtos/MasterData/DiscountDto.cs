using DRS.APA.Domain.Core.Domain.MasterData;
using System;
using System.Collections.Generic;

namespace DRS.APA.Domain.Core.Dtos.MasterData
{
  /// <summary>
  /// Represents a discount
  /// </summary>

  public class DiscountDto
  {

    public short? DiscountId { get; set; }
    /// <summary>
    /// Gets or sets the name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// AssignedToOrderTotal, AssignedToProducts, AssignedToShipping
    /// </summary>
    public string DiscountType { get; set; }

    //      /// <summary>
    //      /// Gets or sets a value indicating whether to use percentage
    //      /// </summary>
    //public bool UsePercentage { get; set; }

    /// <summary>
    /// Gets or sets the discount percentage
    /// </summary>
    public float DiscountPercentage { get; set; }

        //      /// <summary>
        //      /// Gets or sets the discount amount
        //      /// </summary>
        //public float? DiscountAmount { get; set; }

        public bool? IsActive { get; set; }
        public ICollection<DiscountCustomerGroupDto> DiscountCustomerGroups { get; set; } = new List<DiscountCustomerGroupDto>();

    }
}
