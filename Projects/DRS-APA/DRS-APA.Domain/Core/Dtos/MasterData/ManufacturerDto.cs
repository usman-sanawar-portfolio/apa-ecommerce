﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class ManufacturerDto
    {
        public int? ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
        public string SEOKeyword { get; set; }
        public int? SortOrder { get; set; }
        public bool? IsActive { get; set; }

    }
}
