﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData

{
    public class OptionTypeDto 
    {      
        public int? OptionTypeId { get; set; }
        public string OptionTypeName { get; set; }
        public int TypeCode { get; set; }

        public bool? IsActive { get; set; }

        //public ICollection<Option> Options { get; set; } = new List<Option>();
        //public ICollection<OptionVlaue> OptionVlaues { get; set; } = new List<OptionVlaue>();
    }
}
