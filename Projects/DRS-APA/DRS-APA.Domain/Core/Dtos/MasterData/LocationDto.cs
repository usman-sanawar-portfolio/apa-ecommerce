﻿namespace DRS.APA.Domain.Core.Dtos.MasterData
{
    public class LocationDto
    {
        public short? LocationId { get; set; }
        /// <summary>
        /// Mandatory, List from Organizations Entity
        /// </summary>
        public short OrganizationId { get; set; }

        /// <summary>
        /// Mandatory, Max Length 150
        /// </summary>
        public string Name { get; set; }

        public string Address { get; set; }
        /// <summary>
        /// would be used for Type (Corporate/Branch/Sales Office/Plant) Max Length (25)
        /// </summary>
        public string Type { get; set; }

        public string PhoneNo { get; set; }
        public string PhoneNo2 { get; set; }

        public string WhatsappNo { get; set; }

        public string FaxNo { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string PostCode { get; set; }

        public double? Lat { get; set; }

        public double? Lng { get; set; }

        public string Email { get; set; }
        public bool? IsDefault { get; set; }

        public bool? IsActive { get; set; }
    }

}
