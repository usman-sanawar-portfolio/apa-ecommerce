﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;
namespace DRS.APA.Domain.Core.Domain.MasterData

{
    public class OrderStatusDto
    {
        public int? OrderStatusId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        /// <summary>
        /// Fixed types>Order,SaleOrder
        /// </summary>
        public string Type { get; set; }
        public bool? IsActive { get; set; }

    }
}
