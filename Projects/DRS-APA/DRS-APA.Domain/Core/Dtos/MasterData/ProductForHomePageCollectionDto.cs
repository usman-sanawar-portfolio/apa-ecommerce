using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using System.Collections.Generic;
 
namespace DRS.APA.Microserivce.Domain.Core.Dtos.MasterData
{
    public class ProductForHomePageCollectionDto
    {
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public bool IsDeleted { get; set; }
    public string image { get; set; }

    public ICollection<ProductPhotoDto> ProductPhotos { get; set; }
            = new List<ProductPhotoDto>();
     public ICollection<ProductOptionsDto> ProductOptions
    { get; set; } = new List<ProductOptionsDto>();
    
  }
}
