﻿using System;
using System.Collections.Generic;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Dtos.MasterData;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class ProductDto
    {
        #region commented code


        public int? ProductId { get; set; }
        /// <summary>
        /// Rich Text
        /// </summary>
        public string ProductName { get; set; }
        public string Description { get; set; }
        public string SeoH1 { get; set; }
        public string SeoH2 { get; set; }
        public string SeoH3 { get; set; }
        public string ImageTitle { get; set; }
        public string MetaTagTitle { get; set; }
        public string MetaTagDescription { get; set; }
        public string MetaTagKeywords { get; set; }
        public string NewTabName { get; set; }
        /// <summary>
        /// Rich text
        /// </summary>
        public string NewTabContent { get; set; }
        /// <summary>
        /// comma separated tags
        /// </summary>
        public string ProductTags { get; set; }

        public ICollection<ProductPhotoDto> ProductPhotos { get; set; }
            = new List<ProductPhotoDto>();//One-to-Many Relationship
        public string Model { get; set; }
        /// <summary>
        /// stock keeping unit
        /// </summary>
        public string SKU { get; set; }
        /// <summary>
        /// Universal Product Code
        /// </summary>
        public string UPC { get; set; }
        /// <summary>
        /// European Article Number
        /// </summary>
        public string EAN { get; set; }
        /// <summary>
        /// Japanese Article Number
        /// </summary>
        public string JAN { get; set; }
        /// <summary>
        /// Internation standard book number
        /// </summary>
        public string ISBN { get; set; }
        /// <summary>
        /// Manufacturer part number
        /// </summary>
        public string MPN { get; set; }
        public string Location { get; set; }
        public decimal Price { get; set; }
        public int TaxClassId { get; set; }
        public TaxClassDto TaxClass { get; set; }

        public int Quantity { get; set; }
        /// <summary>
        /// Min Ordered Qty
        /// </summary>
        public int? MinimumQuantity { get; set; }
        public bool SubtractStock { get; set; }
        public int? StockStatusId { get; set; }
        public string StockStatusName { get; set; }
        public bool RequiresShipping { get; set; }
        public string SeoUrl { get; set; }
        public DateTime? DateAvailable { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public int? LengthUnitId { get; set; }
        public string LengthUnitName { get; set; }
        public string Weight { get; set; }
        public int? WeightUnitId { get; set; }
        public string WeightUnitName { get; set; }
        public bool? IsActive { get; set; }
        public int SortOrder { get; set; }
        public int? ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
        public string image { get; set; }

        public bool IsDeleted { get; set; }

        public ICollection<ProductCategoriesJunctionDto> ProductCategoriesJunction
        { get; set; } = new List<ProductCategoriesJunctionDto>();

        //stores; one to many relation
        public int? StoreId { get; set; }
        public string StoreName { get; set; }

        public bool? UKBarrierDeliveryPrices { get; set; }
        public bool? UKFreeDeliverPrices { get; set; }
        public bool? UKParcelDelivery { get; set; }
        public bool? UKSpecialDeliveryPrices { get; set; }



        public ICollection<ProductOptionsDto> ProductOptions
        { get; set; } = new List<ProductOptionsDto>();

        public ICollection<DiscountProductDto
            > DiscountProducts
        { get; set; } = new List<DiscountProductDto>();
  

        public int? TotalReviewsCount { get; set; }
        public int? TotalRating { get; set; }

        public ICollection<AttachmentDto> Attachments { get; set; } = new List<AttachmentDto>();

    }
}
