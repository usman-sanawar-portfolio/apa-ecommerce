﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.MasterData
{
    public class SpecialDto
    {
        public int? SpecialId { get; set; }
        public string Priority { get; set; }
        public decimal? Price { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int CustomerGroupId { get; set; }
        public string CustomerGroupName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public bool? IsActive { get; set; }

    }
}
