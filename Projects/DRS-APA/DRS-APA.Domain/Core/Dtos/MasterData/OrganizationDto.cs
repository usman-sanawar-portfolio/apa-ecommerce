﻿using DRS.APA.Domain.Core.Dtos.MasterData;
using System.Collections.Generic;

namespace DRS.APA.Domain.Core.Dtos.MasterData
{
    public class OrganizationDto
    {
        public short? OrganizationId { get; set; }

        public string Name { get; set; }

        public string Website { get; set; }

        public string BusinessNature { get; set; }

        public int? NoOfEmployees { get; set; }

        public string EstablishedYear { get; set; }
        public string description { get; set; }

        public string Logo { get; set; }
        public string OfficeTiming { get; set; }
        public string Code { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Youtube { get; set; }
        public string Twitter { get; set; }

        public bool? IsActive { get; set; }

        public ICollection<LocationDto> Locations { get; set; } = new List<LocationDto>();

    }

}
