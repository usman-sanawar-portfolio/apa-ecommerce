﻿namespace DRS.APA.Domain.Core.Dtos.MasterData
{
    public class CurrencyDto
    {  
        public int? CurrencyId { get; set; }
        public string Name { get; set; }
        public string CurrencyCode { get; set; }
        public bool? IsActive { get; set; }
    }

}
