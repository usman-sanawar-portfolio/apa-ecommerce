﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Shared;

//using MvcSalesApp.SharedKernel.Settings;

namespace DRS.APA.Domain.Core.Dtos.MasterData
{
  public class NewCartDto
  {
    public int? CartId { get;  set; }
    public string CartCookie { get;  set; }
    public DateTime Initialized { get;  set; }
    public DateTime Expires { get;  set; }
    public string SourceUrl { get;  set; }
    public string CustomerId { get;  set; }
    //public ApplicationUser Customer { get; set; }

    //TODO: decide if I should store this property in the database or not
    public string CustomerCookie { get;  set; }

    public ICollection<CartItemDto> CartItems { get; set; } = new List<CartItemDto>();
    }
}