using DRS.APA.Microserivce.Domain.Core.Dtos.MasterData;
using DRS.APA.Microserivce.Domain.Core.Dtos.Orders;
using System.Collections.Generic;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class CustomerDto
    {
        public int? CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsEmailVerified { get; set; }
        public string TaxNumber { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public bool Newsletter { get; set; }
        public bool Status { get; set; }
        public bool Approved { get; set; }
        public bool Safe { get; set; }
        public string Gender { get; set; }
        public string ActivationCode { get; set; }

        public int CustomerGroupId { get; set; }
        public string CustomerGroupName { get; set; }
        public bool? IsActive { get; set; }
        public ICollection<CustomerAddressDto> CustomerAddress { get; set; } = new List<CustomerAddressDto>();
        public ICollection<SaleOrderDto> SaleOrders { get; set; } = new List<SaleOrderDto>();
        public ICollection<OrderDto> Orders { get; set; } = new List<OrderDto>();

    }
}
