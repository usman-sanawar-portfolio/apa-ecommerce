using DRS.APA.Domain.Core.Domain.MasterData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.MasterData
{
    public class ProductOptionCombinationDto
    {
        public int? ProductOptionCombinationId { get; set; }
         public int ProductOptionsId { get; set; }
        public int OptionId { get; set; }
        public string OptionName { get; set; }
        public int? OptionValueId { get; set; }
        public string OptionValueName { get; set; }
        public int? OptionTypeId { get; set; }
        public string OptionTypeName { get; set; }
        public bool? IsActive { get; set; }
    }
}
