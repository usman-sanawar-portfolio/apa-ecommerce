using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.MasterData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.MasterData
{
    public class ReviewDto
    {
        public int? ReviewId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public short Rating { get; set; }
        public string Text { get; set; }
        public DateTime? CreatedOn { get; set; }
       public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public bool? IsActive { get; set; }

    }
}
