﻿using System;

namespace DRS.APA.Domain.Core.Dtos.MasterData
{
    public class CartItemDto
    {
        public int? CartItemId { get;  set; }

        public string CartCookie { get; set; }

        public int CartId { get; set; }
       
        public int ProductId { get; private set; }
        public string ProductName { get; set; }

        public DateTime? SelectedDateTime { get; set; }
        public decimal? CurrentPrice { get; set; }
        public int? Quantity { get; set; }
        public string State { get; private set; }

    }
}