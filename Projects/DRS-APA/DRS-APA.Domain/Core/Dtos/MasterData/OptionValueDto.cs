﻿using DRS.APA.Domain.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DRS.APA.Domain.Core.Domain.MasterData

{
    public class OptionValueDto
    {      
        public int? OptionValueId { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public int? SortOrder { get; set; }
        public int OptionId { get; set; }
        public string OptionName { get; set; }

        public int? OptionTypeId { get; set; }
        public string OptionTypeName { get; set; }
        public bool? IsActive { get; set; }

    }
}
