﻿namespace DRS.APA.Domain.Core.Dtos.MasterData
{
    public class RepairOptionModelDto
    {
        public short? RepairOptionId { get; set; }
        public string RepairOptionName { get; set; }

        public short ModelId { get; set; }
        public string ModelName { get; set; }

        public string Remarks { get; set; }

        public float? UnitPrice { get; set; }

        public short? DeliveryMethodId { get; set; }
        public string DeliveryMethodEligibleName { get; set; }

        public bool? IsActive { get; set; }

    }
}
