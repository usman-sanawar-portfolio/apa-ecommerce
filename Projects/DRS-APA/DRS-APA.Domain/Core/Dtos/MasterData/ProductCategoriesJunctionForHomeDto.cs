﻿using DRS.APA.Domain.Core.Domain.MasterData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.MasterData
{
    public class ProductCategoriesJunctionForHomeDto
    {
        //public int ProductId { get; set; }
        public ProductForHomePageCollectionDto Product { get; set; }
        public int TotalReviews { get; set; }
        public int TotalRating { get; set; }
        public bool? IsActive { get; set; }

    }
}
