﻿namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class CollectionProductDto
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }

        public int CollectionId { get; set; }
        public string CollectionName { get; set; }

        public bool IsFeatured { get; set; }
        /// <summary>
        ///  order set in the collection form
        /// </summary>
        public byte Popular { get; set; }

        public bool? IsActive { get; set; }
    }
}