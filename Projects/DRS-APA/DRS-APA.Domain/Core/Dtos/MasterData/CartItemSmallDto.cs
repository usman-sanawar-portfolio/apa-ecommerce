﻿using System;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Dtos.MasterData
{
    public class CartItemSmallDto
    {
        public Product Product { get; set; }

        public DateTime SelectedDateTime { get; set; }

        public decimal CurrentPrice { get; set; }

        public int Quantity { get; set; }
    }
}
