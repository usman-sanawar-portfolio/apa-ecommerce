﻿namespace DRS.APA.Domain.Core.Dtos.Repair
{
    public class ResourceGroupDto
    {  
        public int? ResourceGroupId { get; set; }
        public string Name { get; set; }        
        public bool? IsActive { get; set; }
    }

}
