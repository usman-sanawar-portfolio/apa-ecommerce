﻿using System.Collections.Generic;
using System.Security.AccessControl;
using DRS.APA.Core.Domain;
using DRS.APA.Domain.Core.Domain.Common;
using DRS.APA.Domain.Core.Domain.Shop;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class PostcodeDto
    {
        public int? PostcodeId { get; set; }

        public string Name { get; set; }
        /// <summary>
        /// general price
        /// </summary>
        public string Remarks { get; set; }

        public string DeliveryMethods { get; set; }

        //public ICollection<DeliveryMethodDto> DeliveryMethods { get; set; } = new List<DeliveryMethodDto>();
         public ICollection<PostcodeDeliveryMethodDto> PostcodeDeliveryMethods { get; set; } = new List<PostcodeDeliveryMethodDto>();

        public bool? IsActive { get; set; }

    }
}
