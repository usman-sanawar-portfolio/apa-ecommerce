﻿using DRS.APA.Core.Domain;
using DRS.APA.Domain.Core.Domain.Common;

namespace DRS.APA.Domain.Core.Domain.MasterData
{
    public class PostcodeDeliveryMethodDto
    {
        public int PostcodeId { get; set; }
        public string PostcodeName { get; set; }
        
        public int DeliveryMethodId { get; set; }
        public string DeliveryMethodName { get; set; }

        public bool? IsActive { get; set; }


    }
}
