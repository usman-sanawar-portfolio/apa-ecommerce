﻿using System;

namespace DRS.APA.Domain.Core.Dtos.Repair
{
    public class ResourceDto
    {
        public int? ResourceId { get; set; }

        public string Name { get; set; }

        public int ResourceGroupId { get; set; }
        public string ResourceGroupName { get; set; }

        public string TimeZone { get; set; }

        public string MobilePhone { get; set; }

        public string Code { get; set; }

        public DateTime? HolidaysStartDate { get; set; }

        public string WorkingHours { get; set; }//150varhcar

        public float? HourlyRate { get; set; }

        public string SpecialHourlyRate { get; set; }//150varhcar

        public string CurrencyCode { get; set; }

        public string WebUserId { get; set; }

        public int? Vacations { get; set; }//No. of days vacations

        public string Skills { get; set; }//300 varchar
        public DateTime? DateOfJoining { get; set; }

        public int? AnnualLeaveAllowance { get; set; }

        public string EmergencyContactDetails { get; set; }

        public bool? Tracked { get; set; }

        public string Thumbnail { get; set; }

        public bool? IsActive { get; set; }
    }

}
