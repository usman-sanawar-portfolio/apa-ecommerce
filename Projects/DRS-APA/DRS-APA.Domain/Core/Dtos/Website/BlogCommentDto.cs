
using System;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Website
{
    public class BlogCommentDto
    {
        public int BlogCommentId { get; set; }
        public int BlogId { get; set; }
        public string BlogTitle { get; set; }
        public string Comment { get; set; }
        public bool IsActive { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
  }
}
