﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Website
{
    public class WebHtmlDto
    {
        public short? WebHtmlId { get; set; }
        public short? WebComponentId { get; set; }
        public string WebComponentTitle { get; set; }
        public string HtmlData { get; set; }
        public short Order { get; set; }
        /// <summary>
        ///1=full, 1/2(half of the parent container) ,1/3 of parent containder ,1/4 of parent container
        /// </summary>
        public string Size { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}
