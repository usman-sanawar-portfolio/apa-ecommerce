using System;
using System.Collections.Generic;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Website
{
    public class BlogDto
    {
        public int BlogId { get; set; }
        public string BlogTitle { get; set; }
        /// <summary>
        /// HTML - Rich Editor Text
        /// </summary>
        public string Content { get; set; }
        //AuthorId will come here, when Auth is implemented
        public bool IsApproved { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string SeoH1 { get; set; }
        public string SeoH2 { get; set; }
        public string SeoH3 { get; set; }
        public string MetaTagTitle { get; set; }
        public string MetaTagDescription { get; set; }
        public string MetaTagKeywords { get; set; }
        public int BlogCategoryId { get; set; }
        public string BlogCategoryName { get; set; }
       public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Image { get; set; }
    public DateTime? CreatedOn { get; set; } 
    public string CreatedBy { get; set; }
    public DateTime? UpdatedOn { get; set; } 
    public string UpdatedBy { get; set; }
    public ICollection<BlogCommentDto> BlogComment { get; set; } = new List<BlogCommentDto>();
        public bool IsActive { get; set; }
    }
}
