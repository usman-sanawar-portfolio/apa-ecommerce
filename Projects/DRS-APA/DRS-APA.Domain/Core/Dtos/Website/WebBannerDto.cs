﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Website
{
    public class WebBannerDto
    {
        public int WebBannerId { get; set; }
        public string HeaderText { get; set; }
        public string SubText { get; set; }
        public string HeaderTextColor { get; set; }
        public string SubTextColor { get; set; }
        public bool ShowButton { get; set; }
        public string ButtonText { get; set; }
        public string ButtonColor { get; set; }
        public string ButtonUrl { get; set; }
        public string PageSlug { get; set; }
        public string PhotoUrl { get; set; }
        public bool IsActive { get; set; }
    }
}
