﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Website
{
    public class WebPageDto
    {
        public short? WebPageId { get; set; }
        [Required]
        public string WebPageTitle { get; set; }
        [Required]
        public string Slug { get; set; }
        public bool? IsActive { get; set; }
        //
        public WebMenuDto WebMenu { get; set; }
        //public short? WebSubMenuId { get; set; }
        //public WebSubMenuDto WebSubMenu { get; set; }
        //
        public ICollection<WebPageComponentDto> WebPageComponentsDto { get; set; } = new List<WebPageComponentDto>();

    }
}
