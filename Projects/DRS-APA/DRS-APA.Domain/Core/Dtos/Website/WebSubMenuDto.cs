﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Website
{
    public class WebSubMenuDto
    {
        public short? WebSubMenuId { get; set; }
        public string WebSubMenuTitle { get; set; }
        public short? WebMenuId { get; set; }
        public string WebMenuTitle { get; set; }
        public short? WebPageId { get; set; }
        public string WebPageTitle { get; set; }
        public string Slug { get; set; }
        public short Order { get; set; }
    }
}