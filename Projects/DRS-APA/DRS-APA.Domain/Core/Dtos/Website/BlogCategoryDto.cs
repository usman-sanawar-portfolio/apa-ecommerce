﻿using System.Collections.Generic;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Website
{
    public class BlogCategoryDto
    {
        public int BlogCategoryId { get; set; }
        public string CategoryName { get; set; }
        public ICollection<BlogDto> Blog { get; set; } = new List<BlogDto>();
        public bool IsActive { get; set; }
    }
}
