﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Website
{
    public class WebCarousalPhotosDto
    {
        public int WebCarousalPhotosId { get; set; }
        public string PhotoURL { get; set; }
        public int WebCarousalId { get; set; }
        public string PageSlug { get; set; }
    }
}
