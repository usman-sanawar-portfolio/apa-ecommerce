﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Website
{
    public class WebComponentDto
    {
        public short? WebComponentId { get; set; }
        public string Title { get; set; }
        /// <summary>
        /// photo,card,video,html,doc
        /// </summary>
        public string Type { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

        //public ICollection<WebPageComponentDto> WebPageComponentsDto { get; set; } = new List<WebPageComponentDto>();
        public ICollection<WebVideoDto> WebVideos { get; set; } = new List<WebVideoDto>();
        public ICollection<WebDocDto> WebDocs { get; set; } = new List<WebDocDto>();
        public ICollection<WebCardDto> WebCards { get; set; } = new List<WebCardDto>();
        public ICollection<WebPhotoDto> WebPhotos { get; set; } = new List<WebPhotoDto>();
        public ICollection<WebHtmlDto> WebHtmls { get; set; } = new List<WebHtmlDto>();
    }
}
