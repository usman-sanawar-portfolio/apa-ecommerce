﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Website
{
    public class WebDocDto
    {
        public short? WebDocId { get; set; }
        public short? WebComponentId { get; set; }
        public string WebComponentTitle { get; set; }
        public string Header { get; set; }//text above document
        public string Icon { get; set; }//display icon
        public string DownloadLink { get; set; }
        public short Order { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}
