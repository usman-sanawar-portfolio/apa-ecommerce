﻿using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Dtos.MasterData;
using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Website
{
    public class HomePageCollectionDto
    {
        public int HomePageCollectionId { get; set; }
        public short ProductCategoryId { get; set; }
        public string ProductCategoryName { get; set; }
        public bool IsActive { get; set; }
        public ICollection<ProductCategoriesJunctionForHomeDto> ProductCategoriesJunctionForHome = new List<ProductCategoriesJunctionForHomeDto>();
    }
}