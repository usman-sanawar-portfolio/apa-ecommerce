﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Website
{
    public class WebPageComponentDto
    {
        public short WebPageId { get; set; }
        public short WebComponentId { get; set; }
        public string WebPageTitle { get; set; }
        public string WebComponentTitle { get; set; }
        public short Order { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

        //public WebPageDto WebPage { get; set; }
        public WebComponentDto WebComponent { get; set; }


    }
}
