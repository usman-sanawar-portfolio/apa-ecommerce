﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Website.Data
{
    public class WebPhotoData
    {
        public string OverlayMainText { get; set; }//Text on photo
        public string OverlayMainTextColor { get; set; }//color to not mix text with photo bg
        public string OverlaySubText { get; set; }
        public string OverlaySubTextColor { get; set; }
        public string ButtonText { get; set; }//if any button required
        public string ButtonLink { get; set; }//click link
        public string PhotoUrl { get; set; }//photo path
        /// <summary>
        ///1=full, 1/2(photo half of the parent container) ,1/3 of parent containder ,1/4 of parent container
        /// </summary>
        public string PhotoSize { get; set; }
    }
}
