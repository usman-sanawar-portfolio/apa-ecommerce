﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Website.Data
{
    public class WebDocData
    {
        
        public string Header { get; set; }//text above document
        public string Icon { get; set; }//display icon
        public string DownloadLink { get; set; }
    }
}
