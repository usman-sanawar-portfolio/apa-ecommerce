﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Website.Data
{
    public class WebCardData
    {
        public string Icon { get; set; }
        public string MainHeading { get; set; }//Card's top heading
        public string SubHeading { get; set; }
    }
}
