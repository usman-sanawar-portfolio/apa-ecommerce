﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Website
{
    public class WebMenuDto
    {
        public short? WebMenuId { get; set; }
        public string WebMenuTitle { get; set; }
        /// <summary>
        /// If Has Sub Menu then WebPagePage will be ignored 
        /// else get web page and its components
        /// </summary>
        public bool HasSubMenu { get; set; }
        public short? WebPageId { get; set; }
        public string WebPageTitle { get; set; }
        public string Slug { get; set; }
        public short Order { get; set; }
        public ICollection<WebSubMenuDto> WebSubMenu { get; set; } = new List<WebSubMenuDto>();
    }
}
