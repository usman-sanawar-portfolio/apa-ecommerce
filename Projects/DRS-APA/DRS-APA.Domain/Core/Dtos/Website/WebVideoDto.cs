﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DRS.APA.Microserivce.Domain.Core.Dtos.Website
{
    public class WebVideoDto
    {
        public short? WebVideoId { get; set; }
        public short? WebComponentId { get; set; }
        public string WebComponentTitle { get; set; }
        /// <summary>
        /// Youtube Video URL
        /// </summary>
        public string VideoUrl { get; set; }
        /// <summary>
        /// 1=full 1/2(video; half of the parent container) ,1/3 of parent containder ,1/4 of parent container
        /// </summary>
        public string VideoPlayerSize { get; set; }
        public short Order { get; set; }

        public bool? IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}
