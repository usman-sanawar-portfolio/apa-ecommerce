﻿using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Domain.Core.Repositories.Website
{
    public interface IWebPageComponentRepository
    {
        Task<IEnumerable<WebPageComponent>> GetAllWebPageComponents();
        Task<IQueryable<WebPageComponent>> GetAllWebPageComponents(bool withPagination);
        Task<WebPageComponent> GetWebPageComponentById(short componentId, short pageId);
        Task<ICollection<WebPageComponent>> GetWebPageComponentsOnPageId(short pageId);
        Task<ICollection<WebPageComponent>> GetWebPageComponentsOnSlug(string slug);
        Task<ICollection<WebPageComponent>> GetWebPageComponentsOnComponentId(short componentId);
        Task AddWebPageComponent(ICollection<WebPageComponent> WebPageComponent);
        void DeleteWebPageComponent(short webComponentId, short webPageId);
        bool UpdateWebPageComponentOk(short webComponentId, short webPageId, WebPageComponent WebPageComponent);
        bool GetWebPageComponentByIdAsNoTracking(short componentId, short pageId);
        Task<bool> WebPageComponentExists(short componentId, short pageId);
    }
}
