﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Domain.Core.Repositories.Website
{
    public interface IHomePageCollectionRepository
    {
        Task<IEnumerable<HomePageCollection>> GetAllHomePageCollection();
        //Task<IQueryable<HomePageCollection>> GetAllHomePageCollection(bool withPagination);
        Task<HomePageCollection> GetHomePageCollectionById(short id);
        Task<IEnumerable<HomePageCollection>> GetHomePageCollectionByComponentId(short id);
        Task AddHomePageCollection(HomePageCollection HomePageCollection);
        void DeleteHomePageCollection(short id);
        bool UpdateHomePageCollectionOk(short id, HomePageCollection HomePageCollection);
        bool GetHomePageCollectionByIdAsNoTracking(short id);

    }
}