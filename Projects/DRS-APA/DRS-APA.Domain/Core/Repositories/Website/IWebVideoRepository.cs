﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Domain.Core.Repositories.Website
{
    public interface IWebVideoRepository
    {
        Task<IEnumerable<WebVideo>> GetAllWebVideos();
        Task<IQueryable<WebVideo>> GetAllWebVideos(bool withPagination);
        Task<WebVideo> GetWebVideoById(short id);
        Task<IEnumerable<WebVideo>> GetWebVideoByComponentId(short id);
        Task AddWebVideo(WebVideo WebVideo);
        void DeleteWebVideo(short id);
        bool UpdateWebVideoOk(short id, WebVideo WebVideo);
        bool GetWebVideoByIdAsNoTracking(short id);

    }
}