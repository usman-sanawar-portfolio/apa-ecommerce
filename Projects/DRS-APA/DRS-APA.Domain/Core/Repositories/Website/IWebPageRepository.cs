﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Domain.Core.Repositories.Website
{
    public interface IWebPageRepository
    {
        Task<IEnumerable<WebPage>> GetAllWebPages();
        Task<IQueryable<WebPage>> GetAllWebPages(bool withPagination);
        Task<WebPage> GetWebPageById(short id);
        Task AddWebPage(WebPage WebPage);
        void DeleteWebPage(short id);
        Task<bool> WebPageExists(short WebPageId);
        bool UpdateWebPageOk(short id, WebPage WebPage);
        bool GetWebPageByIdAsNoTracking(short id);
        Task<bool> IsWebPageTitleExisting(string WebPageTitle);
        Task<bool> IsSlugExisting(string Slug);
        Task<WebPage> GetWebPageByName(string name);
    }
}