﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Domain.Core.Repositories.Website
{
    public interface IWebDocRepository
    {
        Task<IEnumerable<WebDoc>> GetAllWebDocs();
        Task<IQueryable<WebDoc>> GetAllWebDocs(bool withPagination);
        Task<WebDoc> GetWebDocById(short id);
        Task<IEnumerable<WebDoc>> GetWebDocByComponentId(short id);
        Task AddWebDoc(WebDoc WebDoc);
        void DeleteWebDoc(short id);
        bool UpdateWebDocOk(short id, WebDoc WebDoc);
        bool GetWebDocByIdAsNoTracking(short id);

    }
}