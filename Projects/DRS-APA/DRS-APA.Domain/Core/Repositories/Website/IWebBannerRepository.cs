﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Domain.Core.Repositories.Website
{
    public interface IWebBannerRepository
    {
        Task<IEnumerable<WebBanner>> GetAllWebBanner();
        Task<WebBanner> GetWebBannerById(int id);
        Task AddWebBanner(WebBanner webBanner);
        void DeleteWebBanner(int id);
        bool UpdateWebBannerOk(int id, WebBanner webBanner);
        bool GetWebBannerByIdAsNoTracking(int id);
    }
}