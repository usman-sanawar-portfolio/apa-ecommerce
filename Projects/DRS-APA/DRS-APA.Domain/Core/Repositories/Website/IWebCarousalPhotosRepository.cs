﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Domain.Core.Repositories.Website
{
    public interface IWebCarousalPhotosRepository
    {
        Task<IEnumerable<WebCarousalPhotos>> GetAllWebCarousalPhotos();
        Task<IQueryable<WebCarousalPhotos>> GetAllWebCarousalPhotos(bool withPagination);
        Task<WebCarousalPhotos> GetWebCarousalPhotosById(int id);
        Task AddWebCarousalPhotos(WebCarousalPhotos WebCarousalPhotos);
        void DeleteWebCarousalPhotos(int id);
        bool UpdateWebCarousalPhotosOk(int id, WebCarousalPhotos WebCarousalPhotos);
        bool GetWebCarousalPhotosByIdAsNoTracking(int id);
    }
}