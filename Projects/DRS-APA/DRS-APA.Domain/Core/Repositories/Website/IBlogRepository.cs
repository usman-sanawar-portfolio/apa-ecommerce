using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Domain.Core.Repositories.Website
{
  public interface IBlogRepository
  {
    Task<System.Collections.Generic.IEnumerable<Blog>> GetAllBlogs();
    Task<IQueryable<Blog>> GetAllBlogs(bool withPagination);
    Task<Blog> GetBlogById(int id);
    Task AddBlog(Blog blog);
    void DeleteBlog(int id);
    Task<bool> BlogExists(int blogId);
    bool UpdateBlogOk(int id, Blog blog);
    bool GetBlogByIdAsNoTracking(int id);

  }
}
