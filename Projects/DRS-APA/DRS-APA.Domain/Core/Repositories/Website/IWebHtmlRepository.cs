﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Domain.Core.Repositories.Website
{
    public interface IWebHtmlRepository
    {
        Task<IEnumerable<WebHtml>> GetAllWebHtmls();
        Task<IQueryable<WebHtml>> GetAllWebHtmls(bool withPagination);
        Task<WebHtml> GetWebHtmlById(short id);
        Task<IEnumerable<WebHtml>> GetWebHtmlByComponentId(short id);
        Task AddWebHtml(WebHtml WebHtml);
        void DeleteWebHtml(short id);
        bool UpdateWebHtmlOk(short id, WebHtml WebHtml);
        bool GetWebHtmlByIdAsNoTracking(short id);

    }
}