﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Domain.Core.Repositories.Website
{
    public interface IWebCardRepository
    {
        Task<IEnumerable<WebCard>> GetAllWebCards();
        Task<IQueryable<WebCard>> GetAllWebCards(bool withPagination);
        Task<WebCard> GetWebCardById(short id);
        Task<IEnumerable<WebCard>> GetWebCardByComponentId(short id);
        Task AddWebCard(WebCard WebCard);
        void DeleteWebCard(short id);
        bool UpdateWebCardOk(short id, WebCard WebCard);
        bool GetWebCardByIdAsNoTracking(short id);

        //bool GetWebCardByIdAsNoTracking(short id);     
    }
}