﻿using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Domain.Core.Repositories.Website
{
    public interface IWebComponentRepository
    {
        Task<IEnumerable<WebComponent>> GetAllWebComponents();
        Task<IQueryable<WebComponent>> GetAllWebComponents(bool withPagination);
        Task<WebComponent> GetWebComponentById(short id);
        Task AddWebComponent(WebComponent WebComponent);
        void DeleteWebComponent(short id);
        bool UpdateWebComponentOk(short id, WebComponent WebComponent);
        bool GetWebComponentByIdAsNoTracking(short id);
        Task<WebComponent> GetWebComponentByName(string name);
    }
}
