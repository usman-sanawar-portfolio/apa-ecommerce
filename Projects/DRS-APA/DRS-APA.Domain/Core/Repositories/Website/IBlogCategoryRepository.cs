using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Domain.Core.Repositories.Website
{
  public interface IBlogCategoryRepository
  {
    Task<System.Collections.Generic.IEnumerable<BlogCategory>> GetAllBlogCategories();
    Task<IQueryable<BlogCategory>> GetAllBlogCategories(bool withPagination);
    Task<BlogCategory> GetBlogCategoryById(int id);
    Task AddBlogCategory(BlogCategory blogCategory);
    void DeleteBlogCategory(int id);
    Task<bool> BlogCategoryExists(int blogCategoryId);
    bool UpdateBlogCategoryOk(int id, BlogCategory blogCategory);
    bool GetBlogCategoryByIdAsNoTracking(int id);

  }
}
