﻿using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Domain.Core.Repositories.Website
{
    public interface IWebMenuRepository
    {
        Task<IEnumerable<WebMenu>> GetAllWebMenus();
        Task<IQueryable<WebMenu>> GetAllWebMenus(bool withPagination);
        Task<WebMenu> GetWebMenuById(short id);
        Task AddWebMenu(WebMenu WebMenu);
        void DeleteWebMenu(short id);
        bool UpdateWebMenuOk(short id, WebMenu WebMenu);
        bool GetWebMenuByIdAsNoTracking(short id);
    }
}
