﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Domain.Core.Repositories.Website
{
    public interface IWebCarousalRepository
    {
        Task<IEnumerable<WebCarousal>> GetAllWebCarousal();
        //Task<IQueryable<WebCarousal>> GetAllWebCarousal(bool withPagination);
        Task<WebCarousal> GetWebCarousalById(int id);
        Task AddWebCarousal(WebCarousal WebCarousal);
        void DeleteWebCarousal(int id);
        bool UpdateWebCarousalOk(int id, WebCarousal WebCarousal);
        bool GetWebCarousalByIdAsNoTracking(int id);

    }
}