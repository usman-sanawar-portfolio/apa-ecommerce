using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Domain.Core.Repositories.Website
{
  public interface IBlogCommentRepository
  {
    Task<System.Collections.Generic.IEnumerable<BlogComment>> GetAllBlogComments();
    Task<IQueryable<BlogComment>> GetAllBlogComments(bool withPagination);
    Task<BlogComment> GetBlogCommentById(int id);
    Task AddBlogComment(BlogComment blogComment);
    void DeleteBlogComment(int id);
    Task<bool> BlogCommentExists(int blogCommentId);
    bool UpdateBlogCommentOk(int id, BlogComment blogComment);
    bool GetBlogCommentByIdAsNoTracking(int id);

  }
}
