﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Domain.Core.Repositories.Website
{
    public interface IWebPhotoRepository
    {
        Task<IEnumerable<WebPhoto>> GetAllWebPhotos();
        Task<IQueryable<WebPhoto>> GetAllWebPhotos(bool withPagination);
        Task<WebPhoto> GetWebPhotoById(short id);
        Task<IEnumerable<WebPhoto>> GetWebPhotoByComponentId(short id);
        Task AddWebPhoto(WebPhoto WebPhoto);
        void DeleteWebPhoto(short id);
        bool UpdateWebPhotoOk(short id, WebPhoto WebPhoto);
        bool GetWebPhotoByIdAsNoTracking(short id);     
    }
}