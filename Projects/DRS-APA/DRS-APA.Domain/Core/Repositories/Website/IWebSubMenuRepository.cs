﻿using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Domain.Core.Repositories.Website
{
    public interface IWebSubMenuRepository
    {
        Task<IEnumerable<WebSubMenu>> GetAllWebSubMenus();
        Task<IQueryable<WebSubMenu>> GetAllWebSubMenus(bool withPagination);
        Task<WebSubMenu> GetWebSubMenuById(short id);
        Task AddWebSubMenu(WebSubMenu WebSubMenu);
        void DeleteWebSubMenu(short id);
        bool UpdateWebSubMenuOk(short id, WebSubMenu WebSubMenu);
        bool GetWebSubMenuByIdAsNoTracking(short id);
    }
}
