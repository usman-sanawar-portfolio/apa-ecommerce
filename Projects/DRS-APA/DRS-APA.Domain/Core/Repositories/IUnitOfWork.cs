using System;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Repositories;

namespace DRS.APA.Core.Repositories
{
  public interface IUnitOfWork
  {

        Task<bool> CompleteAsync();
        Task<bool> CompleteStoreAsync();
  }
}