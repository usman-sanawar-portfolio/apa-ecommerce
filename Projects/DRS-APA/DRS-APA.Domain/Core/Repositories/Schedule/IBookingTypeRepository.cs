﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Schedule;
using DRS.APA.Domain.Core.Interfaces;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IBookingTypeRepository
    {
        Task<IEnumerable<BookingType>> GetAllBookingTypes();

        Task<IQueryable<BookingType>> GetAllBookingTypes(bool withPagination);

        Task<IQueryable<BookingType>> GetAllBookingTypes(ISpecification<BookingType> spec);

        Task<BookingType> GetBookingTypeById(short id);

        bool GetBookingTypeByIdAsNoTracking(short id);

        Task AddBookingType(BookingType bookingType);

        bool UpdateBookingTypeOk(short id, BookingType bookingType);

        void DeleteBookingType(short id);

        Task<BookingType> GetBookingTypeByName(string name);

    }
}