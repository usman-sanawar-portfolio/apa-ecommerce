﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Schedule;
using DRS.APA.Domain.Core.Interfaces;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IScheduleRepository
    {      
        Task<IEnumerable<Schedule>> GetAllSchedules();

        //Task<IEnumerable<Schedule>> GetAllSchedulesByCategory(int categoryId);
        Task<Schedule> GetWeeklySchedule(DateTime startDate);

        Task<IQueryable<Schedule>> GetAllSchedules(bool withPagination);

        Task<IQueryable<Schedule>> GetAllSchedules(ISpecification<Schedule> spec);

        Task<Schedule> GetScheduleById(int id);

        Task AddSchedule(Schedule schedule);

        void DeleteSchedule(int id);   
        //
        //Task<bool> ScheduleExists(int scheduleId);

        bool UpdateScheduleOk(int id, Schedule schedule);

      //  Task<Schedule> GetScheduleByTitle(string name);

        bool GetScheduleByIdAsNoTracking(int id);
    }
}