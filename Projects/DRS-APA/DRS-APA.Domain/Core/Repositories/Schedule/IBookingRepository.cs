﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Schedule;
using DRS.APA.Domain.Core.Interfaces;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IBookingRepository
    {
        Task<IEnumerable<Booking>> GetAllBookings();

        Task<IQueryable<Booking>> GetAllBookings(bool withPagination);

        Task<IQueryable<Booking>> GetAllBookings(ISpecification<Booking> spec);

        Task<IEnumerable<Booking>> GetAllBookingsBySchedule(int scheduleId);

        Task<IEnumerable<Booking>> GetAllBookingsByDate(DateTime date);

        Task<Booking> GetBookingById(int id);

        bool GetBookingByIdAsNoTracking(int id);

        Task<Booking> GetBookingByTitle(string name);

        Task AddBooking(Booking schedule);

        bool UpdateBookingOk(int id, Booking schedule);

        void DeleteBooking(int id);
    }

}