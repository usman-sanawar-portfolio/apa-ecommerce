using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Domain.Core.Repositories.Shipment
{
  public interface IUK_ExpressDataBagDeliveryRepository
  {
    Task<IEnumerable<UK_ExpressDataBagDelivery>> GetAllUK_ExpressDataBagDelivery();
    Task<IQueryable<UK_ExpressDataBagDelivery>> GetAllUK_ExpressDataBagDelivery(bool withPagination);
    Task<UK_ExpressDataBagDelivery> GetUK_ExpressDataBagDeliveryById(int id);
    Task AddUK_ExpressDataBagDelivery(UK_ExpressDataBagDelivery uk_ExpressDataBagDelivery);
    void DeleteUK_ExpressDataBagDelivery(int id);
    Task<bool> UK_ExpressDataBagDeliveryExists(int uk_ExpressDataBagDeliveryId);
    bool UpdateUK_ExpressDataBagDeliveryOk(int id, UK_ExpressDataBagDelivery uk_ExpressDataBagDelivery);
    bool GetUK_ExpressDataBagDeliveryByIdAsNoTracking(int id);

  }
}
