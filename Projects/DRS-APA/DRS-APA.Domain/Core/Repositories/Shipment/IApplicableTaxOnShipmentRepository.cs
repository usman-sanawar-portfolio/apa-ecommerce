using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Domain.Core.Repositories.Shipment
{
  public interface IApplicableTaxOnShipmentRepository
  {
    Task<IEnumerable<ApplicableTaxOnShipment>> GetAllApplicableTaxOnShipment();
    Task<IQueryable<ApplicableTaxOnShipment>> GetAllApplicableTaxOnShipment(bool withPagination);
    Task<ApplicableTaxOnShipment> GetApplicableTaxOnShipmentById(int id);
    Task AddApplicableTaxOnShipment(ApplicableTaxOnShipment ApplicableTaxOnShipment);
    void DeleteApplicableTaxOnShipment(int id);
    Task<bool> ApplicableTaxOnShipmentExists(int ApplicableTaxOnShipmentId);
    bool UpdateApplicableTaxOnShipmentOk(int id, ApplicableTaxOnShipment ApplicableTaxOnShipment);
    bool GetApplicableTaxOnShipmentByIdAsNoTracking(int id);

  }
}
