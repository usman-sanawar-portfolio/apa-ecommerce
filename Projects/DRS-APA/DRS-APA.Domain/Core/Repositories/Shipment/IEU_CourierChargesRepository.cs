using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Domain.Core.Repositories.Shipment
{
  public interface IEU_CourierChargesRepository
  {
    Task<IEnumerable<EU_CourierCharges>> GetAllEU_CourierCharges();
    Task<IQueryable<EU_CourierCharges>> GetAllEU_CourierCharges(bool withPagination);
    Task<EU_CourierCharges> GetEU_CourierChargesById(int id);
    Task AddEU_CourierCharges(EU_CourierCharges eu_CourierCharges);
    void DeleteEU_CourierCharges(int id);
    Task<bool> EU_CourierChargesExists(int eu_CourierChargesId);
    bool UpdateEU_CourierChargesOk(int id, EU_CourierCharges eu_CourierCharges);
    bool GetEU_CourierChargesByIdAsNoTracking(int id);

  }
}
