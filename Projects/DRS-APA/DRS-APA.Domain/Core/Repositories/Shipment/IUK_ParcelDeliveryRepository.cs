﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IUK_ParcelDeliveryRepository
    {
        Task<IEnumerable<UK_ParcelDelivery>> GetAllUK_ParcelDeliveries();
        Task<IQueryable<UK_ParcelDelivery>> GetAllUK_ParcelDeliveries(bool withPagination);
        Task<UK_ParcelDelivery> GetUK_ParcelDeliveryById(int id);
        Task AddUK_ParcelDelivery(UK_ParcelDelivery parcelDelivery_UK);
        void DeleteUK_ParcelDelivery(int id);
        Task<bool> UK_ParcelDeliveryExists(int parcelDelivery_UKId);
        bool UpdateUK_ParcelDeliveryOk(int id, UK_ParcelDelivery parcelDelivery_UK);
        bool GetUK_ParcelDeliveryByIdAsNoTracking(int id);

    }
}