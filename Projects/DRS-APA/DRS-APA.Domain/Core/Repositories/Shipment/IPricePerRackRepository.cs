using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Domain.Core.Repositories.Shipment
{
    public interface IPricePerRackRepository
    {
        Task<IEnumerable<PricePerRack>> GetAllPricePerRack();
        Task<IQueryable<PricePerRack>> GetAllPricePerRack(bool withPagination);
        Task<PricePerRack> GetPricePerRackById(int id);
        Task AddPricePerRack(PricePerRack PricePerRack);
        void DeletePricePerRack(int id);
        Task<bool> PricePerRackExists(int PricePerRackId);
        bool UpdatePricePerRackOk(int id, PricePerRack PricePerRack);
        bool GetPricePerRackByIdAsNoTracking(int id);
    }
}
