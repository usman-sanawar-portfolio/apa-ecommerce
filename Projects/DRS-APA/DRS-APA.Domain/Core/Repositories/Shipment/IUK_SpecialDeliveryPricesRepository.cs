﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IUK_SpecialDeliveryPricesRepository
    {
        Task<IEnumerable<UK_SpecialDeliveryPrices>> GetAllUK_SpecialDeliveryPrices();
        //Task<IEnumerable<UK_SpecialDeliveryPrices>> GetAllUK_SpecialDeliveryPricessByCategory(List<int> categoryId);
        Task<IQueryable<UK_SpecialDeliveryPrices>> GetAllUK_SpecialDeliveryPrices(bool withPagination);
        //Task<IQueryable<UK_SpecialDeliveryPrices>> GetAllUK_SpecialDeliveryPricess(ISpecification<UK_SpecialDeliveryPrices> spec);
        Task<UK_SpecialDeliveryPrices> GetUK_SpecialDeliveryPricesById(int id);
        Task AddUK_SpecialDeliveryPrices(UK_SpecialDeliveryPrices specialDeliveryPrices_UK);
        void DeleteUK_SpecialDeliveryPrices(int id);
        Task<bool> UK_SpecialDeliveryPricesExists(int specialDeliveryPrices_UKId);
        bool UpdateUK_SpecialDeliveryPricesOk(int id, UK_SpecialDeliveryPrices specialDeliveryPrices_UK);
        //Task<UK_SpecialDeliveryPrices> GetUK_SpecialDeliveryPricesByTitle(string name);
        bool GetUK_SpecialDeliveryPricesByIdAsNoTracking(int id);
    }
}