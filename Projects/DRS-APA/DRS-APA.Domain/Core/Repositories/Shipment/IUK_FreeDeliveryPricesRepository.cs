﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IUK_FreeDeliveryPricesRepository
    {
        Task<IEnumerable<UK_FreeDeliveryPrices>> GetAllUK_FreeDeliveryPrices();
        Task<IQueryable<UK_FreeDeliveryPrices>> GetAllUK_FreeDeliveryPrices(bool withPagination);
        Task<UK_FreeDeliveryPrices> GetUK_FreeDeliveryPricesById(int id);
        Task AddUK_FreeDeliveryPrices(UK_FreeDeliveryPrices freeDeliveryPrices_UK);
        void DeleteUK_FreeDeliveryPrices(int id);
        Task<bool> UK_FreeDeliveryPricesExists(int freeDeliveryPrices_UKId);
        bool UpdateUK_FreeDeliveryPricesOk(int id, UK_FreeDeliveryPrices freeDeliveryPrices_UK);
        bool GetUK_FreeDeliveryPricesByIdAsNoTracking(int id);
    }
}