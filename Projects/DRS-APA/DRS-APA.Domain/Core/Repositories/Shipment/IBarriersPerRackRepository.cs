using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Domain.Core.Repositories.Shipment
{
  public interface IBarriersPerRackRepository
  {
    Task<IEnumerable<BarriersPerRack>> GetAllBarriersPerRack();
    Task<IQueryable<BarriersPerRack>> GetAllBarriersPerRack(bool withPagination);
    Task<BarriersPerRack> GetBarriersPerRackById(int id);
    Task AddBarriersPerRack(BarriersPerRack barriersPerRack);
    void DeleteBarriersPerRack(int id);
    Task<bool> BarriersPerRackExists(int barriersPerRackId);
    bool UpdateBarriersPerRackOk(int id, BarriersPerRack barriersPerRack);
    bool GetBarriersPerRackByIdAsNoTracking(int id);

  }
}
