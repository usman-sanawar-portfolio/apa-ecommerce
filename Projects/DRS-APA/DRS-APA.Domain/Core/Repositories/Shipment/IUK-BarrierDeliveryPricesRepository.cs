using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Domain.Core.Repositories.Shipment
{
  public interface IUK_BarrierDeliveryPricesRepository
  {
    Task<IEnumerable<UK_BarrierDeliveryPrices>> GetAllUK_BarrierDeliveryPrices();
    Task<IQueryable<UK_BarrierDeliveryPrices>> GetAllUK_BarrierDeliveryPrices(bool withPagination);
    Task<UK_BarrierDeliveryPrices> GetUK_BarrierDeliveryPricesById(int id);
    Task AddUK_BarrierDeliveryPrices(UK_BarrierDeliveryPrices uk_BarrierDeliveryPrices);
    void DeleteUK_BarrierDeliveryPrices(int id);
    Task<bool> UK_BarrierDeliveryPricesExists(int uk_BarrierDeliveryPricesId);
    bool UpdateUK_BarrierDeliveryPricesOk(int id, UK_BarrierDeliveryPrices uk_BarrierDeliveryPrices);
    bool GetUK_BarrierDeliveryPricesByIdAsNoTracking(int id);

  }
}
