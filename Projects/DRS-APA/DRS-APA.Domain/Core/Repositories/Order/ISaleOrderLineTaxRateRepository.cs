using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface ISaleOrderLineTaxRateRepository
  {
    Task<IEnumerable<SaleOrderLineTaxRate>> GetAllSaleOrderLineTaxRate();
    Task<IQueryable<SaleOrderLineTaxRate>> GetAllSaleOrderLineTaxRate(bool withPagination);
    Task<SaleOrderLineTaxRate> GetSaleOrderLineTaxRateById(int id);
    Task AddSaleOrderLineTaxRate(SaleOrderLineTaxRate saleOrderLines);
    void DeleteSaleOrderLineTaxRate(int id);
    Task<bool> SaleOrderLineTaxRateExists(int saleOrderLinesId);
    bool UpdateSaleOrderLineTaxRateOk(int id, SaleOrderLineTaxRate saleOrderLines);
    bool GetSaleOrderLineTaxRateByIdAsNoTracking(int id);

  }
}
