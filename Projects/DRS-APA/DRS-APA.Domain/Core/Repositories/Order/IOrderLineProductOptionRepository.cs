﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Domain.Core.Repositories.Order
{
    public interface IOrderLineProductOptionRepository
    {
        Task<OrderLineProductOptions> GetOrderLineProductOptionsById(int id);
        Task AddOrderLineProductOptions(OrderLineProductOptions OrderLineProductOption);
        void DeleteOrderLineProductOptions(int id);
        Task<bool> OrderLineProductOptionsExists(int OrderLineProductOptionId);
        bool GetOrderLineProductOptionsByIdAsNoTracking(int id);

    }
}