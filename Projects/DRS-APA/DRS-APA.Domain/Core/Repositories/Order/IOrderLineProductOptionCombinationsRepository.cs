using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Domain.Core.Repositories.Order
{
    public interface IOrderLineProductOptionCombinationsRepository
    {
        void DeleteOrderLineProductOptionCombinations(int id);
        Task<OrderLineProductOptionCombinations> GetOrderLineProductOptionCombinationsById(int id);
        Task AddOrderLineProductOptionCombinations(OrderLineProductOptionCombinations OrderLineProductOptionCombinations);
        Task<bool> OrderLineProductOptionCombinationsExists(int OrderLineProductOptionCombinationsId);
        bool UpdateOrderLineProductOptionCombinationsOk(int id, OrderLineProductOptionCombinations OrderLineProductOptionCombinations);
        bool GetOrderLineProductOptionCombinationsByIdAsNoTracking(int id);

    }
}
