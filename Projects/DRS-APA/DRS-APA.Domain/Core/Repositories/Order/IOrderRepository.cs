using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Domain.Core.Repositories.Order
{
    public interface IOrderRepository
    {
        Task<IEnumerable<Orders>> GetAllOrders();
        Task<IQueryable<Orders>> GetAllOrders(bool withPagination);
        Task<IQueryable<Orders>> GetAllOrders(ISpecification<Orders> spec);
        Task<Orders> GetOrderById(int id);
        Task AddOrder(Orders order);
        void DeleteOrder(int id);
        Task<bool> OrderExists(int orderId);
        bool UpdateOrderOk(int id, Orders order);
        Task<Orders> GetOrderByName(string name);
        bool GetOrderByIdAsNoTracking(int id);
        // Methods for OrderAddress
        Task AddOrderAddress(OrderAddress orderAddress);
        void DeleteOrderAddress(int id);
        bool UpdateOrderAddressOk(int id, OrderAddress orderAddress);
        Task<OrderAddress> GetOrderAddressById(int id);
        bool GetOrderAddressByIdAsNoTracking(int id);
        bool UpdateActiveStatus(int id, bool status);
        bool UpdateOrderStatusOnId(int orderId, int statusId);
        Task<Orders> GetOrderByidentifier(string orderidentifier);
        bool UpdateOrderStatus(string orderidentifier);
        bool UpdateOrderStatusAfterPaymentRecieve(string orderidentifier);
        bool PaymentFailedOrderStatus(string orderidentifier);
        Task<IQueryable<Orders>> GetOrdersByStatusCode(string OrderStatusCode);
        Task<bool> OrderStatusCodeExist(string orderidentifier);
        Task AddOrderIdentifier(OrderIdentifier order);
        Task<bool> OrderIdentifierExists(int orderIdentifier);
        bool UpdateOrderSessionId(int id, string sessionId, string paymentIntentId);
        bool CheckONOrderSessionId(string sessionId, string paymentIntentId);
        Task<IQueryable<Orders>> GetNotCompletedOrders(bool? notCompletedOrders);
        Task<bool> PrecheckOnNotCompletedOrder(string OrderIdentifier);
    }
}
