using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IOrderLineTaxRateRepository
  {
    Task<IEnumerable<OrderLineTaxRate>> GetAllOrderLineTaxRate();
    Task<IQueryable<OrderLineTaxRate>> GetAllOrderLineTaxRate(bool withPagination);
     Task<OrderLineTaxRate> GetOrderLineTaxRateById(int id);
    Task AddOrderLineTaxRate(OrderLineTaxRate orderLines);
    void DeleteOrderLineTaxRate(int id);
    Task<bool> OrderLineTaxRateExists(int orderLinesId);
    bool UpdateOrderLineTaxRateOk(int id, OrderLineTaxRate orderLines);
    bool GetOrderLineTaxRateByIdAsNoTracking(int id);

  }
}
