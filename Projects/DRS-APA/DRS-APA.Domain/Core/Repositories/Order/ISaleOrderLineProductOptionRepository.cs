﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Domain.Core.Repositories.Order
{
    public interface ISaleOrderLineProductOptionRepository
    {
        Task<SaleOrderLineProductOptions> GetSaleOrderLineProductOptionsById(int id);
        Task AddSaleOrderLineProductOptions(SaleOrderLineProductOptions SaleOrderLineProductOption);
        void DeleteSaleOrderLineProductOptions(int id);
        Task<bool> SaleOrderLineProductOptionsExists(int SaleOrderLineProductOptionId);
        bool GetSaleOrderLineProductOptionsByIdAsNoTracking(int id);

    }
}