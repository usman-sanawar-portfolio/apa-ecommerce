using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Domain.Core.Repositories.Order
{
    public interface IOrderLinesRepository
    {
        Task<IEnumerable<OrderLines>> GetAllOrderLines();
        Task<IQueryable<OrderLines>> GetAllOrderLines(bool withPagination);
        Task<OrderLines> GetOrderLinesById(int id);
        Task AddOrderLines(OrderLines orderLines);
        void DeleteOrderLines(int id);
        Task<bool> OrderLinesExists(int orderLinesId);
        bool UpdateOrderLinesOk(int id, OrderLines orderLines);
        bool GetOrderLinesByIdAsNoTracking(int id);

    }
}
