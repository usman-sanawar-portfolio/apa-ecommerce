using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Domain.Core.Repositories.Order
{
    public interface ISaleOrderRepository
    {
        Task<IEnumerable<SaleOrder>> GetAllSaleOrders();
        Task<IQueryable<SaleOrder>> GetAllSaleOrders(bool withPagination);
        Task<IQueryable<SaleOrder>> GetAllSaleOrders(ISpecification<SaleOrder> spec);
        Task<SaleOrder> GetSaleOrderById(int id);
        Task AddSaleOrder(SaleOrder saleOrder);
        void DeleteSaleOrder(int id);
        Task<bool> SaleOrderExists(int saleOrderId);
        bool UpdateSaleOrderOk(int id, SaleOrder saleOrder);
        Task<SaleOrder> GetSaleOrderByName(string name);
        bool GetSaleOrderByIdAsNoTracking(int id);
        // Methods for SaleOrderAddress
        Task AddSaleOrderAddress(SaleOrderAddress saleOrderAddress);
        void DeleteSaleOrderAddress(int id);
        bool UpdateSaleOrderAddressOk(int id, SaleOrderAddress saleOrderAddress);
        Task<SaleOrderAddress> GetSaleOrderAddressById(int id);
        bool GetSaleOrderAddressByIdAsNoTracking(int id);
        bool UpdateActiveStatus(int id, bool status);
        bool UpdateSaleOrderStatusOnId(int saleOrderId, int statusId);
        int NumberofSaleOrdersWithInYear(int Mounth, int Year);
        void AddSaleOrderForOrder(SaleOrder saleOrder);
        Task<bool> SaleOrderExistsByIdentifier(string orderidentifier);
        bool DeductQuantityAfterSaleOrder(string orderidentifier);
        decimal GetSaleOrdrPriceSumWithInYear(int Mounth, int Year);
        Task<SaleOrder> GetSaleOrderByIdentifier(string SaleOrderIdentifier);

    }
}
