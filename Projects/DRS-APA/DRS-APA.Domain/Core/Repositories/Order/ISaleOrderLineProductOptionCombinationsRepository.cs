﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Domain.Core.Repositories.Order
{
    public interface ISaleOrderLineProductOptionCombinationsRepository
    {
        void DeleteSaleOrderLineProductOptionCombinations(int id);
        Task<SaleOrderLineProductOptionCombinations> GetSaleOrderLineProductOptionCombinationsById(int id);
        Task AddSaleOrderLineProductOptionCombinations(SaleOrderLineProductOptionCombinations SaleOrderLineProductOptionCombinations);
        Task<bool> SaleOrderLineProductOptionCombinationsExists(int SaleOrderLineProductOptionCombinationsId);
        bool UpdateSaleOrderLineProductOptionCombinationsOk(int id, SaleOrderLineProductOptionCombinations SaleOrderLineProductOptionCombinations);
        bool GetSaleOrderLineProductOptionCombinationsByIdAsNoTracking(int id);

    }
}