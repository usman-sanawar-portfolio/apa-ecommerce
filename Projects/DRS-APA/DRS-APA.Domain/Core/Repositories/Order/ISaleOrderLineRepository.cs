﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Domain.Core.Repositories.Order
{
    public interface ISaleOrderLinesRepository
    {
        Task<IEnumerable<SaleOrderLines>> GetAllSaleOrderLines();
        Task<IQueryable<SaleOrderLines>> GetAllSaleOrderLines(bool withPagination);
        Task<SaleOrderLines> GetSaleOrderLinesById(int id);
        Task AddSaleOrderLines(SaleOrderLines saleOrderLines);
        void DeleteSaleOrderLines(int id);
        Task<bool> SaleOrderLinesExists(int saleOrderLinesId);
        bool UpdateSaleOrderLinesOk(int id, SaleOrderLines saleOrderLines);
        bool GetSaleOrderLinesByIdAsNoTracking(int id);

    }
}