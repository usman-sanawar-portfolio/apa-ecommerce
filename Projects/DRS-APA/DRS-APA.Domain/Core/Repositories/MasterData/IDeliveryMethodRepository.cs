﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Core.Domain;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories
{
    public interface IDeliveryMethodRepository
    {      
        Task<IEnumerable<DeliveryMethod>> GetAllDeliveryMethods();

        Task<IQueryable<DeliveryMethod>> GetAllDeliveryMethods(bool withPagination);

        Task<DeliveryMethod> GetDeliveryMethodById(short id);

        Task AddDeliveryMethod(DeliveryMethod deliveryMethod);

        void DeleteDeliveryMethod(short id);   
        //
        Task<bool> DeliveryMethodExists(short deliveryMethodId);

         bool UpdateDeliveryMethodOk(short id, DeliveryMethod deliveryMethod);

        Task<DeliveryMethod> GetDeliveryMethodByName(string name);

        bool GetDeliveryMethodByIdAsNoTracking(short id);


    }
}