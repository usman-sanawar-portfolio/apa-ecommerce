using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Dtos.Shared;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IProductRepository
    {
        Task<IEnumerable<Product>> GetAllProducts();
        Task<IEnumerable<Product>> GetAllProductsWithManufacturerId(int?[] manufacturerId);
        Task<IEnumerable<Product>> GetAllProductsByCategory(List<int> categoryId);
        Task<IQueryable<Product>> GetAllProducts(bool withPagination);
        Task<IQueryable<Product>> GetAllProducts(ISpecification<Product> spec);
        Task<IEnumerable<Product>> GetAllProductsForAdminPanel();

        Task<IQueryable<Product>> GetAllProductsManufacturerId(int?[] manufacturerId);
        Task<Product> GetProductById(int id);
        Task AddProduct(Product product);
        void DeleteProduct(int id);
        Task<bool> ProductExists(int productId);
        bool UpdateProductOk(int id, Product product);
        Task<Product> GetProductByTitle(string name);
        bool GetProductByIdAsNoTracking(int id);
        Task<IEnumerable<Product>> GetAllKeyValueProducts();
        Task<ICollection<Review>> GetProductsLastReviews(int id);
        Task<ICollection<ProductCategoriesJunction>> GetProductCategoriesJunctionOnProductId(int productId);
        Task<IQueryable<Product>> GetProductByProductId(int[] id);
        Task<IEnumerable<Product>> GetKeyAndQuantityProducts(IEnumerable<ProductKeyValueDto> product);
        bool SumProductOptionQuantity(int productId);
        Task<Product> GetProductByIdWithLessJoins(int id);
        IEnumerable<ProductCategory> GetProductCategoryByParent(short? categoryId);
        IEnumerable<ProductCategoriesJunction> GetProductCategories(short? categoryId);
        Task<IEnumerable<Product>> GetProductsByName(string productName);
    }
}
