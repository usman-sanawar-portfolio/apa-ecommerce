﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IDiscountProductRepository
    {      
        Task<IEnumerable<DiscountProduct>> GetAllDiscountProducts();
        Task<IQueryable<DiscountProduct>> GetAllDiscountProducts(bool withPagination);

        Task<DiscountProduct> GetDiscountProductById(short discountId, int productId);

        Task AddDiscountProduct(DiscountProduct discountProduct);

        void DeleteDiscountProduct(short discountId, int productId);         

         bool UpdateDiscountProductOk(short discountId, int productId, DiscountProduct discountProduct);

        bool GetDiscountProductByIdAsNoTracking(short discountId, int productId);


    }
}