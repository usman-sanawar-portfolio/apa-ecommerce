﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IStockStatusRepository
    {
        Task<IEnumerable<StockStatus>> GetAllStockStatuses();
        Task<IQueryable<StockStatus>> GetAllStockStatuses(bool withPagination);
        Task<StockStatus> GetStockStatusById(int id);
        Task AddStockStatus(StockStatus stockStatus);
        void DeleteStockStatus(int id);
        Task<bool> StockStatusExists(int stockStatusId);
        bool UpdateStockStatusOk(int id, StockStatus stockStatus);
        bool GetStockStatusByIdAsNoTracking(int id);
        Task<StockStatus> GetStockStatusByName(string name);
    }
}