﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Repair;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IResourceGroupRepository
    {
        Task<IEnumerable<ResourceGroup>> GetAllResourceGroups();

        Task<IQueryable<ResourceGroup>> GetAllResourceGroups(bool withPagination);

        Task<ResourceGroup> GetResourceGroupById(int id);

        Task AddResourceGroup(ResourceGroup resourceGroup);

        void DeleteResourceGroup(int id);
        //
        Task<bool> ResourceGroupExists(int resourceGroupId);

        bool UpdateResourceGroupOk(int id, ResourceGroup resourceGroup);

        Task<ResourceGroup> GetResourceGroupByName(string name);

        bool GetResourceGroupByIdAsNoTracking(int id);

    }
}