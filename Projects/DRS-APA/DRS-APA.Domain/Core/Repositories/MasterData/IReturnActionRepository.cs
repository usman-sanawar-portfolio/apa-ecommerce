﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IReturnActionRepository
    {

        Task<IQueryable<ReturnAction>> GetAllReturnActions(bool withPagination);
        Task<ReturnAction> GetReturnActionById(int id);
        Task AddReturnAction(ReturnAction returnAction);
        void DeleteReturnAction(int id);
        Task<bool> ReturnActionExists(int returnActionId);
        bool UpdateReturnActionOk(int id, ReturnAction returnAction);
        bool GetReturnActionByIdAsNoTracking(int id);
        Task<ReturnAction> GetReturnActionByName(string name);
    }
}