﻿using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using System.Collections.Generic;
using System.Linq;

using System.Threading.Tasks;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IAttachmentRepository
    {      
        Task<IEnumerable<Attachment>> GetAllAttachments();

        Task<IQueryable<Attachment>> GetAllAttachments(bool withPagination);

        Task<IQueryable<Attachment>> GetAllAttachments(ISpecification<Attachment> spec);


        Task<Attachment> GetAttachmentById(int id);

        Task AddAttachment(Attachment jobtype);

        void DeleteAttachment(int id);   
        //
        Task<bool> AttachmentExists(int attachment);

         bool UpdateAttachmentOk(int id, Attachment attachment);

        Task<Attachment> GetAttachmentByName(string name);

        bool GetAttachmentByIdAsNoTracking(int id);


    }
}