﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IRepairOptionModelRepository
    {
        Task<IEnumerable<RepairOptionModel>> GetAllRepairOptionModels();

        Task<IQueryable<RepairOptionModel>> GetAllRepairOptionModels(bool withPagination);

        Task<RepairOptionModel> GetRepairOptionModelById(short repairoptionId, int modelId);

        //Task<IEnumerable<RepairOptionModel>> GetRepairOptionModelByModel(int modelId);

        bool GetRepairOptionModelByIdAsNoTracking(short repairoptionId, int modelId);

        Task<RepairOptionModel> GetRepairOptionModelByName(string name);

        Task<bool> RepairOptionModelExists(short repairoptionId, int modelId);

        Task AddRepairOptionModel(RepairOptionModel repairOptionModel);

        bool UpdateRepairOptionModelOk(short repairoptionId, int modelId, RepairOptionModel repairOptionModel);

        void DeleteRepairOptionModel(short repairoptionId, int modelId);
    }
}