﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IWeightRepository
    {      
        Task<IEnumerable<Weight>> GetAllWeights();
        Task<IQueryable<Weight>> GetAllWeights(bool withPagination);
        Task<Weight> GetWeightById(int id);
        Task AddWeight(Weight weight);

        void DeleteWeight(int id);   
        //
        Task<bool> WeightExists(int weightId);

         bool UpdateWeightOk(int id, Weight weight);

        Task<Weight> GetWeightByTitle(string name);

        bool GetWeightByIdAsNoTracking(int id);


    }
}