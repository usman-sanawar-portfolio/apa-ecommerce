﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IGeoZonesRepository
    {      
        Task<IEnumerable<GeoZones>> GetAllGeoZones();
        Task<IQueryable<GeoZones>> GetAllGeoZones(bool withPagination);

        Task<GeoZones> GetGeoZonesById(int id);

        Task AddGeoZones(GeoZones geoZones);

        void DeleteGeoZones(int id);   
        
        Task<bool> GeoZonesExists(int geoZonesId);

         bool UpdateGeoZonesOk(int id, GeoZones geoZones);


        bool GetGeoZonesByIdAsNoTracking(int id);


    }
}