﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IZoneToGeoZoneRepository
    {      
        Task<IEnumerable<ZoneToGeoZone>> GetAllZoneToGeoZone();
        Task<IQueryable<ZoneToGeoZone>> GetAllZoneToGeoZone(bool withPagination);
        Task<ZoneToGeoZone> GetZoneToGeoZoneById(int id);
        Task AddZoneToGeoZone(ZoneToGeoZone ZoneToGeoZone);
        void DeleteZoneToGeoZone(int id);   
        Task<bool> ZoneToGeoZoneExists(int ZoneToGeoZoneId);
         bool UpdateZoneToGeoZoneOk(int id, ZoneToGeoZone ZoneToGeoZone);
        bool GetZoneToGeoZoneByIdAsNoTracking(int id);

    }
}