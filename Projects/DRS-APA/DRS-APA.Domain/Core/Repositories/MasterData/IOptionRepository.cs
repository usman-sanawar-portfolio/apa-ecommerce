﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IOptionRepository
    {
        Task<IEnumerable<Option>> GetAllOptions();
        Task<IQueryable<Option>> GetAllOptions(bool withPagination);
        Task<Option> GetOptionById(int id);
        Task AddOption(Option option);
        void DeleteOption(int id);
        Task<bool> OptionExists(int optionId);
        bool UpdateOptionOk(int id, Option Option);
        bool GetOptionByIdAsNoTracking(int id);
        Task<Option> GetOptionByName(string name);
    }
}