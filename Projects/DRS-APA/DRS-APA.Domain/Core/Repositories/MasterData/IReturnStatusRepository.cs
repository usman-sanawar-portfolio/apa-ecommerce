﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IReturnStatusRepository
    {
        Task<IEnumerable<ReturnStatus>> GetAllReturnStatus();

        Task<IQueryable<ReturnStatus>> GetAllReturnStatus(bool withPagination);
        Task<ReturnStatus> GetReturnStatusById(int id);
        Task AddReturnStatus(ReturnStatus returnStatus);
        void DeleteReturnStatus(int id);
        Task<bool> ReturnStatusExists(int returnStatusId);
        bool UpdateReturnStatusOk(int id, ReturnStatus returnStatus);
        bool GetReturnStatusByIdAsNoTracking(int id);
        Task<ReturnStatus> GetReturnStatusByName(string name);
    }
}