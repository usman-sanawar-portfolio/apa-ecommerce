﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IOrganizationRepository
    {      
        Task<IEnumerable<Organization>> GetAllOrganizations();

        Task<IQueryable<Organization>> GetAllOrganizations(bool withPagination);

        Task<Organization> GetOrganizationByCode(string code);

        Task AddOrganization(Organization organization);

        void DeleteOrganization(short id);   
        
        Task<bool> OrganizationExists(short organizationId);

         bool UpdateOrganizationOk(string code, Organization organization);

        Task<Organization> GetOrganizationByName(string name);

        bool GetOrganizationByIdAsNoTracking(short id);

        bool UpdateStatus(short id, bool status);

    }
}