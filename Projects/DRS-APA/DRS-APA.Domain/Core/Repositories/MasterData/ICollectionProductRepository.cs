﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface ICollectionProductRepository
    {      
        Task<IEnumerable<CollectionProduct>> GetAllCollectionProducts();
        Task<IQueryable<CollectionProduct>> GetAllCollectionProducts(bool withPagination);

        Task<CollectionProduct> GetCollectionProductById(int collectinId, int productId);

        Task AddCollectionProduct(CollectionProduct collectionProduct);
        
        void DeleteCollectionProduct(int collectinId, int productId);         

         bool UpdateCollectionProductOk(int collectinId, int productId, CollectionProduct collectionProduct);

        bool GetCollectionProductByIdAsNoTracking(int collectinId, int productId);


    }
}