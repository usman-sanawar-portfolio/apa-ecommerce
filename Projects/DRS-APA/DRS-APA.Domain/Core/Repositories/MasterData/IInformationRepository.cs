﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IInformationRepository
    {
        Task<IEnumerable<Information>> GetAllInformation();

        Task<IQueryable<Information>> GetAllInformation(bool withPagination);
        Task<Information> GetInformationById(int id);
        Task AddInformation(Information information);
        void DeleteInformation(int id);
        Task<bool> InformationExists(int informationId);
        bool UpdateInformationOk(int id, Information information);
        bool GetInformationByIdAsNoTracking(int id);
        Task<Information> GetInformationByName(string name);
    }
}