﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface ICountryRepository
    {
        Task<IEnumerable<Country>> GetAllCountries();

        Task<IQueryable<Country>> GetAllCountries(bool withPagination);

        Task<Country> GetCountryById(int id);

        Task AddCountry(Country country);

        void DeleteCountry(int id);
        //
        Task<bool> CountryExists(int countryId);

        bool UpdateCountryOk(int id, Country country);

        bool GetCountryByIdAsNoTracking(int id);

        Task<Country> GetCountryByName(string name);

    }
}