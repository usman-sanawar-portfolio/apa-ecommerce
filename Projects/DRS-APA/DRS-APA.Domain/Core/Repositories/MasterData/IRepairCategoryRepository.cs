﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IRepairCategoryRepository
    {      
        Task<IEnumerable<RepairCategory>> GetAllRepairCategories();

        Task<IQueryable<RepairCategory>> GetAllRepairCategories(bool withPagination);

        Task<RepairCategory> GetRepairCategoryById(short id);

        Task AddRepairCategory(RepairCategory repairCategory);

        void DeleteRepairCategory(short id);   
        //
        Task<bool> RepairCategoryExists(short repairCategoryId);

         bool UpdateRepairCategoryOk(short id, RepairCategory repairCategory);

        Task<RepairCategory> GetRepairCategoryByName(string name);

        bool GetRepairCategoryByIdAsNoTracking(short id);


    }
}