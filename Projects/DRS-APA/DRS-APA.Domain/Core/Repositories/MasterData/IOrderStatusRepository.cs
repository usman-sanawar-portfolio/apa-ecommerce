﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IOrderStatusRepository
    {
        Task<IEnumerable<OrderStatus>> GetAllOrderStatus();
      
        Task<IQueryable<OrderStatus>> GetAllOrderStatus(bool withPagination);
        Task<OrderStatus> GetOrderStatusById(int id);
        Task AddOrderStatus(OrderStatus orderStatus);
        void DeleteOrderStatus(int id);
        Task<bool> OrderStatusExists(int orderStatusId);
        bool UpdateOrderStatusOk(int id, OrderStatus orderStatus);
        bool GetOrderStatusByIdAsNoTracking(int id);
        Task<OrderStatus> GetOrderStatusByName(string name);
    }
}