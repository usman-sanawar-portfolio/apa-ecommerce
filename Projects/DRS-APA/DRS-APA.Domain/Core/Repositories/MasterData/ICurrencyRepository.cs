﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface ICurrencyRepository
    {
        Task<IEnumerable<Currency>> GetAllCurrencies();

        Task<IQueryable<Currency>> GetAllCurrencies(bool withPagination);

        Task<Currency> GetCurrencyById(short id);

        Task AddCurrency(Currency currency);

        void DeleteCurrency(short id);
        //
        Task<bool> CurrencyExists(short currencyId);

        bool UpdateCurrencyOk(short id, Currency documentType);

        Task<Currency> GetCurrencyByName(string name);

        bool GetCurrencyByIdAsNoTracking(short id);

    }
}