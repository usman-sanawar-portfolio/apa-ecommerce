﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface ITaxClassRepository
    {
        Task<IEnumerable<TaxClass>> GetAllTaxClass();       
        Task<IQueryable<TaxClass>> GetAllTaxClass(bool withPagination);
        Task<TaxClass> GetTaxClassById(int id);
        Task AddTaxClass(TaxClass taxClass);
        void DeleteTaxClass(int id);
        Task<bool> TaxClassExists(int taxClassId);
        bool UpdateTaxClassOk(int id, TaxClass taxClass);
        bool GetTaxClassByIdAsNoTracking(int id);
        Task<TaxClass> GetTaxClassByName(string name);
    }
}