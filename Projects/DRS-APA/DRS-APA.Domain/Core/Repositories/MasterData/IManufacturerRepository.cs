﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IManufacturerRepository
    {
        Task<IEnumerable<Manufacturer>> GetAllManufacturers();
        Task<IQueryable<Manufacturer>> GetAllManufacturers(bool withPagination);
        Task<Manufacturer> GetManufacturerById(int id);
        Task AddManufacturer(Manufacturer zone);
        void DeleteManufacturer(int id);
        Task<bool> ManufacturerExists(int zoneId);
        bool UpdateManufacturerOk(int id, Manufacturer zone);
        bool GetManufacturerByIdAsNoTracking(int id);
    }
}