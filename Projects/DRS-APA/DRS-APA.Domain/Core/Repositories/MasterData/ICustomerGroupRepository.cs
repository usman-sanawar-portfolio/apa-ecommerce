﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface ICustomerGroupRepository
    {      
        Task<IEnumerable<CustomerGroup>> GetAllCustomerGroups();

        Task<IQueryable<CustomerGroup>> GetAllCustomerGroups(bool withPagination);

        Task<CustomerGroup> GetCustomerGroupById(int id);

        Task AddCustomerGroup(CustomerGroup customer);

        void DeleteCustomerGroup(int id);   
        //
        Task<bool> CustomerGroupExists(int customerId);

         bool UpdateCustomerGroupOk(int id, CustomerGroup customer);


        bool GetCustomerGroupByIdAsNoTracking(int id);


    }
}