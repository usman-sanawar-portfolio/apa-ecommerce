﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface ILocationRepository
    {      
        Task<IEnumerable<Location>> GetAllLocations();

        Task<IQueryable<Location>> GetAllLocations(bool withPagination);

        Task<Location> GetLocationById(short id);

        Task AddLocation(Location location);

        void DeleteLocation(short id);   
        //
        Task<bool> LocationExists(short locationId);

         bool UpdateLocationOk(short id, Location location);

        Task<Location> GetLocationByName(string name);

        bool GetLocationByIdAsNoTracking(short id);

        bool UpdateStatus(short id, bool status);

    }
}