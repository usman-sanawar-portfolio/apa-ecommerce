﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Repair;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IResourceRepository
    {
        Task<IEnumerable<Resource>> GetAllResources();

        Task<IQueryable<Resource>> GetAllResources(bool withPagination);

        Task<Resource> GetResourceById(int id);

        Task AddResource(Resource resource);

        void DeleteResource(int id);
        //
        Task<bool> ResourceExists(int resourceId);

        bool UpdateResourceOk(int id, Resource documentType);

        Task<Resource> GetResourceByName(string name);

        bool GetResourceByIdAsNoTracking(int id);


    }
}