﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IStoreRepository
    {
        Task<IEnumerable<Store>> GetAllStores();
        Task<IQueryable<Store>> GetAllStores(bool withPagination);
        Task<Store> GetStoreById(int id);
        Task AddStore(Store store);
        void DeleteStore(int id);
        Task<bool> StoreExists(int storeId);
        bool UpdateStoreOk(int id, Store store);
        bool GetStoreByIdAsNoTracking(int id);
        Task<Store> GetStoreByName(string name);
    }
}