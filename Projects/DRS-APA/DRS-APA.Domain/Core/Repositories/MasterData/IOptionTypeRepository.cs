﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IOptionTypeRepository
    {
        Task<IEnumerable<OptionType>> GetAllOptionTypes();
        Task<IQueryable<OptionType>> GetAllOptionTypes(bool withPagination);
        Task<OptionType> GetOptionTypeById(int id);
        Task AddOptionType(OptionType optionType);
        void DeleteOptionType(int id);
        Task<bool> OptionTypeExists(int optionTypeId);
        bool UpdateOptionTypeOk(int id, OptionType optionType);
        bool GetOptionTypeByIdAsNoTracking(int id);
        Task<OptionType> GetOptionTypeByName(string name);
    }
}