﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IPostcodeDeliveryMethodRepository
    {
        Task<IEnumerable<PostcodeDeliveryMethod>> GetAllPostcodeDeliveryMethods();

        Task<IQueryable<PostcodeDeliveryMethod>> GetAllPostcodeDeliveryMethods(bool withPagination);

        Task<PostcodeDeliveryMethod> GetPostcodeDeliveryMethodById(int postcodeId, short deliveryMethodId);

        bool GetPostcodeDeliveryMethodByIdAsNoTracking(int postcodeId, short deliveryMethodId);

        Task<bool> PostcodeDeliveryMethodExists(int postcodeId, short deliveryMethodId);

        Task AddPostcodeDeliveryMethod(PostcodeDeliveryMethod postcodeDeliveryMethod);

        bool UpdatePostcodeDeliveryMethodOk(int postcodeId, short deliveryMethodId, PostcodeDeliveryMethod postcodeDeliveryMethod);

        void DeletePostcodeDeliveryMethod(int postcodeId, short deliveryMethodId);

        Task<IEnumerable<PostcodeDeliveryMethod>> GetAllPostcodeDeliveryMethodsByPostcode(int postcodeId);
    }
}
