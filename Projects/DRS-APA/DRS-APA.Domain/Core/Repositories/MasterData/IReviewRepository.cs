﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IReviewRepository
    {
        Task<IEnumerable<Review>> GetAllReviews();
        Task<IQueryable<Review>> GetAllReviews(bool withPagination);
        Task<Review> GetReviewById(int id);
        Task AddReview(Review review);
        void DeleteReview(int id);
        Task<bool> ReviewExists(int reviewId);
        bool UpdateReviewOk(int id, Review review);
        bool GetReviewByIdAsNoTracking(int id);

        Task<int> GetTotalReviewCountByProductId(int? id);
        Task<int> GetTotalReviewRatingByProductId(int? id);
    }
}