﻿using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Domain.Core.Repositories.MasterData
{
    public interface IProductOptionCombinationRepository
    {
        Task<IEnumerable<Option>> GetProductOptionsOnProductId(int productId);
        void DeleteProductOptionCombination(int id);
        Task<ProductOptionCombination> GetProductOptionCombinationById(int id);
        Task AddProductOptionCombination(ProductOptionCombination ProductOptionCombination);
        Task<bool> ProductOptionCombinationExists(int ProductOptionCombinationId);
        bool UpdateProductOptionCombinationOk(int id, ProductOptionCombination ProductOptionCombination);
        bool GetProductOptionCombinationByIdAsNoTracking(int id);
    }

}
