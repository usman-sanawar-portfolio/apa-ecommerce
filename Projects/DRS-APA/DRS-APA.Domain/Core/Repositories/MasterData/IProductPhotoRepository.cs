﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Core.Domain;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories
{
    public interface IProductPhotoRepository
    {      
        Task<IEnumerable<ProductPhoto>> GetAllProductPhotos();

        Task<IEnumerable<ProductPhoto>> GetAllProductPhotosByProduct(int productId);

        Task<IQueryable<ProductPhoto>> GetAllProductPhotos(bool withPagination);

        Task<ProductPhoto> GetProductPhotoById(int id);

        Task AddProductPhoto(ProductPhoto productPhoto);

        void DeleteProductPhoto(int id);   
        //
        Task<bool> ProductPhotoExists(int productPhotoId);

         bool UpdateProductPhotoOk(int id, ProductPhoto productPhoto);

        Task<ProductPhoto> GetProductPhotoByName(string name);

        bool GetProductPhotoByIdAsNoTracking(int id);


    }
}