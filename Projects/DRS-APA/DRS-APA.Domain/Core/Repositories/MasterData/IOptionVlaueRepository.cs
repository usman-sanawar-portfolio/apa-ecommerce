﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IOptionValueRepository
    {
        Task<IEnumerable<OptionValue>> GetAllOptionValues();
        Task<IQueryable<OptionValue>> GetAllOptionValues(bool withPagination);
        Task<OptionValue> GetOptionValueById(int id);
        Task AddOptionValue(OptionValue optionValue);
        void DeleteOptionValue(int id);
        Task<bool> OptionValueExists(int optionValueId);
        bool UpdateOptionValueOk(int id, OptionValue optionValue);
        bool GetOptionValueByIdAsNoTracking(int id);
        Task<OptionValue> GetOptionValueByName(string name);
    }
}