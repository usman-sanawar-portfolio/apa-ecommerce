﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IReturnReasonRepository
    {
        Task<IEnumerable<ReturnReason>> GetAllReturnReasons();
        Task<IQueryable<ReturnReason>> GetAllReturnReasons(bool withPagination);
        Task<ReturnReason> GetReturnReasonById(int id);
        Task AddReturnReason(ReturnReason returnReason);
        void DeleteReturnReason(int id);
        Task<bool> ReturnReasonExists(int returnReasonId);
        bool UpdateReturnReasonOk(int id, ReturnReason returnReason);
        bool GetReturnReasonByIdAsNoTracking(int id);
        Task<ReturnReason> GetReturnReasonByName(string name);
    }
}