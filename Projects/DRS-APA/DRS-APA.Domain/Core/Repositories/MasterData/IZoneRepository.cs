﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IZoneRepository
    {
        Task<IEnumerable<Zone>> GetAllZones();   
        Task<IQueryable<Zone>> GetAllZones(bool withPagination);
        Task<Zone> GetZoneById(int id);
        Task AddZone(Zone zone);
        void DeleteZone(int id);
        Task<bool> ZoneExists(int zoneId);
        bool UpdateZoneOk(int id, Zone zone);
        bool GetZoneByIdAsNoTracking(int id);
        Task<Zone> GetZoneByName(string name);
    }
}