﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IProductCategoriesJunctionRepository
    {
        Task<IEnumerable<ProductCategoriesJunction>> GetAllProductCategoriesJunctions();   
        Task<IQueryable<ProductCategoriesJunction>> GetAllProductCategoriesJunctions(bool withPagination);
        Task<ProductCategoriesJunction> GetProductCategoriesJunctionById(int id);
        Task AddProductCategoriesJunction(ProductCategoriesJunction ProductCategoriesJunction);
        void DeleteProductCategoriesJunction(int productId, short productCategoryId);
        Task<bool> ProductCategoriesJunctionExists(int ProductCategoriesJunctionId);
        bool UpdateProductCategoriesJunctionOk(int id, ProductCategoriesJunction ProductCategoriesJunction);
        bool GetProductCategoriesJunctionByIdAsNoTracking(int productId, short productCategoryId);
    }
}