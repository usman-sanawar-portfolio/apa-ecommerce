using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface ICustomerRepository
    {
        Task<IEnumerable<Customer>> GetAllCustomers();
        Task<IQueryable<Customer>> GetAllCustomers(bool withPagination);
        Task<Customer> GetCustomerById(int id);
        Task<Customer> GetCustomerByIdDetailed(int id);
        Task<Customer> GetCustomerWithSaleOrderById(int id);
        Task<Customer> GetCustomerWithOrderById(int id);
        Task AddCustomer(Customer customer);
        void DeleteCustomer(int id);
        Task<bool> CustomerExists(int customerId);
        bool UpdateCustomerOk(int id, Customer customer);
        bool GetCustomerByIdAsNoTracking(int id);
        // Methods for CustomerAddress
        Task AddCustomerAddress(CustomerAddress customerAddress);
        void DeleteCustomerAddress(int id);
        bool UpdateCustomerAddressOk(int id, CustomerAddress customerAddress);
        bool UpdateCustomerNewsLetterSub(int customerId, bool newsletter);

        Task<CustomerAddress> GetCustomerAddressById(int id);
        bool GetCustomerAddressByIdAsNoTracking(int id);
        bool ActivateCustomer(string ActivationCode);
        bool ChangeCustomerPassword(int id, string ActivationCode, Customer customer);
        bool VerifyPassword(int customerId, string oldPassword);
        bool AssignResetToken(string resetToken, string email);
        bool ChangePasswordAfterEmail(Customer customer);
        bool ChangePasswordByAdmin(Customer customer);
        bool CheckEmailExistCustomer(string email);

  }
}
