﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IProductCategoryRepository
    {
        Task<IEnumerable<ProductCategory>> GetAllProductCategories();
        Task<IQueryable<ProductCategory>> GetAllProductCategories(bool withPagination);
        Task<IEnumerable<ProductCategory>> GetAllProductCategoriesWithSubCategories();
        Task<ProductCategory> GetProductCategoryById(short id);
        Task AddProductCategory(ProductCategory productCategory);
        void DeleteProductCategory(short id);
        Task<bool> ProductCategoryExists(short productCategoryId);
        bool UpdateProductCategoryOk(short id, ProductCategory productCategory);
        Task<ProductCategory> GetProductCategoryByName(string name);
        bool GetProductCategoryByIdAsNoTracking(short id);
        Task<IQueryable<ProductCategory>> GetAllProductCategoriesWithoutChiledCategoryies(short categoryId);
        Task<IEnumerable<CategoryCode>> GetAllProductCategoryCodes();
        Task<IEnumerable<ProductCategory>> GetProductCategoriesOnProductId(int ProductId);
    }
}