﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface ITaxRateRepository
    {
        Task<IEnumerable<TaxRate>> GetAllTaxRates();
   
        Task<IQueryable<TaxRate>> GetAllTaxRates(bool withPagination);
        Task<TaxRate> GetTaxRateById(int id);
        Task AddTaxRate(TaxRate taxRate);
        void DeleteTaxRate(int id);
        Task<bool> TaxRateExists(int taxRateId);
        bool UpdateTaxRateOk(int id, TaxRate taxRate);
        bool GetTaxRateByIdAsNoTracking(int id);
        Task<TaxRate> GetTaxRateByName(string name);


        // TaxRate CustomerGroup Method
        Task<TaxRatesCustomerGroups> GetTaxRatesCustomerGroupsById(int customerGroupId, int taxRateId);
         bool GetTaxRatesCustomerGroupsByIdAsNoTracking(int customerGroupId, int taxRateId);
        void DeleteTaxRatesCustomerGroups(int customerGroupid, int taxRateId);
     Task AddTaxRatesCustomerGroups(TaxRatesCustomerGroups taxRatesCustomerGroups);
    }
}