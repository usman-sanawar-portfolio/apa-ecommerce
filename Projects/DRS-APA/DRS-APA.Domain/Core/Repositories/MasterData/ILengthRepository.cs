﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface ILengthRepository
    {      
        Task<IEnumerable<Length>> GetAllLengths();


        Task<IQueryable<Length>> GetAllLengths(bool withPagination);

        Task<Length> GetLengthById(int id);

        Task AddLength(Length length);

        void DeleteLength(int id);   
        //
        Task<bool> LengthExists(int lengthId);

         bool UpdateLengthOk(int id, Length length);

        Task<Length> GetLengthByTitle(string name);

        bool GetLengthByIdAsNoTracking(int id);


    }
}