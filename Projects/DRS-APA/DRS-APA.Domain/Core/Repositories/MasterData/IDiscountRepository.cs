﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IDiscountRepository
    {      
        Task<IEnumerable<Discount>> GetAllDiscounts();

        Task<IQueryable<Discount>> GetAllDiscounts(bool withPagination);

        Task<Discount> GetDiscountById(short id);

        Task AddDiscount(Discount discount);

        void DeleteDiscount(short id);   
        //
        Task<bool> DiscountExists(short discountId);

         bool UpdateDiscountOk(short id, Discount discount);

        Task<Discount> GetDiscountByName(string name);

        bool GetDiscountByIdAsNoTracking(short id);


    }
}