﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Core.Domain;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos.MasterData;

namespace DRS.APA.Domain.Core.Repositories
{
    public interface IRepairOptionRepository
    {      
        Task<IEnumerable<RepairOption>> GetAllRepairOptions();

        Task<IQueryable<RepairOption>> GetAllRepairOptions(bool withPagination);

        Task<RepairOption> GetRepairOptionById(short id);

        //Task<IEnumerable<RepairOption>> GetRepairOptionsByModel(int modelId);

        Task AddRepairOption(RepairOption repairOption);

        void DeleteRepairOption(short id);   
        //
        Task<bool> RepairOptionExists(short repairOptionId);

        Task<Product> GetProductByType();

         bool UpdateRepairOptionOk(short id, RepairOption repairOption);

        Task<RepairOption> GetRepairOptionByName(string name);

        bool GetRepairOptionByIdAsNoTracking(short id);


    }
}