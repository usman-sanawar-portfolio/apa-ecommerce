﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.Repair;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IAccountSettingRepository
    {      
        Task<IEnumerable<AccountSetting>> GetAllAccountSettings();

        Task<IQueryable<AccountSetting>> GetAllAccountSettings(bool withPagination);

        Task<AccountSetting> GetAccountSettingById(int id);

        Task AddAccountSetting(AccountSetting accountSetting);

        void DeleteAccountSetting(int id);   
        //
        Task<bool> AccountSettingExists(int accountSettingId);

         bool UpdateAccountSettingOk(int id, AccountSetting accountSetting);

        Task<AccountSetting> GetAccountSettingByName(string name);

        bool GetAccountSettingByIdAsNoTracking(int id);


    }
}