﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IPostcodeRepository
    {
        Task<IEnumerable<Postcode>> GetAllPostcodes();
        Task<IQueryable<Postcode>> GetAllPostcodes(bool withPagination);
        Task<Postcode> GetPostcodeById(int id);
        Task<IEnumerable<PostcodeDeliveryMethod>> GetPostcodesByPostcode(int postcodeId);
        bool GetPostcodeByIdAsNoTracking(int id);
        Task<Postcode> GetPostcodeByName(string name);
        Task<Product> GetProductByType();
        Task<bool> PostcodeExists(int postcodeId);
        Task AddPostcode(Postcode postcode);
        bool UpdatePostcodeOk(int id, Postcode postcode);
        void DeletePostcode(int id);
    }
}
