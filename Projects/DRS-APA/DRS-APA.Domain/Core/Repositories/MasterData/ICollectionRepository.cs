﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface ICollectionRepository
    {      
        Task<IEnumerable<Collection>> GetAllCollections();

        Task<IQueryable<Collection>> GetAllCollections(bool withPagination);

        Task<Collection> GetCollectionById(int id);

        Task AddCollection(Collection collection);

        void DeleteCollection(int id);   
        //
        Task<bool> CollectionExists(int collectionId);

         bool UpdateCollectionOk(int id, Collection collection);

        Task<Collection> GetCollectionByName(string name);

        bool GetCollectionByIdAsNoTracking(int id);


    }
}