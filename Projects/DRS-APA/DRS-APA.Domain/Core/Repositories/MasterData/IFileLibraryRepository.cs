﻿using DRS.APA.Domain.Core.Domain.MasterData;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Domain.Core.Repositories
{
    public interface IFileLibraryRepository
    {      
        Task<IEnumerable<FileLibrary>> GetAllFileLibrary();

        Task<IQueryable<FileLibrary>> GetAllFileLibrary(bool withPagination);

        Task<FileLibrary> GetFileLibraryById(short id);

        Task AddFileLibrary(FileLibrary fileLibrary);

        void DeleteFileLibrary(short id);   
        //
        Task<bool> FileLibraryExists(short fileLibraryId);

         bool UpdateFileLibraryOk(short id, FileLibrary fileLibrary);

        Task<FileLibrary> GetFileLibraryByName(string name);

        bool GetFileLibraryByIdAsNoTracking(short id);

        bool UpdateStatus(short id, bool status);

    }
}