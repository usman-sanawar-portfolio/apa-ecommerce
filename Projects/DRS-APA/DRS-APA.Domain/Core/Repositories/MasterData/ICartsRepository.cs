﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using ShopApp.Core.Models;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface ICartsRepository
    {

        Task<IEnumerable<NewCart>> GetAllCarts();

        Task<IQueryable<NewCart>> GetAllCarts(bool withPagination);

        Task<NewCart> GetCartById(int id);

        Task AddCart(NewCart newCart);

        bool UpdateCartOk(int id, NewCart newCart);

        bool GetCartByIdAsNoTracking(int id);

        void DeleteCart(int id);

        bool GetCartItemByIdAsNoTracking(int id);

        void DeleteCartItem(int id);
        
        Task<RevisitedCart> StoreCartWithInitialProduct(NewCart newCart);

       // RevisitedCart StoreCartWithInitialProduct(NewCart newCart);

        //void CheckForExistingCustomer(NewCart newCart);

        RevisitedCart RetrieveCart(int cartId);

        RevisitedCart RetrieveCart(string cartCookie);

        void StoreNewCartItem(CartItem item);

        void UpdateItemsForExistingCart(RevisitedCart cart);

        void DeleteCart(IEnumerable<CartItem> cartitems);

        Task<IEnumerable<CartItem>> GetCartItems(string coolkievalue);


        void UpdateCart(int cartitemId, int quantity);
    }
}
;