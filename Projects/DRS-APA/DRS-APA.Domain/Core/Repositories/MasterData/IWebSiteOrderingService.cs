﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IWebSiteOrderingService
    {
        Task<RevisitedCart> ItemSelected(int productId, int quantity,
            decimal displayedPrice, string sourceUrl,
            string memberCookie, int cartId);

        Task<RevisitedCart> InitializeCart(int productId, int quantity,
            decimal displayedPrice, string sourceUrl,
            string memberCookie);

        RevisitedCart AddItemToCart(int productId, int quantity,
            decimal displayedPrice, int cartId, string cookievalue);

        RevisitedCart RetrieveCart(string cartCookie);
    }
}
