﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IDiscountCustomerGroupRepository
    {      
        Task<IEnumerable<DiscountCustomerGroup>> GetAllDiscountCustomerGroups();
        Task<IQueryable<DiscountCustomerGroup>> GetAllDiscountCustomerGroups(bool withPagination);

        Task<DiscountCustomerGroup> GetDiscountCustomerGroupById(short discountId, int customerGroupId);

        Task AddDiscountCustomerGroup(DiscountCustomerGroup discountCustomerGroup);

        void DeleteDiscountCustomerGroup(short discountId, int customerGroupId);         

         bool UpdateDiscountCustomerGroupOk(short discountId, int customerGroupId, DiscountCustomerGroup discountCustomerGroup);

        bool GetDiscountCustomerGroupByIdAsNoTracking(short discountId, int customerGroupId);


    }
}