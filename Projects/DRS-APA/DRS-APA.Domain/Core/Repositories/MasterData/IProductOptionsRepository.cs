﻿using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Domain.Core.Repositories.MasterData
{
    public interface IProductOptionsRepository
    {
        Task<ProductOptions> GetProductOptionsById(int id);
        Task AddProductOptions(ProductOptions ProductOptions);
        void DeleteProductOptions(int id);
        Task<bool> ProductOptionsExists(int ProductOptionsId);
        bool UpdateProductOptionsOk(int id, ProductOptions ProductOptions);
        bool GetProductOptionsByIdAsNoTracking(int id);
    }
}
