﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.Repair;

namespace DRS.APA.Domain.Core.Repositories.MasterData
{
    public interface IAccountRepository
    {      
        Task<IEnumerable<Account>> GetAllAccounts();

        Task<IQueryable<Account>> GetAllAccounts(bool withPagination);

        Task<Account> GetAccountById(int id);

        Task AddAccount(Account account);

        void DeleteAccount(int id);   
        //
        Task<bool> AccountExists(int accountId);

         bool UpdateAccountOk(int id, Account account);

        Task<IEnumerable<Account>> GetAccountByName(string name);

        bool GetAccountByIdAsNoTracking(int id);


    }
}