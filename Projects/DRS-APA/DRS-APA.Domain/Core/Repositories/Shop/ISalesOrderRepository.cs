﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Shop;
using DRS.APA.Domain.Core.Interfaces;

namespace DRS.APA.Domain.Core.Repositories.Shop
{
    public interface ISalesOrderRepository
    {
        Task<IEnumerable<SalesOrder>> GetAllSalesOrders();

        Task<IQueryable<SalesOrder>> GetAllSalesOrders(bool withPagination);

        Task<IQueryable<SalesOrder>> GetAllSalesOrders(ISpecification<SalesOrder> spec);

        Task<IEnumerable<LineItem>> GetAllSalesOrdersByCategory(int categoryId);

        Task<IEnumerable<SalesOrder>> GetAllSalesOrdersByCustomer(int customerId);

        Task<IEnumerable<LineItem>> GetAllLineItemsBySalesOrder(int salesOrderId);

        Task<SalesOrder> GetSalesOrderById(int id);

        Task<LineItem> GetLineItemById(int id);

        bool GetSalesOrderByIdAsNoTracking(int id);

        bool GetItemLineByIdAsNoTracking(int id);

        Task AddSalesOrder(SalesOrder salesOrder);

        Task AddLineItem(LineItem lineItem);

        bool UpdateSalesOrderOk(int id, SalesOrder salesOrder);

        bool UpdateLineItemOk(int id, LineItem lineItem);

        void DeleteSalesOrder(int id);

        void DeleteLineItem(int id);
    }
}