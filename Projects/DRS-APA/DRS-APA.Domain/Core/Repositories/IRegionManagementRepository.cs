﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ServiceManagement.Domain.Core.Domain;

namespace ServiceManagement.Domain.Core.Repositories
{
    public interface IRegionManagementRepository
    {
        Task<Region> SearchByName(string name);
        Task<IEnumerable<Region>> GetAllRegions();
        Task<Region> GetRegionById(int id);
        Task AddRegion(Region region);
        void DeleteRegion(int id);
        Task<Region> GetRegion(int regionId, bool includeCountries = false);
        Task<IEnumerable<Region>> GetRegions(bool includeCountries = false);
        //
        Task<bool> RegionExists(int regionId);
        void UpdateRegion(int id,Region region);
       // Task<IEnumerable<Country>> GetCountries(int regionId);
       // Task<IEnumerable<Country>> GetCountries(int regionId, IEnumerable<int> showIds);
       // Task AddCountry(int regionId, Country country);

        //Task<IEnumerable<SelectList>> GetList();

        //FluentModel
        //using (FluentModel dbContext = new FluentModel())
        //    {
        //       Customer customer = dbContext.Customers.FirstOrDefault();
        //        customer.ContactName = customer.ContactName + "Changed";

        //       IList<Customer> dirtyCustomers = dbContext.GetChanges().GetUpdates<Customer>();
        //        Debug.Assert(dirtyCustomers.Count == 1);

        //       dbContext.FlushChanges();

        //       dirtyCustomers = dbContext.GetChanges().GetUpdates<Customer>();
        //       Debug.Assert(dirtyCustomers.Count == 0);
        //    }

    }
}