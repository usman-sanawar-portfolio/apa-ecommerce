﻿using System;
using System.ComponentModel;

namespace DRSAPA.Domain.Core.Domain.Common
{
    public interface IAuditableEntity
    {
        DateTime? CreatedOn { get; set; }
        string CreatedBy { get; set; }
        DateTime? UpdatedOn { get; set; }
        string UpdatedBy { get; set; }
        bool? IsActive { get; set; }
    }

    public abstract class AuditableEntity : IAuditableEntity
    {
        public DateTime? CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public string UpdatedBy { get; set; }
        [DefaultValue(true)]
        public bool? IsActive { get; set; }
    }
}