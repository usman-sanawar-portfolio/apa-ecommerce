﻿using System;
using System.Collections.Generic;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos.MasterData;

namespace DRS.APA.Domain.Core.Interfaces
{
    public interface INewCart
    {
        string CartCookie { get; }
        int CartId { get; }
        ICollection<CartItem> CartItems { get; }
        string CustomerCookie { get; }

        string CustomerId { get; }

        //ApplicationUser Customer { get; set; }
        DateTime Expires { get; }
        DateTime Initialized { get; }
        string SourceUrl { get; }

        void CustomerFound(string customerId);

  
    }
}