﻿

using DRS.APA.Domain.Core.Shared;

namespace DRS.APA.Domain.Core.Interfaces
{
  public interface IStateObject
  {
    ObjectState State { get; }
  }
}