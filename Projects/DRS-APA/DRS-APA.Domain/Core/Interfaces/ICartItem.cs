﻿using System;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Shared;


namespace DRS.APA.Domain.Core.Interfaces
{
    public interface ICartItem
    {
        string CartCookie { get; }
        int CartId { get; set; }
        decimal CurrentPrice { get; }
        int ProductId { get; }
        Product Product { get; set; }
        int Quantity { get; }
        DateTime? SelectedDateTime { get; }
        ObjectState State { get; }

        void UpdateQuantity(int newQuantity);
    }
}