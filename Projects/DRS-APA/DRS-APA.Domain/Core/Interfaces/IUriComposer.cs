﻿namespace DRS.APA.Domain.Core.Interfaces
{
    public interface IUriComposer
    {
        string ComposePicUri(string uriTemplate);
    }
}
