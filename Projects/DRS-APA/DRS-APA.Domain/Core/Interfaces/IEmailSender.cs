﻿using System.Threading.Tasks;

namespace DRS.APA.Domain.Core.Interfaces
{

    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
