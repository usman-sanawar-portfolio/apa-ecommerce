﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;


namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    
    public class CartsRepository: ICartsRepository
    {
        private readonly DRSAPAContext _context;
        //ReferenceContext refContext
        public CartsRepository(DRSAPAContext context)
        {
            _context = context;
            //_refContext = refContext;
        }

        public async Task<IEnumerable<NewCart>> GetAllCarts()
        {
            return await _context.Carts
                    .ToListAsync();
        }
        public async Task<IQueryable<NewCart>> GetAllCarts(bool withPagination)
        {
            return _context.Carts;
            //return await Task.Run(() => { _context.Models; });
        }
        public async Task<NewCart> GetCartById(int id)
        {
            return await _context.Carts.FindAsync(id);
        }
        public async Task AddCart(NewCart newCart)
        {
                await _context.AddAsync(newCart);
        }

        public bool UpdateCartOk(int id, NewCart newCart)
        {
            //var entity = _context.Models.AsNoTracking().FirstOrDefault(d => d.ModelId==id);
            var entity = _context.Carts.Any(d => d.CartId == id);
            if (entity == false)
            {
                return false;
                //return NotFound();
            }
            if (newCart.CartId != id)
            {
                newCart.CartId = id;

            }
            else { }
            // set original entity state to detached
            // _ctx.Entry(existingEG).State = EntityState.Detached;

            _context.Carts.Attach(newCart);
            _context.Entry(newCart).State = EntityState.Modified;
            return true;
        }
        public bool GetCartByIdAsNoTracking(int id)
        {
            return _context.Carts.Any(e => e.CartId == id);
        }

        public void DeleteCart(int id)
        {
            var cart = new NewCart { CartId = id };
            _context.Carts.Attach(cart);
            _context.Entry(cart).State = EntityState.Deleted;
        }


        public bool GetCartItemByIdAsNoTracking(int id)
        {
            return _context.CartItems.Any(e => e.CartId == id);
        }

        public void DeleteCartItem(int id)
        {
            var cartitem = _context.CartItems.FirstOrDefault(i => i.CartItemId == id);
            _context.CartItems.Attach(cartitem ?? throw new InvalidOperationException());
            _context.Entry(cartitem).State = EntityState.Deleted;
        }



        public async Task<RevisitedCart> StoreCartWithInitialProduct(NewCart newCart)
        {
            if (newCart.CartItems.Count != 1) return null;
            ////  CheckForExistingCustomer(newCart);
             _context.Carts.Add(newCart);
             await _context.SaveChangesAsync();
            var cart =  RevisitedCart.CreateWithItems(newCart.CartId, newCart.CartItems);
            cart.SetCookieData(newCart.CartCookie, newCart.Expires);
            return  cart;
        }

        //private void CheckForExistingCustomer(NewCart newCart)
        //{
        //    if (newCart.CustomerCookie != null)
        //    {
        //        var customerId = _context.Users.AsNoTracking()
        //         .Where(c => c.CustomerCookie == newCart.CustomerCookie)
        //         .Select(c => c.Id).FirstOrDefault();
        //        if (customerId != null)
        //        {
        //            newCart.CustomerFound(customerId);
        //        }
        //    }
        //}

        public RevisitedCart RetrieveCart(int cartId)
        {
            var cart = _context.Carts.AsNoTracking().Where(c => c.CartId == cartId)
                .Select(c => new { c.CartId, c.CartItems }).SingleOrDefault();
            if (cart != null) return RevisitedCart.CreateWithItems(cart.CartId, cart.CartItems);
            return RevisitedCart.Create(cartId);
        }

        public void DeleteCart(IEnumerable<CartItem> cartitems)
        {
           // _context.Configuration.AutoDetectChangesEnabled = false;
            int cartid = 0;
            foreach (var item in cartitems)
            {
                
                _context.CartItems.Attach(item);
                _context.CartItems.Remove(item);
                cartid = item.CartId;
            }
           
            //_context.Carts.Remove(cart);
            //_context.SaveChanges();

            var cart = _context.Carts.AsNoTracking().SingleOrDefault(c => c.CartId == cartid);
            
            _context.Carts.Attach(cart);
            _context.Carts.Remove(cart);
            _context.SaveChanges();
            _context.ChangeTracker.DetectChanges();
            _context.FixState();
           
        }
        public void UpdateCart(int cartitemId, int quantity)
        {
           //_context.Configuration.AutoDetectChangesEnabled = false;
            var cartitem = _context.CartItems.SingleOrDefault(d => d.CartItemId == cartitemId);
            cartitem.UpdateQuantity(quantity);
            _context.CartItems.Attach(cartitem);
            //_context.SaveChanges();
            _context.ChangeTracker.DetectChanges();
            _context.FixState();

        }


        public RevisitedCart RetrieveCart(string cartCookie)
        {
            var cart = _context.Carts.AsNoTracking()
              .Where(c => c.CartCookie == cartCookie)
              .Select(c => new { c.CartId, c.CartItems }).SingleOrDefault();
            if (cart != null) return RevisitedCart.CreateWithItems(cart.CartId, cart.CartItems);
           ////////   CookieUtilities.RemoveCartCookie(cartCookie);    remove session
            return null;
        }

        public void StoreNewCartItem(CartItem item)
        {
            //item should be valid before we get here but one last check
            if (item.CartId == 0)
                throw new InvalidDataException("Cart Item is not associated with a cart",
                  new InvalidDataException("CartId is 0"));
            _context.CartItems.Add(item);
            _context.SaveChanges();
        }

        public void UpdateItemsForExistingCart(RevisitedCart cart)
        {
           // _context.Configuration.AutoDetectChangesEnabled = false;
            foreach (var item in cart.CartItems)
            {
                _context.CartItems.Attach(item);
            }
            _context.ChangeTracker.DetectChanges();
            _context.FixState();
            _context.SaveChanges();
        }

        public async Task<IEnumerable<CartItem>> GetCartItems(string coolkievalue)
        {

            var cartitems = await _context.CartItems
                .Where(c => c.Cart.CartCookie == coolkievalue)
                .Include(n => n.Product)
                //.Include(c => c.Cart)
                .ToListAsync();
            
            return cartitems;

        }
    }
}
