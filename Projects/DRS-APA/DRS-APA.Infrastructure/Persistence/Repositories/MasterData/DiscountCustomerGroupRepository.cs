﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    public class DiscountCustomerGroupRepository : IDiscountCustomerGroupRepository
    {
        private DRSAPAContext _context;

        public DiscountCustomerGroupRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<DiscountCustomerGroup>> GetAllDiscountCustomerGroups()
        {
            return await  _context.DiscountCustomerGroups
                   .Include(x => x.Discount)
                .Include(u => u.CustomerGroup)
                .ToListAsync();
        }
        public async Task<IQueryable<DiscountCustomerGroup>> GetAllDiscountCustomerGroups(bool withPagination)
        {
            return _context.DiscountCustomerGroups
                .Include(x=>x.Discount)
                .Include(u=>u.CustomerGroup);
            //return await Task.Run(() => { _context.DiscountCustomerGroups; });
        }

        public async Task<DiscountCustomerGroup> GetDiscountCustomerGroupById(short discountId, int customerGroupId)
        {
            return await  _context.DiscountCustomerGroups
                   .Include(x => x.Discount)
                .Include(u => u.CustomerGroup).FirstOrDefaultAsync(x=>x.DiscountId==discountId && x.CustomerGroupId == customerGroupId);
        }

       
        public bool GetDiscountCustomerGroupByIdAsNoTracking(short discountId, int customerGroupId)
        {
            return _context.DiscountCustomerGroups.Any(e => e.DiscountId == discountId && e.CustomerGroupId==customerGroupId);
        }
        

       public async Task AddDiscountCustomerGroup(DiscountCustomerGroup discountCustomerGroup)
        {
            if (!await _context.DiscountCustomerGroups.Where(a => a.DiscountId == discountCustomerGroup.DiscountId && a.CustomerGroupId == discountCustomerGroup.CustomerGroupId).AnyAsync())
            {
                await _context.AddAsync(discountCustomerGroup);
            }
            else
            {
                throw new Exception("Discount CustomerGroup  Already Exists");
            }
        }

        // disable async warning - no code 
        public bool UpdateDiscountCustomerGroupOk(short discountId, int customerGroupId, DiscountCustomerGroup discountCustomerGroup)
        {
            var sameName = _context.DiscountCustomerGroups.AsNoTracking().FirstOrDefault(d => d.DiscountId != discountCustomerGroup.DiscountId && d.CustomerGroupId == discountCustomerGroup.CustomerGroupId);
            if (sameName != null)
            {
                throw new Exception("DiscountCustomerGroup Name Already Exists");
            }
            var entity = _context.DiscountCustomerGroups.Any(d => d.DiscountId == discountId && d.CustomerGroupId== customerGroupId);
            if (entity == false)
            {                
                return false;
                //return NotFound();
            }
      

            _context.DiscountCustomerGroups.Attach(discountCustomerGroup);
            _context.Entry(discountCustomerGroup).State = EntityState.Modified;
            _context.Entry(discountCustomerGroup).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(discountCustomerGroup).Property(x => x.CreatedOn).IsModified = false;

            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeleteDiscountCustomerGroup(short discountId, int customerGroupId)
        {
            var discountCustomerGroup = new DiscountCustomerGroup {DiscountId=discountId, CustomerGroupId=customerGroupId };
            _context.DiscountCustomerGroups.Attach(discountCustomerGroup);
            _context.Entry(discountCustomerGroup).State = EntityState.Deleted;
        }
  
    }
}
