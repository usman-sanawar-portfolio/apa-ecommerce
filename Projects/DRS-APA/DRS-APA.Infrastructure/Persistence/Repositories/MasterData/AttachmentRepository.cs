﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Domain.Core.Repositories.MasterData;
using Microsoft.EntityFrameworkCore;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    public class AttachmentRepository : IAttachmentRepository
    {
        private DRSAPAContext _context;

        public AttachmentRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Attachment>> GetAllAttachments()
        {
            return await  _context.Attachments.OrderBy(a => a.Name).ToListAsync();
        }
        public async Task<IQueryable<Attachment>> GetAllAttachments(bool withPagination)
        {
            return _context.Attachments;
        }


#pragma warning disable 1998
        public async Task<IQueryable<Attachment>> GetAllAttachments(ISpecification<Attachment> spec)
#pragma warning restore 1998
        {
            return ApplySpecification(spec);
        }

        private IQueryable<Attachment> ApplySpecification(ISpecification<Attachment> spec)
        {
            return SpecificationEvaluator<Attachment>.GetQuery(_context.Set<Attachment>().AsQueryable(), spec);
        }




        public async Task<Attachment> GetAttachmentById(int id)
        {
            return await  _context.Attachments.FindAsync(id);
        }
       
        public bool GetAttachmentByIdAsNoTracking(int id)
        {
            return _context.Attachments.Any(e => e.AttachmentId == id);
        }

        public async Task<Attachment> GetAttachmentByName(string name)
        {
            return await _context.Attachments.Where(a => name.Contains(a.Name)).SingleAsync();
        }
        public async Task<bool> AttachmentExists(int attachmentId)
        {
            return await _context.Attachments.AnyAsync(t => t.AttachmentId == attachmentId);
        }

       public async Task AddAttachment(Attachment attachment)
        {
            if (!await _context.Attachments.Where(a => a.Name == attachment.Name || a.AttachmentId == attachment.AttachmentId).AnyAsync())
            {
                await _context.AddAsync(attachment);
            }
            else
            {
                throw new Exception("Attachment Already Exists");
            }
        }

        // disable async warning - no code 
        public bool UpdateAttachmentOk(int id,Attachment attachment)
        {
            var entity = _context.Attachments.Any(d => d.AttachmentId == id);
            if (entity == false)
            {                
                return false;
                //return NotFound();
            }
            if (attachment.AttachmentId != id)
            {
                attachment.AttachmentId = id;

            }

            _context.Attachments.Attach(attachment);
            _context.Entry(attachment).State = EntityState.Modified;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeleteAttachment(int id)
        {
            var attachment = new Attachment { AttachmentId = id };
            _context.Attachments.Attach(attachment);
            _context.Entry(attachment).State = EntityState.Deleted;
        }
  
    }
}
