﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Repair;
using DRS.APA.Domain.Core.Repositories.MasterData;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    public class ResourceRepository : IResourceRepository
    {
        private DRSAPAContext _context;

        public ResourceRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Resource>> GetAllResources()
        {
            return await _context.Resources.OrderBy(a => a.Name).ToListAsync();
        }
        public async Task<IQueryable<Resource>> GetAllResources(bool withPagination)
        {
            return _context.Resources;
            //return await Task.Run(() => { _context.Resources; });
        }


        public async Task<Resource> GetResourceById(int id)
        {
            return await _context.Resources.FindAsync(id);
        }
        public bool GetResourceByIdAsNoTracking(int id)
        {
            return _context.Resources.Any(e => e.ResourceId == id);
        }
        public async Task<Resource> GetResourceByName(string name)
        {
            return await _context.Resources.Where(a => name.Contains(a.Name)).SingleAsync();
        }
        public async Task<bool> ResourceExists(int resourceId)
        {
            return await _context.Resources.AnyAsync(t => t.ResourceId == resourceId);
        }

        public async Task AddResource(Resource resource)
        {
            if (!await _context.Resources.Where(a => a.Name == resource.Name).AnyAsync())
            {
                await _context.AddAsync(resource);
            }
            else
            {
                throw new Exception("Staff Name Already Exists");
            }
        }

        // disable async warning - no code 
        public bool UpdateResourceOk(int id, Resource Resource)
        {
            //var entity = _context.Resources.AsNoTracking().FirstOrDefault(d => d.ResourceId==id);
            var entity = _context.Resources.Any(d => d.ResourceId == id);
            if (entity == false)
            {
                return false;
                //return NotFound();
            }
            if (Resource.ResourceId != id)
            {
                Resource.ResourceId = id;

            }
            else { }


            // set original entity state to detached
            // _ctx.Entry(existingEG).State = EntityState.Detached;


            _context.Resources.Attach(Resource);
            _context.Entry(Resource).State = EntityState.Modified;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeleteResource(int id)
        {
            var Resource = new Resource { ResourceId = id };
            _context.Resources.Attach(Resource);
            _context.Entry(Resource).State = EntityState.Deleted;
        }

    }
}
