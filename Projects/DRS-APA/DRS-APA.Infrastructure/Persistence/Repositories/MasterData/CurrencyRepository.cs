﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    public class CurrencyRepository : ICurrencyRepository
    {
        private DRSAPAContext _context;

        public CurrencyRepository(DRSAPAContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Currency>> GetAllCurrencies()
        {
            return await _context.Currencies.OrderBy(a => a.Name).ToListAsync();
        }
        public async Task<IQueryable<Currency>> GetAllCurrencies(bool withPagination)
        {
            return _context.Currencies;
            //return await Task.Run(() => { _context.DocumentTypes; });
        }

        public async Task<Currency> GetCurrencyById(short id)
        {
            return await _context.Currencies.FindAsync(id);
        }
        public bool GetCurrencyByIdAsNoTracking(short id)
        {
            return _context.Currencies.Any(e => e.CurrencyId == id);
        }
        public async Task<Currency> GetCurrencyByName(string name)
        {
            return await _context.Currencies.Where(a => name.Contains(a.Name)).SingleAsync();
        }
        public async Task<bool> CurrencyExists(short currencyId)
        {
            return await _context.Currencies.AnyAsync(t => t.CurrencyId == currencyId);
        }

        public async Task AddCurrency(Currency currency)
        {
            if (!await _context.Currencies.Where(a => a.Name == currency.Name || a.CurrencyId == currency.CurrencyId).AnyAsync())
            {
                await _context.AddAsync(currency);
            }
            else
            {
                throw new Exception("Resource Group Already Exists");
            }
        }
        // disable async warning - no code 
        public bool UpdateCurrencyOk(short id, Currency currency)
        {
            //var entity = _context.Resources.AsNoTracking().FirstOrDefault(d => d.ResourceId==id);
            var entity = _context.Currencies.Any(d => d.CurrencyId == id);
            if (entity == false)
            {
                return false;
                //return NotFound();
            }
            if (currency.CurrencyId != id)
            {
                currency.CurrencyId = id;

            }
            else { }


            // set original entity state to detached
            // _ctx.Entry(existingEG).State = EntityState.Detached;


            _context.Currencies.Attach(currency);
            _context.Entry(currency).State = EntityState.Modified;
            _context.Entry(currency).Property(x => x.CurrencyId).IsModified = false;
            _context.Entry(currency).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(currency).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        // disable async warning - no RemoveAsync available
        // disable async warning - no code 
        public void UpdateCurrency(short id, Currency currency)
        {
            if (currency.CurrencyId != id)
            {
                currency.CurrencyId = (short)id;

            }
            else { }
            _context.Currencies.Attach(currency);
            _context.Entry(currency).State = EntityState.Modified;
        }

        // disable async warning - no RemoveAsync available
        public void DeleteCurrency(short id)
        {
            var Currency = new Currency { CurrencyId = (short)id };
            _context.Currencies.Attach(Currency);
            _context.Entry(Currency).State = EntityState.Deleted;
        }
    }
}
