﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    public class RepairOptionModelRepository : IRepairOptionModelRepository
    {
        private DRSAPAContext _context;

        public RepairOptionModelRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<RepairOptionModel>> GetAllRepairOptionModels()
        {
            return await  _context.RepairOptionModels
                .Include(z => z.Model)
                .Include(z => z.RepairOption).ToListAsync();
        }
        public async Task<IQueryable<RepairOptionModel>> GetAllRepairOptionModels(bool withPagination)
        {
            return _context.RepairOptionModels
                .Include(z => z.Model)
                .Include(z => z.RepairOption);
            //return await Task.Run(() => { _context.RepairOptionModels; });
        }
        
        public async Task<RepairOptionModel> GetRepairOptionModelById(short repairoptionId, int modelId)
        {
            return await  _context.RepairOptionModels.FindAsync(repairoptionId, modelId);
        }

       
        public bool GetRepairOptionModelByIdAsNoTracking(short repairoptionId, int modelId)
        {
            return _context.RepairOptionModels.Any(e => e.RepairOptionId == repairoptionId & e.ModelId== modelId);
        }

        public async Task<RepairOptionModel> GetRepairOptionModelByName(string name)
        {
            return await _context.RepairOptionModels.Where(a => name.Contains(a.Remarks)).SingleAsync();
        }
        public async Task<bool> RepairOptionModelExists(short repairoptionId, int modelId)
        {
            return await _context.RepairOptionModels.AnyAsync(e => e.RepairOptionId == repairoptionId & e.ModelId == modelId);
        }

       public async Task AddRepairOptionModel(RepairOptionModel repairOptionModel)
        {
            if (!await _context.RepairOptionModels.Where(a => a.RepairOptionId == repairOptionModel.RepairOptionId && a.ModelId == repairOptionModel.ModelId).AnyAsync())
            {
                await _context.AddAsync(repairOptionModel);
            }
            else
            {
                throw new Exception("RepairOptionModel Already Exists");
            }
        }

        // disable async warning - no code 
        public bool UpdateRepairOptionModelOk(short repairoptionId, int modelId,RepairOptionModel repairOptionModel)
        {
            //var entity = _context.RepairOptionModels.AsNoTracking().FirstOrDefault(d => d.RepairOptionModelId==id);
            var entity = _context.RepairOptionModels.Any(d => d.RepairOptionId == repairoptionId && d.ModelId== modelId);
            if (entity == false)
            {                
                return false;
                //return NotFound();
            }
            if (repairOptionModel.RepairOptionId != repairoptionId && repairOptionModel.ModelId != modelId)
            {
                repairOptionModel.RepairOptionId = repairoptionId;
                repairOptionModel.ModelId = modelId;

            }
            else { }


            // set original entity state to detached
           // _ctx.Entry(existingEG).State = EntityState.Detached;


            _context.RepairOptionModels.Attach(repairOptionModel);
            _context.Entry(repairOptionModel).State = EntityState.Modified;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeleteRepairOptionModel(short repairoptionId, int modelId)
        {
            var repairOptionModel = new RepairOptionModel { RepairOptionId = repairoptionId, ModelId = modelId};
            _context.RepairOptionModels.Attach(repairOptionModel);
            _context.Entry(repairOptionModel).State = EntityState.Deleted;
        }
  
    }
}
