﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.Shop;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Domain.Core.Repositories.Shop;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{

    public class SalesOrderRepository : ISalesOrderRepository
    {
        private DRSAPAContext _context;

        public SalesOrderRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<SalesOrder>> GetAllSalesOrders()
        {
            return await  _context.SalesOrders
                .Include(m => m.DeliveryMethod)
                .OrderBy(a => a.SalesOrderId).ToListAsync();
        }
#pragma warning disable 1998
        public async Task<IQueryable<SalesOrder>> GetAllSalesOrders(bool withPagination)
#pragma warning restore 1998
        {
            return _context.SalesOrders
                .Include(m => m.DeliveryMethod)
                .Include(p => p.Customer); //.Where(t => t.IsPrimary==true));
            //return await Task.Run(() => { _context.SalesOrders; });
        }

#pragma warning disable 1998
        public async Task<IQueryable<SalesOrder>> GetAllSalesOrders(ISpecification<SalesOrder> spec)
#pragma warning restore 1998
#pragma warning restore 1998
        {
            return ApplySpecification(spec);
        }

        private IQueryable<SalesOrder> ApplySpecification(ISpecification<SalesOrder> spec)
        {
            return SpecificationEvaluator<SalesOrder>.GetQuery(_context.Set<SalesOrder>().AsQueryable(), spec);
        }


        public async Task<IEnumerable<LineItem>> GetAllSalesOrdersByCategory(int categoryId)
        {
            return await _context.LineItems
                 .Include(m => m.SalesOrder)
                 .Include(m => m.Product)
                 .Where(m => m.Product.ProductCategoryId == categoryId)
                 .OrderBy(a => a.SalesOrder.SalesOrderId).ToListAsync();
        }

        public async Task<IEnumerable<SalesOrder>> GetAllSalesOrdersByCustomer(int customerId)
        {
            return await _context.SalesOrders
                .Include(m => m.LineItems)
                .ThenInclude(p => p.Product)
                .Where(m => m.CustomerId == customerId)
                .OrderBy(a => a.SalesOrderId).ToListAsync();
        }

        public async Task<IEnumerable<LineItem>> GetAllLineItemsBySalesOrder(int salesOrderId)
        {
            return await _context.LineItems
                .Include(m => m.SalesOrder)
                .Include(m => m.Product)
                .Where(m => m.SalesOrderId == salesOrderId)
                .OrderBy(a => a.SalesOrder.SalesOrderId).ToListAsync();
        }

        public async Task<SalesOrder> GetSalesOrderById(int id)
        {
            return await  _context.SalesOrders
                .Include(p => p.LineItems)
                .Where(i => i.SalesOrderId==id)
                .SingleAsync();
        }

        public async Task<LineItem> GetLineItemById(int id)
        {
            return await _context.LineItems
                .Include(p => p.Product)
                .Where(i => i.LineItemId == id)
                .SingleAsync();
        }


        public bool GetSalesOrderByIdAsNoTracking(int id)
        {
            return _context.SalesOrders.Any(e => e.SalesOrderId == id);
        }

        public bool GetItemLineByIdAsNoTracking(int id)
        {
            return _context.LineItems.Any(e => e.LineItemId == id);
        }

        public async Task AddSalesOrder(SalesOrder salesOrder)
        {
            //if (!await _context.SalesOrders.Where(a => a.Title == salesOrder.Title || a.SalesOrderId == salesOrder.SalesOrderId).AnyAsync())
            //{
                await _context.AddAsync(salesOrder);
            //}
            //else
            //{
            //    throw new Exception("SalesOrder Title Already Exists");
            //}
        }
       public async Task AddLineItem(LineItem lineItem)
       {
           //if (!await _context.SalesOrders.Where(a => a.Title == salesOrder.Title || a.SalesOrderId == salesOrder.SalesOrderId).AnyAsync())
           //{
           await _context.AddAsync(lineItem);
           //}
           //else
           //{
           //    throw new Exception("SalesOrder Title Already Exists");
           //}
       }

        // disable async warning - no code 
        public bool UpdateSalesOrderOk(int id, SalesOrder salesOrder)
        {
            //var entity = _context.SalesOrders.AsNoTracking().FirstOrDefault(d => d.SalesOrderId==id);
            var entity = _context.SalesOrders.Any(d => d.SalesOrderId == id);
            if (entity == false)
            {                
                return false;
                //return NotFound();
            }
            if (salesOrder.SalesOrderId != id)
            {
                salesOrder.SalesOrderId = id;

            }
            else { }

            // set original entity state to detached
           // _ctx.Entry(existingEG).State = EntityState.Detached;

            _context.SalesOrders.Attach(salesOrder);
            _context.Entry(salesOrder).State = EntityState.Modified;
            return true;
        }
        public bool UpdateLineItemOk(int id, LineItem lineItem)
        {
            //var entity = _context.SalesOrders.AsNoTracking().FirstOrDefault(d => d.SalesOrderId==id);
            var entity = _context.LineItems.Any(d => d.LineItemId == id);
            if (entity == false)
            {
                return false;
                //return NotFound();
            }
            if (lineItem.LineItemId != id)
            {
                lineItem.LineItemId = id;

            }
            else { }

            // set original entity state to detached
            // _ctx.Entry(existingEG).State = EntityState.Detached;

            _context.LineItems.Attach(lineItem);
            _context.Entry(lineItem).State = EntityState.Modified;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeleteSalesOrder(int id)
        {
            var salesOrder = new SalesOrder { SalesOrderId = id };
            _context.SalesOrders.Attach(salesOrder);
            _context.Entry(salesOrder).State = EntityState.Deleted;
        }
        public void DeleteLineItem(int id)
        {
            var lineItem = new LineItem {LineItemId = id };
            _context.LineItems.Attach(lineItem);
            _context.Entry(lineItem).State = EntityState.Deleted;
        }

    }
}
