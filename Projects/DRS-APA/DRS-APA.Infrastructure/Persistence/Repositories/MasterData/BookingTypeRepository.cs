﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.Schedule;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Domain.Core.Repositories.MasterData;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{

    public class BookingTypeRepository : IBookingTypeRepository
    {
        private DRSAPAContext _context;

        public BookingTypeRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<BookingType>> GetAllBookingTypes()
        {
            return await  _context.BookingTypes
                .OrderBy(a => a.BookingTypeId).ToListAsync();
        }
#pragma warning disable 1998
        public async Task<IQueryable<BookingType>> GetAllBookingTypes(bool withPagination)
#pragma warning restore 1998
        {
            return _context.BookingTypes;
            //return await Task.Run(() => { _context.BookingTypes; });
        }

#pragma warning disable 1998
        public async Task<IQueryable<BookingType>> GetAllBookingTypes(ISpecification<BookingType> spec)
#pragma warning restore 1998
#pragma warning restore 1998
        {
            return ApplySpecification(spec);
        }

        private IQueryable<BookingType> ApplySpecification(ISpecification<BookingType> spec)
        {
            return SpecificationEvaluator<BookingType>.GetQuery(_context.Set<BookingType>().AsQueryable(), spec);
        }

        public async Task<BookingType> GetBookingTypeById(short id)
        {
            return await  _context.BookingTypes
                .Where(i => i.BookingTypeId==id)
                .SingleAsync();
        }

        public bool GetBookingTypeByIdAsNoTracking(short id)
        {
            return _context.BookingTypes.Any(e => e.BookingTypeId == id);
        }

        public async Task<BookingType> GetBookingTypeByName(string name)
        {
            return await _context.BookingTypes.Where(a => name.Contains(a.Name)).SingleAsync();
        }

        //public async Task<bool> BookingTypeExists(short bookingTypeId)
        //{
        //    return await _context.BookingTypes.AnyAsync(t => t.BookingTypeId == bookingTypeId);
        //}

        public async Task AddBookingType(BookingType bookingType)
        {
            //if (!await _context.BookingTypes.Where(a => a.Title == bookingType.Title || a.BookingTypeId == bookingType.BookingTypeId).AnyAsync())
            //{
                await _context.AddAsync(bookingType);
            //}
            //else
            //{
            //    throw new Exception("BookingType Title Already Exists");
            //}
        }

        // disable async warning - no code 
        public bool UpdateBookingTypeOk(short id, BookingType bookingType)
        {
            //var entity = _context.BookingTypes.AsNoTracking().FirstOrDefault(d => d.BookingTypeId==id);
            var entity = _context.BookingTypes.Any(d => d.BookingTypeId == id);
            if (entity == false)
            {                
                return false;
                //return NotFound();
            }
            if (bookingType.BookingTypeId != id)
            {
                bookingType.BookingTypeId = id;

            }
            else { }

            // set original entity state to detached
           // _ctx.Entry(existingEG).State = EntityState.Detached;

            _context.BookingTypes.Attach(bookingType);
            _context.Entry(bookingType).State = EntityState.Modified;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeleteBookingType(short id)
        {
            var bookingType = new BookingType { BookingTypeId = id };
            _context.BookingTypes.Attach(bookingType);
            _context.Entry(bookingType).State = EntityState.Deleted;
        }
  
    }
}
