﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Infrastructure.EntityConfigurations.MasterData;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
   
    public class PostcodeDeliveryMethodRepository : IPostcodeDeliveryMethodRepository
    {
        private DRSAPAContext _context;

        public PostcodeDeliveryMethodRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<PostcodeDeliveryMethod>> GetAllPostcodeDeliveryMethods()
        {
            return await  _context.PostcodeDeliveryMethods
                .Include(z => z.DeliveryMethod)
                .Include(z => z.Postcode).ToListAsync();
        }
        public async Task<IQueryable<PostcodeDeliveryMethod>> GetAllPostcodeDeliveryMethods(bool withPagination)
        {
            return _context.PostcodeDeliveryMethods
                .Include(z => z.DeliveryMethod)
                .Include(z => z.Postcode);
            //return await Task.Run(() => { _context.PostcodeDeliveryMethods; });
        }
        public async Task<IEnumerable<PostcodeDeliveryMethod>> GetAllPostcodeDeliveryMethodsByPostcode(int postcodeId)
        {
            return await _context.PostcodeDeliveryMethods
                .Where(p => p.PostcodeId==postcodeId)
                .Include(z => z.DeliveryMethod)
                .Include(z => z.Postcode).ToListAsync();
        }

        public async Task<PostcodeDeliveryMethod> GetPostcodeDeliveryMethodById(int postcodeId, short deliveryMethodId)
        {
            return await  _context.PostcodeDeliveryMethods.FindAsync(postcodeId, deliveryMethodId);
        }

       
        public bool GetPostcodeDeliveryMethodByIdAsNoTracking(int postcodeId, short deliveryMethodId)
        {
            return _context.PostcodeDeliveryMethods.Any(e => e.PostcodeId == postcodeId & e.DeliveryMethodId== deliveryMethodId);
        }

      
        public async Task<bool> PostcodeDeliveryMethodExists(int postcodeId, short deliveryMethodId)
        {
            return await _context.PostcodeDeliveryMethods.AnyAsync(e => e.PostcodeId == postcodeId & e.DeliveryMethodId == deliveryMethodId);
        }

       public async Task AddPostcodeDeliveryMethod(PostcodeDeliveryMethod postcodeDeliveryMethod)
        {
            if (!await _context.PostcodeDeliveryMethods.Where(a => a.PostcodeId == postcodeDeliveryMethod.PostcodeId && a.DeliveryMethodId == postcodeDeliveryMethod.DeliveryMethodId).AnyAsync())
            {
                await _context.AddAsync(postcodeDeliveryMethod);
            }
            else
            {
                throw new Exception("PostcodeDeliveryMethod Already Exists");
            }
        }

        // disable async warning - no code 
        public bool UpdatePostcodeDeliveryMethodOk(int postcodeId, short deliveryMethodId, PostcodeDeliveryMethod postcodeDeliveryMethod)
        {
            //var entity = _context.PostcodeDeliveryMethods.AsNoTracking().FirstOrDefault(d => d.PostcodeDeliveryMethodId==id);
            var entity = _context.PostcodeDeliveryMethods.Any(d => d.PostcodeId == postcodeId && d.DeliveryMethodId== deliveryMethodId);
            if (entity == false)
            {                
                return false;
                //return NotFound();
            }
            if (postcodeDeliveryMethod.PostcodeId != postcodeId && postcodeDeliveryMethod.DeliveryMethodId != deliveryMethodId)
            {
                postcodeDeliveryMethod.PostcodeId = postcodeId;
                postcodeDeliveryMethod.DeliveryMethodId = deliveryMethodId;

            }
            else { }


            // set original entity state to detached
           // _ctx.Entry(existingEG).State = EntityState.Detached;


            _context.PostcodeDeliveryMethods.Attach(postcodeDeliveryMethod);
            _context.Entry(postcodeDeliveryMethod).State = EntityState.Modified;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeletePostcodeDeliveryMethod(int postcodeId, short deliveryMethodId)
        {
            var postcodeDeliveryMethod = new PostcodeDeliveryMethod { PostcodeId = postcodeId, DeliveryMethodId = deliveryMethodId};
            _context.PostcodeDeliveryMethods.Attach(postcodeDeliveryMethod);
            _context.Entry(postcodeDeliveryMethod).State = EntityState.Deleted;
        }
  
    }
}
