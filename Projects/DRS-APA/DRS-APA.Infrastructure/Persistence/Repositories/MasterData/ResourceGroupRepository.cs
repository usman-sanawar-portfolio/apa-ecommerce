﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Repair;
using DRS.APA.Domain.Core.Repositories.MasterData;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    public class ResourceGroupRepository : IResourceGroupRepository
    {
        private DRSAPAContext _context;

        public ResourceGroupRepository(DRSAPAContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<ResourceGroup>> GetAllResourceGroups()
        {
            return await _context.ResourceGroups.OrderBy(a => a.Name).ToListAsync();
        }
        public async Task<IQueryable<ResourceGroup>> GetAllResourceGroups(bool withPagination)
        {
            return _context.ResourceGroups;
            //return await Task.Run(() => { _context.DocumentTypes; });
        }

        public async Task<ResourceGroup> GetResourceGroupById(int id)
        {
            return await  _context.ResourceGroups.FindAsync(id);
        }
        public bool GetResourceGroupByIdAsNoTracking(int id)
        {
            return _context.ResourceGroups.Any(e => e.ResourceGroupId == id);
        }
        public async Task<ResourceGroup> GetResourceGroupByName(string name)
        {
            return await _context.ResourceGroups.Where(a => name.Contains(a.Name)).SingleAsync();
        }
        public async Task<bool> ResourceGroupExists(int resourceGroupId)
        {
            return await _context.ResourceGroups.AnyAsync(t => t.ResourceGroupId == resourceGroupId);
        }       

       public async Task AddResourceGroup(ResourceGroup resourceGroup)
        {
            if (!await _context.ResourceGroups.Where(a => a.Name == resourceGroup.Name || a.ResourceGroupId == resourceGroup.ResourceGroupId).AnyAsync())
            {
                await _context.AddAsync(resourceGroup);
            }
            else
            {
                throw new Exception("Resource Group Already Exists");
            }
        }
        // disable async warning - no code 
        public bool UpdateResourceGroupOk(int id, ResourceGroup resourceGroup)
        {
            //var entity = _context.Resources.AsNoTracking().FirstOrDefault(d => d.ResourceId==id);
            var entity = _context.ResourceGroups.Any(d => d.ResourceGroupId == id);
            if (entity == false)
            {
                return false;
                //return NotFound();
            }
            if (resourceGroup.ResourceGroupId != id)
            {
                resourceGroup.ResourceGroupId = id;

            }
            else { }


            // set original entity state to detached
            // _ctx.Entry(existingEG).State = EntityState.Detached;


            _context.ResourceGroups.Attach(resourceGroup);

            _context.Entry(resourceGroup).State = EntityState.Modified;
            return true;
        }
        // disable async warning - no RemoveAsync available
        // disable async warning - no code 
        public void UpdateResourceGroup(int id,ResourceGroup resourceGroup)
        {
            if (resourceGroup.ResourceGroupId != id)
            {
                resourceGroup.ResourceGroupId =(int) id;
            }
            else { }

            _context.ResourceGroups.Attach(resourceGroup);

            _context.Entry(resourceGroup).State = EntityState.Modified;
        }

        // disable async warning - no RemoveAsync available
        public void DeleteResourceGroup(int id)
        {
            var ResourceGroup = new ResourceGroup { ResourceGroupId = (int)id };

            _context.ResourceGroups.Attach(ResourceGroup);

            _context.Entry(ResourceGroup).State = EntityState.Deleted;
        }
    }
}
