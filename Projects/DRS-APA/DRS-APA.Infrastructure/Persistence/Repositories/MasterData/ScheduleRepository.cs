﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Schedule;
using DRS.APA.Domain.Core.Domain.Shop;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Repositories.Shop;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    public class ScheduleRepository : IScheduleRepository
    {
        private DRSAPAContext _context;

        public ScheduleRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Schedule>> GetAllSchedules()
        {
            return await  _context.Schedules
                .Include(m => m.RepairCategory)
                .Include(m => m.Location)
                .OrderBy(a => a.ScheduleId).ToListAsync();
        }
#pragma warning disable 1998
        public async Task<IQueryable<Schedule>> GetAllSchedules(bool withPagination)
#pragma warning restore 1998
        {
            return _context.Schedules
                .Include(m => m.RepairCategory)
                .Include(p => p.Location); //.Where(t => t.IsPrimary==true));
            //return await Task.Run(() => { _context.Schedules; });
        }

#pragma warning disable 1998
        public async Task<IQueryable<Schedule>> GetAllSchedules(ISpecification<Schedule> spec)
#pragma warning restore 1998
#pragma warning restore 1998
        {
            return ApplySpecification(spec);
        }

        private IQueryable<Schedule> ApplySpecification(ISpecification<Schedule> spec)
        {
            return SpecificationEvaluator<Schedule>.GetQuery(_context.Set<Schedule>().AsQueryable(), spec);
        }

        //public async Task<IEnumerable<Schedule>> GetAllSchedulesByCategory(int categoryId)
        //{
        //    return await _context.Schedules
        //        .Include(m => m.Location)
        //        .Include(m => m.RepairCategory)
        //        .Where(m => m.RepairCategoryId == categoryId)
        //        .OrderBy(a => a.ScheduleId).ToListAsync();
        //}
        public async Task<Schedule> GetWeeklySchedule(DateTime startDate)
        {
            return await _context.Schedules
                .Include(m => m.Location)
                .Include(m => m.RepairCategory)
                .Where(m => m.Start <= startDate && m.End >= startDate).SingleOrDefaultAsync();

        }

        public async Task<Schedule> GetScheduleById(int id)
        {
            return await  _context.Schedules
                .Include(p => p.Bookings)
                .Where(i => i.ScheduleId==id)
                .SingleAsync();
        }

       
        public bool GetScheduleByIdAsNoTracking(int id)
        {
            return _context.Schedules.Any(e => e.ScheduleId == id);
        }

        //public async Task<Schedule> GetScheduleByTitle(string name)
        //{
        //    return await _context.Schedules.Where(a => name.Contains(a.Title)).SingleAsync();
        //}

        //public async Task<bool> ScheduleExists(int scheduleId)
        //{
        //    return await _context.Schedules.AnyAsync(t => t.ScheduleId == scheduleId);
        //}

       public async Task AddSchedule(Schedule schedule)
        {
            //if (!await _context.Schedules.Where(a => a.Title == schedule.Title || a.ScheduleId == schedule.ScheduleId).AnyAsync())
            //{
                await _context.AddAsync(schedule);
            //}
            //else
            //{
            //    throw new Exception("Schedule Title Already Exists");
            //}
        }

        // disable async warning - no code 
        public bool UpdateScheduleOk(int id, Schedule schedule)
        {
            //var entity = _context.Schedules.AsNoTracking().FirstOrDefault(d => d.ScheduleId==id);
            var entity = _context.Schedules.Any(d => d.ScheduleId == id);
            if (entity == false)
            {                
                return false;
                //return NotFound();
            }
            if (schedule.ScheduleId != id)
            {
                schedule.ScheduleId = id;

            }
            else { }

            // set original entity state to detached
           // _ctx.Entry(existingEG).State = EntityState.Detached;

            _context.Schedules.Attach(schedule);
            _context.Entry(schedule).State = EntityState.Modified;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeleteSchedule(int id)
        {
            var schedule = new Schedule { ScheduleId = id };
            _context.Schedules.Attach(schedule);
            _context.Entry(schedule).State = EntityState.Deleted;
        }
  
    }
}
