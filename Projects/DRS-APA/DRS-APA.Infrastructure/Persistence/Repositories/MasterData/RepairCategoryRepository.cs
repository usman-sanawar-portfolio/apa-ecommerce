﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    public class RepairCategoryRepository : IRepairCategoryRepository
    {
        private DRSAPAContext _context;

        public RepairCategoryRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<RepairCategory>> GetAllRepairCategories()
        {
            return await  _context.RepairCategories.OrderBy(a => a.Name).ToListAsync();
        }
        public async Task<IQueryable<RepairCategory>> GetAllRepairCategories(bool withPagination)
        {
            return _context.RepairCategories;
            //return await Task.Run(() => { _context.RepairCategories; });
        }


        public async Task<RepairCategory> GetRepairCategoryById(short id)
        {
            return await  _context.RepairCategories.FindAsync(id);
        }

       
        public bool GetRepairCategoryByIdAsNoTracking(short id)
        {
            return _context.RepairCategories.Any(e => e.RepairCategoryId == id);
        }


        public async Task<RepairCategory> GetRepairCategoryByName(string name)
        {
            return await _context.RepairCategories.Where(a => name.Contains(a.Name)).SingleAsync();
        }
        public async Task<bool> RepairCategoryExists(short repairCategoryId)
        {
            return await _context.RepairCategories.AnyAsync(t => t.RepairCategoryId == repairCategoryId);
        }

       public async Task AddRepairCategory(RepairCategory repairCategory)
        {
            if (!await _context.RepairCategories.Where(a => a.Name == repairCategory.Name || a.RepairCategoryId == repairCategory.RepairCategoryId).AnyAsync())
            {
                await _context.AddAsync(repairCategory);
            }
            else
            {
                throw new Exception("RepairCategory Name Already Exists");
            }
        }

        // disable async warning - no code 
        public bool UpdateRepairCategoryOk(short id,RepairCategory RepairCategory)
        {
            //var entity = _context.RepairCategories.AsNoTracking().FirstOrDefault(d => d.RepairCategoryId==id);
            var entity = _context.RepairCategories.Any(d => d.RepairCategoryId == id);
            if (entity == false)
            {                
                return false;
                //return NotFound();
            }
            if (RepairCategory.RepairCategoryId != id)
            {
                RepairCategory.RepairCategoryId = id;

            }
            else { }


            // set original entity state to detached
           // _ctx.Entry(existingEG).State = EntityState.Detached;


            _context.RepairCategories.Attach(RepairCategory);
            _context.Entry(RepairCategory).State = EntityState.Modified;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeleteRepairCategory(short id)
        {
            var RepairCategory = new RepairCategory { RepairCategoryId = id };
            _context.RepairCategories.Attach(RepairCategory);
            _context.Entry(RepairCategory).State = EntityState.Deleted;
        }
  
    }
}
