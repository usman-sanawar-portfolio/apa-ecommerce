﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using System.Collections.Generic;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    public class CountryRepository : ICountryRepository
    {
        private DRSAPAContext _context;

        public CountryRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Country>> GetAllCountries()
        {
            return await  _context.Countries
                .OrderBy(a => a.CountryName).ToListAsync();
        }
        public async Task<IQueryable<Country>> GetAllCountries(bool withPagination)
        {
            return _context.Countries;
        }

        public async Task<Country> GetCountryById(int id)
        {
            return await  _context.Countries
                //.Include(x => x.GeoZones)
                .FirstOrDefaultAsync(x=>x.CountryId==id);
        }
       
        public bool GetCountryByIdAsNoTracking(int id)
        {
            return _context.Countries.Any(e => e.CountryId == id);
        }

        public async Task<Country> GetCountryByName(string name)
        {
            return await _context.Countries.Where(a => name.Contains(a.CountryName)).SingleOrDefaultAsync();
        }
        public async Task<bool> CountryExists(int countryId)
        {
            return await _context.Countries.AnyAsync(t => t.CountryId == countryId);
        }

       public async Task AddCountry(Country country)
        {
            if (!await _context.Countries.Where(a => a.CountryName == country.CountryName).AnyAsync())
            {
                await _context.AddAsync(country);
            }
            else
            {
                throw new Exception("Country Name Already Exists");
            }
        }

        // disable async warning - no code 
        public bool UpdateCountryOk(int id,Country country)
        {
            var sameName = _context.Countries.AsNoTracking().FirstOrDefault(d => d.CountryId != id && d.CountryName == country.CountryName);
            if (sameName != null)
            {
                throw new Exception("Country Name Already Exists");
            }
            var entity = _context.Countries.Any(d => d.CountryId == id);
            if (entity == false)
            {                
                return false;
            }
            if (country.CountryId != id)
            {
                country.CountryId = id;

            }

            _context.Countries.Attach(country);
            _context.Entry(country).State = EntityState.Modified;
            _context.Entry(country).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(country).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeleteCountry(int id)
        {
            var Country = new Country { CountryId = id };
            _context.Countries.Attach(Country);
            _context.Entry(Country).State = EntityState.Deleted;
        }  
    }
}
