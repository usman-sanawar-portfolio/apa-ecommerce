﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    public class CustomerGroupRepository : ICustomerGroupRepository
    {
        private DRSAPAContext _context;

        public CustomerGroupRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<CustomerGroup>> GetAllCustomerGroups()
        {
            return await _context.CustomerGroups.Include(c => c.DiscountCustomerGroups).Where(x=>x.IsDeleted==false)
                .OrderBy(a => a.CustomerGroupId).ToListAsync();
        }
        public async Task<IQueryable<CustomerGroup>> GetAllCustomerGroups(bool withPagination)
        {
            return _context.CustomerGroups.Include(c => c.DiscountCustomerGroups).Where(x => x.IsDeleted == false);

        }

     
        public async Task<CustomerGroup> GetCustomerGroupById(int id)
        {
            return await _context.CustomerGroups
                .Where(i => i.CustomerGroupId == id && i.IsDeleted==false).Include(c=>c.TaxRatesCustomerGroups)
                .SingleOrDefaultAsync();
        }


        public bool GetCustomerGroupByIdAsNoTracking(int id)
        {
            return _context.CustomerGroups.Any(e => e.CustomerGroupId == id);
        }

        public async Task<bool> CustomerGroupExists(int customerGroupId)
        {
            return await _context.CustomerGroups.AnyAsync(t => t.CustomerGroupId == customerGroupId);
        }

        public async Task AddCustomerGroup(CustomerGroup customerGroup)
        {
            if (!await _context.CustomerGroups.Where(a => a.CustomerGroupId == customerGroup.CustomerGroupId || a.CustomerGroupName==customerGroup.CustomerGroupName).AnyAsync())
            {
                await _context.AddAsync(customerGroup);
            }
            else
            {
                throw new Exception("CustomerGroup Already Exists");
            }
        }

        // disable async warning - no code 
        public bool UpdateCustomerGroupOk(int id, CustomerGroup customerGroup)
        {
            //var entity = _context.CustomerGroups.AsNoTracking().FirstOrDefault(d => d.CustomerGroupId==id);
            var entity = _context.CustomerGroups.Any(d => d.CustomerGroupId == id);
            if (entity == false)
            {
                return false;
            }
            if (customerGroup.CustomerGroupId != id)
            {
                customerGroup.CustomerGroupId = id;

            }
            else { }

            _context.CustomerGroups.Attach(customerGroup);
            _context.Entry(customerGroup).State = EntityState.Modified;
            _context.Entry(customerGroup).Property(x => x.CustomerGroupId).IsModified = false;
            _context.Entry(customerGroup).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(customerGroup).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
       
        public void DeleteCustomerGroup(int id)
        {
            var CustomerGroup = new CustomerGroup { CustomerGroupId = id };
            CustomerGroup.IsDeleted = true;

            _context.CustomerGroups.Attach(CustomerGroup);
            _context.Entry(CustomerGroup).Property(x => x.IsDeleted).IsModified = true;
        }
    }
}
