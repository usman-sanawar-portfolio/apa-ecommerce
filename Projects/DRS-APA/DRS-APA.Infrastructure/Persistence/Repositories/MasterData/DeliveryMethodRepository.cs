﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Core.Domain;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Infrastructure.Persistence.Repositories;
using DRS.APA.Domain.Core;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.Repositories
{
    public class DeliveryMethodRepository : IDeliveryMethodRepository
    {
        private DRSAPAContext _context;

        public DeliveryMethodRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<DeliveryMethod>> GetAllDeliveryMethods()
        {
            return await  _context.DeliveryMethods.OrderBy(a => a.Name).ToListAsync();
        }
        public async Task<IQueryable<DeliveryMethod>> GetAllDeliveryMethods(bool withPagination)
        {
            return _context.DeliveryMethods;
        }


        public async Task<DeliveryMethod> GetDeliveryMethodById(short id)
        {
            return await  _context.DeliveryMethods.FindAsync(id);
        }

       
        public bool GetDeliveryMethodByIdAsNoTracking(short id)
        {
            return _context.DeliveryMethods.Any(e => e.DeliveryMethodId == id);
        }


        public async Task<DeliveryMethod> GetDeliveryMethodByName(string name)
        {
            return await _context.DeliveryMethods.Where(a => name.Contains(a.Name)).SingleAsync();
        }
        public async Task<bool> DeliveryMethodExists(short deliveryMethodId)
        {
            return await _context.DeliveryMethods.AnyAsync(t => t.DeliveryMethodId == deliveryMethodId);
        }

       public async Task AddDeliveryMethod(DeliveryMethod deliveryMethod)
        {
            if (!await _context.DeliveryMethods.Where(a => a.Name == deliveryMethod.Name || a.DeliveryMethodId == deliveryMethod.DeliveryMethodId).AnyAsync())
            {
                await _context.AddAsync(deliveryMethod);
            }
            else
            {
                throw new Exception("DeliveryMethod Name Already Exists");
            }
        }

        // disable async warning - no code 
        public bool UpdateDeliveryMethodOk(short id,DeliveryMethod DeliveryMethod)
        {
            var sameName = _context.DeliveryMethods.AsNoTracking().FirstOrDefault(d => d.DeliveryMethodId != id && d.Name == DeliveryMethod.Name);
            if (sameName != null)
            {
                throw new Exception("DeliveryMethod Name Already Exists");
            }
            var entity = _context.DeliveryMethods.Any(d => d.DeliveryMethodId == id);
            if (entity == false)
            {                
                return false;
                //return NotFound();
            }
            if (DeliveryMethod.DeliveryMethodId != id)
            {
                DeliveryMethod.DeliveryMethodId = id;

            }



            _context.DeliveryMethods.Attach(DeliveryMethod);
            _context.Entry(DeliveryMethod).State = EntityState.Modified;
            _context.Entry(DeliveryMethod).Property(x => x.DeliveryMethodId).IsModified = false;
            _context.Entry(DeliveryMethod).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(DeliveryMethod).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeleteDeliveryMethod(short id)
        {
            var DeliveryMethod = new DeliveryMethod { DeliveryMethodId = id };
            _context.DeliveryMethods.Attach(DeliveryMethod);
            _context.Entry(DeliveryMethod).State = EntityState.Deleted;
        }
  
    }
}
