﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    public class CollectionProductRepository : ICollectionProductRepository
    {
        private DRSAPAContext _context;

        public CollectionProductRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<CollectionProduct>> GetAllCollectionProducts()
        {
            return await _context.CollectionProducts
                .OrderBy(a => a.Popular).ToListAsync();
        }
        public async Task<IQueryable<CollectionProduct>> GetAllCollectionProducts(bool withPagination)
        {
            return _context.CollectionProducts;
        }

        public async Task<CollectionProduct> GetCollectionProductById(int collectinId, int productId)
        {
            return await _context.CollectionProducts.FindAsync(collectinId, productId);
        }


        public bool GetCollectionProductByIdAsNoTracking(int collectinId, int productId)
        {
            return _context.CollectionProducts.Any(e => e.CollectionId == collectinId && e.ProductId == productId);
        }



        public async Task AddCollectionProduct(CollectionProduct collectionProduct)
        {
            if (!await _context.CollectionProducts.Where(a => a.CollectionId == collectionProduct.CollectionId && a.ProductId == collectionProduct.ProductId).AnyAsync())
            {
                await _context.AddAsync(collectionProduct);
            }
            else
            {
                throw new Exception("Collection Product  Already Exists");
            }
        }

        // disable async warning - no code 
        public bool UpdateCollectionProductOk(int collectinId, int productId, CollectionProduct CollectionProduct)
        {
            var entity = _context.CollectionProducts.Any(d => d.CollectionId == collectinId && d.ProductId == productId);
            if (entity == false)
            {
                return false;
            }
            if (CollectionProduct.CollectionId != collectinId && CollectionProduct.ProductId != productId)
            {
                CollectionProduct.CollectionId = collectinId;
                CollectionProduct.ProductId = productId;

            }
            else { }


            _context.CollectionProducts.Attach(CollectionProduct);
            _context.Entry(CollectionProduct).State = EntityState.Modified;
            _context.Entry(CollectionProduct).Property(x => x.CollectionId).IsModified = false;
            _context.Entry(CollectionProduct).Property(x => x.ProductId).IsModified = false;
            _context.Entry(CollectionProduct).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(CollectionProduct).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeleteCollectionProduct(int collectinId, int productId)
        {
            var collectionProduct = new CollectionProduct { CollectionId = collectinId, ProductId = productId };
            _context.CollectionProducts.Attach(collectionProduct);
            _context.Entry(collectionProduct).State = EntityState.Deleted;
        }

    }
}
