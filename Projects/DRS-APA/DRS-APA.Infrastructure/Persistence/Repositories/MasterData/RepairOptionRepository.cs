﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    public class RepairOptionRepository : IRepairOptionRepository
    {
        private DRSAPAContext _context;

        public RepairOptionRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<RepairOption>> GetAllRepairOptions()
        {
            return await  _context.RepairOptions
                .Include(z => z.RepairCategory)
                .OrderBy(a => a.Name).ToListAsync();
        }
        public async Task<IQueryable<RepairOption>> GetAllRepairOptions(bool withPagination)
        {
            return _context.RepairOptions
                .Include(z => z.RepairCategory);
            //return await Task.Run(() => { _context.RepairOptions; });
        }

        public async Task<RepairOption> GetRepairOptionById(short id)
        {
            return await  _context.RepairOptions.FindAsync(id);
        }

        //public async Task<IEnumerable<RepairOption>> GetRepairOptionsByModel(int modelId)
        //{
        //    //var parameters = new List<SqlParameter> {
        //       // new SqlParameter("@p1", startDate.AddDays(-1)),
        //       // new SqlParameter("@p1", modelId) };
           
        //    SqlParameter parameterS = new SqlParameter("@s", modelId);

        //    string strSql = @"SELECT RepairOptions.RepairOptionId, RepairOptions.Name, RepairOptions.UnitPrice, RepairOptions.CreatedBy,RepairOptions.CreatedOn,RepairOptions.UpdatedOn,RepairOptions.UpdatedBy,RepairOptions.RepairCategoryId,RepairOptions.ImagePath,RepairOptions.IsActive,RepairOptions.Remarks, RepairOptions.Common, RepairOptionModels.ModelId
        //                    FROM  RepairOptions LEFT OUTER JOIN
        //                    RepairOptionModels ON RepairOptions.RepairOptionId = RepairOptionModels.RepairOptionId
        //                    WHERE(RepairOptionModels.ModelId = @s) OR
        //                    (RepairOptionModels.ModelId IS NULL)";
        //    return await _context.RepairOptions.FromSql(strSql, parameterS).ToListAsync();

            

        //    // return await _context.RepairOptions
        //    // .Select(d => d.RepairOptionModels.Where(d => d.ModelId==modelId || d.ModelId = null))
        //    //.Include(z => z.Model)
        //    //.Include(z => z.RepairOption)
        //    //.Where(d => d.ModelId == modelId)
        //    //.ToListAsync();

        //    //var join = await _context.RepairOptions
        //    //    .Where(p => p.IsActive == true)
        //    //    .SelectMany(p => p.RepairOptionModels.DefaultIfEmpty(), (p, b) => new
        //    //    {
        //    //        RepairOptionId=b.RepairOptionId,
        //    //        ModelId=b.ModelId,
        //    //        Name=p.Name,
        //    //        UnitPrice=p.UnitPrice
        //    //    }).ToListAsync();

        //    //return join;

        //    //var result = from person in _dbContext.Person
        //    //    join detail in _dbContext.PersonDetails on person.Id equals detail.PersonId into Details
        //    //    from m in Details.DefaultIfEmpty()
        //    //    select new
        //    //    {
        //    //        id = person.Id,
        //    //        firstname = person.Firstname,
        //    //        lastname = person.Lastname,
        //    //        detailText = m.DetailText
        //    //    };

        //    //var result = from repairOptions in _context.RepairOptions
        //    //             join repairOptionModels in _context.RepairOptionModels on repairOptions.RepairOptionId equals repairOptionModels.RepairOptionId into Details
        //    //    from m in Details.DefaultIfEmpty().Where(d => d.ModelId== modelId)
        //    //    select new RepairOptionDto
        //    //    {
        //    //        RepairOptionId = repairOptions.RepairOptionId,
        //    //        Name = repairOptions.Name,

        //    //    };
           

        //}

        public bool GetRepairOptionByIdAsNoTracking(short id)
        {
            return _context.RepairOptions.Any(e => e.RepairOptionId == id);
        }

        public async Task<RepairOption> GetRepairOptionByName(string name)
        {
            return await _context.RepairOptions.Where(a => name.Contains(a.Name)).SingleAsync();
        }
        public async Task<Product> GetProductByType()
        {
            return await _context.Products.Where(a => a.Type.Equals("Service")).FirstOrDefaultAsync();
        }

        public async Task<bool> RepairOptionExists(short repairOptionId)
        {
            return await _context.RepairOptions.AnyAsync(t => t.RepairOptionId == repairOptionId);
        }

       public async Task AddRepairOption(RepairOption repairOption)
        {
            if (!await _context.RepairOptions.Where(a => a.Name == repairOption.Name || a.RepairOptionId == repairOption.RepairOptionId).AnyAsync())
            {
                await _context.AddAsync(repairOption);
            }
            else
            {
                throw new Exception("RepairOption Name Already Exists");
            }
        }

        // disable async warning - no code 
        public bool UpdateRepairOptionOk(short id,RepairOption repairOption)
        {
            //var entity = _context.RepairOptions.AsNoTracking().FirstOrDefault(d => d.RepairOptionId==id);
            var entity = _context.RepairOptions.Any(d => d.RepairOptionId == id);
            if (entity == false)
            {                
                return false;
                //return NotFound();
            }
            if (repairOption.RepairOptionId != id)
            {
                repairOption.RepairOptionId = id;

            }
            else { }


            // set original entity state to detached
           // _ctx.Entry(existingEG).State = EntityState.Detached;


            _context.RepairOptions.Attach(repairOption);
            _context.Entry(repairOption).State = EntityState.Modified;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeleteRepairOption(short id)
        {
            var RepairOption = new RepairOption { RepairOptionId = id };
            _context.RepairOptions.Attach(RepairOption);
            _context.Entry(RepairOption).State = EntityState.Deleted;
        }
  
    }
}
