﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Schedule;
using DRS.APA.Domain.Core.Domain.Shop;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Repositories.Shop;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{

    public class BookingRepository : IBookingRepository
    {
        private DRSAPAContext _context;

        public BookingRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Booking>> GetAllBookings()
        {
            return await  _context.Bookings
                .Include(m => m.Schedule)
                .Include(m => m.SalesOrder)
                .OrderBy(a => a.BookingId).ToListAsync();
        }
#pragma warning disable 1998
        public async Task<IQueryable<Booking>> GetAllBookings(bool withPagination)
#pragma warning restore 1998
        {
            return _context.Bookings
                .Include(m => m.Schedule)
                .Include(p => p.SalesOrder); //.Where(t => t.IsPrimary==true));
            //return await Task.Run(() => { _context.Bookings; });
        }

#pragma warning disable 1998
        public async Task<IQueryable<Booking>> GetAllBookings(ISpecification<Booking> spec)
#pragma warning restore 1998
#pragma warning restore 1998
        {
            return ApplySpecification(spec);
        }

        private IQueryable<Booking> ApplySpecification(ISpecification<Booking> spec)
        {
            return SpecificationEvaluator<Booking>.GetQuery(_context.Set<Booking>().AsQueryable(), spec);
        }
        public async Task<IEnumerable<Booking>> GetAllBookingsBySchedule(int scheduleId)
        {
            return await _context.Bookings
                 .Include(m => m.Schedule)
                 .Include(m => m.SalesOrder)
                 .Where(s => s.ScheduleId== scheduleId)
                 .OrderBy(a => a.BookingId).ToListAsync();
        }

        public async Task<IEnumerable<Booking>> GetAllBookingsByDate(DateTime date)
        {
            return await _context.Bookings
                .Include(m => m.Schedule)
                .Include(m => m.SalesOrder)
                .Where(s => s.BookingDate == date && s.Confirmed==true)
                .OrderBy(a => a.BookingDate).ToListAsync();
        }

        public async Task<Booking> GetBookingById(int id)
        {
            return await  _context.Bookings
                .Include(p => p.Schedule)
                .Include(p => p.Schedule)
                .Where(i => i.BookingId==id)
                .SingleAsync();
        }


        public bool GetBookingByIdAsNoTracking(int id)
        {
            return _context.Bookings.Any(e => e.BookingId == id);
        }

        public async Task<Booking> GetBookingByTitle(string name)
        {
            return await _context.Bookings.Where(a => name.Contains(a.Title)).SingleAsync();
        }

        //public async Task<bool> BookingExists(int scheduleId)
        //{
        //    return await _context.Bookings.AnyAsync(t => t.BookingId == scheduleId);
        //}

        public async Task AddBooking(Booking schedule)
        {
            //if (!await _context.Bookings.Where(a => a.Title == schedule.Title || a.BookingId == schedule.BookingId).AnyAsync())
            //{
                await _context.AddAsync(schedule);
            //}
            //else
            //{
            //    throw new Exception("Booking Title Already Exists");
            //}
        }

        // disable async warning - no code 
        public bool UpdateBookingOk(int id, Booking schedule)
        {
            //var entity = _context.Bookings.AsNoTracking().FirstOrDefault(d => d.BookingId==id);
            var entity = _context.Bookings.Any(d => d.BookingId == id);
            if (entity == false)
            {                
                return false;
                //return NotFound();
            }
            if (schedule.BookingId != id)
            {
                schedule.BookingId = id;

            }
            else { }

            // set original entity state to detached
           // _ctx.Entry(existingEG).State = EntityState.Detached;

            _context.Bookings.Attach(schedule);
            _context.Entry(schedule).State = EntityState.Modified;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeleteBooking(int id)
        {
            var schedule = new Booking { BookingId = id };
            _context.Bookings.Attach(schedule);
            _context.Entry(schedule).State = EntityState.Deleted;
        }
  
    }
}
