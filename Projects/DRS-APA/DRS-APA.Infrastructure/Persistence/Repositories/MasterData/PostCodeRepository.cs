﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    public class PostcodeRepository : IPostcodeRepository
    {
        private DRSAPAContext _context;

        public PostcodeRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Postcode>> GetAllPostcodes()
        {
            return await  _context.Postcodes
                .OrderBy(a => a.Name).ToListAsync();
        }
        public async Task<IQueryable<Postcode>> GetAllPostcodes(bool withPagination)
        {
            return _context.Postcodes;
            //return await Task.Run(() => { _context.Postcodes; });
        }

        public async Task<Postcode> GetPostcodeById(int id)
        {
            return await  _context.Postcodes.FindAsync(id);
        }

        public async Task<IEnumerable<PostcodeDeliveryMethod>> GetPostcodesByPostcode(int postcodeId)
        {
            return await _context.PostcodeDeliveryMethods
                .Where(d => d.PostcodeId == postcodeId)
                .Include(z => z.DeliveryMethod)
                .Include(z => z.Postcode)
                .ToListAsync();
        }

        public bool GetPostcodeByIdAsNoTracking(int id)
        {
            return _context.Postcodes.Any(e => e.PostcodeId == id);
        }

        public async Task<Postcode> GetPostcodeByName(string name)
        {
            try
            {
                return await _context.Postcodes
                    .Include(d => d.PostcodeDeliveryMethods)
                    .ThenInclude(s => s.DeliveryMethod)
                    .Where(a => name.Contains(a.Name))
                    .SingleAsync();
            }
            catch (Exception)
            {
                return null;
            }
           
        }
        public async Task<Product> GetProductByType()
        {
            return await _context.Products.Where(a => a.Type.Equals("Service")).FirstOrDefaultAsync();
        }

        public async Task<bool> PostcodeExists(int postcodeId)
        {
            return await _context.Postcodes.AnyAsync(t => t.PostcodeId == postcodeId);
        }

       public async Task AddPostcode(Postcode postcode)
        {
            if (!await _context.Postcodes.Where(a => a.Name == postcode.Name || a.PostcodeId == postcode.PostcodeId).AnyAsync())
            {
                await _context.AddAsync(postcode);
            }
            else
            {
                throw new Exception("Postcode Name Already Exists");
            }
        }

        // disable async warning - no code 
        public bool UpdatePostcodeOk(int id,Postcode postcode)
        {
            //var entity = _context.Postcodes.AsNoTracking().FirstOrDefault(d => d.PostcodeId==id);
            var entity = _context.Postcodes.Any(d => d.PostcodeId == id);
            if (entity == false)
            {                
                return false;
                //return NotFound();
            }
            if (postcode.PostcodeId != id)
            {
                postcode.PostcodeId = id;

            }
            else { }


            // set original entity state to detached
           // _ctx.Entry(existingEG).State = EntityState.Detached;


            _context.Postcodes.Attach(postcode);
            _context.Entry(postcode).State = EntityState.Modified;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeletePostcode(int id)
        {
            var Postcode = new Postcode { PostcodeId = id };
            _context.Postcodes.Attach(Postcode);
            _context.Entry(Postcode).State = EntityState.Deleted;
        }
  
    }
}
