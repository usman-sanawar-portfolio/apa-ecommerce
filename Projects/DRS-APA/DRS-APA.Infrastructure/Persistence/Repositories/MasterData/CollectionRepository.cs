﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Core.Domain;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Infrastructure.Persistence.Repositories;
using DRS.APA.Domain.Core;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;

namespace DRS.APA.Infrastructure.Repositories
{
    public class CollectionRepository : ICollectionRepository
    {
        private DRSAPAContext _context;

        public CollectionRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Collection>> GetAllCollections()
        {
            return await  _context.Collections
                .OrderBy(a => a.Popular).ToListAsync();
        }
        public async Task<IQueryable<Collection>> GetAllCollections(bool withPagination)
        {
            return _context.Collections;
        }

        public async Task<Collection> GetCollectionById(int id)
        {
            return await  _context.Collections.FindAsync(id);
        }

       
        public bool GetCollectionByIdAsNoTracking(int id)
        {
            return _context.Collections.Any(e => e.CollectionId == id);
        }


        public async Task<Collection> GetCollectionByName(string name)
        {
            return await _context.Collections.Where(a => name.Contains(a.Name)).SingleAsync();
        }
        public async Task<bool> CollectionExists(int collectionId)
        {
            return await _context.Collections.AnyAsync(t => t.CollectionId == collectionId);
        }

       public async Task AddCollection(Collection collection)
        {
            if (!await _context.Collections.Where(a => a.Name == collection.Name || a.CollectionId == collection.CollectionId).AnyAsync())
            {
                await _context.AddAsync(collection);
            }
            else
            {
                throw new Exception("Collection Name Already Exists");
            }
        }

        // disable async warning - no code 
        public bool UpdateCollectionOk(int id,Collection Collection)
        {
            //var entity = _context.Collections.AsNoTracking().FirstOrDefault(d => d.CollectionId==id);
            var entity = _context.Collections.Any(d => d.CollectionId == id);
            if (entity == false)
            {                
                return false;
                //return NotFound();
            }
            if (Collection.CollectionId != id)
            {
                Collection.CollectionId = id;

            }
            else { }

            _context.Collections.Attach(Collection);
            _context.Entry(Collection).State = EntityState.Modified;
            _context.Entry(Collection).Property(x => x.CollectionId).IsModified = false;
            _context.Entry(Collection).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(Collection).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeleteCollection(int id)
        {
            var Collection = new Collection { CollectionId = id };
            _context.Collections.Attach(Collection);
            _context.Entry(Collection).State = EntityState.Deleted;
        }
  
    }
}
