using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Repositories.Common;
using DRS.APA.Microserivce.Domain.Core.Domain.Newsletter;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    public class CustomerRepository : ICustomerRepository
    {
        private ICommonFunctions _commonFunctions;
        private DRSAPAContext _context;
        public CustomerRepository(DRSAPAContext context, ICommonFunctions commonFunctions)
        {
            _context = context;
            _commonFunctions = commonFunctions;
        }

        public async Task<IEnumerable<Customer>> GetAllCustomers()
        {
            return await _context.Customers.Include(c => c.CustomerGroup)
                .OrderBy(a => a.CustomerId).ToListAsync();
        }
        public async Task<IQueryable<Customer>> GetAllCustomers(bool withPagination)
        {
            return _context.Customers.Include(c => c.CustomerGroup);
        }

        public async Task<Customer> GetCustomerById(int id)
        {
            return await _context.Customers.Include(c => c.CustomerAddress)
                .Where(i => i.CustomerId == id).Include(c => c.Blog).Include(x => x.BlogCommentAuthor).Include(x => x.Review)
                .SingleAsync();
        }
        public async Task<Customer> GetCustomerByIdDetailed(int id)
        {
            return await _context.Customers
                .Include(c => c.SaleOrders).ThenInclude(c => c.OrderStatus)
                .Include(c => c.Orders).ThenInclude(c => c.OrderStatus)
                .Include(c => c.CustomerAddress)
                .Include(c => c.Blog).Include(x => x.BlogCommentAuthor).Include(x => x.Review)
                   .Where(i => i.CustomerId == id)
                   .FirstOrDefaultAsync();
        }
        public async Task<Customer> GetCustomerWithSaleOrderById(int id)
        {
            return await _context.Customers.Include(c => c.CustomerAddress)
                .Include(c => c.SaleOrders).ThenInclude(c => c.OrderStatus)
                .Where(i => i.CustomerId == id)
                .SingleAsync();
        }
        public async Task<Customer> GetCustomerWithOrderById(int id)
        {
            return await _context.Customers.Include(c => c.Orders).ThenInclude(c => c.OrderStatus)
                .Include(c => c.CustomerAddress)
                .Where(i => i.CustomerId == id)
                .SingleAsync();
        }
        public bool GetCustomerByIdAsNoTracking(int id)
        {
            return _context.Customers.Any(e => e.CustomerId == id);
        }

        public async Task<bool> CustomerExists(int customerId)
        {
            return await _context.Customers.AnyAsync(t => t.CustomerId == customerId);
        }

        public async Task AddCustomer(Customer customer)
        {
            if (!await _context.Customers.Where(a => a.Email == customer.Email).AnyAsync())
            {
                customer.Password = _commonFunctions.ConvertToEncrypt(customer.Password);
                await _context.AddAsync(customer);
            }
            else
            {
                throw new Exception("Customer Already Exists With This Email.");
            }
        }

        // disable async warning - no code 
        public bool UpdateCustomerOk(int id, Customer customer)
        {

            var entity = _context.Customers.Any(d => d.CustomerId == id);
            if (entity == false)
            {
                return false;
            }
            if (customer.CustomerId != id)
            {
                customer.CustomerId = id;
            }

            _context.Customers.Attach(customer);
            _context.Entry(customer).State = EntityState.Modified;

            _context.Entry(customer).Property(x => x.Email).IsModified = false;
            _context.Entry(customer).Property(x => x.Password).IsModified = false;
            _context.Entry(customer).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(customer).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        public bool UpdateCustomerNewsLetterSub(int customerId, bool newsletter)
        {
            var entity = _context.Customers.Where(d => d.CustomerId == customerId).FirstOrDefault();
            if (entity == null)
            {
                return false;
            }
            entity.Newsletter = newsletter;
            UpdateCustomerOk(customerId, entity);

            var SubEntity = _context.Subscribers.AsNoTracking().FirstOrDefault(d => d.Email == entity.Email);
            if (SubEntity == null)
            {
                Subscriber subscriber = new Subscriber();
                subscriber.IsActive = newsletter;
                subscriber.Email = entity.Email;
                _context.AddAsync(subscriber);
                return true;
            }
            SubEntity.IsActive = newsletter;
            _context.Subscribers.Attach(SubEntity);
            _context.Entry(SubEntity).State = EntityState.Modified;
            _context.Entry(SubEntity).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(SubEntity).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }

        // disable async warning - no RemoveAsync available
        public void DeleteCustomer(int id)
        {
            var Customer = new Customer { CustomerId = id };
            _context.Customers.Attach(Customer);
            _context.Entry(Customer).State = EntityState.Deleted;
        }

        // Address Methods
        public async Task AddCustomerAddress(CustomerAddress customerAddress)
        {
            await _context.AddAsync(customerAddress);
        }

        public bool UpdateCustomerAddressOk(int id, CustomerAddress customerAddress)
        {
            var entity = _context.CustomerAddress.Any(d => d.CustomerAddressId == id);
            if (entity == false)
            {
                return false;
            }
            if (customerAddress.CustomerAddressId != id)
            {
                customerAddress.CustomerAddressId = id;
            }
            _context.CustomerAddress.Attach(customerAddress);

            _context.Entry(customerAddress).State = EntityState.Modified;
            _context.Entry(customerAddress).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(customerAddress).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }

        public void DeleteCustomerAddress(int id)
        {
            var customerAddress = new CustomerAddress { CustomerAddressId = id };
            _context.CustomerAddress.Attach(customerAddress);
            _context.Entry(customerAddress).State = EntityState.Deleted;
        }
        public async Task<CustomerAddress> GetCustomerAddressById(int id)
        {
            return await _context.CustomerAddress.FirstOrDefaultAsync(x => x.CustomerAddressId == id);
        }

        public bool GetCustomerAddressByIdAsNoTracking(int id)
        {
            return _context.CustomerAddress.Any(e => e.CustomerAddressId == id);
        }
        public bool ActivateCustomer(string ActivationCode)
        {
            var entity = _context.Customers.Where(d => d.ActivationCode == ActivationCode).AsNoTracking()
                .FirstOrDefault();
            if (entity == null)
            {
                return false;
            }
            Customer user = new Customer();
            user.CustomerId = entity.CustomerId;
            user.IsEmailVerified = true;
            _context.Customers.Attach(user);
            _context.Entry(user).Property(x => x.IsEmailVerified).IsModified = true;
            return true;
        }
        public bool ChangeCustomerPassword(int id, string ActivationCode, Customer customer)
        {
            var entity = _context.Customers.Where(d => d.CustomerId == id).FirstOrDefault();
            if (entity == null)
            {
                return false;
            }
            entity.Password = _commonFunctions.ConvertToEncrypt(customer.Password);
            entity.ActivationCode = ActivationCode;
            _context.Customers.Attach(entity);
            _context.Entry(entity).Property(x => x.ActivationCode).IsModified = true;
            _context.Entry(entity).Property(x => x.Password).IsModified = true;
            _context.SaveChanges();

            return true;
        }

        public bool VerifyPassword(int customerId, string oldPassword)
        {
            var entity = _context.Customers.Where(d => d.CustomerId == customerId).FirstOrDefault();

            if (entity.Password == _commonFunctions.ConvertToEncrypt(oldPassword))
            {
                return true;
            }
            return false;


        }
        public bool AssignResetToken(string resetToken, string email)
        {
            var entity = _context.Customers.Where(d => d.Email == email).FirstOrDefault();
            if (entity == null)
            {
                return false;
            }
            entity.ActivationCode = resetToken;
            _context.Customers.Attach(entity);
            _context.Entry(entity).Property(x => x.ActivationCode).IsModified = true;
            _context.SaveChanges();
            return true;
        }
        public bool ChangePasswordAfterEmail(Customer customer)
        {
            var entity = _context.Customers.Where(d => d.ActivationCode == customer.ActivationCode).FirstOrDefault();
            if (entity == null)
            {
                return false;
            }
            entity.Password = _commonFunctions.ConvertToEncrypt(customer.Password);
            entity.IsEmailVerified = true;
            _context.Customers.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
            return true;
        }
        public bool ChangePasswordByAdmin(Customer customer)
        {
            var entity = _context.Customers.Where(d => d.CustomerId == customer.CustomerId).FirstOrDefault();
            if (entity == null)
            {
                return false;
            }
            entity.Password = _commonFunctions.ConvertToEncrypt(customer.Password);
            _context.Customers.Attach(entity);
            _context.Entry(entity).Property(x => x.Password).IsModified = true;
            _context.SaveChanges();

            return true;
        }
        public bool CheckEmailExistCustomer(string email)
        {
            var entity = _context.Customers.Where(d => d.Email == email).FirstOrDefault();
            if (entity == null)
            {
                return false;
            }
            return true;

        }
    }
}
