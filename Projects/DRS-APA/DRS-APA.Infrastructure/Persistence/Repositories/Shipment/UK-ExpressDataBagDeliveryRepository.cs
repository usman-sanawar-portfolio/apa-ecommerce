using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using DRS.APA.Microserivce.Domain.Core.Repositories.Shipment;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Shipment
{
    public class UK_ExpressDataBagDeliveryRepository  : IUK_ExpressDataBagDeliveryRepository
  {
        private DRSAPAContext _context;

        public UK_ExpressDataBagDeliveryRepository(DRSAPAContext context)
        {
            _context = context;
        }
    public async Task<IEnumerable<UK_ExpressDataBagDelivery>> GetAllUK_ExpressDataBagDelivery()
    {
      return await _context.UK_ExpressDataBagDeliveries.OrderBy(a => a.Duration).ToListAsync();
    }
    public async Task<IQueryable<UK_ExpressDataBagDelivery>> GetAllUK_ExpressDataBagDelivery(bool withPagination)
    {
       return _context.UK_ExpressDataBagDeliveries;
    }
    public async Task<UK_ExpressDataBagDelivery> GetUK_ExpressDataBagDeliveryById(int id)
    {
      return await _context.UK_ExpressDataBagDeliveries.FirstOrDefaultAsync(x => x.UK_ExpressDataBagDeliveryId == id);
    }
    public bool GetUK_ExpressDataBagDeliveryByIdAsNoTracking(int id)
    {
      return _context.UK_ExpressDataBagDeliveries.Any(e => e.UK_ExpressDataBagDeliveryId == id);
    }
    public async Task<bool> UK_ExpressDataBagDeliveryExists(int uk_ExpressDataBagDeliveryId)
    {
      return await _context.UK_ExpressDataBagDeliveries.AnyAsync(t => t.UK_ExpressDataBagDeliveryId == uk_ExpressDataBagDeliveryId);
    }
    public async Task AddUK_ExpressDataBagDelivery(UK_ExpressDataBagDelivery uk_ExpressDataBagDelivery)
    {
      await _context.AddAsync(uk_ExpressDataBagDelivery);
    }
    // disable async warning - no code
    public bool UpdateUK_ExpressDataBagDeliveryOk(int id, UK_ExpressDataBagDelivery uk_ExpressDataBagDelivery)
    {
      var entity = _context.UK_ExpressDataBagDeliveries.Any(d => d.UK_ExpressDataBagDeliveryId == id);
      if (entity == false)
      {
        return false;
      }
      if (uk_ExpressDataBagDelivery.UK_ExpressDataBagDeliveryId != id)
      {
        uk_ExpressDataBagDelivery.UK_ExpressDataBagDeliveryId = id;

      }
      _context.UK_ExpressDataBagDeliveries.Attach(uk_ExpressDataBagDelivery);
      _context.Entry(uk_ExpressDataBagDelivery).State = EntityState.Modified;
      return true;
    }
    // disable async warning - no RemoveAsync available
    public void DeleteUK_ExpressDataBagDelivery(int id)
    {
      var uk_ExpressDataBagDelivery = new UK_ExpressDataBagDelivery { UK_ExpressDataBagDeliveryId = id };
      _context.UK_ExpressDataBagDeliveries.Attach(uk_ExpressDataBagDelivery);
      _context.Entry(uk_ExpressDataBagDelivery).State = EntityState.Deleted;
    }
    public bool UpdateStatus(int id, bool status)
    {
      var found = _context.UK_ExpressDataBagDeliveries.Any(d => d.UK_ExpressDataBagDeliveryId == id);
      if (found == false)
      {
        return false;
      }
      var entity = new UK_ExpressDataBagDelivery() { UK_ExpressDataBagDeliveryId = id };
      entity.IsActive = status;
      _context.UK_ExpressDataBagDeliveries.Attach(entity);
      _context.Entry(entity).Property(x => x.IsActive).IsModified = true;
      return true;
    }

  }
}
