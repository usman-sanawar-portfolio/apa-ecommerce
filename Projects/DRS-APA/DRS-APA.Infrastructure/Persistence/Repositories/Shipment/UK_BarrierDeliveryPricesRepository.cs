using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using DRS.APA.Microserivce.Domain.Core.Repositories.Shipment;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Shipment
{
    public class UK_BarrierDeliveryPricesRepository  : IUK_BarrierDeliveryPricesRepository
  {
        private DRSAPAContext _context;

        public UK_BarrierDeliveryPricesRepository(DRSAPAContext context)
        {
            _context = context;
        }
    public async Task<IEnumerable<UK_BarrierDeliveryPrices>> GetAllUK_BarrierDeliveryPrices()
    {
      return await _context.UK_BarrierDeliveryPrices.OrderBy(a => a.Qty).ToListAsync();
    }
    public async Task<IQueryable<UK_BarrierDeliveryPrices>> GetAllUK_BarrierDeliveryPrices(bool withPagination)
    {
       return _context.UK_BarrierDeliveryPrices;
    }
    public async Task<UK_BarrierDeliveryPrices> GetUK_BarrierDeliveryPricesById(int id)
    {
      return await _context.UK_BarrierDeliveryPrices.FirstOrDefaultAsync(x => x.UK_BarrierDeliveryPricesId == id);
    }
    public bool GetUK_BarrierDeliveryPricesByIdAsNoTracking(int id)
    {
      return _context.UK_BarrierDeliveryPrices.Any(e => e.UK_BarrierDeliveryPricesId == id);
    }
    public async Task<bool> UK_BarrierDeliveryPricesExists(int uk_BarrierDeliveryPricesId)
    {
      return await _context.UK_BarrierDeliveryPrices.AnyAsync(t => t.UK_BarrierDeliveryPricesId == uk_BarrierDeliveryPricesId);
    }
    public async Task AddUK_BarrierDeliveryPrices(UK_BarrierDeliveryPrices uk_BarrierDeliveryPrices)
    {
      await _context.AddAsync(uk_BarrierDeliveryPrices);
    }
    // disable async warning - no code 
    public bool UpdateUK_BarrierDeliveryPricesOk(int id, UK_BarrierDeliveryPrices uk_BarrierDeliveryPrices)
    {
      var entity = _context.UK_BarrierDeliveryPrices.Any(d => d.UK_BarrierDeliveryPricesId == id);
      if (entity == false)
      {
        return false;
      }
      if (uk_BarrierDeliveryPrices.UK_BarrierDeliveryPricesId != id)
      {
        uk_BarrierDeliveryPrices.UK_BarrierDeliveryPricesId = id;

      }
      _context.UK_BarrierDeliveryPrices.Attach(uk_BarrierDeliveryPrices);

       
      _context.Entry(uk_BarrierDeliveryPrices).State = EntityState.Modified;
      _context.Entry(uk_BarrierDeliveryPrices).Property(x => x.CreatedBy).IsModified = false;
      _context.Entry(uk_BarrierDeliveryPrices).Property(x => x.CreatedOn).IsModified = false;

      return true;
    }
    // disable async warning - no RemoveAsync available
    public void DeleteUK_BarrierDeliveryPrices(int id)
    {
      var uk_BarrierDeliveryPrices = new UK_BarrierDeliveryPrices { UK_BarrierDeliveryPricesId = id };
      _context.UK_BarrierDeliveryPrices.Attach(uk_BarrierDeliveryPrices);
      _context.Entry(uk_BarrierDeliveryPrices).State = EntityState.Deleted;
    }
    public bool UpdateStatus(int id, bool status)
    {
      var found = _context.UK_BarrierDeliveryPrices.Any(d => d.UK_BarrierDeliveryPricesId == id);
      if (found == false)
      {
        return false;
      }
      var entity = new UK_BarrierDeliveryPrices() { UK_BarrierDeliveryPricesId = id };
      entity.IsActive = status;
      _context.UK_BarrierDeliveryPrices.Attach(entity);
      _context.Entry(entity).Property(x => x.IsActive).IsModified = true;
      return true;
    }

  }
}
