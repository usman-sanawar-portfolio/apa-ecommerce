using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using DRS.APA.Microserivce.Domain.Core.Repositories.Shipment;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Shipment
{
    public class ApplicableTaxOnShipmentRepository : IApplicableTaxOnShipmentRepository
    {
        private DRSAPAContext _context;

        public ApplicableTaxOnShipmentRepository(DRSAPAContext context)
        {
            _context = context;
        }
    public async Task<IEnumerable<ApplicableTaxOnShipment>> GetAllApplicableTaxOnShipment()
    {
      return await _context.ApplicableTaxOnShipments.OrderBy(a => a.TaxClass.TaxClassTitle).Include(x=>x.TaxClass).ToListAsync();
    }
    public async Task<IQueryable<ApplicableTaxOnShipment>> GetAllApplicableTaxOnShipment(bool withPagination)
    {
       return _context.ApplicableTaxOnShipments.Include(x => x.TaxClass);
    }
    public async Task<ApplicableTaxOnShipment> GetApplicableTaxOnShipmentById(int id)
    {
      return await _context.ApplicableTaxOnShipments.Include(x => x.TaxClass).FirstOrDefaultAsync(x => x.ApplicableTaxOnShipmentId == id);
    }
    public bool GetApplicableTaxOnShipmentByIdAsNoTracking(int id)
    {
      return _context.ApplicableTaxOnShipments.Any(e => e.ApplicableTaxOnShipmentId == id);
    }
    public async Task<bool> ApplicableTaxOnShipmentExists(int ApplicableTaxOnShipmentId)
    {
      return await _context.ApplicableTaxOnShipments.AnyAsync(t => t.ApplicableTaxOnShipmentId == ApplicableTaxOnShipmentId);
    }
    public async Task AddApplicableTaxOnShipment(ApplicableTaxOnShipment ApplicableTaxOnShipment)
    {
      await _context.AddAsync(ApplicableTaxOnShipment);
    }
    // disable async warning - no code 
    public bool UpdateApplicableTaxOnShipmentOk(int id, ApplicableTaxOnShipment ApplicableTaxOnShipment)
    {
      var entity = _context.ApplicableTaxOnShipments.Any(d => d.ApplicableTaxOnShipmentId == id);
      if (entity == false)
      {
        return false;
      }
      if (ApplicableTaxOnShipment.ApplicableTaxOnShipmentId != id)
      {
        ApplicableTaxOnShipment.ApplicableTaxOnShipmentId = id;

      }
      _context.ApplicableTaxOnShipments.Attach(ApplicableTaxOnShipment);
      _context.Entry(ApplicableTaxOnShipment).State = EntityState.Modified;
      return true;
    }
    // disable async warning - no RemoveAsync available
    public void DeleteApplicableTaxOnShipment(int id)
    {
      var ApplicableTaxOnShipment = new ApplicableTaxOnShipment { ApplicableTaxOnShipmentId = id };
      _context.ApplicableTaxOnShipments.Attach(ApplicableTaxOnShipment);
      _context.Entry(ApplicableTaxOnShipment).State = EntityState.Deleted;
    }
    public bool UpdateStatus(int id, bool status)
    {
      var found = _context.ApplicableTaxOnShipments.Any(d => d.ApplicableTaxOnShipmentId == id);
      if (found == false)
      {
        return false;
      }
      var entity = new ApplicableTaxOnShipment() { ApplicableTaxOnShipmentId = id };
      entity.IsActive = status;
      _context.ApplicableTaxOnShipments.Attach(entity);
      _context.Entry(entity).Property(x => x.IsActive).IsModified = true;
      return true;
    }

  }
}
