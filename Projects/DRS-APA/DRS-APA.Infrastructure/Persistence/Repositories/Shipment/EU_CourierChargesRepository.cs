using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using DRS.APA.Microserivce.Domain.Core.Repositories.Shipment;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Shipment
{
    public class EU_CourierChargesRepository : IEU_CourierChargesRepository
    {
        private DRSAPAContext _context;

        public EU_CourierChargesRepository(DRSAPAContext context)
        {
            _context = context;
        }
    public async Task<IEnumerable<EU_CourierCharges>> GetAllEU_CourierCharges()
    {
      return await _context.EU_CourierCharges.OrderBy(a => a.First20KgPrice).ToListAsync();
    }
    public async Task<IQueryable<EU_CourierCharges>> GetAllEU_CourierCharges(bool withPagination)
    {
       return _context.EU_CourierCharges;
    }
    public async Task<EU_CourierCharges> GetEU_CourierChargesById(int id)
    {
      return await _context.EU_CourierCharges.FirstOrDefaultAsync(x => x.EU_CourierChargesId == id);
    }
    public bool GetEU_CourierChargesByIdAsNoTracking(int id)
    {
      return _context.EU_CourierCharges.Any(e => e.EU_CourierChargesId == id);
    }
    public async Task<bool> EU_CourierChargesExists(int eu_CourierChargesId)
    {
      return await _context.EU_CourierCharges.AnyAsync(t => t.EU_CourierChargesId == eu_CourierChargesId);
    }
    public async Task AddEU_CourierCharges(EU_CourierCharges eu_CourierCharges)
    {
      await _context.AddAsync(eu_CourierCharges);
    }
    // disable async warning - no code 
    public bool UpdateEU_CourierChargesOk(int id, EU_CourierCharges eu_CourierCharges)
    {
      var entity = _context.EU_CourierCharges.Any(d => d.EU_CourierChargesId == id);
      if (entity == false)
      {
        return false;
      }
      if (eu_CourierCharges.EU_CourierChargesId != id)
      {
        eu_CourierCharges.EU_CourierChargesId = id;

      }
      _context.EU_CourierCharges.Attach(eu_CourierCharges);
      _context.Entry(eu_CourierCharges).State = EntityState.Modified;
      return true;
    }
    // disable async warning - no RemoveAsync available
    public void DeleteEU_CourierCharges(int id)
    {
      var eu_CourierCharges = new EU_CourierCharges { EU_CourierChargesId = id };
      _context.EU_CourierCharges.Attach(eu_CourierCharges);
      _context.Entry(eu_CourierCharges).State = EntityState.Deleted;
    }
    public bool UpdateStatus(int id, bool status)
    {
      var found = _context.EU_CourierCharges.Any(d => d.EU_CourierChargesId == id);
      if (found == false)
      {
        return false;
      }
      var entity = new EU_CourierCharges() { EU_CourierChargesId = id };
      entity.IsActive = status;
      _context.EU_CourierCharges.Attach(entity);
      _context.Entry(entity).Property(x => x.IsActive).IsModified = true;
      return true;
    }

  }
}
