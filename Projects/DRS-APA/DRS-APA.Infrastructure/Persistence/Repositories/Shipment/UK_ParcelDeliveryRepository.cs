﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    public class UK_ParcelDeliveryRepository : IUK_ParcelDeliveryRepository
    {
        private DRSAPAContext _context;

        public UK_ParcelDeliveryRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<UK_ParcelDelivery>> GetAllUK_ParcelDeliveries()
        {
            return await _context.UK_ParcelDeliveries
                 .OrderBy(a => a.Duration).ToListAsync();
        }
        public async Task<IQueryable<UK_ParcelDelivery>> GetAllUK_ParcelDeliveries(bool withPagination)
        {
            return _context.UK_ParcelDeliveries;


        }
        public async Task<UK_ParcelDelivery> GetUK_ParcelDeliveryById(int id)
        {
            return await _context.UK_ParcelDeliveries
                 .Where(i => i.UK_ParcelDeliveryId == id)
                 .FirstOrDefaultAsync();
        }


        public bool GetUK_ParcelDeliveryByIdAsNoTracking(int id)
        {
            return _context.UK_ParcelDeliveries.Any(e => e.UK_ParcelDeliveryId == id);
        }

        public async Task<bool> UK_ParcelDeliveryExists(int parcelDelivery_UKId)
        {
            return await _context.UK_ParcelDeliveries.AnyAsync(t => t.UK_ParcelDeliveryId == parcelDelivery_UKId);
        }

        public async Task AddUK_ParcelDelivery(UK_ParcelDelivery parcelDelivery_UK)
        {

            await _context.AddAsync(parcelDelivery_UK);
          
        }

        public bool UpdateUK_ParcelDeliveryOk(int id, UK_ParcelDelivery parcelDelivery_UK)
        {

            var entity = _context.UK_ParcelDeliveries.AsNoTracking().FirstOrDefault(d => d.UK_ParcelDeliveryId == id);

          
            if (entity == null)
            {
                return false;
            }
            if (parcelDelivery_UK.UK_ParcelDeliveryId != id)
            {
                parcelDelivery_UK.UK_ParcelDeliveryId = id;
            }


            _context.UK_ParcelDeliveries.Attach(parcelDelivery_UK);
            _context.Entry(parcelDelivery_UK).State = EntityState.Modified;
            _context.Entry(parcelDelivery_UK).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(parcelDelivery_UK).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeleteUK_ParcelDelivery(int id)
        {
            var parcelDelivery_UK = new UK_ParcelDelivery { UK_ParcelDeliveryId = id };
            _context.UK_ParcelDeliveries.Attach(parcelDelivery_UK);
            _context.Entry(parcelDelivery_UK).State = EntityState.Deleted;
        }


    }
}
