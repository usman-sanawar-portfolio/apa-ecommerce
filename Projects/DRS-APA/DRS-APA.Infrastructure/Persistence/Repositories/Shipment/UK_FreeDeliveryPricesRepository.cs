﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    public class UK_FreeDeliveryPricesRepository : IUK_FreeDeliveryPricesRepository
    {
        private DRSAPAContext _context;

        public UK_FreeDeliveryPricesRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<UK_FreeDeliveryPrices>> GetAllUK_FreeDeliveryPrices()
        {
            return await _context.UK_FreeDeliveryPrices
                 .OrderBy(a => a.FreeOrderStartsFrom).ToListAsync();
        }
        public async Task<IQueryable<UK_FreeDeliveryPrices>> GetAllUK_FreeDeliveryPrices(bool withPagination)
        {
            return _context.UK_FreeDeliveryPrices;


        }
        //public async Task<IQueryable<UK_FreeDeliveryPrices>> GetAllUK_FreeDeliveryPrices(ISpecification<UK_FreeDeliveryPrices> spec)
        //{
        //    return ApplySpecification(spec);
        //}

        //private IQueryable<UK_FreeDeliveryPrices> ApplySpecification(ISpecification<UK_FreeDeliveryPrices> spec)
        //{
        //    return SpecificationEvaluator<UK_FreeDeliveryPrices>.GetQuery(_context.Set<UK_FreeDeliveryPrices>().AsQueryable(), spec);
        //}



        public async Task<UK_FreeDeliveryPrices> GetUK_FreeDeliveryPricesById(int id)
        {
            return await _context.UK_FreeDeliveryPrices
                .Where(i => i.UK_FreeDeliveryPricesId == id)
                 .FirstOrDefaultAsync();
        }


        public bool GetUK_FreeDeliveryPricesByIdAsNoTracking(int id)
        {
            return _context.UK_FreeDeliveryPrices.Any(e => e.UK_FreeDeliveryPricesId == id);
        }

        public async Task<bool> UK_FreeDeliveryPricesExists(int freeDeliveryPrices_UKId)
        {
            return await _context.UK_FreeDeliveryPrices.AnyAsync(t => t.UK_FreeDeliveryPricesId == freeDeliveryPrices_UKId);
        }

        public async Task AddUK_FreeDeliveryPrices(UK_FreeDeliveryPrices freeDeliveryPrices_UK)
        {


            //if (!await _context.UK_FreeDeliveryPrices.Where(a => a.Text == freeDeliveryPrices_UK.Text).AnyAsync())
            //{
            await _context.AddAsync(freeDeliveryPrices_UK);
            //}
            //else
            //{
            //    throw new Exception("UK_FreeDeliveryPrices Name Already Exists");
            //}
        }

        // disable async warning - no code 
        public bool UpdateUK_FreeDeliveryPricesOk(int id, UK_FreeDeliveryPrices freeDeliveryPrices_UK)
        {

            var entity = _context.UK_FreeDeliveryPrices.AsNoTracking().FirstOrDefault(d => d.UK_FreeDeliveryPricesId == id);

            //var entity = _context.UK_FreeDeliveryPrices.Any(d => d.UK_FreeDeliveryPricesId == id);
            if (entity == null)
            {
                return false;
            }
            if (freeDeliveryPrices_UK.UK_FreeDeliveryPricesId != id)
            {
                freeDeliveryPrices_UK.UK_FreeDeliveryPricesId = id;
            }


            _context.UK_FreeDeliveryPrices.Attach(freeDeliveryPrices_UK);
            _context.Entry(freeDeliveryPrices_UK).State = EntityState.Modified;
            _context.Entry(freeDeliveryPrices_UK).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(freeDeliveryPrices_UK).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeleteUK_FreeDeliveryPrices(int id)
        {
            var freeDeliveryPrices_UK = new UK_FreeDeliveryPrices { UK_FreeDeliveryPricesId = id };
            _context.UK_FreeDeliveryPrices.Attach(freeDeliveryPrices_UK);
            _context.Entry(freeDeliveryPrices_UK).State = EntityState.Deleted;
        }

    }
}
