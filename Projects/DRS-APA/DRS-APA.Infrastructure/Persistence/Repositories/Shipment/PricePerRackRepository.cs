using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using DRS.APA.Microserivce.Domain.Core.Repositories.Shipment;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Shipment
{
    public class PricePerRackRepository : IPricePerRackRepository
    {
        private DRSAPAContext _context;

        public PricePerRackRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<PricePerRack>> GetAllPricePerRack()
        {
            return await _context.PricePerRacks.OrderBy(a => a.Price).ToListAsync();
        }
        public async Task<IQueryable<PricePerRack>> GetAllPricePerRack(bool withPagination)
        {
            return _context.PricePerRacks;
        }
        public async Task<PricePerRack> GetPricePerRackById(int id)
        {
            return await _context.PricePerRacks.FirstOrDefaultAsync(x => x.PricePerRackId == id);
        }
        public bool GetPricePerRackByIdAsNoTracking(int id)
        {
            return _context.PricePerRacks.Any(e => e.PricePerRackId == id);
        }
        public async Task<bool> PricePerRackExists(int PricePerRackId)
        {
            return await _context.PricePerRacks.AnyAsync(t => t.PricePerRackId == PricePerRackId);
        }
        public async Task AddPricePerRack(PricePerRack PricePerRack)
        {
            await _context.AddAsync(PricePerRack);
        }
        // disable async warning - no code 
        public bool UpdatePricePerRackOk(int id, PricePerRack PricePerRack)
        {
            var entity = _context.PricePerRacks.Any(d => d.PricePerRackId == id);
            if (entity == false)
            {
                return false;
            }
            if (PricePerRack.PricePerRackId != id)
            {
                PricePerRack.PricePerRackId = id;

            }
            _context.PricePerRacks.Attach(PricePerRack);
            _context.Entry(PricePerRack).State = EntityState.Modified;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeletePricePerRack(int id)
        {
            var PricePerRack = new PricePerRack { PricePerRackId = id };
            _context.PricePerRacks.Attach(PricePerRack);
            _context.Entry(PricePerRack).State = EntityState.Deleted;
        }
        public bool UpdateStatus(int id, bool status)
        {
            var found = _context.PricePerRacks.Any(d => d.PricePerRackId == id);
            if (found == false)
            {
                return false;
            }
            var entity = new PricePerRack() { PricePerRackId = id };
            entity.IsActive = status;
            _context.PricePerRacks.Attach(entity);
            _context.Entry(entity).Property(x => x.IsActive).IsModified = true;
            return true;
        }

    }
}
