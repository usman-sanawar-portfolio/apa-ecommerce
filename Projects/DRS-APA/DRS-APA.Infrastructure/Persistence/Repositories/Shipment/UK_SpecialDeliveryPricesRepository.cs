﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Interfaces;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;

namespace DRS.APA.Infrastructure.Persistence.Repositories.MasterData
{
    public class UK_SpecialDeliveryPricesRepository : IUK_SpecialDeliveryPricesRepository
    {
        private DRSAPAContext _context;

        public UK_SpecialDeliveryPricesRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<UK_SpecialDeliveryPrices>> GetAllUK_SpecialDeliveryPrices()
        {
            return await _context.UK_SpecialDeliveryPrices
                 .OrderBy(a => a.RetrofitDeliveryCost).ToListAsync();
        }
        public async Task<IQueryable<UK_SpecialDeliveryPrices>> GetAllUK_SpecialDeliveryPrices(bool withPagination)
        {
            return _context.UK_SpecialDeliveryPrices;


        }


        public async Task<UK_SpecialDeliveryPrices> GetUK_SpecialDeliveryPricesById(int id)
        {
            return await _context.UK_SpecialDeliveryPrices
                .Where(i => i.UK_SpecialDeliveryPricesId == id)
                 .FirstOrDefaultAsync();
        }


        public bool GetUK_SpecialDeliveryPricesByIdAsNoTracking(int id)
        {
            return _context.UK_SpecialDeliveryPrices.Any(e => e.UK_SpecialDeliveryPricesId == id);
        }

        public async Task<bool> UK_SpecialDeliveryPricesExists(int specialDeliveryPrices_UKId)
        {
            return await _context.UK_SpecialDeliveryPrices.AnyAsync(t => t.UK_SpecialDeliveryPricesId == specialDeliveryPrices_UKId);
        }

        public async Task AddUK_SpecialDeliveryPrices(UK_SpecialDeliveryPrices specialDeliveryPrices_UK)
        {


            //if (!await _context.UK_SpecialDeliveryPrices.Where(a => a.Text == specialDeliveryPrices_UK.Text).AnyAsync())
            //{
            await _context.AddAsync(specialDeliveryPrices_UK);
            //}
            //else
            //{
            //    throw new Exception("UK_SpecialDeliveryPrices Name Already Exists");
            //}
        }
        public bool UpdateUK_SpecialDeliveryPricesOk(int id, UK_SpecialDeliveryPrices specialDeliveryPrices_UK)
        {

            var entity = _context.UK_SpecialDeliveryPrices.AsNoTracking().FirstOrDefault(d => d.UK_SpecialDeliveryPricesId == id);

            if (entity == null)
            {
                return false;
            }
            if (specialDeliveryPrices_UK.UK_SpecialDeliveryPricesId != id)
            {
                specialDeliveryPrices_UK.UK_SpecialDeliveryPricesId = id;
            }


            _context.UK_SpecialDeliveryPrices.Attach(specialDeliveryPrices_UK);
            _context.Entry(specialDeliveryPrices_UK).State = EntityState.Modified;
            _context.Entry(specialDeliveryPrices_UK).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(specialDeliveryPrices_UK).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        // disable async warning - no RemoveAsync available
        public void DeleteUK_SpecialDeliveryPrices(int id)
        {
            var specialDeliveryPrices_UK = new UK_SpecialDeliveryPrices { UK_SpecialDeliveryPricesId = id };
            _context.UK_SpecialDeliveryPrices.Attach(specialDeliveryPrices_UK);
            _context.Entry(specialDeliveryPrices_UK).State = EntityState.Deleted;
        }

    }
}
