using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using DRS.APA.Microserivce.Domain.Core.Repositories.Shipment;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Shipment
{
    public class BarriersPerRackRepository : IBarriersPerRackRepository
    {
        private DRSAPAContext _context;

        public BarriersPerRackRepository(DRSAPAContext context)
        {
            _context = context;
        }
    public async Task<IEnumerable<BarriersPerRack>> GetAllBarriersPerRack()
    {
      return await _context.BarriersPerRacks.OrderBy(a => a.BarrierPerRackQty).ToListAsync();
    }
    public async Task<IQueryable<BarriersPerRack>> GetAllBarriersPerRack(bool withPagination)
    {
       return _context.BarriersPerRacks;
    }
    public async Task<BarriersPerRack> GetBarriersPerRackById(int id)
    {
      return await _context.BarriersPerRacks.FirstOrDefaultAsync(x => x.BarriersPerRackId == id);
    }
    public bool GetBarriersPerRackByIdAsNoTracking(int id)
    {
      return _context.BarriersPerRacks.Any(e => e.BarriersPerRackId == id);
    }
    public async Task<bool> BarriersPerRackExists(int barriersPerRackId)
    {
      return await _context.BarriersPerRacks.AnyAsync(t => t.BarriersPerRackId == barriersPerRackId);
    }
    public async Task AddBarriersPerRack(BarriersPerRack barriersPerRack)
    {
      await _context.AddAsync(barriersPerRack);
    }
    // disable async warning - no code 
    public bool UpdateBarriersPerRackOk(int id, BarriersPerRack barriersPerRack)
    {
      var entity = _context.BarriersPerRacks.Any(d => d.BarriersPerRackId == id);
      if (entity == false)
      {
        return false;
      }
      if (barriersPerRack.BarriersPerRackId != id)
      {
        barriersPerRack.BarriersPerRackId = id;

      }
      _context.BarriersPerRacks.Attach(barriersPerRack);
      _context.Entry(barriersPerRack).State = EntityState.Modified;
      return true;
    }
    // disable async warning - no RemoveAsync available
    public void DeleteBarriersPerRack(int id)
    {
      var barriersPerRack = new BarriersPerRack { BarriersPerRackId = id };
      _context.BarriersPerRacks.Attach(barriersPerRack);
      _context.Entry(barriersPerRack).State = EntityState.Deleted;
    }
    public bool UpdateStatus(int id, bool status)
    {
      var found = _context.BarriersPerRacks.Any(d => d.BarriersPerRackId == id);
      if (found == false)
      {
        return false;
      }
      var entity = new BarriersPerRack() { BarriersPerRackId = id };
      entity.IsActive = status;
      _context.BarriersPerRacks.Attach(entity);
      _context.Entry(entity).Property(x => x.IsActive).IsModified = true;
      return true;
    }

  }
}
