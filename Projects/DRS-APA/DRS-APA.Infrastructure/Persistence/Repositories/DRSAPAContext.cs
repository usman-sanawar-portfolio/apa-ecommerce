using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Core.Repositories;
using DRS.APA.Infrastructure.EntityConfigurations.MasterData;
using DRS.APA.Domain.Core.Domain.Common;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Infrastructure.EntityConfigurations.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Infrastructure.EntityConfiguration;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using DRS.APA.Infrastructure.EntityConfigurations.Order;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using DRS.APA.Infrastructure.EntityConfigurations.Shipment;
using DRS.APA.Infrastructure.EntityConfigurations.Website;
using DRS.APA.Domain.Core.Domain.JWT;
using DRS.APA.Microserivce.Domain.Core.Domain.Newsletter;
using DRS.APA.Microserivce.Domain.Core.Repositories.Common;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace DRS.APA.Infrastructure.Persistence.Repositories
{
    public class DRSAPAContext : DbContext
    {
        public const string DEFAULT_SCHEMA = "APA";
        public DbSet<DeliveryMethod> DeliveryMethods { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductPhoto> ProductPhotos { get; set; }
        public DbSet<Collection> Collections { get; set; }
        public DbSet<CollectionProduct> CollectionProducts { get; set; }
        public DbSet<Discount> Discounts { get; set; }
        public DbSet<DiscountProduct> DiscountProducts { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerGroup> CustomerGroups { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Zone> Zones { get; set; }
        public DbSet<GeoZones> GeoZones { get; set; }
        public DbSet<Length> Lengths { get; set; }
        public DbSet<Weight> Weights { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<StockStatus> StockStatus { get; set; }
        public DbSet<TaxClass> TaxClass { get; set; }
        public DbSet<TaxRate> TaxRates { get; set; }
        public DbSet<Information> Information { get; set; }
        public DbSet<Store> Stores { get; set; }
        public DbSet<TaxRatesCustomerGroups> TaxRatesCustomerGroups { get; set; }
        public DbSet<OrderStatus> OrderStatus { get; set; }
        public DbSet<ReturnStatus> ReturnStatus { get; set; }
        public DbSet<ReturnAction> ReturnActions { get; set; }
        public DbSet<ReturnReason> ReturnReasons { get; set; }
        public DbSet<Option> Options { get; set; }
        public DbSet<OptionType> OptionTypes { get; set; }
        public DbSet<OptionValue> OptionValues { get; set; }
        public DbSet<WebPage> WebPages { get; set; }
        public DbSet<WebComponent> WebComponents { get; set; }
        public DbSet<WebPageComponent> WebPageComponents { get; set; }
        public DbSet<WebCard> WebCards { get; set; }
        public DbSet<WebDoc> WebDocs { get; set; }
        public DbSet<WebPhoto> WebPhotos { get; set; }
        public DbSet<WebHtml> WebHtmls { get; set; }
        public DbSet<WebVideo> WebVideos { get; set; }
        public DbSet<WebMenu> WebMenus { get; set; }
        public DbSet<WebSubMenu> WebSubMenus { get; set; }
        public DbSet<ProductCategoriesJunction> ProductCategoriesJunctions { get; set; }
        public DbSet<ProductOptions> ProductOptions { get; set; }
        public DbSet<ProductOptionCombination> ProductOptionCombinations { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<FileLibrary> FileLibraries { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<SaleOrder> SaleOrders { get; set; }
        public DbSet<SaleOrderLines> SaleOrderLines { get; set; }
        public DbSet<SaleOrderAddress> SaleOrderAddress { get; set; }
        public DbSet<HomePageCollection> HomePageCollection { get; set; }
        public DbSet<WebCarousal> WebCarousal { get; set; }
        public DbSet<WebCarousalPhotos> WebCarousalPhotos { get; set; }
        public DbSet<WebBanner> WebBanners { get; set; }
        public DbSet<CustomerAddress> CustomerAddress { get; set; }
        public DbSet<SaleOrderLineProductOptions> SaleOrderLineProductOptions { get; set; }
        public DbSet<SaleOrderLineProductOptionCombinations> SaleOrderLineProductOptionCombinations { get; set; }
        public DbSet<OrderLines> OrderLines { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public DbSet<OrderLineProductOptions> OrderLineProductOptions { get; set; }
        public DbSet<OrderLineProductOptionCombinations> OrderLineProductOptionCombinations { get; set; }
        public DbSet<OrderAddress> OrderAddress { get; set; }
        public DbSet<SaleOrderLineTaxRate> SaleOrderLineTaxRates { get; set; }
        public DbSet<OrderLineTaxRate> OrderLineTaxRates { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<BarriersPerRack> BarriersPerRacks { get; set; }
        public DbSet<UK_BarrierDeliveryPrices> UK_BarrierDeliveryPrices { get; set; }
        public DbSet<UK_ExpressDataBagDelivery> UK_ExpressDataBagDeliveries { get; set; }
        public DbSet<UK_FreeDeliveryPrices> UK_FreeDeliveryPrices { get; set; }
        public DbSet<UK_ParcelDelivery> UK_ParcelDeliveries { get; set; }
        public DbSet<UK_SpecialDeliveryPrices> UK_SpecialDeliveryPrices { get; set; }
        public DbSet<DiscountCustomerGroup> DiscountCustomerGroups { get; set; }
        public DbSet<OrderIdentifier> OrderIdentifiers { get; set; }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<BlogCategory> BlogCategories { get; set; }
        public DbSet<BlogComment> BlogComments { get; set; }
        public DbSet<CategoryCode> CategoryCodes { get; set; }
        public DbSet<PricePerRack> PricePerRacks { get; set; }
        public DbSet<EU_CourierCharges> EU_CourierCharges { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<UserPermission> UserPermissions { get; set; }
        public DbSet<Newsletter> Newsletters { get; set; }
        public DbSet<NewsletterSent> NewsletterSent { get; set; }
        public DbSet<Subscriber> Subscribers { get; set; }
        public DbSet<ApplicableTaxOnShipment> ApplicableTaxOnShipments { get; set; }
        public DbSet<RolePermission> RolePermissions { get; set; }
        public DbSet<Role> Roles { get; set; }
    

        public DbSet<ZoneToGeoZone> ZoneToGeoZones { get; set; }

        private readonly IUserInfoService _userInfoService;
        private readonly IConfiguration _config;
        private IHttpContextAccessor _httpContextAccessor;

        public DRSAPAContext()
        {
        }

        public DRSAPAContext(DbContextOptions<DRSAPAContext> options, IConfiguration config, IHttpContextAccessor httpContextAccessor)
              : base(options)
        {
            _httpContextAccessor = httpContextAccessor;
            _config = config;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DeliveryMethodEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProductCategoryEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProductEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProductPhotoEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CollectionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CollectionProductEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DiscountEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DiscountProductEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerGroupEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CountryEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ZoneEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new GeoZones_EntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new Length_EntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new Weight_EntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ManufacturerEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new StockStatusEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TaxClassEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TaxRateEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new InformationEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new StoreEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TaxRatesCustomerGroupsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderStatusEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ReturnStatusEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ReturnActionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ReturnReasonEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OptionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OptionValueEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OptionTypeEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new WebComponentEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WebPageEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WebPageComponentEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new WebCardEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WebDocEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WebHtmlEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WebPhotoEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WebVideoEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new WebMenuEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WebSubMenuEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new ProductCategoriesJunctionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProductOptionsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProductOptionCombinationEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ReviewEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new FileLibraryEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new LocationEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new OrganizationEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new SaleOrderEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new SaleOrderLinesEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new SaleOrderAddressEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new HomePageCollectionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WebCarousalEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WebCarousalPhotosEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WebBannerEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerAddressEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new SaleOrderLineProductOptionsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new SaleOrderLineProductOptionCombinationsEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new OrderEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderLinesEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderLineProductOptionsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderLineProductOptionCombinationsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderAddressEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new SaleOrderLineTaxRateEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new SaleOrderLineTaxRateEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AttachmentEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderLineTaxRateEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new BarriersPerRackEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UK_BarrierDeliveryPricesEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UK_ExpressDataBagDeliveryntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UK_ParcelDeliveryEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UK_SpecialDeliveryPricesEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UK_FreeDeliveryPricesEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DiscountCustomerGroupEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderIdentifiersEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new BlogCategoryEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new BlogCommentEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new BlogEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CategoryCodeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PricePerRackEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new EU_CourierChargesEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new SaleOrderLineProductOptionCombinationsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PermissionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserPermissionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new SubscriberEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new NewsletterEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new NewsletterSentEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ApplicableTaxOnShipmentEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ZoneToGeoZone_EntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RoleEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RolePermissionEntityTypeConfiguration());
      
    }

        public DRSAPAContext(DbContextOptions<DRSAPAContext> options,
                IUserInfoService userInfoService)
                : base(options)
        {
            _userInfoService = userInfoService ?? throw new ArgumentNullException(nameof(userInfoService));
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return null;
        }
        public Task<int> SaveChangesStoreAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return null;
        }
        public override int SaveChanges()
        {
            var entities = from e in ChangeTracker.Entries()
                           where e.State == EntityState.Added
                                 || e.State == EntityState.Modified
                           select e.Entity;

            foreach (var entity in entities)
            {
                var validationContext = new ValidationContext(entity);
                Validator.ValidateObject(entity, validationContext);
            }

            return base.SaveChanges();
        }
    }
}
