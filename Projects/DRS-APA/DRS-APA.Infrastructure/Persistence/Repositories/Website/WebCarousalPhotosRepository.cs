﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Domain.Core.Repositories.Website;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Website
{
    public class WebCarousalPhotosRepository : IWebCarousalPhotosRepository
    {
        private DRSAPAContext _context;
        public WebCarousalPhotosRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<WebCarousalPhotos>> GetAllWebCarousalPhotos()
        {
            return await _context.WebCarousalPhotos.Include(x => x.WebCarousal).OrderBy(a => a.WebCarousalPhotosId).ToListAsync();
        }
        public async Task<IQueryable<WebCarousalPhotos>> GetAllWebCarousalPhotos(bool withPagination)
        {
            return _context.WebCarousalPhotos.Include(x => x.WebCarousal);
        }
        public async Task<WebCarousalPhotos> GetWebCarousalPhotosById(int id)
        {
            return await _context.WebCarousalPhotos.Include(x => x.WebCarousal).Where(c => c.WebCarousalPhotosId == id).FirstOrDefaultAsync();
        }

        public async Task AddWebCarousalPhotos(WebCarousalPhotos WebCarousalPhotos)
        {
            await _context.AddAsync(WebCarousalPhotos);
        }
        public bool UpdateWebCarousalPhotosOk(int id, WebCarousalPhotos WebCarousalPhotos)
        {            
            var entity = _context.WebCarousalPhotos.Any(d => d.WebCarousalPhotosId == id);
            if (entity == false)
            {
                return false;
            }
            if (WebCarousalPhotos.WebCarousalPhotosId != id)
            {
                WebCarousalPhotos.WebCarousalPhotosId = id;
            }         
            _context.WebCarousalPhotos.Attach(WebCarousalPhotos);
            _context.Entry(WebCarousalPhotos).State = EntityState.Modified;
            _context.Entry(WebCarousalPhotos).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(WebCarousalPhotos).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        public void DeleteWebCarousalPhotos(int id)
        {
            var WebCarousalPhotos = new WebCarousalPhotos { WebCarousalPhotosId = id };
            _context.WebCarousalPhotos.Attach(WebCarousalPhotos);
            _context.Entry(WebCarousalPhotos).State = EntityState.Deleted;
        }
        public bool GetWebCarousalPhotosByIdAsNoTracking(int id)
        {
            return _context.WebCarousalPhotos.Any(e => e.WebCarousalPhotosId == id);
        }
    }
}
