﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Domain.Core.Repositories.Website;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Website
{
    public class WebPageRepository : IWebPageRepository
    {
        private DRSAPAContext _context;
        public WebPageRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<WebPage>> GetAllWebPages()
        {
            return await _context.WebPages.OrderBy(a => a.WebPageTitle).ToListAsync();
        }
        public async Task<IQueryable<WebPage>> GetAllWebPages(bool withPagination)
        {
            return _context.WebPages;
        }
        public async Task<WebPage> GetWebPageById(short id)
        {
            return await _context.WebPages
                .Include(x => x.WebMenu).ThenInclude(x => x.WebSubMenu)
                .Where(x => x.WebPageId == id).FirstOrDefaultAsync();
        }
        public bool GetWebPageByIdAsNoTracking(short id)
        {
            return _context.WebPages.Any(e => e.WebPageId == id);
        }
        public async Task<WebPage> GetWebPageByName(string name)
        {
            return await _context.WebPages.Where(a => name.Contains(a.WebPageTitle)).SingleOrDefaultAsync();
        }
        public async Task<bool> WebPageExists(short WebPageId)
        {
            return await _context.WebPages.AnyAsync(t => t.WebPageId == WebPageId);
        }
        public async Task AddWebPage(WebPage WebPage)
        {
            if (!await _context.WebPages.Where(a => a.Slug == WebPage.Slug
            || string.IsNullOrWhiteSpace(WebPage.Slug)
            || string.IsNullOrWhiteSpace(WebPage.WebPageTitle)).AnyAsync())
            {
                await _context.AddAsync(WebPage);
            }
            else
            {
                if (string.IsNullOrWhiteSpace(WebPage.WebPageTitle) || string.IsNullOrWhiteSpace(WebPage.Slug))
                {
                    throw new Exception("Empty WebPageTitle OR Slug is not allowed.");
                }
                throw new Exception("Slug Already Exists");
            }
        }

        public bool UpdateWebPageOk(short id, WebPage WebPage)
        {
            if (string.IsNullOrWhiteSpace(WebPage.WebPageTitle) || string.IsNullOrWhiteSpace(WebPage.Slug))
            {
                return false;
            }
            var entity = _context.WebPages.Any(d => d.WebPageId == id);
            if (entity == false)
            {
                return false;
            }
            if (WebPage.WebPageId != id)
            {
                WebPage.WebPageId = id;
            }


            _context.WebPages.Attach(WebPage);
            _context.Entry(WebPage).State = EntityState.Modified;
            _context.Entry(WebPage).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(WebPage).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        public void DeleteWebPage(short id)
        {
            var WebPage = new WebPage { WebPageId = id };
            _context.WebPages.Attach(WebPage);
            _context.Entry(WebPage).State = EntityState.Deleted;
        }
        public async Task<bool> IsWebPageTitleExisting(string WebPageTitle)
        {
            return await _context.WebPages.AnyAsync(t => t.WebPageTitle == WebPageTitle);
        }
        public async Task<bool> IsSlugExisting(string Slug)
        {
            return await _context.WebPages.AnyAsync(t => t.Slug == Slug);
        }
    }
}
