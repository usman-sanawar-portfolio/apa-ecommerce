﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Domain.Core.Repositories.Website;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Website
{
    public class WebVideoRepository : IWebVideoRepository
    {
        private DRSAPAContext _context;
        public WebVideoRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<WebVideo>> GetAllWebVideos()
        {
            return await _context.WebVideos.Include(x => x.WebComponent).OrderBy(a => a.Order).ToListAsync();
        }
        public async Task<IQueryable<WebVideo>> GetAllWebVideos(bool withPagination)
        {
            return _context.WebVideos.Include(x => x.WebComponent);
        }
        public async Task<WebVideo> GetWebVideoById(short id)
        {
            return await _context.WebVideos.Include(x => x.WebComponent).Where(x=>x.WebVideoId==id).FirstOrDefaultAsync();
        }
        public async Task<IEnumerable<WebVideo>> GetWebVideoByComponentId(short id)
        {
            return await _context.WebVideos.Include(x => x.WebComponent).Where(x => x.WebComponentId == id).ToListAsync();
        }
        public async Task AddWebVideo(WebVideo WebVideo)
        {
            if (WebVideo.WebComponentId > 0)
            {
                await _context.AddAsync(WebVideo);
            }
            else
            {
                throw new Exception("Cannot add Web Video without component.");
            }
        }

        public bool UpdateWebVideoOk(short id, WebVideo WebVideo)
        {
            if (WebVideo.WebComponentId <= 0)
            {
                return false;
            }
            var entity = _context.WebVideos.Any(d => d.WebVideoId == id);
            if (entity == false)
            {
                return false;
            }
            if (WebVideo.WebVideoId != id)
            {
                WebVideo.WebVideoId = id;
            }
            else { }

            _context.WebVideos.Attach(WebVideo);
            _context.Entry(WebVideo).State = EntityState.Modified;
            _context.Entry(WebVideo).Property(x => x.WebComponentId).IsModified = false;
            _context.Entry(WebVideo).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(WebVideo).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        public void DeleteWebVideo(short id)
        {
            var WebVideo = new WebVideo { WebVideoId = id };
            _context.WebVideos.Attach(WebVideo);
            _context.Entry(WebVideo).State = EntityState.Deleted;
        }
        public bool GetWebVideoByIdAsNoTracking(short id)
        {
            return _context.WebVideos.Any(e => e.WebVideoId == id);
        }
    }
}
