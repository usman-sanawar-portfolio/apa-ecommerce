using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Domain.Core.Repositories.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Website
{
    public class HomePageCollectionRepository : IHomePageCollectionRepository
    {
        private DRSAPAContext _context;
        public HomePageCollectionRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<HomePageCollection>> GetAllHomePageCollection()
        {
            ICollection<Product> productCollection = new List<Product>();
            ICollection<ProductCategoriesJunction> productCategoriesJunctionCollection = new List<ProductCategoriesJunction>();
            Product productObj = new Product();

             var homePageCollections =await _context.HomePageCollection
                .Include(x => x.ProductCategory)
                .ThenInclude(x => x.ProductCategoriesJunction)
                .ThenInclude(x => x.Product).ThenInclude(c=>c.ProductPhotos)
                   .Include(x => x.ProductCategory)
                .ThenInclude(x => x.ProductCategoriesJunction)
                .ThenInclude(x => x.Product).ThenInclude(c => c.Store)
                  .Include(x => x.ProductCategory)
                .ThenInclude(x => x.ProductCategoriesJunction)
                .ThenInclude(x => x.Product).ThenInclude(c => c.Reviews)
                  .Include(x => x.ProductCategory)
                .ThenInclude(x => x.ProductCategoriesJunction)
                .ThenInclude(x => x.Product).ThenInclude(c => c.Manufacturer)
                    .Include(x => x.ProductCategory)
                .ThenInclude(x => x.ProductCategoriesJunction)
                .ThenInclude(x => x.Product).ThenInclude(c => c.Store)
                    .Include(x => x.ProductCategory)
                .ThenInclude(x => x.ProductCategoriesJunction)
                .ThenInclude(x => x.Product).ThenInclude(c => c.ProductCategoriesJunction)
                     .Include(x => x.ProductCategory)
                .ThenInclude(x => x.ProductCategoriesJunction)
                .ThenInclude(x => x.Product).ThenInclude(c => c.DiscountProducts).ThenInclude(c => c.Discount).ThenInclude(c => c.DiscountCustomerGroups)
                      .Include(x => x.ProductCategory)
                .ThenInclude(x => x.ProductCategoriesJunction)
                .ThenInclude(x => x.Product).ThenInclude(c => c.TaxClass).ThenInclude(c => c.TaxRates).ThenInclude(c => c.TaxRatesCustomerGroups)
                 .Include(x => x.ProductCategory)
              .ThenInclude(x => x.ProductCategoriesJunction)
         .ThenInclude(x => x.Product).ThenInclude(c => c.ProductOptions).ThenInclude(x => x.ProductOptionCombination).ThenInclude(x => x.Option).ThenInclude(x => x.OptionType).ThenInclude(x => x.OptionValues)
            .OrderBy(a => a.HomePageCollectionId).ToListAsync();
                    return homePageCollections;
        }

        public async Task<HomePageCollection> GetHomePageCollectionById(short id)
        {
            return await _context.HomePageCollection
                .Include(x => x.ProductCategory)
                .ThenInclude(x => x.ProductCategoriesJunction)
                .ThenInclude(x => x.Product)
                .Where(c => c.HomePageCollectionId == id).FirstOrDefaultAsync();
        }
        public async Task<IEnumerable<HomePageCollection>> GetHomePageCollectionByComponentId(short id)
        {
            return await _context.HomePageCollection
                .Include(x => x.ProductCategory)
                .ThenInclude(x => x.ProductCategoriesJunction)
                .ThenInclude(x => x.Product)
                .Where(x => x.ProductCategoryId == id).ToListAsync();
        }
        public async Task AddHomePageCollection(HomePageCollection HomePageCollection)
        {
            if (HomePageCollection.ProductCategoryId > 0)
            {
                await _context.AddAsync(HomePageCollection);
            }
            else
            {
                throw new Exception("Cannot add.");
            }
        }

        public bool UpdateHomePageCollectionOk(short id, HomePageCollection HomePageCollection)
        {

            if (HomePageCollection.ProductCategoryId <= 0)
            {
                return false;
            }
            var entity = _context.HomePageCollection.Any(d => d.HomePageCollectionId == id);
            if (entity == false)
            {
                return false;
            }
            if (HomePageCollection.HomePageCollectionId != id)
            {
                HomePageCollection.HomePageCollectionId = id;
            }
            else { }

            _context.HomePageCollection.Attach(HomePageCollection);
            _context.Entry(HomePageCollection).State = EntityState.Modified;
            _context.Entry(HomePageCollection).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(HomePageCollection).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        public void DeleteHomePageCollection(short id)
        {
            var HomePageCollection = new HomePageCollection { HomePageCollectionId = id };
            _context.HomePageCollection.Attach(HomePageCollection);
            _context.Entry(HomePageCollection).State = EntityState.Deleted;
        }
        public bool GetHomePageCollectionByIdAsNoTracking(short id)
        {
            return _context.HomePageCollection.Any(e => e.HomePageCollectionId == id);
        }
    }
}
