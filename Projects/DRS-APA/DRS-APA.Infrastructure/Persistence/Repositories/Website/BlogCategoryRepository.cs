using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Repositories.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using Microsoft.EntityFrameworkCore;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Website
{
  public class BlogCategoryRepository : IBlogCategoryRepository
  {
    private DRSAPAContext _context;

    public BlogCategoryRepository(DRSAPAContext context)
    {
      _context = context;
    }
    public async Task<IEnumerable<BlogCategory>> GetAllBlogCategories()
    {
      return await _context.BlogCategories.OrderBy(a => a.CategoryName).ToListAsync();
    }
    public async Task<IQueryable<BlogCategory>> GetAllBlogCategories(bool withPagination)
    {
      return _context.BlogCategories;
    }
    public async Task<BlogCategory> GetBlogCategoryById(int id)
    {
      return await _context.BlogCategories.Include(c => c.Blog).FirstOrDefaultAsync(x => x.BlogCategoryId == id);
    }
    public async Task<bool> BlogCategoryExists(int blogCategoryId)
    {
      return await _context.BlogCategories.AnyAsync(t => t.BlogCategoryId == blogCategoryId);
    }
    public async Task AddBlogCategory(BlogCategory blogCategory)
    {
      await _context.AddAsync(blogCategory);
    }
    public void DeleteBlogCategory(int id)
    {
      var blog = new BlogCategory { BlogCategoryId = id };
      _context.BlogCategories.Attach(blog);
      _context.Entry(blog).State = EntityState.Deleted;
    }
    public bool UpdateBlogCategoryOk(int id, BlogCategory blogCategory)
    {
      var entity = _context.BlogCategories.Any(d => d.BlogCategoryId == id);
      if (entity == false)
      {
        return false;
      }
      if (blogCategory.BlogCategoryId != id)
      {
        blogCategory.BlogCategoryId = id;

      }
      _context.BlogCategories.Attach(blogCategory);
      _context.Entry(blogCategory).State = EntityState.Modified;
      return true;
    }

    public bool GetBlogCategoryByIdAsNoTracking(int id)
    {
      return _context.BlogCategories.Any(e => e.BlogCategoryId == id);
    }

  }
}
