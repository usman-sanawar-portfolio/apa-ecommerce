using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Repositories.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using Microsoft.EntityFrameworkCore;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Website
{
  public class BlogCommentRepository : IBlogCommentRepository
  {
    private DRSAPAContext _context;

    public BlogCommentRepository(DRSAPAContext context)
    {
      _context = context;
    }
    public async Task<IEnumerable<BlogComment>> GetAllBlogComments()
    {
      return await _context.BlogComments.Include(x=>x.Blog).OrderBy(a => a.Comment).Include(x => x.Customer).ToListAsync();
    }
    public async Task<IQueryable<BlogComment>> GetAllBlogComments(bool withPagination)
    {
      return _context.BlogComments.Include(x => x.Blog).Include(x => x.Customer);
    }

    public async Task<BlogComment> GetBlogCommentById(int id)
    {
      return await _context.BlogComments.Include(x => x.Blog).FirstOrDefaultAsync(x => x.BlogCommentId == id);
    }
    public async Task<bool> BlogCommentExists(int blogCommentId)
    {
      return await _context.BlogComments.AnyAsync(t => t.BlogCommentId == blogCommentId);
    }
    public async Task AddBlogComment(BlogComment blogComment)
    {
      await _context.AddAsync(blogComment);
    }
    public void DeleteBlogComment(int id)
    {
      var blogComment = new BlogComment { BlogCommentId = id };
      _context.BlogComments.Attach(blogComment);
      _context.Entry(blogComment).State = EntityState.Deleted;
    }
    public bool UpdateBlogCommentOk(int id, BlogComment blogComment)
    {
      var entity = _context.BlogComments.Any(d => d.BlogCommentId == id);
      if (entity == false)
      {
        return false;
      }
      if (blogComment.BlogCommentId != id)
      {
        blogComment.BlogCommentId = id;

      }
      _context.BlogComments.Attach(blogComment);
      _context.Entry(blogComment).State = EntityState.Modified;
      return true;
    }

    public bool GetBlogCommentByIdAsNoTracking(int id)
    {
      return _context.BlogComments.Any(e => e.BlogCommentId == id);
    }

  }
}
