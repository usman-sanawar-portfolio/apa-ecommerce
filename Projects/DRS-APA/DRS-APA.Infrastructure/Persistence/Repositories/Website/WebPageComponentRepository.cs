﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Repositories.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using System.Security.Cryptography.X509Certificates;
using System.Runtime.CompilerServices;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Website
{
    public class WebPageComponentRepository : IWebPageComponentRepository
    {
        private DRSAPAContext _context;
        public WebPageComponentRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<WebPageComponent>> GetAllWebPageComponents()
        {
            return await _context.WebPageComponents
                .Include(x => x.WebComponent)
                .Include(x => x.WebPage).OrderBy(a => a.WebComponent.Title).ToListAsync();
        }
        public async Task<IQueryable<WebPageComponent>> GetAllWebPageComponents(bool withPagination)
        {
            return _context.WebPageComponents.Include(x => x.WebComponent).Include(x => x.WebPage);
        }
        public async Task<WebPageComponent> GetWebPageComponentById(short componentId, short pageId)
        {
            return await _context.WebPageComponents
                .Include(x => x.WebComponent)
                .Include(x => x.WebComponent.WebCards)
                .Include(x => x.WebComponent.WebDocs)
                .Include(x => x.WebComponent.WebHtmls)
                .Include(x => x.WebComponent.WebPhotos)
                .Include(x => x.WebComponent.WebVideos)
                .Include(x => x.WebPage)
                .Where(e => e.WebComponentId == componentId && e.WebPageId == pageId).FirstOrDefaultAsync();
        }
        public async Task<ICollection<WebPageComponent>> GetWebPageComponentsOnPageId(short pageId)
        {
            return await _context.WebPageComponents.Include(x => x.WebComponent)
                .Include(x => x.WebComponent.WebCards)
                .Include(x => x.WebComponent.WebDocs)
                .Include(x => x.WebComponent.WebHtmls)
                .Include(x => x.WebComponent.WebPhotos)
                .Include(x => x.WebComponent.WebVideos)
                .Include(x => x.WebPage)
                .Where(e => e.WebPageId == pageId).ToListAsync();
        }
        public async Task<ICollection<WebPageComponent>> GetWebPageComponentsOnSlug(string slug)
        {
            WebPage webPage = await _context.WebPages.Where(x => x.Slug.ToUpper() == slug.ToUpper()).FirstOrDefaultAsync();
            if (webPage != null)
            {
                return await _context.WebPageComponents.Include(x => x.WebComponent)
                    .Include(x => x.WebComponent.WebCards)
                    .Include(x => x.WebComponent.WebDocs)
                    .Include(x => x.WebComponent.WebHtmls)
                    .Include(x => x.WebComponent.WebPhotos)
                    .Include(x => x.WebComponent.WebVideos)
                    .Include(x => x.WebPage)
                    .Where(e => e.WebPageId == webPage.WebPageId).ToListAsync();
            }
            return null;
        }

        public async Task<ICollection<WebPageComponent>> GetWebPageComponentsOnComponentId(short componentId)
        {
            return await _context.WebPageComponents.Include(x => x.WebComponent).Include(x => x.WebPage)
                .Where(e => e.WebComponentId == componentId).ToListAsync();
        }

        public async Task AddWebPageComponent(ICollection<WebPageComponent> WebPageComponent)
        {
            foreach (var v in WebPageComponent)
            {
                v.WebComponent = null;
                await _context.AddAsync(v);
            }
        }
        public void DeleteWebPageComponent(short componentId, short pageId)
        {
            var WebPageComponent = new WebPageComponent { WebComponentId = componentId, WebPageId = pageId };
            _context.WebPageComponents.Attach(WebPageComponent);
            _context.Entry(WebPageComponent).State = EntityState.Deleted;
        }
        public bool UpdateWebPageComponentOk(short webComponentId, short webPageId, WebPageComponent WebPageComponent)
        {
            _context.WebPageComponents.Attach(WebPageComponent);
            _context.Entry(WebPageComponent).State = EntityState.Modified;
            _context.Entry(WebPageComponent).Property(x => x.WebComponentId).IsModified = false;
            _context.Entry(WebPageComponent).Property(x => x.WebPageId).IsModified = false;
            _context.Entry(WebPageComponent).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(WebPageComponent).Property(x => x.CreatedOn).IsModified = false;

            return true;
        }

        public bool GetWebPageComponentByIdAsNoTracking(short componentId, short pageId)
        {
            return _context.WebPageComponents.Include(x => x.WebComponent).Include(x => x.WebPage)
                .Any(e => e.WebComponentId == componentId && e.WebPageId == pageId);
        }
        public async Task<bool> WebPageComponentExists(short componentId, short pageId)
        {
            return await _context.WebPageComponents.AnyAsync(t => t.WebComponentId == componentId && t.WebPageId == pageId);
        }





    }
}
