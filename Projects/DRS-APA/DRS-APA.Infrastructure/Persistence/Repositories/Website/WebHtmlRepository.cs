﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Domain.Core.Repositories.Website;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Website
{
    public class WebHtmlRepository : IWebHtmlRepository
    {
        private DRSAPAContext _context;
        public WebHtmlRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<WebHtml>> GetAllWebHtmls()
        {
            return await _context.WebHtmls.Include(x => x.WebComponent).OrderBy(a => a.Order).ToListAsync();
        }
        public async Task<IQueryable<WebHtml>> GetAllWebHtmls(bool withPagination)
        {
            return _context.WebHtmls.Include(x => x.WebComponent);
        }
        public async Task<WebHtml> GetWebHtmlById(short id)
        {
            return await _context.WebHtmls.Include(x => x.WebComponent).Where(x=>x.WebHtmlId==id).FirstOrDefaultAsync();
        }
        public async Task<IEnumerable<WebHtml>> GetWebHtmlByComponentId(short id)
        {
            return await _context.WebHtmls.Include(x => x.WebComponent).Where(x => x.WebComponentId == id).ToListAsync();
        }
        public async Task AddWebHtml(WebHtml WebHtml)
        {
            if (WebHtml.WebComponentId > 0)
            {
                await _context.AddAsync(WebHtml);
            }
            else
            {
                throw new Exception("Cannot add Web HTML without component.");
            }
        }

        public bool UpdateWebHtmlOk(short id, WebHtml WebHtml)
        {
            if (WebHtml.WebComponentId <= 0)
            {
                return false;
            }
            var entity = _context.WebHtmls.Any(d => d.WebHtmlId == id);
            if (entity == false)
            {
                return false;
            }
            if (WebHtml.WebHtmlId != id)
            {
                WebHtml.WebHtmlId = id;
            }
            else { }

            _context.WebHtmls.Attach(WebHtml);
            _context.Entry(WebHtml).State = EntityState.Modified;
            _context.Entry(WebHtml).Property(x => x.WebComponentId).IsModified = false;
            _context.Entry(WebHtml).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(WebHtml).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        public void DeleteWebHtml(short id)
        {
            var WebHtml = new WebHtml { WebHtmlId = id };
            _context.WebHtmls.Attach(WebHtml);
            _context.Entry(WebHtml).State = EntityState.Deleted;
        }
        public bool GetWebHtmlByIdAsNoTracking(short id)
        {
            return _context.WebHtmls.Any(e => e.WebHtmlId == id);
        }
    }
}
