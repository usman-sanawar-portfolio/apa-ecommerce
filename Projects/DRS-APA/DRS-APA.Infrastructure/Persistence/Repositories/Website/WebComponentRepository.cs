﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Repositories.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Website
{
    public class WebComponentRepository : IWebComponentRepository
    {
        private DRSAPAContext _context;
        public WebComponentRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<WebComponent>> GetAllWebComponents()
        {
            return await _context.WebComponents.OrderBy(a => a.Title).ToListAsync();
        }
        public async Task<IQueryable<WebComponent>> GetAllWebComponents(bool withPagination)
        {
            return _context.WebComponents;
        }
        public async Task<WebComponent> GetWebComponentById(short id)
        {
            return await _context.WebComponents.FindAsync(id);
        }
        public bool GetWebComponentByIdAsNoTracking(short id)
        {
            return _context.WebComponents.Any(e => e.WebComponentId == id);
        }
        public async Task<WebComponent> GetWebComponentByName(string name)
        {
            return await _context.WebComponents.Where(a => name.Contains(a.Title)).SingleOrDefaultAsync();
        }
        public async Task<bool> WebComponentExists(short WebComponentId)
        {
            return await _context.WebComponents.AnyAsync(t => t.WebComponentId == WebComponentId);
        }
        public async Task AddWebComponent(WebComponent WebComponent)
        {
            if (!await _context.WebComponents.Where(a => a.Title == WebComponent.Title
            || string.IsNullOrWhiteSpace(WebComponent.Title)
            || string.IsNullOrWhiteSpace(WebComponent.Type)).AnyAsync())
            {
                await _context.AddAsync(WebComponent);
            }
            else
            {
                if (string.IsNullOrWhiteSpace(WebComponent.Title) || string.IsNullOrWhiteSpace(WebComponent.Type))
                {
                    throw new Exception("Empty WebComponent Title OR Type is not allowed.");
                }
                throw new Exception("WebComponent Title Already Exists");
            }
        }

        public bool UpdateWebComponentOk(short id, WebComponent webComponent)
        {
            var sameName = _context.WebComponents.AsNoTracking().FirstOrDefault(d => d.WebComponentId != webComponent.WebComponentId && d.Title == webComponent.Title);
            if (sameName != null)
            {
                throw new Exception("WebComponent Name Already Exists");
            }
            if (string.IsNullOrWhiteSpace(webComponent.Title) || string.IsNullOrWhiteSpace(webComponent.Type))
            {
                return false;
            }
            var entity = _context.WebComponents.Any(d => d.WebComponentId == id);
            if (entity == false)
            {
                return false;
            }
            if (webComponent.WebComponentId != id)
            {
                webComponent.WebComponentId = id;
            }
            else { }

            _context.WebComponents.Attach(webComponent);
            _context.Entry(webComponent).State = EntityState.Modified;
            _context.Entry(webComponent).Property(x => x.Type).IsModified = false;
            _context.Entry(webComponent).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(webComponent).Property(x => x.CreatedOn).IsModified = false;

            return true;
        }
        public void DeleteWebComponent(short id)
        {
            var WebComponent = new WebComponent { WebComponentId = id };
            _context.WebComponents.Attach(WebComponent);
            _context.Entry(WebComponent).State = EntityState.Deleted;
        }
    }
}
