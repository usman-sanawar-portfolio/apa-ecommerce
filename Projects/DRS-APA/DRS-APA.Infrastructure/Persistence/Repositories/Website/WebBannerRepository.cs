﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Domain.Core.Repositories.Website;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Website
{
    public class WebBannerRepository : IWebBannerRepository
    {
        private DRSAPAContext _context;
        public WebBannerRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<WebBanner>> GetAllWebBanner()
        {
            return await _context.WebBanners.OrderBy(a => a.WebBannerId).ToListAsync();
        }
        public async Task<WebBanner> GetWebBannerById(int id)
        {
            return await _context.WebBanners.Where(c => c.WebBannerId == id).FirstOrDefaultAsync();
        }

        public async Task AddWebBanner(WebBanner WebBanner)
        {
            await _context.AddAsync(WebBanner);
        }

        public bool UpdateWebBannerOk(int id, WebBanner WebBanner)
        {            
            var entity = _context.WebBanners.Any(d => d.WebBannerId == id);
            if (entity == false)
            {
                return false;
            }
            if (WebBanner.WebBannerId != id)
            {
                WebBanner.WebBannerId = id;
            }         
            _context.WebBanners.Attach(WebBanner);
            _context.Entry(WebBanner).State = EntityState.Modified;
            _context.Entry(WebBanner).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(WebBanner).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        public void DeleteWebBanner(int id)
        {
            var WebBanner = new WebBanner { WebBannerId = id };
            _context.WebBanners.Attach(WebBanner);
            _context.Entry(WebBanner).State = EntityState.Deleted;
        }
        public bool GetWebBannerByIdAsNoTracking(int id)
        {
            return _context.WebBanners.Any(e => e.WebBannerId == id);
        }
    }
}
