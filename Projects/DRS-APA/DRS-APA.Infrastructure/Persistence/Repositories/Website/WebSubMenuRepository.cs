﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Domain.Core.Repositories.Website;
using DRS.APA.Microserivce.Domain.Core.Repositories.Website;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Website
{
    public class WebSubMenuRepository : IWebSubMenuRepository
    {
        private DRSAPAContext _context;
        public WebSubMenuRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<WebSubMenu>> GetAllWebSubMenus()
        {
            return await _context.WebSubMenus.Include(x => x.WebMenu)
                .Include(x => x.WebPage)
                .OrderBy(a => a.WebSubMenuTitle).ToListAsync();
        }
        public async Task<IQueryable<WebSubMenu>> GetAllWebSubMenus(bool withPagination)
        {
            return _context.WebSubMenus
                .Include(x => x.WebMenu)
                .Include(x => x.WebPage);
        }
        public async Task<WebSubMenu> GetWebSubMenuById(short id)
        {
            return await _context.WebSubMenus
                .Include(x => x.WebMenu)
                .Include(x => x.WebPage)
                .Where(c => c.WebSubMenuId == id).FirstOrDefaultAsync();
        }
       
        public async Task AddWebSubMenu(WebSubMenu WebSubMenu)
        {
            if (!string.IsNullOrWhiteSpace(WebSubMenu.WebSubMenuTitle))
            {
                await _context.AddAsync(WebSubMenu);
            }
            else
            {
                throw new Exception("Cannot add empty web sub menu title.");
            }
        }

        public bool UpdateWebSubMenuOk(short id, WebSubMenu WebSubMenu)
        {
            if (string.IsNullOrWhiteSpace(WebSubMenu.WebSubMenuTitle))
            {
                return false;
            }
            var entity = _context.WebSubMenus.Any(d => d.WebSubMenuId == id);
            if (entity == false)
            {
                return false;
            }
            if (WebSubMenu.WebSubMenuId != id)
            {
                WebSubMenu.WebSubMenuId = id;
            }
            else { }

            _context.WebSubMenus.Attach(WebSubMenu);
            _context.Entry(WebSubMenu).State = EntityState.Modified;
            _context.Entry(WebSubMenu).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(WebSubMenu).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        public void DeleteWebSubMenu(short id)
        {
            var WebSubMenu = new WebSubMenu { WebSubMenuId = id };
            _context.WebSubMenus.Attach(WebSubMenu);
            _context.Entry(WebSubMenu).State = EntityState.Deleted;
        }
        public bool GetWebSubMenuByIdAsNoTracking(short id)
        {
            return _context.WebSubMenus.Any(e => e.WebSubMenuId == id);
        }
    }
}
