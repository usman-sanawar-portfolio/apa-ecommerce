﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Domain.Core.Repositories.Website;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Website
{
    public class WebCarousalRepository : IWebCarousalRepository
    {
        private DRSAPAContext _context;
        public WebCarousalRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<WebCarousal>> GetAllWebCarousal()
        {
            return await _context.WebCarousal.Include(x => x.WebCarousalPhotos).OrderBy(a => a.WebCarousalId).ToListAsync();
        }
        public async Task<WebCarousal> GetWebCarousalById(int id)
        {
            return await _context.WebCarousal.Include(x => x.WebCarousalPhotos).Where(c => c.WebCarousalId == id).FirstOrDefaultAsync();
        }

        public async Task AddWebCarousal(WebCarousal WebCarousal)
        {
            await _context.AddAsync(WebCarousal);
        }

        public bool UpdateWebCarousalOk(int id, WebCarousal WebCarousal)
        {            
            var entity = _context.WebCarousal.Any(d => d.WebCarousalId == id);
            if (entity == false)
            {
                return false;
            }
            if (WebCarousal.WebCarousalId != id)
            {
                WebCarousal.WebCarousalId = id;
            }         
            _context.WebCarousal.Attach(WebCarousal);
            _context.Entry(WebCarousal).State = EntityState.Modified;
            _context.Entry(WebCarousal).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(WebCarousal).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        public void DeleteWebCarousal(int id)
        {
            var WebCarousal = new WebCarousal { WebCarousalId = id };
            _context.WebCarousal.Attach(WebCarousal);
            _context.Entry(WebCarousal).State = EntityState.Deleted;
        }
        public bool GetWebCarousalByIdAsNoTracking(int id)
        {
            return _context.WebCarousal.Any(e => e.WebCarousalId == id);
        }
    }
}
