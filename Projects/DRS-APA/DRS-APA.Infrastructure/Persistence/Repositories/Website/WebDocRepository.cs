﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Domain.Core.Repositories.Website;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Website
{
    public class WebDocRepository : IWebDocRepository
    {
        private DRSAPAContext _context;
        public WebDocRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<WebDoc>> GetAllWebDocs()
        {
            return await _context.WebDocs.Include(x => x.WebComponent).OrderBy(a => a.Order).ToListAsync();
        }
        public async Task<IQueryable<WebDoc>> GetAllWebDocs(bool withPagination)
        {
            return _context.WebDocs.Include(x => x.WebComponent);
        }
        public async Task<WebDoc> GetWebDocById(short id)
        {
            return await _context.WebDocs.Include(x => x.WebComponent).Where(x=>x.WebDocId==id).FirstOrDefaultAsync();
        }
        public async Task<IEnumerable<WebDoc>> GetWebDocByComponentId(short id)
        {
            return await _context.WebDocs.Include(x => x.WebComponent).Where(x => x.WebComponentId == id).ToListAsync();
        }
        public async Task AddWebDoc(WebDoc WebDoc)
        {
            if (WebDoc.WebComponentId > 0)
            {
                await _context.AddAsync(WebDoc);
            }
            else
            {
                throw new Exception("Cannot add web document without component.");
            }
        }

        public bool UpdateWebDocOk(short id, WebDoc WebDoc)
        {
            if (WebDoc.WebComponentId <= 0)
            {
                return false;
            }
            var entity = _context.WebDocs.Any(d => d.WebDocId == id);
            if (entity == false)
            {
                return false;
            }
            if (WebDoc.WebDocId != id)
            {
                WebDoc.WebDocId = id;
            }
            else { }

            _context.WebDocs.Attach(WebDoc);
            _context.Entry(WebDoc).State = EntityState.Modified;
            _context.Entry(WebDoc).Property(x => x.WebComponentId).IsModified = false;
            _context.Entry(WebDoc).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(WebDoc).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        public void DeleteWebDoc(short id)
        {
            var WebDoc = new WebDoc { WebDocId = id };
            _context.WebDocs.Attach(WebDoc);
            _context.Entry(WebDoc).State = EntityState.Deleted;
        }
        public bool GetWebDocByIdAsNoTracking(short id)
        {
            return _context.WebDocs.Any(e => e.WebDocId == id);
        }
    }
}
