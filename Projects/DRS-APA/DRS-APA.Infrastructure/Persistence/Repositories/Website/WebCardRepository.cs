﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Domain.Core.Repositories.Website;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Website
{
    public class WebCardRepository : IWebCardRepository
    {
        private DRSAPAContext _context;
        public WebCardRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<WebCard>> GetAllWebCards()
        {
            return await _context.WebCards.Include(x => x.WebComponent).OrderBy(a => a.Order).ToListAsync();
        }
        public async Task<IQueryable<WebCard>> GetAllWebCards(bool withPagination)
        {
            return _context.WebCards.Include(x => x.WebComponent);
        }
        public async Task<WebCard> GetWebCardById(short id)
        {
            return await _context.WebCards.Include(x => x.WebComponent).Where(c => c.WebCardId == id).FirstOrDefaultAsync();
        }
        public async Task<IEnumerable<WebCard>> GetWebCardByComponentId(short id)
        {
            return await _context.WebCards.Include(x => x.WebComponent).Where(x => x.WebComponentId == id).ToListAsync();
        }
        public async Task AddWebCard(WebCard WebCard)
        {
            if (WebCard.WebComponentId > 0)
            {
                await _context.AddAsync(WebCard);
            }
            else
            {
                throw new Exception("Cannot add web card without component.");
            }
        }

        public bool UpdateWebCardOk(short id, WebCard WebCard)
        {
            if (WebCard.WebComponentId <= 0)
            {
                return false;
            }
            var entity = _context.WebCards.Any(d => d.WebCardId == id);
            if (entity == false)
            {
                return false;
            }
            if (WebCard.WebCardId != id)
            {
                WebCard.WebCardId = id;
            }
            else { }

            _context.WebCards.Attach(WebCard);
            _context.Entry(WebCard).State = EntityState.Modified;
            _context.Entry(WebCard).Property(x => x.WebComponentId).IsModified = false;
            _context.Entry(WebCard).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(WebCard).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        public void DeleteWebCard(short id)
        {
            var WebCard = new WebCard { WebCardId = id };
            _context.WebCards.Attach(WebCard);
            _context.Entry(WebCard).State = EntityState.Deleted;
        }
        public bool GetWebCardByIdAsNoTracking(short id)
        {
            return _context.WebCards.Any(e => e.WebCardId == id);
        }
    }
}
