using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DRS.APA.Domain.Core.Repositories.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using Microsoft.EntityFrameworkCore;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Website
{
  public class BlogRepository : IBlogRepository
  {
    private DRSAPAContext _context;

    public BlogRepository(DRSAPAContext context)
    {
      _context = context;
    }
    public async Task<IEnumerable<Blog>> GetAllBlogs()
    {
      return await _context.Blogs.Include(x=>x.BlogCategory).OrderBy(a => a.BlogTitle).Include(x => x.Customer).ToListAsync();
    }
    public async Task<IQueryable<Blog>> GetAllBlogs(bool withPagination)
    {
      return _context.Blogs.Include(x => x.BlogCategory).Include(x => x.Customer);
    }

    public async Task<Blog> GetBlogById(int id)
    {
      return await _context.Blogs.Include(c => c.BlogComment).Include(x => x.BlogCategory).Include(x => x.Customer).FirstOrDefaultAsync(x => x.BlogId == id);
    }
    public async Task<bool> BlogExists(int blogId)
    {
      return await _context.Blogs.AnyAsync(t => t.BlogId == blogId);
    }
    public async Task AddBlog(Blog blog)
    {
      await _context.AddAsync(blog);
    }
    public void DeleteBlog(int id)
    {
      var blog = new Blog { BlogId = id };
      _context.Blogs.Attach(blog);
      _context.Entry(blog).State = EntityState.Deleted;
    }
    public bool UpdateBlogOk(int id, Blog blog)
    {
      var entity = _context.Blogs.Any(d => d.BlogId == id);
      if (entity == false)
      {
        return false;
      }
      if (blog.BlogId != id)
      {
        blog.BlogId = id;

      }
      _context.Blogs.Attach(blog);
      _context.Entry(blog).State = EntityState.Modified;

      _context.Entry(blog).Property(x => x.BlogId).IsModified = false;
      _context.Entry(blog).Property(x => x.CreatedBy).IsModified = false;
      _context.Entry(blog).Property(x => x.CreatedOn).IsModified = false;
      return true;
    }

    public bool GetBlogByIdAsNoTracking(int id)
    {
      return _context.Blogs.Any(e => e.BlogId == id);
    }


  }
}
