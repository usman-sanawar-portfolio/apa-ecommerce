﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Domain.Core.Repositories.Website;
using DRS.APA.Microserivce.Domain.Core.Repositories.Website;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Website
{
    public class WebMenuRepository : IWebMenuRepository
    {
        private DRSAPAContext _context;
        public WebMenuRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<WebMenu>> GetAllWebMenus()
        {
            return await _context.WebMenus.Include(x => x.WebPage)
                .Include(x => x.WebSubMenu).ThenInclude(x => x.WebPage)
                .OrderBy(a => a.WebMenuTitle).ToListAsync();
        }
        public async Task<IQueryable<WebMenu>> GetAllWebMenus(bool withPagination)
        {
            return _context.WebMenus.Include(x => x.WebPage).Include(x => x.WebSubMenu).ThenInclude(x => x.WebPage);
        }
        public async Task<WebMenu> GetWebMenuById(short id)
        {
            return await _context.WebMenus.Include(x => x.WebSubMenu).ThenInclude(x=>x.WebPage)
                .Include(x => x.WebPage)
                .Where(c => c.WebMenuId == id)
                .FirstOrDefaultAsync();
        }

        public async Task AddWebMenu(WebMenu WebMenu)
        {
            if (!string.IsNullOrWhiteSpace(WebMenu.WebMenuTitle))
            {
                await _context.AddAsync(WebMenu);
            }
            else
            {
                throw new Exception("Cannot add empty web menu title.");
            }
        }

        public bool UpdateWebMenuOk(short id, WebMenu WebMenu)
        {
            if (string.IsNullOrWhiteSpace(WebMenu.WebMenuTitle))
            {
                return false;
            }
            var entity = _context.WebMenus.Any(d => d.WebMenuId == id);
            if (entity == false)
            {
                return false;
            }
            if (WebMenu.WebMenuId != id)
            {
                WebMenu.WebMenuId = id;
            }
            else { }

            _context.WebMenus.Attach(WebMenu);
            _context.Entry(WebMenu).State = EntityState.Modified;
            _context.Entry(WebMenu).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(WebMenu).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        public void DeleteWebMenu(short id)
        {
            var WebMenu = new WebMenu { WebMenuId = id };
            _context.WebMenus.Attach(WebMenu);
            _context.Entry(WebMenu).State = EntityState.Deleted;
        }
        public bool GetWebMenuByIdAsNoTracking(short id)
        {
            return _context.WebMenus.Any(e => e.WebMenuId == id);
        }
    }
}
