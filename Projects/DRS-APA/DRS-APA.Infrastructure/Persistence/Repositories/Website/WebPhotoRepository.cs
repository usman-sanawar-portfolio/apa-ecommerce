﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Domain.Core.Repositories.Website;

namespace DRS.APA.Infrastructure.Persistence.Repositories.Website
{
    public class WebPhotoRepository : IWebPhotoRepository
    {
        private DRSAPAContext _context;
        public WebPhotoRepository(DRSAPAContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<WebPhoto>> GetAllWebPhotos()
        {
            return await _context.WebPhotos.Include(x => x.WebComponent).OrderBy(a => a.Order).ToListAsync();
        }
        public async Task<IQueryable<WebPhoto>> GetAllWebPhotos(bool withPagination)
        {
            return _context.WebPhotos.Include(x => x.WebComponent);
        }
        public async Task<WebPhoto> GetWebPhotoById(short id)
        {
            return await _context.WebPhotos.Include(x => x.WebComponent).Where(x=>x.WebPhotoId==id).FirstOrDefaultAsync();
        }
        public async Task<IEnumerable<WebPhoto>> GetWebPhotoByComponentId(short id)
        {
            return await _context.WebPhotos.Include(x => x.WebComponent).Where(x => x.WebComponentId == id).ToListAsync();
        }
        public async Task AddWebPhoto(WebPhoto WebPhoto)
        {
            if (WebPhoto.WebComponentId > 0)
            {
                await _context.AddAsync(WebPhoto);
            }
            else
            {
                throw new Exception("Cannot add Web Photo without component.");
            }
        }

        public bool UpdateWebPhotoOk(short id, WebPhoto WebPhoto)
        {
            if (WebPhoto.WebComponentId <= 0)
            {
                return false;
            }
            var entity = _context.WebPhotos.Any(d => d.WebPhotoId == id);
            if (entity == false)
            {
                return false;
            }
            if (WebPhoto.WebPhotoId != id)
            {
                WebPhoto.WebPhotoId = id;
            }
            else { }

            _context.WebPhotos.Attach(WebPhoto);
            _context.Entry(WebPhoto).State = EntityState.Modified;
            _context.Entry(WebPhoto).Property(x => x.WebComponentId).IsModified = false;
            _context.Entry(WebPhoto).Property(x => x.CreatedBy).IsModified = false;
            _context.Entry(WebPhoto).Property(x => x.CreatedOn).IsModified = false;
            return true;
        }
        public void DeleteWebPhoto(short id)
        {
            var WebPhoto = new WebPhoto { WebPhotoId = id };
            _context.WebPhotos.Attach(WebPhoto);
            _context.Entry(WebPhoto).State = EntityState.Deleted;
        }
        public bool GetWebPhotoByIdAsNoTracking(short id)
        {
            return _context.WebPhotos.Any(e => e.WebPhotoId == id);
        }
    }
}
