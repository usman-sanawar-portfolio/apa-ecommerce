using DRS.APA.Core.Repositories;
using System.Threading.Tasks;

namespace DRS.APA.Infrastructure.Persistence.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DRSAPAContext _context;

        public UnitOfWork(DRSAPAContext context)
        {
            this._context = context;
        }
       
        public async Task<bool> CompleteAsync()
        {
            try
            {
                return (await _context.SaveChangesAsync() >= 0);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }
        public async Task<bool> CompleteStoreAsync()
        {
            try
            {
                return (await _context.SaveChangesStoreAsync() >= 0);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }
        
    }
}