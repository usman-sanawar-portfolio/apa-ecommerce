﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class RepairOptionEntityTypeConfiguration : IEntityTypeConfiguration<RepairOption>
    {
        public void Configure(EntityTypeBuilder<RepairOption> entityConfiguration)
        {
            //entityConfiguration.ToTable("Regions", HRManagementContext.DEFAULT_SCHEMA);

            entityConfiguration.ToTable("RepairOptions");

            entityConfiguration.HasKey(o => new {o.RepairOptionId });
            //entityConfiguration.Ignore(b => b.DomainEvents);
            entityConfiguration.Property(o => o.RepairOptionId).ValueGeneratedOnAdd();

            entityConfiguration.Property(b => b.Name).HasColumnType("varchar(100)").IsRequired(true);

            entityConfiguration.Property(b => b.ImagePath).HasColumnType("varchar(150)").IsRequired(false);

            entityConfiguration.Property(b => b.Remarks).HasColumnType("varchar(250)").IsRequired(false);


            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.HasOne(d => d.RepairCategory)
                   .WithMany(e => e.RepairOptions)
                   .HasForeignKey(f => new { f.RepairCategoryId})
                   .OnDelete(DeleteBehavior.Restrict)
                   .IsRequired();

            //entityConfiguration.HasOne(d => d.DeliveryMethodEligible)
            //    .WithMany(e => e.RepairOptions)
            //    .HasForeignKey(f => new { f.DeliveryMethodId })
            //    .OnDelete(DeleteBehavior.Restrict)
            //    .IsRequired(false);

        }
    }
}
