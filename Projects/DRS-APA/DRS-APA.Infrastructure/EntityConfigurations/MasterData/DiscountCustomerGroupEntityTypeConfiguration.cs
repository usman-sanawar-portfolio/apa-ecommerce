using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class DiscountCustomerGroupEntityTypeConfiguration : IEntityTypeConfiguration<DiscountCustomerGroup>
    {
        public void Configure(EntityTypeBuilder<DiscountCustomerGroup> entityConfiguration)
        {
            entityConfiguration.ToTable("DiscountCustomerGroups");
            entityConfiguration.HasKey(o => new {o.DiscountId, o.CustomerGroupId });
            entityConfiguration.Property(o => o.DiscountId).ValueGeneratedNever();
            entityConfiguration.Property(o => o.CustomerGroupId).ValueGeneratedNever();
            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

      entityConfiguration.HasOne(d => d.Discount)
              .WithMany(e => e.DiscountCustomerGroups)
              .HasForeignKey(f => new { f.DiscountId })
              .OnDelete(DeleteBehavior.Cascade);

      entityConfiguration.HasOne(d => d.CustomerGroup)
             .WithMany(e => e.DiscountCustomerGroups)
             .HasForeignKey(f => new { f.CustomerGroupId })
             .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
