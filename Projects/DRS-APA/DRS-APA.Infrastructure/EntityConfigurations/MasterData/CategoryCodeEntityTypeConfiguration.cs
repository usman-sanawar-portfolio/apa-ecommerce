﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class CategoryCodeEntityTypeConfiguration : IEntityTypeConfiguration<CategoryCode>
    {
        public void Configure(EntityTypeBuilder<CategoryCode> entityConfiguration)
        {
            entityConfiguration.ToTable("CategoryCodes");
            entityConfiguration.HasKey(o => new { o.CategoryCodeId });

            entityConfiguration.HasData(
               new CategoryCode { CategoryCodeId = 1, ProductName = "Retrofit", Code = "ret" },
               new CategoryCode { CategoryCodeId = 2, ProductName = "Operator", Code = "op" }
               );
        }
    }
}
