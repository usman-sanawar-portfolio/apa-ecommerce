﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class CollectionEntityTypeConfiguration : IEntityTypeConfiguration<Collection>
    {
        public void Configure(EntityTypeBuilder<Collection> entityConfiguration)
        {

            entityConfiguration.ToTable("Collections");

            entityConfiguration.HasKey(o => new {o.CollectionId });
            entityConfiguration.Property(o => o.CollectionId).ValueGeneratedOnAdd();

            entityConfiguration.Property(b => b.Name).HasColumnType("varchar(100)").IsRequired(true);

            entityConfiguration.Property(b => b.ThumbPath).HasColumnType("varchar(150)").IsRequired(false);

            entityConfiguration.Property(b => b.MetaDescription).HasColumnType("varchar(150)").IsRequired(false);

            entityConfiguration.Property(b => b.MetaTitle).HasColumnType("varchar(150)").IsRequired(false);

            entityConfiguration.Property(b => b.MetaKeywords).HasColumnType("varchar(150)").IsRequired(false);

            entityConfiguration.Property(b => b.UrlSeo).HasColumnType("varchar(150)").IsRequired(false);
            


            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.PublishedDate).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
           

        }
    }
}
