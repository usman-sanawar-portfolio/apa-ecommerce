﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class InformationEntityTypeConfiguration : IEntityTypeConfiguration<Information>
    {
        public void Configure(EntityTypeBuilder<Information> entityConfiguration)
        {

            entityConfiguration.ToTable("Information");

            entityConfiguration.HasKey(o => new { o.InformationId });

            entityConfiguration.Property(o => o.InformationId).ValueGeneratedOnAdd();

            entityConfiguration.Property(b => b.Title).HasColumnType("varchar(100)").IsRequired(true);

           
            entityConfiguration.Property(b => b.Description).HasColumnType("varchar(500)").IsRequired(false);
           
            entityConfiguration.Property(b => b.MetaTagDescription).HasColumnType("varchar(150)").IsRequired(false);
          
            entityConfiguration.Property(b => b.MetaTagKeyword).HasColumnType("varchar(150)").IsRequired(false);
           
           
            entityConfiguration.Property(b => b.SeoTitle).HasColumnType("varchar(150)").IsRequired(false);
            
            entityConfiguration.Property(b => b.Tags).HasColumnType("varchar(150)").IsRequired(false);
           
            entityConfiguration.Property(b => b.SeoKeywords).HasColumnType("varchar(150)").IsRequired(false);
           
            entityConfiguration.Property(b => b.Bottom).IsRequired(false);
           
            entityConfiguration.Property(b => b.SortOrder).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.IsActive).IsRequired(false);


        }
    }
}
