﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class CustomerGroupEntityTypeConfiguration : IEntityTypeConfiguration<CustomerGroup>
    {
        public void Configure(EntityTypeBuilder<CustomerGroup> entityConfiguration)
        {

            entityConfiguration.ToTable("CustomerGroups");

            entityConfiguration.HasKey(o => new {o.CustomerGroupId });

            entityConfiguration.Property(o => o.CustomerGroupId).ValueGeneratedOnAdd();

            entityConfiguration.Property(b => b.CustomerGroupName).HasColumnType("varchar(100)").IsRequired(true);

            entityConfiguration.Property(b => b.Description).HasColumnType("varchar(150)").IsRequired(false);

            entityConfiguration.Property(b => b.ApproveNewCustomer).IsRequired(true).HasDefaultValue(false);
            entityConfiguration.Property(b => b.IsDeleted).IsRequired(true).HasDefaultValue(false);

            entityConfiguration.Property(b => b.SortOrder).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

            entityConfiguration.HasData(
            new CustomerGroup { CustomerGroupId = 2, CustomerGroupName = "UK VAT Registered", Description = "", ApproveNewCustomer = true, SortOrder = 1, IsDeleted = false, DisplayOnSite = true}
            );

        }
    }
}
