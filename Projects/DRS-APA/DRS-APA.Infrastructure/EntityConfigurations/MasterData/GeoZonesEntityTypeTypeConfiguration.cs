﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class GeoZones_EntityTypeConfiguration : IEntityTypeConfiguration<GeoZones>
    {
        public void Configure(EntityTypeBuilder<GeoZones> entityConfiguration)
        {
            

            entityConfiguration.ToTable("GeoZones");

            entityConfiguration.HasKey(o => new {o.GeoZonesId});
            entityConfiguration.Property(o => o.GeoZonesId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.Name).HasColumnType("varchar(150)").IsRequired(true);
            entityConfiguration.HasIndex(x => x.Name).IsUnique();
            entityConfiguration.Property(b => b.Description).HasColumnType("varchar(150)").IsRequired(true);
            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("khalid");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(x => x.IsActive).IsRequired(false);

        }
    }
}
