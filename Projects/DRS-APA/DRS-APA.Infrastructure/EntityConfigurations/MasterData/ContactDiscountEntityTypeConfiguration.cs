﻿using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Infrastructure.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DRS.APA.Infrastructure.EntityConfigurations
{
    public class ContactDiscountEntityTypeConfiguration : IEntityTypeConfiguration<ContactDiscount>
    {
        public void Configure(EntityTypeBuilder<ContactDiscount> itemConfiguration)
        {
            itemConfiguration.ToTable("ContactDiscounts", APAContext.DEFAULT_SCHEMA);

            itemConfiguration.HasKey(x => new {x.ContactId, x.DiscountId });

            itemConfiguration.Property(o => o.ContactId).ValueGeneratedNever();
            itemConfiguration.Property(o => o.DiscountId).ValueGeneratedNever();


            itemConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(false);

            itemConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            itemConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            itemConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            itemConfiguration.Property(b => b.IsActive).IsRequired(false);

            itemConfiguration
                .HasOne(div => div.Contact)
                .WithMany(m => m.ContactDiscounts)
                .HasForeignKey(f => new {f.ContactId })
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            itemConfiguration
                .HasOne(div => div.Discount)
                .WithMany(m => m.ContactDiscounts)
                .HasForeignKey(f => new {f.DiscountId })
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired(false);


        }
    }
}