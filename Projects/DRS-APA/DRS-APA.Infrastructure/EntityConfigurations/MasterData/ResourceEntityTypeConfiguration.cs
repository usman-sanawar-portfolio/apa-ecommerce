﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Repair;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class ResourceEntityTypeConfiguration : IEntityTypeConfiguration<Resource>
    {
        public void Configure(EntityTypeBuilder<Resource> entityConfiguration)
        {
            //entityConfiguration.ToTable("Regions", HRManagementContext.DEFAULT_SCHEMA);

            entityConfiguration.ToTable("Resources");

            entityConfiguration.HasKey(o => new {o.ResourceId});
            //entityConfiguration.Ignore(b => b.DomainEvents);
            entityConfiguration.Property(o => o.ResourceId).ValueGeneratedOnAdd();

            entityConfiguration.Property(b => b.Name).HasColumnType("varchar(150)").IsRequired(true);

            entityConfiguration.Property(b => b.TimeZone).HasColumnType("varchar(50)").IsRequired(false);

            entityConfiguration.Property(b => b.MobilePhone).HasColumnType("varchar(20)").IsRequired(false);

            entityConfiguration.Property(b => b.Code).HasColumnType("varchar(10)").IsRequired(false);

            entityConfiguration.Property(b => b.Thumbnail).HasColumnType("varchar(150)").IsRequired(false);

            entityConfiguration.Property(b => b.SpecialHourlyRate).HasColumnType("varchar(150)").IsRequired(false);

            entityConfiguration.Property(b => b.WorkingHours).HasColumnType("varchar(150)").IsRequired(false);

            entityConfiguration.Property(b => b.Skills).HasColumnType("varchar(150)").IsRequired(false);

            entityConfiguration.Property(b => b.EmergencyContactDetails).HasColumnType("varchar(200)")
                .IsRequired(false);
            entityConfiguration.Property(b => b.WebUserId).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.HourlyRate).IsRequired(false);
            entityConfiguration.Property(b => b.Vacations).IsRequired(false);
            entityConfiguration.Property(b => b.AnnualLeaveAllowance).IsRequired(false);

            entityConfiguration.Property(b => b.Tracked).IsRequired(false).HasDefaultValue(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false).HasDefaultValue(true);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(false)
                .HasDefaultValue("khalid");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            //.ForSqlServerUseSequenceHiLo("entityseq", HRManagementContext.DEFAULT_SCHEMA);

            //Address value object persisted as owned entity type supported since EF Core 2.0          

            //var navigation = entityConfiguration.Metadata.FindNavigation(nameof(Order.OrderItems));

            // DDD Patterns comment:
            //Set as field (New since EF 1.1) to access the OrderItem collection property through its field
            //navigation.SetPropertyAccessMode(PropertyAccessMode.Field);

            entityConfiguration.HasOne(d => d.ResourceGroup)
                .WithMany(e => e.Resources)
                .HasForeignKey(f => new {f.ResourceGroupId})
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            //entityConfiguration.HasOne(d => d.Manager)
            //    .WithMany()
            //    .HasForeignKey(f => new {f.ManagerId})
            //    .OnDelete(DeleteBehavior.Restrict)
            //    .IsRequired(false);

            //entityConfiguration.HasOne(d => d.Currency)
            //    .WithMany()
            //    .HasForeignKey(f => new {f.CurrencyId})
            //    .OnDelete(DeleteBehavior.Restrict)
            //    .IsRequired(false);


            //entityConfiguration.HasMany(c => c.Countries)
            //.WithOne(e => e.Region)
            //.HasForeignKey(a=>new{a.RegionId}) // <--
            //.OnDelete(DeleteBehavior.Cascade);
        }
    }
}