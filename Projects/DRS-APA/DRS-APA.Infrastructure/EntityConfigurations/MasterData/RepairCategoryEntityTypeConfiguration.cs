﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class RepairCategoryEntityTypeConfiguration : IEntityTypeConfiguration<RepairCategory>
    {
        public void Configure(EntityTypeBuilder<RepairCategory> entityConfiguration)
        {
            //entityConfiguration.ToTable("Regions", HRManagementContext.DEFAULT_SCHEMA);

            entityConfiguration.ToTable("RepairCategories");

            entityConfiguration.HasKey(o => new {o.RepairCategoryId });
            //entityConfiguration.Ignore(b => b.DomainEvents);
            entityConfiguration.Property(o => o.RepairCategoryId).ValueGeneratedOnAdd();

            entityConfiguration.Property(b => b.Name).HasColumnType("varchar(100)").IsRequired(true);

            entityConfiguration.Property(b => b.ImagePath).HasColumnType("varchar(150)").IsRequired(false);

          


            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
           

        }
    }
}
