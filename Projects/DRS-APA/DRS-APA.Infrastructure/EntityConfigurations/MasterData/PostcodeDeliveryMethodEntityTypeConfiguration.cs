﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class PostcodeDeliveryMethodEntityTypeConfiguration : IEntityTypeConfiguration<PostcodeDeliveryMethod>
    {
        public void Configure(EntityTypeBuilder<PostcodeDeliveryMethod> entityConfiguration)
        {
            //entityConfiguration.ToTable("Regions", HRManagementContext.DEFAULT_SCHEMA);

            entityConfiguration.ToTable("PostcodeDeliveryMethods");

            entityConfiguration.HasKey(o => new {o.PostcodeId,o.DeliveryMethodId});
            //entityConfiguration.Ignore(b => b.DomainEvents);
            entityConfiguration.Property(o => o.PostcodeId).ValueGeneratedNever();

            entityConfiguration.Property(o => o.DeliveryMethodId).ValueGeneratedNever();


            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.HasOne(d => d.Postcode)
                   .WithMany(e => e.PostcodeDeliveryMethods)
                   .HasForeignKey(f => new { f.PostcodeId })
                   .OnDelete(DeleteBehavior.Restrict)
                   .IsRequired();

            entityConfiguration.HasOne(d => d.DeliveryMethod)
                .WithMany(e => e.PostcodeDeliveryMethods)
                .HasForeignKey(f => new { f.DeliveryMethodId })
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}
