﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class DiscountProductEntityTypeConfiguration : IEntityTypeConfiguration<DiscountProduct>
    {
        public void Configure(EntityTypeBuilder<DiscountProduct> entityConfiguration)
        {
            entityConfiguration.ToTable("DiscountProducts");
            entityConfiguration.HasKey(o => new {o.DiscountId, o.ProductId });
            entityConfiguration.Property(o => o.DiscountId).ValueGeneratedNever();
            entityConfiguration.Property(o => o.ProductId).ValueGeneratedNever();
            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.HasOne(d => d.Discount)
                    .WithMany(e => e.DiscountProducts)
                    .HasForeignKey(f => new { f.DiscountId })
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();

            entityConfiguration.HasOne(d => d.Product)
                   .WithMany(e => e.DiscountProducts)
                   .HasForeignKey(f => new { f.ProductId })
                   .OnDelete(DeleteBehavior.Restrict)
                   .IsRequired();
        }
    }
}
