﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core;
using System;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Infrastructure.Persistence.Repositories;

namespace DRS.APA.Infrastructure.Repositories
{
    class DocumentType_EntityTypeConfiguration : IEntityTypeConfiguration<DocumentType>
    {
        public void Configure(EntityTypeBuilder<DocumentType> entityConfiguration)
        {
            //entityConfiguration.ToTable("Regions", HRManagementContext.DEFAULT_SCHEMA);

            entityConfiguration.ToTable("DocumentTypes");

            entityConfiguration.HasKey(o => new {o.DocumentTypeId });
            //entityConfiguration.Ignore(b => b.DomainEvents);
            entityConfiguration.Property(o => o.DocumentTypeId).ValueGeneratedOnAdd();

            entityConfiguration.Property(b => b.Name).HasColumnType("varchar(100)").IsRequired(true);

            entityConfiguration.Property(b => b.Notes).HasColumnType("varchar(250)").IsRequired(false);

            entityConfiguration.Property(b => b.Label).HasColumnType("varchar(100)").IsRequired(false);

            entityConfiguration.Property(b => b.ReferencePrefix).HasColumnType("char(10)").IsRequired(false);

            entityConfiguration.Property(b => b.ReferenceFormat).HasColumnType("char(15)").IsRequired(false);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("Azhar");

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
           

        }
    }
}
