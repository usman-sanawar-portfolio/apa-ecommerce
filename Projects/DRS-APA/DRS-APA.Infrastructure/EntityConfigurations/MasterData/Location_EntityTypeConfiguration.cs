﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.Repair;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class Location_EntityTypeConfiguration : IEntityTypeConfiguration<Location>
    {
        public void Configure(EntityTypeBuilder<Location> locationConfiguration)
        {
            //locationConfiguration.ToTable("Locations", SCMContext.DEFAULT_SCHEMA);

            locationConfiguration.HasKey(x => new { x.LocationId });
            
            locationConfiguration.Property(o => o.LocationId).ValueGeneratedNever();

            locationConfiguration.Property(b => b.Name).HasColumnType("varchar(100)").IsRequired(true);

            locationConfiguration.Property(b => b.LocationAddress).HasColumnType("varchar(250)").IsRequired(true);

            locationConfiguration.Property(b => b.City).HasColumnType("varchar(50)").IsRequired(false);

            locationConfiguration.Property(b => b.Type).HasColumnType("varchar(20)").IsRequired(false);

            locationConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(false);

            locationConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            locationConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            locationConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            locationConfiguration.Property(b => b.IsActive).IsRequired(false);

           // locationConfiguration
           //.HasOne(comp => comp.Company)
           //.WithMany(loc => loc.Locations)
           //.HasForeignKey(f => f.CompanyId)
           //.OnDelete(DeleteBehavior.Restrict)
           //.IsRequired();
        }
    }
}
