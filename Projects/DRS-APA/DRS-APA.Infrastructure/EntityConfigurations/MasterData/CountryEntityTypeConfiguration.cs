﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class CountryEntityTypeConfiguration : IEntityTypeConfiguration<Country>
    {
        public void Configure(EntityTypeBuilder<Country> entityConfiguration)
        {

            entityConfiguration.ToTable("Countries");

            entityConfiguration.HasKey(o => new { o.CountryId });

            entityConfiguration.Property(o => o.CountryId).ValueGeneratedOnAdd();

            entityConfiguration.Property(b => b.CountryName).HasColumnType("varchar(100)").IsRequired(true);

            entityConfiguration.Property(b => b.ISOCode2).HasColumnType("varchar(50)").IsRequired(false);

            entityConfiguration.Property(b => b.ISOCode3).HasColumnType("varchar(50)").IsRequired(false);

            entityConfiguration.Property(b => b.PostcodeRequired).IsRequired(true).HasDefaultValue(false);


            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

        }
    }
}
