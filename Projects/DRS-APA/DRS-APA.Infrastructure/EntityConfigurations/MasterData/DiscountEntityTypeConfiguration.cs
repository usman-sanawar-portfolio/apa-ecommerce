using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class DiscountEntityTypeConfiguration : IEntityTypeConfiguration<Discount>
    {
        public void Configure(EntityTypeBuilder<Discount> entityConfiguration)
        {

            entityConfiguration.ToTable("Discounts");

            entityConfiguration.HasKey(o => new {o.DiscountId });
           entityConfiguration.Property(o => o.DiscountId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.Name).HasColumnType("varchar(100)").IsRequired(true);
            entityConfiguration.Property(b => b.DiscountType).HasColumnType("char(20)").IsRequired(false);
 
            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);


        }
    }
}
