﻿using DRS.APA.Domain.Core.Domain.MasterData;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class AttachmentEntityTypeConfiguration : IEntityTypeConfiguration<Attachment>
    {
        public void Configure(EntityTypeBuilder<Attachment> entityConfiguration)
        {

            entityConfiguration.ToTable("Attachments");

            entityConfiguration.HasKey(o => new { o.AttachmentId });
            entityConfiguration.Property(o => o.AttachmentId).ValueGeneratedOnAdd();

            entityConfiguration.Property(b => b.Name).HasColumnType("varchar(150)").IsRequired(true);

            entityConfiguration.Property(b => b.Type).HasColumnType("varchar(100)").IsRequired(false);
            entityConfiguration.Property(b => b.Type).HasColumnType("varchar(100)").IsRequired(false);

            entityConfiguration.Property(b => b.IsVisible).IsRequired(false).HasDefaultValue(true);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);


            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("Azhar");

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);


            entityConfiguration.HasOne(t => t.Product)
                .WithMany(t => t.Attachments)
                .HasForeignKey(f => f.ProductId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired(false);

         

        }
    }
}
