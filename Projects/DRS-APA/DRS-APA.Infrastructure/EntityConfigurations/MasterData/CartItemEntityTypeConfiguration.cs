﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class CartItemEntityTypeConfiguration : IEntityTypeConfiguration<CartItem>
    {
        public void Configure(EntityTypeBuilder<CartItem> entityConfiguration)
        {
            //entityConfiguration.ToTable("Regions", HRManagementContext.DEFAULT_SCHEMA);

            entityConfiguration.ToTable("CartItems");
            //entityConfiguration.Ignore<RevisitedCartItem>();
            entityConfiguration.HasKey(o => new {o.CartItemId });
            //entityConfiguration.Ignore(b => b.DomainEvents);
            entityConfiguration.Property(o => o.CartItemId).ValueGeneratedOnAdd();
                
            entityConfiguration.Property(b => b.CartCookie).HasColumnType("varchar(150)").IsRequired(true);


            //entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");

            //entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.SelectedDateTime).IsRequired(false);

            //entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.HasOne(d => d.Cart)
                    .WithMany(e => e.CartItems)
                    .HasForeignKey(f => new { f.CartId })
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();

            entityConfiguration.HasOne(ci => ci.Product)
                .WithMany()
                .HasForeignKey(ci => ci.ProductId);

            //builder.HasOne(ci => ci.CatalogBrand)
            //    .WithMany()
            //    .HasForeignKey(ci => ci.CatalogBrandId);
        }
    }
}
