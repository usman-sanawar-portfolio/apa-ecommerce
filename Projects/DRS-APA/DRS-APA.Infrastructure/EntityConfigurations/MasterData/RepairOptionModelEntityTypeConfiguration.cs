﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class RepairOptionModelEntityTypeConfiguration : IEntityTypeConfiguration<RepairOptionModel>
    {
        public void Configure(EntityTypeBuilder<RepairOptionModel> entityConfiguration)
        {
            //entityConfiguration.ToTable("Regions", HRManagementContext.DEFAULT_SCHEMA);

            entityConfiguration.ToTable("RepairOptionModels");

            entityConfiguration.HasKey(o => new {o.RepairOptionId , o.ModelId});
            //entityConfiguration.Ignore(b => b.DomainEvents);
            entityConfiguration.Property(o => o.RepairOptionId).ValueGeneratedNever();
            entityConfiguration.Property(o => o.ModelId).ValueGeneratedNever();

            entityConfiguration.Property(b => b.Remarks).HasColumnType("varchar(250)").IsRequired(false);

            entityConfiguration.Property(b => b.UnitPrice).IsRequired(false);

            entityConfiguration.Property(b => b.Remarks).HasColumnType("varchar(250)").IsRequired(false);


            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.HasOne(d => d.RepairOption)
                   .WithMany(e => e.RepairOptionModels)
                   .HasForeignKey(f => new { f.RepairOptionId})
                   .OnDelete(DeleteBehavior.Restrict)
                   .IsRequired();

            entityConfiguration.HasOne(d => d.Model)
                .WithMany(e => e.RepairOptionModels)
                .HasForeignKey(f => new { f.ModelId })
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            //entityConfiguration.HasOne(d => d.DeliveryMethodEligible)
            //    .WithMany(e => e.RepairOptionModels)
            //    .HasForeignKey(f => new { f.DeliveryMethodId })
            //    .OnDelete(DeleteBehavior.Restrict)
            //    .IsRequired(false);

        }
    }
}
