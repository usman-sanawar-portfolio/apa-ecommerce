﻿
using DRS.APA.Domain.Core.Domain.MasterData;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    public class FileLibraryEntityTypeConfiguration : IEntityTypeConfiguration<FileLibrary>
    {
        public void Configure(EntityTypeBuilder<FileLibrary> itemConfiguration)
        {

            itemConfiguration.HasKey(x => new {x.FileLibraryId,});

            itemConfiguration.Property(o => o.FileLibraryId).ValueGeneratedOnAdd();

            itemConfiguration.Property(b => b.Name).HasColumnType("varchar(120)").IsRequired(false);

            itemConfiguration.Property(b => b.FileName).HasColumnType("varchar(150)").IsRequired(false);

            itemConfiguration.Property(b => b.Type).HasColumnType("varchar(150)").IsRequired(false);


            itemConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(false);

            itemConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            itemConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            itemConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            itemConfiguration.Property(b => b.IsActive).IsRequired(false);
          

        }
    }
}