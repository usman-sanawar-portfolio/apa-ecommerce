﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class CollectionProductEntityTypeConfiguration : IEntityTypeConfiguration<CollectionProduct>
    {
        public void Configure(EntityTypeBuilder<CollectionProduct> entityConfiguration)
        {

            entityConfiguration.ToTable("CollectionProducts");

            entityConfiguration.HasKey(o => new {o.CollectionId, o.ProductId });
            entityConfiguration.Property(o => o.CollectionId).ValueGeneratedNever();

            entityConfiguration.Property(o => o.ProductId).ValueGeneratedNever();

            entityConfiguration.Property(b => b.Popular).IsRequired(false);

            entityConfiguration.Property(b => b.IsFeatured).IsRequired(false);


            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.HasOne(d => d.Collection)
                    .WithMany(e => e.ProductCollections)
                    .HasForeignKey(f => new { f.CollectionId })
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();



        }
    }
}
