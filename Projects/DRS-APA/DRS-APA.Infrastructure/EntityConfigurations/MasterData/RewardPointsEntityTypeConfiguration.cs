﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class RewardPointsEntityTypeConfiguration : IEntityTypeConfiguration<RewardPoints>
    {
        public void Configure(EntityTypeBuilder<RewardPoints> entityConfiguration)
        {
            entityConfiguration.ToTable("RewardPoints");
            entityConfiguration.HasKey(o => new { o.RewardPointsId });
            entityConfiguration.Property(o => o.RewardPointsId).ValueGeneratedOnAdd();
            entityConfiguration.Property(o => o.Points).IsRequired(true);
            entityConfiguration.Property(o => o.ProductId).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

            //entityConfiguration.HasOne(d => d.Product)
            //       .WithOne(e => e.RewardPoints)
            //       .HasForeignKey<RewardPoints>(f => f.ProductId)
            //       .OnDelete(DeleteBehavior.Restrict)
            //       .IsRequired(false);

        }
    }
}
