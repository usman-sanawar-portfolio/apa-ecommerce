﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class RewardPointCustomerGroupEntityTypeConfiguration : IEntityTypeConfiguration<RewardPointCustomerGroup>
    {
        public void Configure(EntityTypeBuilder<RewardPointCustomerGroup> entityConfiguration)
        {
            //entityConfiguration.ToTable("RewardPointCustomerGroup");
            //entityConfiguration.HasKey(o => new { o.RewardPointsId,o.CustomerGroupId });
            //entityConfiguration.Property(o => o.RewardPointsId).ValueGeneratedNever();
            //entityConfiguration.Property(o => o.CustomerGroupId).ValueGeneratedNever();           
            //entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            //entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            //entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            //entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            //entityConfiguration.Property(b => b.IsActive).IsRequired(false);

            //entityConfiguration.HasOne(d => d.RewardPoints)
            //       .WithMany(e => e.RewardPointCustomerGroup)
            //       .HasForeignKey(f => new { f.RewardPointsId })
            //       .OnDelete(DeleteBehavior.Restrict);

            //entityConfiguration.HasOne(d => d.CustomerGroup)
            //      .WithMany(e => e.RewardPointCustomerGroup)
            //      .HasForeignKey(f => new { f.CustomerGroupId })
            //      .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
