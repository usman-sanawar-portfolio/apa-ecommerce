﻿using DRS.APA.Domain.Core.Domain.Repair;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace phixhut.Infrastructure.EntityConfigurations.MasterData
{
    class AccountSetting_EntityTypeConfiguration : IEntityTypeConfiguration<AccountSetting>
    {
        public void Configure(EntityTypeBuilder<AccountSetting> entityConfiguration)
        {
            //entityConfiguration.ToTable("Regions", HRManagementContext.DEFAULT_SCHEMA);

            entityConfiguration.ToTable("AccountSettings");

            entityConfiguration.HasKey(o => new {o.AccountSettingId });
            //entityConfiguration.Ignore(b => b.DomainEvents);
            entityConfiguration.Property(o => o.AccountSettingId).ValueGeneratedOnAdd();

            entityConfiguration.Property(b => b.Name).HasColumnType("varchar(250)").IsRequired(true);

            entityConfiguration.Property(b => b.Type).HasColumnType("varchar(100)").IsRequired(false);

            entityConfiguration.Property(b => b.Value).HasColumnType("varchar(250)").IsRequired(false);

            entityConfiguration.Property(b => b.Module).HasColumnType("varchar(100)").IsRequired(false);

            

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("Azhar");

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.HasOne(a => a.Account)
                .WithMany(m => m.AccountSettings)
                .HasForeignKey(f => f.AccountId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
           

        }
    }
}
