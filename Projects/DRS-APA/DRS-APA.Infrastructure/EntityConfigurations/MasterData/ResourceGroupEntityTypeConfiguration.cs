﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Repair;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class ResourceGroupEntityTypeConfiguration : IEntityTypeConfiguration<ResourceGroup>
    {
        public void Configure(EntityTypeBuilder<ResourceGroup> entityConfiguration)
        {
            //entityConfiguration.ToTable("Regions", HRManagementContext.DEFAULT_SCHEMA);

            entityConfiguration.ToTable("ResourceGroups");

            entityConfiguration.HasKey(o => new {o.ResourceGroupId});
            //entityConfiguration.Ignore(b => b.DomainEvents);
            entityConfiguration.Property(o => o.ResourceGroupId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.Name).HasColumnType("varchar(100)").IsRequired(true);
           
            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("khalid");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            //.ForSqlServerUseSequenceHiLo("entityseq", HRManagementContext.DEFAULT_SCHEMA);

            //Address value object persisted as owned entity type supported since EF Core 2.0          

            //var navigation = entityConfiguration.Metadata.FindNavigation(nameof(Order.OrderItems));

            // DDD Patterns comment:
            //Set as field (New since EF 1.1) to access the OrderItem collection property through its field
            //navigation.SetPropertyAccessMode(PropertyAccessMode.Field);


            //entityConfiguration.HasMany(c => c.Countries)
            //.WithOne(e => e.Region)
            //.HasForeignKey(a=>new{a.RegionId}) // <--
            //.OnDelete(DeleteBehavior.Cascade);


        }
    }
}
