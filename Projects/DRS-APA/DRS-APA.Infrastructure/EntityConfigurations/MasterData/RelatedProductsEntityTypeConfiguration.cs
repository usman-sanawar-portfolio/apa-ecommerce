﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class RelatedProductsEntityTypeConfiguration : IEntityTypeConfiguration<RelatedProducts>
    {
        public void Configure(EntityTypeBuilder<RelatedProducts> entityConfiguration)
        {
            entityConfiguration.ToTable("RelatedProducts");
            entityConfiguration.HasKey(o => new { o.ProductId, o.RelatedProductId });
            entityConfiguration.Property(o => o.ProductId).ValueGeneratedNever();
            entityConfiguration.Property(o => o.RelatedProductId).ValueGeneratedNever();
            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

            //entityConfiguration.HasOne(d => d.Product)
            //       .WithMany(e => e.RelatedProducts)
            //       .HasForeignKey(f => new { f.ProductId })
            //       .OnDelete(DeleteBehavior.Restrict);

            //entityConfiguration.HasOne(d => d.RelatedProduct)
            //      .WithMany(e => e.Products)
            //      .HasForeignKey(f => new { f.RelatedProductId })
            //      .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
