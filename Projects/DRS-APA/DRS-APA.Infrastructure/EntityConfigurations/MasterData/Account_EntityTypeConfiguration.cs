﻿using DRS.APA.Domain.Core.Domain.Repair;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class Account_EntityTypeConfiguration : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> entityConfiguration)
        {
            //entityConfiguration.ToTable("Regions", HRManagementContext.DEFAULT_SCHEMA);

            entityConfiguration.ToTable("Accounts");

            entityConfiguration.HasKey(o => new {o.AccountId });
            //entityConfiguration.Ignore(b => b.DomainEvents);
            entityConfiguration.Property(o => o.AccountId).ValueGeneratedOnAdd();

            entityConfiguration.Property(b => b.Name).HasColumnType("varchar(100)").IsRequired(true);

            entityConfiguration.Property(b => b.AddressLine1).HasColumnType("varchar(100)").IsRequired(false);

            entityConfiguration.Property(b => b.AddressLine2).HasColumnType("varchar(100)").IsRequired(false);

            entityConfiguration.Property(b => b.AddressLine3).HasColumnType("varchar(100)").IsRequired(false);





            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("Azhar");

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
           

        }
    }
}
