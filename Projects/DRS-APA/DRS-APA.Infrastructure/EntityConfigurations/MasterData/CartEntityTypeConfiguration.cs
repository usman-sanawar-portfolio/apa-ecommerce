﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class CartEntityTypeConfiguration : IEntityTypeConfiguration<NewCart>
    {
        public void Configure(EntityTypeBuilder<NewCart> entityConfiguration)
        {
            //entityConfiguration.ToTable("Regions", HRManagementContext.DEFAULT_SCHEMA);

            entityConfiguration.ToTable("Carts");
            //entityConfiguration.Ignore<RevisitedCart>();
            entityConfiguration.HasKey(o => new {o.CartId });
            //entityConfiguration.Ignore(b => b.DomainEvents);
            entityConfiguration.Property(o => o.CartId).ValueGeneratedOnAdd();

            entityConfiguration.Property(b => b.CartCookie).HasColumnType("varchar(150)").IsRequired(true);

            entityConfiguration.Property(b => b.SourceUrl).HasColumnType("varchar(150)").IsRequired(false);

            entityConfiguration.Property(b => b.CustomerId).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.CustomerCookie).IsRequired(false);


            //entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");

            //entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            //entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            //entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            //entityConfiguration.HasOne(d => d.Make)
            //        .WithMany(e => e.Carts)
            //        .HasForeignKey(f => new { f.MakeId })
            //        .OnDelete(DeleteBehavior.Restrict)
            //        .IsRequired();

            //builder.HasOne(ci => ci.CatalogBrand)
            //    .WithMany()
            //    .HasForeignKey(ci => ci.CatalogBrandId);

        }
    }
}
