﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Schedule;
using DRS.APA.Domain.Core.Domain.Shop;

namespace DRS.APA.Infrastructure.EntityConfigurations
{
    class BookingEntityTypeConfiguration : IEntityTypeConfiguration<Booking>
    {
        public void Configure(EntityTypeBuilder<Booking> entityConfiguration)
        {
            //entityConfiguration.ToTable("Regions", HRManagementContext.DEFAULT_SCHEMA);

            entityConfiguration.ToTable("Bookings");

            entityConfiguration.HasKey(o => new {o.BookingId });

            //entityConfiguration.Ignore(b => b.DomainEvents);

            entityConfiguration.Property(o => o.BookingId).ValueGeneratedOnAdd();

            //entityConfiguration.Property(b => b.BookingType).HasColumnType("varchar(15)").IsRequired(true);

            entityConfiguration.Property(b => b.Title).HasColumnType("varchar(150)").IsRequired(true);

            entityConfiguration.Property(b => b.Remarks).HasColumnType("varchar(250)").IsRequired(false);

            entityConfiguration.Property(b => b.IsPotentiallyConflicting).IsRequired(true).HasDefaultValue(false);

            entityConfiguration.Property(b => b.Confirmed).IsRequired(true).HasDefaultValue(false);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.HasOne(d => d.Schedule)
                    .WithMany(e => e.Bookings)
                    .HasForeignKey(f => new { f.ScheduleId })
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();

            entityConfiguration.HasOne(d => d.SalesOrder)
                   .WithMany(e => e.Bookings)
                   .HasForeignKey(f => new { f.SalesOrderId })
                   .OnDelete(DeleteBehavior.Restrict)
                   .IsRequired(false);

            entityConfiguration.HasOne(d => d.BookingType)
                .WithMany(e => e.Bookings)
                .HasForeignKey(f => new { f.BookingTypeId })
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}
