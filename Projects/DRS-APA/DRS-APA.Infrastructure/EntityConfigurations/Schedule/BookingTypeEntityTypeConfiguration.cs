﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Schedule;
using DRS.APA.Domain.Core.Domain.Shop;

namespace DRS.APA.Infrastructure.EntityConfigurations
{
    class BookingTypeEntityTypeConfiguration : IEntityTypeConfiguration<BookingType>
    {
        public void Configure(EntityTypeBuilder<BookingType> entityConfiguration)
        {
            //entityConfiguration.ToTable("Regions", HRManagementContext.DEFAULT_SCHEMA);

            entityConfiguration.ToTable("BookingTypes");

            entityConfiguration.HasKey(o => new {o.BookingTypeId });

            //entityConfiguration.Ignore(b => b.DomainEvents);

            entityConfiguration.Property(o => o.BookingTypeId).ValueGeneratedOnAdd();

            entityConfiguration.Property(b => b.Name).HasColumnType("varchar(150)").IsRequired(true);

            entityConfiguration.Property(b => b.Code).HasColumnType("varchar(20)").IsRequired(true);

    
            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);


        }
    }
}
