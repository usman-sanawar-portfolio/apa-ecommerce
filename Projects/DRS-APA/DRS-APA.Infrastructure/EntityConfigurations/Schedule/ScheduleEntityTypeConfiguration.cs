﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Schedule;
using DRS.APA.Domain.Core.Domain.Shop;

namespace DRS.APA.Infrastructure.EntityConfigurations
{
    class ScheduleEntityTypeConfiguration : IEntityTypeConfiguration<Schedule>
    {
        public void Configure(EntityTypeBuilder<Schedule> entityConfiguration)
        {
            //entityConfiguration.ToTable("Regions", HRManagementContext.DEFAULT_SCHEMA);

            entityConfiguration.ToTable("Schedules");

            entityConfiguration.HasKey(o => new {o.ScheduleId });

            //entityConfiguration.Ignore(b => b.DomainEvents);

            entityConfiguration.Property(o => o.ScheduleId).ValueGeneratedOnAdd();

            entityConfiguration.Property(b => b.ScheduleType).HasColumnType("varchar(15)").IsRequired(true);

            entityConfiguration.Property(b => b.SlotDuration).IsRequired(true).HasDefaultValue(0);

            entityConfiguration.Property(b => b.ResourcePerSlot).IsRequired(true).HasDefaultValue(0);

            //entityConfiguration.Property(b => b.IsPotentiallyConflicting).IsRequired(true).HasDefaultValue(false);

        
   
            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.HasOne(d => d.Location)
                    .WithMany(e => e.Schedules)
                    .HasForeignKey(f => new { f.LocationId })
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();

            entityConfiguration.HasOne(d => d.RepairCategory)
                   .WithMany(e => e.Schedules)
                   .HasForeignKey(f => new { f.RepairCategoryId })
                   .OnDelete(DeleteBehavior.Restrict)
                   .IsRequired(false);

   
        }
    }
}
