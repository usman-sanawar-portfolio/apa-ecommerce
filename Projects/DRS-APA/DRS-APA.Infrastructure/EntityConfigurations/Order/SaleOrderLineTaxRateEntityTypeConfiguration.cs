using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Infrastructure.EntityConfigurations.Order
{
  class SaleOrderLineTaxRateEntityTypeConfiguration : IEntityTypeConfiguration<SaleOrderLineTaxRate>
  {
    public void Configure(EntityTypeBuilder<SaleOrderLineTaxRate> entityConfiguration)
    {
      entityConfiguration.ToTable("SaleOrderLineTaxRates");

      entityConfiguration.HasKey(o => new { o.SaleOrderLineTaxRateId });
      entityConfiguration.Property(o => o.SaleOrderLineTaxRateId).ValueGeneratedOnAdd();
      entityConfiguration.Property(p => p.SaleOrderLineTaxRateName).HasColumnType("varchar(150)").IsRequired(true);
      entityConfiguration.Property(p => p.SaleOrderTaxRate).IsRequired(true);
      entityConfiguration.Property(p => p.SaleOrderLineTaxRateCode).HasColumnType("varchar(80)").IsRequired(false);
    

      entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
      entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
      entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
      entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

      //One To Many Relation
      entityConfiguration.HasOne(d => d.SaleOrderLines)
              .WithMany(e => e.SaleOrderLineTaxRates)
              .HasForeignKey(f => new { f.SaleOrderLinesId })
              .OnDelete(DeleteBehavior.Cascade)
              .IsRequired(true);


    }
  }
}
