using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Infrastructure.EntityConfigurations.Order
{
  class  OrderEntityTypeConfiguration : IEntityTypeConfiguration<Orders>
  {
    public void Configure(EntityTypeBuilder<Orders> entityConfiguration)
        {
            entityConfiguration.ToTable("Orders");

            entityConfiguration.HasKey(o => new { o.OrderId });
            entityConfiguration.Property(o => o.OrderId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.OrderIdentifier).HasColumnType("varchar(150)").IsRequired(false);
            entityConfiguration.HasIndex(b => b.OrderIdentifier).IsUnique();
            entityConfiguration.Property(b => b.IsOnlineOrder).IsRequired(true);
            entityConfiguration.Property(b => b.IsPaymentOnline).IsRequired(true);
            entityConfiguration.Property(b => b.OnlinePaymentId).HasColumnType("varchar(150)").IsRequired(false);
            entityConfiguration.Property(b => b.OrderDate).IsRequired(true);
            entityConfiguration.Property(b => b.OrderDueDate).IsRequired(false);
            entityConfiguration.Property(b => b.IsCancelled).IsRequired(true).HasDefaultValue(false);
            entityConfiguration.Property(b => b.CancelReason).HasColumnType("varchar(150)").IsRequired(false);
            entityConfiguration.Property(b => b.OrderAmountWithTaxAndDiscount).HasColumnType("decimal(18,2)").IsRequired(true);
            entityConfiguration.Property(b => b.OrderNotes).HasColumnType("varchar(150)").IsRequired(false);
            entityConfiguration.Property(b => b.SessionId).HasColumnType("varchar(250)").IsRequired(false);
            entityConfiguration.Property(b => b.PaymentIntentId).HasColumnType("varchar(250)").IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);
            

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.HasOne(o => o.OrderStatus)
             .WithMany(m => m.Orders)
             .HasForeignKey(f => new { f.OrderStatusId })
             .OnDelete(DeleteBehavior.Cascade)
             .IsRequired(true);

      entityConfiguration.HasOne(d => d.Customers)
         .WithMany(e => e.Orders)
         .HasForeignKey(f => new { f.CustomerId })
         .OnDelete(DeleteBehavior.Cascade)
         .IsRequired(false);

      entityConfiguration.HasOne(d => d.SaleOrder)
              .WithOne(e => e.Orders)
              .HasForeignKey<SaleOrder>(f => f.OrderId)
              .OnDelete(DeleteBehavior.Restrict)
              .IsRequired(true);
    }
    }
}
