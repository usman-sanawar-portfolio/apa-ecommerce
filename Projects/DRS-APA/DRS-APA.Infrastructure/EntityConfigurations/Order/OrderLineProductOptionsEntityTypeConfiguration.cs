using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Infrastructure.EntityConfigurations.Order
{
    class OrderLineProductOptionsEntityTypeConfiguration : IEntityTypeConfiguration<OrderLineProductOptions>
    {
        public void Configure(EntityTypeBuilder<OrderLineProductOptions> entityConfiguration)
        {
            entityConfiguration.ToTable("OrderLineProductOptions");
            entityConfiguration.HasKey(c => c.OrderLineProductOptionsId);
            entityConfiguration.Property(c => c.OrderLineProductOptionsId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.OptionModel).IsRequired(true);
            entityConfiguration.Property(c => c.SubtractStock).IsRequired(true);
            entityConfiguration.Property(b => b.PriceParam).IsRequired(true);
            entityConfiguration.Property(b => b.OptionPrice).IsRequired(true).HasDefaultValue(false);
            entityConfiguration.Property(b => b.WeightParam).HasColumnType("varchar(150)").IsRequired(false);
            entityConfiguration.Property(b => b.OptionWeight).HasColumnType("varchar(150)").IsRequired(false);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("Admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

            entityConfiguration.HasOne(o => o.OrderLines)
                .WithMany(m => m.OrderLineProductOptions)
                .HasForeignKey(f => new { f.OrderLinesId })
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();
        }
    }
}
