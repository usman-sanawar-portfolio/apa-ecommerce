using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Infrastructure.EntityConfigurations.Order
{
    class SaleOrderLineProductOptionCombinationsEntityTypeConfiguration : IEntityTypeConfiguration<SaleOrderLineProductOptionCombinations>
    {
        public void Configure(EntityTypeBuilder<SaleOrderLineProductOptionCombinations> entityConfiguration)
        {
            entityConfiguration.ToTable("SaleOrderLineProductOptionCombinations");
            entityConfiguration.HasKey(c => c.SaleOrderLineProductOptionCombinationsId);
            entityConfiguration.Property(c => c.SaleOrderLineProductOptionCombinationsId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.SaleOrderLineProductOptionsId).IsRequired(true);
          entityConfiguration.Property(b => b.Value).HasColumnType("varchar(128)").IsRequired(false);

         entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

            entityConfiguration.HasOne(d => d.SaleOrderLineProductOptions)
                   .WithMany(e => e.SaleOrderLineProductOptionCombinations)
                   .HasForeignKey(f => new { f.SaleOrderLineProductOptionsId })
                   .OnDelete(DeleteBehavior.Cascade);

            entityConfiguration.HasOne(d => d.Option)
                   .WithMany(e => e.SaleOrderLineProductOptionCombinations)
                   .HasForeignKey(f => new { f.OptionId })
                   .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
