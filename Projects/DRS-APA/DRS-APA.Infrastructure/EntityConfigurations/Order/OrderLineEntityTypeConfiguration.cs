using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Infrastructure.EntityConfigurations.Order
{
    class OrderLinesEntityTypeConfiguration : IEntityTypeConfiguration<OrderLines>
    {
        public void Configure(EntityTypeBuilder<OrderLines> entityConfiguration)
        {
            entityConfiguration.ToTable("OrderLines");
            entityConfiguration.HasKey(c => c.OrderLinesId);
            entityConfiguration.Property(c => c.OrderLinesId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.Quantity).IsRequired(true);
            entityConfiguration.Property(b => b.UnitPrice).HasColumnType("decimal(18,4)").IsRequired(true);
            entityConfiguration.Property(b => b.DiscountPercentage).HasColumnType("decimal(18,4)").IsRequired(true);
            entityConfiguration.Property(b => b.IsProductReturn).IsRequired(true).HasDefaultValue(false);
            entityConfiguration.Property(b => b.LineTotal).HasColumnType("decimal(18,4)").IsRequired(true);
            entityConfiguration.Property(b => b.ReturnReason).HasColumnType("varchar(150)").IsRequired(false);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("Admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

            entityConfiguration.HasOne(o => o.Orders)
                .WithMany(m => m.OrderLines)
                .HasForeignKey(f => new { f.OrderId })
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            entityConfiguration.HasOne(o => o.Product)
            .WithMany(m => m.OrderLines)
            .HasForeignKey(f => new { f.ProductId })
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired();

            entityConfiguration.HasOne(o => o.TaxClass)
                  .WithMany(m => m.OrderLines)
                  .HasForeignKey(f => new { f.TaxClassId })
                  .OnDelete(DeleteBehavior.SetNull)
                  .IsRequired(false);

            entityConfiguration.HasOne(o => o.Discount)
            .WithMany(m => m.OrderLines)
            .HasForeignKey(f => new { f.DiscountId })
            .OnDelete(DeleteBehavior.SetNull)
            .IsRequired(false);

        }
    }
}
