using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Infrastructure.EntityConfigurations.Order
{
    class SaleOrderLinesEntityTypeConfiguration : IEntityTypeConfiguration<SaleOrderLines>
    {
        public void Configure(EntityTypeBuilder<SaleOrderLines> entityConfiguration)
        {
            entityConfiguration.ToTable("SaleOrderLines");
            entityConfiguration.HasKey(c => c.SaleOrderLinesId);
            entityConfiguration.Property(c => c.SaleOrderLinesId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.Quantity).IsRequired(true);
            entityConfiguration.Property(b => b.UnitPrice).HasColumnType("decimal(18,4)").IsRequired(true);
            entityConfiguration.Property(b => b.DiscountPercentage).HasColumnType("decimal(18,4)").IsRequired(true);
            entityConfiguration.Property(b => b.IsProductReturn).IsRequired(true).HasDefaultValue(false);
            entityConfiguration.Property(b => b.LineTotal).HasColumnType("decimal(18,4)").IsRequired(true);
            entityConfiguration.Property(b => b.ReturnReason).HasColumnType("varchar(150)").IsRequired(false);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("Admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

            entityConfiguration.HasOne(o => o.SaleOrder)
                .WithMany(m => m.SaleOrderLines)
                .HasForeignKey(f => new { f.SaleOrderId })
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            entityConfiguration.HasOne(o => o.Product)
            .WithMany(m => m.SaleOrderLines)
            .HasForeignKey(f => new { f.ProductId })
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired();

            entityConfiguration.HasOne(o => o.TaxClass)
                  .WithMany(m => m.SaleOrderLines)
                  .HasForeignKey(f => new { f.TaxClassId })
                  .OnDelete(DeleteBehavior.SetNull)
                  .IsRequired(false);

            entityConfiguration.HasOne(o => o.Discount)
            .WithMany(m => m.SaleOrderLines)
            .HasForeignKey(f => new { f.DiscountId })
            .OnDelete(DeleteBehavior.SetNull)
            .IsRequired(false);

        }
    }
}
