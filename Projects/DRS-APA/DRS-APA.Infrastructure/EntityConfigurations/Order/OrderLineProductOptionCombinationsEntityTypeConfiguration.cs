using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Infrastructure.EntityConfigurations.Order
{
    class OrderLineProductOptionCombinationsEntityTypeConfiguration : IEntityTypeConfiguration<OrderLineProductOptionCombinations>
    {
        public void Configure(EntityTypeBuilder<OrderLineProductOptionCombinations> entityConfiguration)
        {
            entityConfiguration.ToTable("OrderLineProductOptionCombinations");
            entityConfiguration.HasKey(c => c.OrderLineProductOptionCombinationsId);
            entityConfiguration.Property(c => c.OrderLineProductOptionCombinationsId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.OrderLineProductOptionsId).IsRequired(true);
            entityConfiguration.Property(b => b.Value).HasColumnType("varchar(128)").IsRequired(false);
 
            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

            entityConfiguration.HasOne(d => d.OrderLineProductOptions)
                   .WithMany(e => e.OrderLineProductOptionCombinations)
                   .HasForeignKey(f => new { f.OrderLineProductOptionsId })
                   .OnDelete(DeleteBehavior.Cascade);

            entityConfiguration.HasOne(d => d.Option)
                   .WithMany(e => e.OrderLineProductOptionCombinations)
                   .HasForeignKey(f => new { f.OptionId })
                   .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
