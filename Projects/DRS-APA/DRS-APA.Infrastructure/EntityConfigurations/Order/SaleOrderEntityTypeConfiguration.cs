using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class SaleOrderEntityTypeConfiguration : IEntityTypeConfiguration<SaleOrder>
    {
        public void Configure(EntityTypeBuilder<SaleOrder> entityConfiguration)
        {
            entityConfiguration.ToTable("SaleOrders");

            entityConfiguration.HasKey(o => new { o.SaleOrderId });
           entityConfiguration.Property(o => o.SaleOrderId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.SaleIdentifier).HasColumnType("varchar(150)").IsRequired(false);
            entityConfiguration.HasIndex(b => b.SaleIdentifier).IsUnique();
            entityConfiguration.Property(b => b.IsOnlineOrder).IsRequired(true);
            entityConfiguration.Property(b => b.IsPaymentOnline).IsRequired(true);
            entityConfiguration.Property(b => b.OnlinePaymentId).HasColumnType("varchar(150)").IsRequired(false);
            entityConfiguration.Property(b => b.SaleOrderDate).IsRequired(true);
            entityConfiguration.Property(b => b.OrderDueDate).IsRequired(false);
            entityConfiguration.Property(b => b.IsCancelled).IsRequired(true).HasDefaultValue(false);
            entityConfiguration.Property(b => b.CancelReason).HasColumnType("varchar(150)").IsRequired(false);
            entityConfiguration.Property(b => b.SaleOrderAmountWithTaxAndDiscount).HasColumnType("decimal(18,4)").IsRequired(true);
            entityConfiguration.Property(b => b.ShipmentTotal).HasColumnType("decimal(18,4)").IsRequired(true);
            entityConfiguration.Property(b => b.SaleOrderNotes).HasColumnType("varchar(150)").IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);
              entityConfiguration.Property(b => b.OrderId).HasDefaultValue(null);
            

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.HasOne(o => o.OrderStatus)
             .WithMany(m => m.SaleOrder)
             .HasForeignKey(f => new { f.OrderStatusId })
             .OnDelete(DeleteBehavior.Cascade)
             .IsRequired(true);

            //One To Many Relation
          entityConfiguration.HasOne(d => d.Customers)
              .WithMany(e => e.SaleOrders)
              .HasForeignKey(f => new { f.CustomerId })
              .OnDelete(DeleteBehavior.Cascade)
              .IsRequired(false);
    }
    }
}
