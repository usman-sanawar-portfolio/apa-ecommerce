using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Infrastructure.EntityConfigurations.Order
{
  class OrderLineTaxRateEntityTypeConfiguration : IEntityTypeConfiguration<OrderLineTaxRate>
  {
    public void Configure(EntityTypeBuilder<OrderLineTaxRate> entityConfiguration)
    {
      entityConfiguration.ToTable("OrderLineTaxRates");

      entityConfiguration.HasKey(o => new { o.OrderLineTaxRateId });
      entityConfiguration.Property(o => o.OrderLineTaxRateId).ValueGeneratedOnAdd();
      entityConfiguration.Property(p => p.OrderLineTaxRateName).HasColumnType("varchar(150)").IsRequired(true);
      entityConfiguration.Property(p => p.OrderTaxRate).IsRequired(true);
      entityConfiguration.Property(p => p.OrderLineTaxRateCode).HasColumnType("varchar(80)").IsRequired(false);
    

      entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
      entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
      entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
      entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

      //One To Many Relation
      entityConfiguration.HasOne(d => d.OrderLines)
              .WithMany(e => e.OrderLineTaxRates)
              .HasForeignKey(f => new { f.OrderLinesId })
              .OnDelete(DeleteBehavior.Cascade)
              .IsRequired(true);


    }
  }
}
