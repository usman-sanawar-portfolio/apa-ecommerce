using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Infrastructure.EntityConfigurations.Order
{
    class OrderIdentifiersEntityTypeConfiguration : IEntityTypeConfiguration<OrderIdentifier>
    {
        public void Configure(EntityTypeBuilder<OrderIdentifier> entityConfiguration)
        {
            entityConfiguration.ToTable("OrderIdentifiers");
            entityConfiguration.HasKey(c => c.OrderIdentifierId);
            entityConfiguration.Property(c => c.OrderIdentifierId).IsRequired(true).ValueGeneratedOnAdd();
           

        }
    }
}
