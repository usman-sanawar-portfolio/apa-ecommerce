using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;

namespace DRS.APA.Infrastructure.EntityConfigurations.Order
{
    class OrderAddressEntityTypeConfiguration : IEntityTypeConfiguration<OrderAddress>
    {
        public void Configure(EntityTypeBuilder<OrderAddress> entityConfiguration)
        {
            entityConfiguration.ToTable("OrderAddress");

            entityConfiguration.HasKey(o => new { o.OrderAddressId });
            entityConfiguration.Property(o => o.OrderAddressId).ValueGeneratedOnAdd();
            entityConfiguration.Property(p => p.FirstName).HasColumnType("varchar(150)").IsRequired(true);
            entityConfiguration.Property(p => p.LastName).HasColumnType("varchar(150)").IsRequired(false);
            entityConfiguration.Property(p => p.Phone).HasColumnType("varchar(80)").IsRequired(true);
            entityConfiguration.Property(p => p.Email).HasColumnType("varchar(100)").IsRequired(false);
            entityConfiguration.Property(p => p.Street).HasColumnType("varchar(250)").IsRequired(false);
            entityConfiguration.Property(p => p.CompanyName).HasColumnType("varchar(100)").IsRequired(false);
            entityConfiguration.Property(p => p.City).HasColumnType("varchar(150)").IsRequired(true);
            entityConfiguration.Property(p => p.State).HasColumnType("varchar(150)").IsRequired(false);
            entityConfiguration.Property(p => p.Country).HasColumnType("varchar(150)").IsRequired(true);
            entityConfiguration.Property(p => p.ZipCode).HasColumnType("varchar(150)").IsRequired(false);
            entityConfiguration.Property(p => p.Longitude).IsRequired(false);
            entityConfiguration.Property(p => p.Latitude).IsRequired(false);
            entityConfiguration.Property(p => p.AddressType).HasColumnType("varchar(50)").IsRequired(true);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.HasOne(d => d.Orders)
                    .WithMany(e => e.OrderAddress)
                    .HasForeignKey(f => new { f.OrderId })
                    .OnDelete(DeleteBehavior.Cascade)
                    .IsRequired(true);


        }
    }
}
