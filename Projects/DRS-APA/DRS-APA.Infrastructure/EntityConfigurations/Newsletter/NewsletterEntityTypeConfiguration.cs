using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Newsletter;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class NewsletterEntityTypeConfiguration : IEntityTypeConfiguration<Newsletter>
    {
        public void Configure(EntityTypeBuilder<Newsletter> entityConfiguration)
        {
            entityConfiguration.ToTable("Newsletters");

            entityConfiguration.HasKey(o => new { o.NewsletterId });
            entityConfiguration.Property(o => o.NewsletterId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.Title).HasColumnType("varchar(150)").IsRequired(true);
            entityConfiguration.Property(b => b.Description).HasColumnType("varchar(150)").IsRequired(true);
            entityConfiguration.Property(b => b.Content).HasColumnType("varchar(max)").IsRequired(true);
            entityConfiguration.Property(b => b.IsSent).IsRequired(true);
            entityConfiguration.Property(b => b.SentDate).IsRequired(false);
            entityConfiguration.Property(b => b.IsResent).IsRequired(true);
            entityConfiguration.Property(b => b.ResentDate).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);
        }
    }
}
