﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Microserivce.Domain.Core.Domain.Newsletter;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class SubscriberEntityTypeConfiguration : IEntityTypeConfiguration<Subscriber>
    {
        public void Configure(EntityTypeBuilder<Subscriber> entityConfiguration)
        {
            entityConfiguration.ToTable("Subscribers");

            entityConfiguration.HasKey(o => new { o.SubscriberId });
            entityConfiguration.Property(o => o.SubscriberId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.Email).HasColumnType("varchar(150)").IsRequired(true);
            entityConfiguration.HasIndex(b => b.Email).IsUnique();
            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);
        }
    }
}
