﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Newsletter;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class NewsletterSentEntityTypeConfiguration : IEntityTypeConfiguration<NewsletterSent>
    {
        public void Configure(EntityTypeBuilder<NewsletterSent> entityConfiguration)
        {
            entityConfiguration.ToTable("NewsletterSents");
            entityConfiguration.HasKey(o => new {o.NewsletterId, o.SubscriberId });
            entityConfiguration.Property(o => o.NewsletterId).ValueGeneratedNever();
            entityConfiguration.Property(o => o.SubscriberId).ValueGeneratedNever();
            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.HasOne(d => d.Newsletter)
                    .WithMany(e => e.NewsletterSent)
                    .HasForeignKey(f => new { f.NewsletterId })
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();

            entityConfiguration.HasOne(d => d.Subscriber)
                   .WithMany(e => e.NewsletterSent)
                   .HasForeignKey(f => new { f.SubscriberId })
                   .OnDelete(DeleteBehavior.Restrict)
                   .IsRequired();
        }
    }
}
