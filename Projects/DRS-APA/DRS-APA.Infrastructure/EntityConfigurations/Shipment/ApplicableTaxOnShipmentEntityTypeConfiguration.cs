using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders; 
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;

namespace DRS.APA.Infrastructure.EntityConfigurations.Shipment
{
    class ApplicableTaxOnShipmentEntityTypeConfiguration : IEntityTypeConfiguration<ApplicableTaxOnShipment>
    {
        public void Configure(EntityTypeBuilder<ApplicableTaxOnShipment> entityConfiguration)
        {
            entityConfiguration.ToTable("ApplicableTaxOnShipments");

            entityConfiguration.HasKey(o => new { o.ApplicableTaxOnShipmentId });
            entityConfiguration.Property(o => o.ApplicableTaxOnShipmentId).ValueGeneratedOnAdd(); 

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

      entityConfiguration.HasOne(d => d.TaxClass)
              .WithMany(e => e.ApplicableTaxOnShipment)
              .HasForeignKey(f => new { f.TaxClassId })
              .OnDelete(DeleteBehavior.Cascade);


    }
    }
}
