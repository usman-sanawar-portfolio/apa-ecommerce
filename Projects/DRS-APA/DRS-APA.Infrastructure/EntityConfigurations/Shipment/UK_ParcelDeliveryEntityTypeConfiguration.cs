using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;

namespace DRS.APA.Infrastructure.EntityConfigurations.Shipment
{
    class UK_ParcelDeliveryEntityTypeConfiguration : IEntityTypeConfiguration<UK_ParcelDelivery>
    {
        public void Configure(EntityTypeBuilder<UK_ParcelDelivery> entityConfiguration)
        {
            entityConfiguration.ToTable("UK_ParcelDeliveries");

            entityConfiguration.HasKey(o => new { o.UK_ParcelDeliveryId });
            entityConfiguration.Property(o => o.UK_ParcelDeliveryId).ValueGeneratedOnAdd();
            entityConfiguration.Property(o => o.Duration).HasColumnType("varchar(150)").IsRequired(false);
            entityConfiguration.Property(o => o.PricePerCustomKg).HasColumnType("decimal(18,4)").IsRequired(true);
            entityConfiguration.Property(o => o.CustomKgMax).IsRequired(true);
            entityConfiguration.Property(o => o.CustomKgMin).IsRequired(true);
            entityConfiguration.Property(o => o.AdditionalPerKg).HasColumnType("decimal(18,4)").IsRequired(true);
            entityConfiguration.Property(o => o.Notes).HasColumnType("varchar(200)").IsRequired(false);


            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

        }
    }
}
