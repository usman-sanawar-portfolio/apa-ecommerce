using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;

namespace DRS.APA.Infrastructure.EntityConfigurations.Shipment
{
    class PricePerRackEntityTypeConfiguration : IEntityTypeConfiguration<PricePerRack>
    {
        public void Configure(EntityTypeBuilder<PricePerRack> entityConfiguration)
        {
            entityConfiguration.ToTable("PricePerRacks");

            entityConfiguration.HasKey(o => new { o.PricePerRackId });
            entityConfiguration.Property(o => o.PricePerRackId).ValueGeneratedOnAdd();
            entityConfiguration.Property(o => o.Price).HasColumnType("decimal(18,4)").IsRequired(true);
            entityConfiguration.Property(o => o.Notes).HasColumnType("varchar(200)").IsRequired(false);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

        }
    }
}
