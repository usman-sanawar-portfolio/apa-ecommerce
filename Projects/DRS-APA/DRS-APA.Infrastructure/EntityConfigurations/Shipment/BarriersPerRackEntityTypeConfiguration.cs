using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;

namespace DRS.APA.Infrastructure.EntityConfigurations.Shipment
{
    class BarriersPerRackEntityTypeConfiguration : IEntityTypeConfiguration<BarriersPerRack>
    {
        public void Configure(EntityTypeBuilder<BarriersPerRack> entityConfiguration)
        {
            entityConfiguration.ToTable("BarriersPerRacks");

            entityConfiguration.HasKey(o => new { o.BarriersPerRackId });
            entityConfiguration.Property(o => o.BarriersPerRackId).ValueGeneratedOnAdd();
            entityConfiguration.Property(o => o.BarrierPerRackQty).IsRequired(true); 

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

         }
    }
}
