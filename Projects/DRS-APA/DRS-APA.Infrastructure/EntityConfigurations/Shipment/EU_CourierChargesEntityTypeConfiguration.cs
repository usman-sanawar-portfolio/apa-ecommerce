using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;

namespace DRS.APA.Infrastructure.EntityConfigurations.Shipment
{
    class EU_CourierChargesEntityTypeConfiguration : IEntityTypeConfiguration<EU_CourierCharges>
    {
        public void Configure(EntityTypeBuilder<EU_CourierCharges> entityConfiguration)
        {
            entityConfiguration.ToTable("EU_CourierChargess");

            entityConfiguration.HasKey(o => new { o.EU_CourierChargesId });
            entityConfiguration.Property(o => o.EU_CourierChargesId).ValueGeneratedOnAdd();
            entityConfiguration.Property(o => o.First20KgPrice).HasColumnType("decimal(18,4)").IsRequired(true); 
            entityConfiguration.Property(o => o.AdditionalPerKgPrice).HasColumnType("decimal(18,4)").IsRequired(true); 
            entityConfiguration.Property(o => o.TransitTimes).HasColumnType("varchar(100)").IsRequired(false);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.HasOne(x => x.GeoZones)
                .WithMany(c => c.EU_CourierCharges)
                .HasForeignKey(x => new { x.GeoZonesId })
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired(true);
         }
    }
}
