using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;

namespace DRS.APA.Infrastructure.EntityConfigurations.Shipment
{
    class UK_SpecialDeliveryPricesEntityTypeConfiguration : IEntityTypeConfiguration<UK_SpecialDeliveryPrices>
    {
        public void Configure(EntityTypeBuilder<UK_SpecialDeliveryPrices> entityConfiguration)
        {
            entityConfiguration.ToTable("UK_SpecialDeliveryPrices");

            entityConfiguration.HasKey(o => new { o.UK_SpecialDeliveryPricesId });
            entityConfiguration.Property(o => o.UK_SpecialDeliveryPricesId).ValueGeneratedOnAdd();
            entityConfiguration.Property(o => o.RetrofitDeliveryCost).HasColumnType("decimal(18,4)").IsRequired(true); 
            entityConfiguration.Property(o => o.OperatorDeliveryCost).HasColumnType("decimal(18,4)").IsRequired(true);
            entityConfiguration.Property(o => o.Notes).HasColumnType("varchar(200)").IsRequired(false); 

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

        }
    }
}
