﻿using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Infrastructure.EntityConfigurations.Website
{
    public class WebCardEntityTypeConfiguration : IEntityTypeConfiguration<WebCard>
    {
        public void Configure(EntityTypeBuilder<WebCard> entityConfiguration)
        {
            entityConfiguration.ToTable("WebCards");
            entityConfiguration.HasKey(o => new { o.WebCardId });
            entityConfiguration.Property(o => o.WebCardId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.Icon).HasColumnType("varchar(100)").IsRequired(false);
            entityConfiguration.Property(b => b.MainHeading).HasColumnType("varchar(200)").IsRequired(false);
            entityConfiguration.Property(b => b.SubHeading).HasColumnType("varchar(200)").IsRequired(false);
            entityConfiguration.Property(b => b.Order).IsRequired(true);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

            entityConfiguration.HasOne(d => d.WebComponent)
                  .WithMany(e => e.WebCards)
                  .HasForeignKey(f => new { f.WebComponentId })
                  .OnDelete(DeleteBehavior.Cascade)
                  .IsRequired();
        }
    }
}