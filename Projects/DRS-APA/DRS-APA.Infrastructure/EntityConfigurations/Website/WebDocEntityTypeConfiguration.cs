using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Infrastructure.EntityConfigurations.Website
{
    public class WebDocEntityTypeConfiguration : IEntityTypeConfiguration<WebDoc>
    {
        public void Configure(EntityTypeBuilder<WebDoc> entityConfiguration)
        {
            entityConfiguration.ToTable("WebDocs");
            entityConfiguration.HasKey(o => new { o.WebDocId });
            entityConfiguration.Property(o => o.WebDocId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.Icon).HasColumnType("varchar(100)").IsRequired(false);
            entityConfiguration.Property(b => b.Header).HasColumnType("varchar(200)").IsRequired(false);
            entityConfiguration.Property(b => b.DownloadLink).HasColumnType("varchar(200)").IsRequired(false);
            entityConfiguration.Property(b => b.Order).IsRequired(true);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

            entityConfiguration.HasOne(d => d.WebComponent)
                .WithMany(e => e.WebDocs)
                .HasForeignKey(f => new { f.WebComponentId })
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();
        }
    }
}
