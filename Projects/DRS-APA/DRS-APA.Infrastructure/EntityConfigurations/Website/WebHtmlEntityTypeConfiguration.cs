using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Infrastructure.EntityConfigurations.Website
{
    public class WebHtmlEntityTypeConfiguration : IEntityTypeConfiguration<WebHtml>
    {
        public void Configure(EntityTypeBuilder<WebHtml> entityConfiguration)
        {
            entityConfiguration.ToTable("WebHtmls");
            entityConfiguration.HasKey(o => new { o.WebHtmlId });
            entityConfiguration.Property(o => o.WebHtmlId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.HtmlData).IsRequired(true);            
            entityConfiguration.Property(b => b.Order).IsRequired(true);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

      entityConfiguration.HasOne(d => d.WebComponent)
          .WithMany(e => e.WebHtmls)
          .HasForeignKey(f => new { f.WebComponentId })
          .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
