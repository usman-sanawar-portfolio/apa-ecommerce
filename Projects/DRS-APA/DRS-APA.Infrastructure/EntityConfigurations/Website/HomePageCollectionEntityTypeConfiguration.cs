﻿using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Infrastructure.EntityConfigurations.Website
{
    public class HomePageCollectionEntityTypeConfiguration : IEntityTypeConfiguration<HomePageCollection>
    {
        public void Configure(EntityTypeBuilder<HomePageCollection> entityConfiguration)
        {
            entityConfiguration.ToTable("HomePageCollections");
            entityConfiguration.HasKey(o => new { o.HomePageCollectionId });
            entityConfiguration.Property(o => o.HomePageCollectionId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.ProductCategoryId).IsRequired(true);
          
            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

            entityConfiguration.HasOne(d => d.ProductCategory)
                  .WithMany(e => e.HomePageCollection)
                  .HasForeignKey(f => new { f.ProductCategoryId })
                  .OnDelete(DeleteBehavior.Cascade)
                  .IsRequired();
        }
    }
}