using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Infrastructure.EntityConfigurations.MasterData
{
    class WebPageEntityTypeConfiguration : IEntityTypeConfiguration<WebPage>
    {
        public void Configure(EntityTypeBuilder<WebPage> entityConfiguration)
        {
            entityConfiguration.ToTable("WebPages");
            entityConfiguration.HasKey(o => new { o.WebPageId });
            entityConfiguration.Property(o => o.WebPageId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.WebPageTitle).HasColumnType("varchar(100)").IsRequired(true);
            entityConfiguration.Property(b => b.Slug).HasColumnType("varchar(100)").IsRequired(true);
            entityConfiguration.HasIndex(b => b.Slug).IsUnique();
            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

      entityConfiguration.HasOne(d => d.WebMenu)
          .WithOne(e => e.WebPage)
          .HasForeignKey<WebMenu>(f => f.WebPageId)
          .OnDelete(DeleteBehavior.Cascade);

            //entityConfiguration.HasOne(d => d.WebSubMenu)
            //    .WithOne(e => e.WebPage)
            //    .HasForeignKey<WebSubMenu>(f => f.WebPageId)
            //    .OnDelete(DeleteBehavior.Restrict)
            //    .IsRequired(false);
        }
    }
}
