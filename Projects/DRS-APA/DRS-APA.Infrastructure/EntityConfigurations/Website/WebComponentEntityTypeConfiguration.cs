﻿using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Infrastructure.EntityConfigurations.Website
{
    public class WebComponentEntityTypeConfiguration : IEntityTypeConfiguration<WebComponent>
    {
        public void Configure(EntityTypeBuilder<WebComponent> entityConfiguration)
        {
            entityConfiguration.ToTable("WebComponents");
            entityConfiguration.HasKey(o => new { o.WebComponentId });
            entityConfiguration.Property(o => o.WebComponentId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.Title).HasColumnType("varchar(100)").IsRequired(true);
            entityConfiguration.Property(b => b.Type).HasColumnType("varchar(100)").IsRequired(true);
            
            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);
        }
    }
}