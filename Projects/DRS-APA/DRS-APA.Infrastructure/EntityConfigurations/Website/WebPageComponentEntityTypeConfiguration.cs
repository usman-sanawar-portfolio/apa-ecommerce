using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Infrastructure.EntityConfigurations.Website
{
    class WebPageComponentEntityTypeConfiguration : IEntityTypeConfiguration<WebPageComponent>
    {
        public void Configure(EntityTypeBuilder<WebPageComponent> entityConfiguration)
        {
            entityConfiguration.ToTable("WebPageComponents");
            entityConfiguration.HasKey(o => new { o.WebPageId, o.WebComponentId });
            entityConfiguration.Property(o => o.WebPageId).ValueGeneratedNever();
            entityConfiguration.Property(o => o.WebComponentId).ValueGeneratedNever();
            entityConfiguration.Property(b => b.Order).IsRequired(true);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

            entityConfiguration.HasOne(d => d.WebPage)
                   .WithMany(e => e.WebPageComponents)
                   .HasForeignKey(f => new { f.WebPageId })
                   .OnDelete(DeleteBehavior.Cascade)
                   .IsRequired();

      entityConfiguration.HasOne(d => d.WebComponent)
            .WithMany(e => e.WebPageComponents)
            .HasForeignKey(f => new { f.WebComponentId })
            .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
