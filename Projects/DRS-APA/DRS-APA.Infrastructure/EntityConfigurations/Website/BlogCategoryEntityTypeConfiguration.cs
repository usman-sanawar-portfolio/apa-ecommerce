using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders; 
using DRS.APA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Infrastructure.EntityConfigurations.Website
{
    class BlogCategoryEntityTypeConfiguration : IEntityTypeConfiguration<BlogCategory>
    {
        public void Configure(EntityTypeBuilder<BlogCategory> entityConfiguration)
        {
            entityConfiguration.ToTable("BlogCategories");

            entityConfiguration.HasKey(o => new { o.BlogCategoryId });
            entityConfiguration.Property(o => o.BlogCategoryId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.CategoryName).HasColumnType("varchar(255)").IsRequired(true);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

         
    }
    }
}
