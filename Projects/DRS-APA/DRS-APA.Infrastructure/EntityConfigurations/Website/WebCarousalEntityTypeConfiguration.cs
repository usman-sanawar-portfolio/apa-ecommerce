﻿using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Infrastructure.EntityConfigurations.Website
{
    public class WebCarousalEntityTypeConfiguration : IEntityTypeConfiguration<WebCarousal>
    {
        public void Configure(EntityTypeBuilder<WebCarousal> entityConfiguration)
        {
            entityConfiguration.ToTable("WebCarousals");
            entityConfiguration.HasKey(o => new { o.WebCarousalId });
            entityConfiguration.Property(o => o.WebCarousalId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.HeaderText).HasColumnType("varchar(100)").IsRequired(false);
            entityConfiguration.Property(b => b.SubText).HasColumnType("varchar(200)").IsRequired(false);
            entityConfiguration.Property(b => b.HeaderTextColor).HasColumnType("varchar(50)").IsRequired(false);
            entityConfiguration.Property(b => b.SubTextColor).HasColumnType("varchar(50)").IsRequired(false);
            entityConfiguration.Property(b => b.ShowButton).IsRequired(true);
            entityConfiguration.Property(b => b.ButtonText).HasColumnType("varchar(50)").IsRequired(false);
            entityConfiguration.Property(b => b.ButtonColor).HasColumnType("varchar(50)").IsRequired(false);
            entityConfiguration.Property(b => b.PageSlug).HasColumnType("varchar(50)").IsRequired(false);
            entityConfiguration.Property(b => b.ButtonUrl).HasColumnType("varchar(100)").IsRequired(false);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

        }
    }
}