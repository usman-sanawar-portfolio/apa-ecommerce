﻿using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Infrastructure.EntityConfigurations.Website
{
    public class WebCarousalPhotosEntityTypeConfiguration : IEntityTypeConfiguration<WebCarousalPhotos>
    {
        public void Configure(EntityTypeBuilder<WebCarousalPhotos> entityConfiguration)
        {
            entityConfiguration.ToTable("WebCarousalPhotos");
            entityConfiguration.HasKey(o => new { o.WebCarousalPhotosId });
            entityConfiguration.Property(o => o.WebCarousalPhotosId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.PhotoURL).HasColumnType("varchar(150)").IsRequired(true);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

            entityConfiguration.HasOne(x => x.WebCarousal)
                .WithMany(x => x.WebCarousalPhotos)
                .HasForeignKey(x => new { x.WebCarousalId })
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();
        }
    }
}