using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Infrastructure.EntityConfigurations.Website
{
    public class WebSubMenuEntityTypeConfiguration : IEntityTypeConfiguration<WebSubMenu>
    {
        public void Configure(EntityTypeBuilder<WebSubMenu> entityConfiguration)
        {
            entityConfiguration.ToTable("WebSubMenus");
            entityConfiguration.HasKey(o => new { o.WebSubMenuId });
            entityConfiguration.Property(o => o.WebSubMenuId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.WebSubMenuTitle).HasColumnType("varchar(200)").IsRequired(true);
            entityConfiguration.Property(b => b.Order).IsRequired(true);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

      entityConfiguration.HasOne(d => d.WebMenu)
            .WithMany(e => e.WebSubMenu)
            .HasForeignKey(f => f.WebMenuId)
            .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
