using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Infrastructure.EntityConfigurations.Website
{
    class BlogCommentEntityTypeConfiguration : IEntityTypeConfiguration<BlogComment>
    {
        public void Configure(EntityTypeBuilder<BlogComment> entityConfiguration)
        {
            entityConfiguration.ToTable("BlogComments");

            entityConfiguration.HasKey(o => new { o.BlogCommentId });
            entityConfiguration.Property(o => o.BlogCommentId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.Comment).HasColumnType("varchar(255)").IsRequired(true);
            entityConfiguration.Property(x => x.CustomerId).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            //One To Many Relation
            entityConfiguration.HasOne(o => o.Blog)
              .WithMany(m => m.BlogComment)
              .HasForeignKey(f => new { f.BlogId })
              .OnDelete(DeleteBehavior.Cascade)
              .IsRequired(false);

            entityConfiguration.HasOne(o => o.Customer)
             .WithMany(m => m.BlogCommentAuthor)
             .HasForeignKey(f => new { f.CustomerId })
             .OnDelete(DeleteBehavior.SetNull)
             .IsRequired(false);
        }
    }
}
