using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Microserivce.Infrastructure.EntityConfigurations.Website
{
    public class WebPhotoEntityTypeConfiguration : IEntityTypeConfiguration<WebPhoto>
    {
        public void Configure(EntityTypeBuilder<WebPhoto> entityConfiguration)
        {
            entityConfiguration.ToTable("WebPhotos");
            entityConfiguration.HasKey(o => new { o.WebPhotoId });
            entityConfiguration.Property(o => o.WebPhotoId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.OverlayMainText).IsRequired(false);
            entityConfiguration.Property(b => b.OverlayMainTextColor).IsRequired(false);
            entityConfiguration.Property(b => b.OverlaySubText).IsRequired(false);
            entityConfiguration.Property(b => b.OverlaySubTextColor).IsRequired(false);
            entityConfiguration.Property(b => b.ButtonText).IsRequired(false);
            entityConfiguration.Property(b => b.ButtonLink).IsRequired(false);
            entityConfiguration.Property(b => b.PhotoUrl).IsRequired(true);
            entityConfiguration.Property(b => b.PhotoSize).IsRequired(true);

            entityConfiguration.Property(b => b.Order).IsRequired(true);
            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.IsActive).IsRequired(false);

      entityConfiguration.HasOne(d => d.WebComponent)
          .WithMany(e => e.WebPhotos)
          .HasForeignKey(f => new { f.WebComponentId })
          .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
