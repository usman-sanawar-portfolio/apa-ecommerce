using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;

namespace DRS.APA.Infrastructure.EntityConfigurations.Website
{
    class BlogEntityTypeConfiguration : IEntityTypeConfiguration<Blog>
    {
        public void Configure(EntityTypeBuilder<Blog> entityConfiguration)
        {
            entityConfiguration.ToTable("Blogs");

            entityConfiguration.HasKey(o => new { o.BlogId });
            entityConfiguration.Property(o => o.BlogId).ValueGeneratedOnAdd();
            entityConfiguration.Property(b => b.BlogTitle).HasColumnType("varchar(255)").IsRequired(true);
            entityConfiguration.Property(b => b.Content).HasColumnType("varchar(MAX)").IsRequired(true);
            entityConfiguration.Property(b => b.IsApproved).IsRequired(true).HasDefaultValue(false);
            entityConfiguration.Property(b => b.ApprovedDate).HasColumnType("varchar(255)").IsRequired(false);
            entityConfiguration.Property(b => b.SeoH1).HasColumnType("varchar(255)").IsRequired(false);
            entityConfiguration.Property(b => b.SeoH2).HasColumnType("varchar(255)").IsRequired(false);
            entityConfiguration.Property(b => b.SeoH3).HasColumnType("varchar(255)").IsRequired(false);
            entityConfiguration.Property(b => b.MetaTagTitle).HasColumnType("varchar(255)").IsRequired(false);
            entityConfiguration.Property(b => b.MetaTagDescription).IsRequired(false);
            entityConfiguration.Property(b => b.MetaTagKeywords).HasColumnType("varchar(255)").IsRequired(false);


            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");
            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);
            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);
            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);


            //One To Many Relation
            entityConfiguration.HasOne(o => o.BlogCategory)
             .WithMany(m => m.Blog)
             .HasForeignKey(f => new { f.BlogCategoryId })
             .OnDelete(DeleteBehavior.Cascade)
             .IsRequired(false);

            entityConfiguration.HasOne(o => o.Customer)
           .WithMany(m => m.Blog)
           .HasForeignKey(f => new { f.CustomerId })
          .OnDelete(DeleteBehavior.SetNull)
             .IsRequired(false);
        }
    }
}
