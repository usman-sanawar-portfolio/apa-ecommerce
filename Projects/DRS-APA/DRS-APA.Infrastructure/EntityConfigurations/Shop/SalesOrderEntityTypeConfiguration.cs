﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Shop;

namespace DRS.APA.Infrastructure.EntityConfigurations.Shop
{
    class SalesOrderEntityTypeConfiguration : IEntityTypeConfiguration<SalesOrder>
    {
        public void Configure(EntityTypeBuilder<SalesOrder> entityConfiguration)
        {
            //entityConfiguration.ToTable("Regions", HRManagementContext.DEFAULT_SCHEMA);

            entityConfiguration.ToTable("SalesOrders");

            entityConfiguration.HasKey(o => new {o.SalesOrderId });

            //entityConfiguration.Ignore(b => b.DomainEvents);

            entityConfiguration.Property(o => o.SalesOrderId).ValueGeneratedOnAdd();

            entityConfiguration.Property(b => b.SalesOrderNumber).HasColumnType("varchar(30)").IsRequired(false);

            entityConfiguration.Property(b => b.Reference).HasColumnType("varchar(100)").IsRequired(false);

            entityConfiguration.Property(b => b.Comment).HasColumnType("varchar(250)").IsRequired(false);

            entityConfiguration.Property(b => b.Status).HasColumnType("varchar(35)").IsRequired(false);

            entityConfiguration.Property(b => b.State).HasColumnType("varchar(35)").IsRequired(false);

            entityConfiguration.Property(b => b.FulfillmentStatus).HasColumnType("varchar(35)").IsRequired(false);

            entityConfiguration.Property(b => b.PaymentGateway).HasColumnType("varchar(100)").IsRequired(false);

            entityConfiguration.Property(b => b.CancelReason).HasColumnType("varchar(150)").IsRequired(false);

            entityConfiguration.Property(b => b.PaymentMethod).HasColumnType("varchar(150)").IsRequired(false);

            entityConfiguration.Property(b => b.BrowserIp).HasColumnType("varchar(25)").IsRequired(false);

            entityConfiguration.Property(b => b.SubTotal).HasColumnType("decimal(19, 4)").IsRequired(false);

   
            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.OnlineOrder).IsRequired(false);



            entityConfiguration.OwnsOne(m => m.ShippingAddress, a =>
            {
                a.Property(p => p.Street).HasColumnType("varchar(150)").IsRequired(true).HasDefaultValue("");

                a.Property(p => p.City).HasColumnType("varchar(150)").IsRequired(true).HasDefaultValue("");

                a.Property(p => p.Country).HasColumnType("varchar(150)").IsRequired(true).HasDefaultValue("");

                a.Property(p => p.Phone).HasColumnType("varchar(80)").IsRequired(true).HasDefaultValue("");

                a.Property(p => p.FirstName).HasColumnType("varchar(150)").IsRequired(true).HasDefaultValue("");

                a.Property(p => p.LastName).HasColumnType("varchar(150)").IsRequired(true).HasDefaultValue("");

                a.Property(p => p.CompanyName).HasColumnType("varchar(100)").IsRequired(true).HasDefaultValue("");

                a.Property(p => p.ZipCode).HasColumnType("varchar(50)").IsRequired(true).HasDefaultValue("");

                a.Property(p => p.ZipCode).HasColumnType("varchar(50)").IsRequired(true).HasDefaultValue("");
            });


            entityConfiguration.OwnsOne(m => m.BillingAddress, a =>
            {
                a.Property(p => p.Street).HasColumnType("varchar(150)").IsRequired(true).HasDefaultValue("");

                a.Property(p => p.City).HasColumnType("varchar(150)").IsRequired(true).HasDefaultValue("");

                a.Property(p => p.Country).HasColumnType("varchar(150)").IsRequired(true).HasDefaultValue("");

                a.Property(p => p.Phone).HasColumnType("varchar(80)").IsRequired(true).HasDefaultValue("");

                a.Property(p => p.FirstName).HasColumnType("varchar(150)").IsRequired(true).HasDefaultValue("");

                a.Property(p => p.LastName).HasColumnType("varchar(150)").IsRequired(true).HasDefaultValue("");

                a.Property(p => p.CompanyName).HasColumnType("varchar(100)").IsRequired(true).HasDefaultValue("");

                a.Property(p => p.ZipCode).HasColumnType("varchar(50)").IsRequired(true).HasDefaultValue("");

                a.Property(p => p.ZipCode).HasColumnType("varchar(50)").IsRequired(true).HasDefaultValue("");
            });



            entityConfiguration.HasOne(d => d.Location)
                    .WithMany(e => e.SalesOrders)
                    .HasForeignKey(f => new { f.LocationId })
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();

            entityConfiguration.HasOne(d => d.DeliveryMethod)
                   .WithMany(e => e.SalesOrders)
                   .HasForeignKey(f => new { f.DeliveryMethodId })
                   .OnDelete(DeleteBehavior.Restrict)
                   .IsRequired(false);

            entityConfiguration.HasOne(d => d.Customer)
                .WithMany(e => e.SalesOrders)
                .HasForeignKey(f => new { f.CustomerId })
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}
