﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Shop;

namespace DRS.APA.Infrastructure.EntityConfigurations.Shop
{
    class LineItemEntityTypeConfiguration : IEntityTypeConfiguration<LineItem>
    {
        public void Configure(EntityTypeBuilder<LineItem> entityConfiguration)
        {
            //entityConfiguration.ToTable("Regions", HRManagementContext.DEFAULT_SCHEMA);

            entityConfiguration.ToTable("LineItems");

            entityConfiguration.HasKey(o => new {o.LineItemId });
            //entityConfiguration.Ignore(b => b.DomainEvents);
            entityConfiguration.Property(o => o.LineItemId).ValueGeneratedOnAdd();

            entityConfiguration.Property(b => b.UnitPrice).HasColumnType("decimal(19, 4)").IsRequired(true).HasDefaultValue(0);

            entityConfiguration.Property(b => b.CreatedBy).HasColumnType("varchar(128)").IsRequired(true).HasDefaultValue("admin");

            entityConfiguration.Property(b => b.UpdatedBy).HasColumnType("varchar(128)").IsRequired(false);

            entityConfiguration.Property(b => b.UpdatedOn).IsRequired(false);

            entityConfiguration.Property(b => b.CreatedOn).IsRequired(false);

            entityConfiguration.HasOne(d => d.SalesOrder)
                    .WithMany(e => e.LineItems)
                    .HasForeignKey(f => new { f.SalesOrderId })
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();

            entityConfiguration.HasOne(d => d.RepairOption)
                   .WithMany(e => e.LineItems)
                   .HasForeignKey(f => new { f.RepairOptionId })
                   .OnDelete(DeleteBehavior.Restrict)
                   .IsRequired(false);

        }
    }
}
