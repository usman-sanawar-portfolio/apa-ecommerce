﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.Domain.Core.Specifications;
using DRS.APA.API.Services;
using DRS.APA.Microservice.API.Controllers.Common;
using DRS.APA.Microserivce.Domain.Core.Dtos.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Dtos.Shared;


[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class AttachmentsController : Controller
    {
        private readonly IAttachmentRepository _attachmentRepository;
       
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        protected readonly LinkGenerator _linkGenerator;

        public AttachmentsController(IAttachmentRepository attachmentRepository, 
        IUnitOfWork unitOfWork, IMapper mapper, ILogger<AttachmentsController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _attachmentRepository = attachmentRepository;
            
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }

        /// <summary>
        /// List all Attachments, by default sorted by AttachmentName
        /// </summary>
        /// <remarks>With pagination, you can fetch up to x records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetAttachments")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAttachments(
            string sortOrder,int? productId, bool? isVisible, string searchString, int? page = 1, int? pageSize = ConstantProps.maxPageSize)
        {
            try
            {
                ResponseObject resObject;
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var dataFromRepo = await _attachmentRepository.GetAllAttachments();
                    if (isVisible != null)
                    {
                        dataFromRepo = dataFromRepo.Where(x => x.IsVisible == isVisible);
                    }
                    var dataList = _mapper.Map<IEnumerable<AttachmentDto>>(dataFromRepo);
                 
                    resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                    return Json(resObject);
                }

                // Sorting -------
                HttpContext.Session.SetString("NameSortParam", String.IsNullOrEmpty(sortOrder) ? "name_desc" : "");
                HttpContext.Session.SetString("NameSortParam", String.IsNullOrEmpty(sortOrder) ? "name_asc" : "");

                HttpContext.Session.SetString("TypeSortParam", String.IsNullOrEmpty(sortOrder) ? "type_desc" : "");
                HttpContext.Session.SetString("TypeSortParam", String.IsNullOrEmpty(sortOrder) ? "type_asc" : "");

                HttpContext.Session.SetString("TitleSortParam", String.IsNullOrEmpty(sortOrder) ? "title_desc" : "");
                HttpContext.Session.SetString("TitleSortParam", String.IsNullOrEmpty(sortOrder) ? "title_asc" : "");

            

                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;
                }
                IQueryable<Attachment> data = null;
                var filterSpecification = new AttachmentFilterSpecification(productId,isVisible);
                data = await _attachmentRepository.GetAllAttachments(filterSpecification);
              

               
               
                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    data = data.Where(s => s.Name.Contains(searchString)
                                             || s.Title.Contains(searchString)
                                             || s.Type.Contains(searchString)
                                             );
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "name_desc":
                        data = data.OrderByDescending(s => s.Name);
                        break;
                    case "title_desc":
                        data = data.OrderByDescending(s => s.Title);
                        break;
                    case "title_asc":
                        data = data.OrderBy(s => s.Title);
                        break;
                    case "type_desc":
                        data = data.OrderByDescending(s => s.Type);
                        break;
                    case "type_asc":
                        data = data.OrderBy(s => s.Type);
                        break;
                  
                    default:
                        data = data.OrderBy(s => s.Name);
                        break;
                }//
        

                // calculate data for metadata
                var totalCount = await data.CountAsync();
                PaggingAndSorting paggingSorting = new PaggingAndSorting(_linkGenerator);
                paggingSorting.PaggingAndSortingData(HttpContext, totalCount, "GetAttachments", page, pageSize);

                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();

                var pagedData = _mapper.Map<IEnumerable<AttachmentDto>>(items);

    

                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetAttachments");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }
        /// <summary>
        /// Get Attachment By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetAttachmentById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAttachmentById(int id)  // --------------------------------------------------
        {
            try
            {

                ResponseObject resObject;

                _logger.LogInformation("GetAttachmentById");
                var dataFromRepo = await _attachmentRepository.GetAttachmentById(id);

                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetAttachmentById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                var data = _mapper.Map<AttachmentDto>(dataFromRepo);

                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetAttachmentById");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }
        /// <summary>
        /// Add Attachment - POST api/Attachment
        /// </summary>
        /// <param name="attachment"></param>
        /// <returns></returns>

        [HttpPost(Name = "AddAttachment")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddAttachment([FromBody] AttachmentDto attachment)
        {
            try
            {
                ResponseObject resObject;
                if (attachment == null)
                {
                    _logger.LogError(ConstantProps.NullObjectError("Attachment"));
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (AttachmentDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidObjectError("Attachment"));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var attachmentRepo = _mapper.Map<Attachment>(attachment);
                await _attachmentRepository.AddAttachment(attachmentRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "Attachment {ID} Created", attachmentRepo.AttachmentId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Adding", "Attachment"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetAttachmentById), new { id = attachmentRepo.AttachmentId }, attachmentRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<AttachmentDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SubmittedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Create");
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject
                {
                    Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

        // PUT api/Attachment/5
        [HttpPut("{id}", Name = "UpdateAttachment")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateAttachment(int id, [FromBody] AttachmentDto attachment)
        {
            try
            {
                ResponseObject resObject;
                if (attachment == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = (AttachmentDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidDataModelText);
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var attachmentRepo = _mapper.Map<Attachment>(attachment);
                if (!_attachmentRepository.UpdateAttachmentOk(id, attachmentRepo))
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Updating", "Attachment"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetAttachmentById), new { id = attachmentRepo.AttachmentId }, attachmentRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<AttachmentDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SavedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message2 = msg,
                    Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

        /// <summary>
        /// DELETE api/Attachment/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}", Name = "DeleteAttachment")]
        public async Task<IActionResult> DeleteAttachment(int id)
        {
            try
            {
                ResponseObject resObject;
                var entity = _attachmentRepository.GetAttachmentByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                _attachmentRepository.DeleteAttachment(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Deleting", "Attachment"));
                }
                resObject = new ResponseObject
                {
                    Message = ConstantProps.DeletedSuccessfullyText,
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Delete");
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }
    }
}
