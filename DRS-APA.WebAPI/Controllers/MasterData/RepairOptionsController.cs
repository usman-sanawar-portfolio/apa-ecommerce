﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Shared;

//using System.Data.Entity.Validation;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class RepairOptionsController : Controller
    {
        private readonly IRepairOptionRepository _repairOptionRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const int maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;

       
        public RepairOptionsController(IRepairOptionRepository jobtypeRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<RepairOptionsController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _repairOptionRepository = jobtypeRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }               

        /// <summary>
        /// List all public RepairOptions sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetRepairOptions")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetRepairOptions(int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var repairOptionsFromRepo = await _repairOptionRepository.GetAllRepairOptions();
                    var repairOptionsList = _mapper.Map<IEnumerable<RepairOptionDto>>(repairOptionsFromRepo);
                    return Ok(repairOptionsList);
                }

                IQueryable<RepairOption> repairOptions = null;
                repairOptions = await _repairOptionRepository.GetAllRepairOptions(true);

                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount =await repairOptions.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetRepairOptions", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetRepairOptions", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";
                
                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

                repairOptions = await _repairOptionRepository.GetAllRepairOptions(true);
                // return result
                var items = await repairOptions
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();
                //.Select(eg => _expenseGroupFactory.CreateDataShapedObject(eg, lstOfFields))

                var pagedItems = _mapper.Map<IEnumerable<RepairOptionDto>>(items);
                return Ok(pagedItems);                

            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetItems Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }

       

      
        //[HttpGet("models/{modelId}", Name = "GetRepairOptionsByModel")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status500InternalServerError)]
        //public async Task<IActionResult> GetRepairOptionsByModel(int modelId)
        //{
        //    try
        //    {

        //        var models = await _repairOptionRepository.GetRepairOptionsByModel(modelId);
        //        // return result

        //        Product product = await _repairOptionRepository.GetProductByType();
        //        var productId = product.ProductId;
        //        var pagedItems = _mapper.Map<IEnumerable<RepairOptionDto>>(models);
        //        foreach (var item in pagedItems)
        //        {
        //            item.ProductId = productId;
        //            item.ProductName = "Repair Service";
        //        }

        //        return Ok(pagedItems);

        //    }
        //    catch (Exception e)
        //    {
        //        string msg = "Unable to Process GetItems Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
        //        _logger.LogError(msg);
        //        return StatusCode((int)HttpStatusCode.InternalServerError, msg);
        //    }
        //}




        [HttpGet("{id}", Name = "GetRepairOptionById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]

        public async Task<IActionResult> GetRepairOptionById(short id)  // --------------------------------------------------
        {
            try
            {
                _logger.LogInformation("GetRepairOptionById");
                var repairOptionFromRepo = await _repairOptionRepository.GetRepairOptionById(id);
                if (repairOptionFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetRepairOptionById({ID}) NOT FOUND", id);
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found!-" + id);
                    //return NotFound();
                }

                var repairOption = _mapper.Map<RepairOptionDto>(repairOptionFromRepo);
                return Ok(repairOption);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetById Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }
     
        // POST api/RepairOption
        [HttpPost(Name = "AddRepairOption")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddRepairOption([FromBody] RepairOptionDto repairOption)  // ---------------------
        {
            try
            {

                if (repairOption == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, repairOption);
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(ModelState);
                }

                var repairOptionRepo = _mapper.Map<RepairOption>(repairOption);
                await _repairOptionRepository.AddRepairOption(repairOptionRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", repairOptionRepo.RepairOptionId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a RepairOption failed on save.");
                }
                return CreatedAtAction(nameof(GetRepairOptionById), new { id = repairOptionRepo.RepairOptionId }, repairOptionRepo);
                //return StatusCode((int)HttpStatusCode.Created);
                // return Content("{ \"message\":\"Form Submitted\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process Create Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        // PUT api/RepairOption/5
        [HttpPut("{id}", Name = "UpdateRepairOption")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateRepairOption(short id, [FromBody] RepairOptionDto repairOption) // ------------
        {
            try
            {

                if (repairOption == null)
                {
                    _logger.LogError("Object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, repairOption);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid object sent from client-ModelState");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(ModelState);
                }

                var repairOptionRepo = _mapper.Map<RepairOption>(repairOption);
                if (!_repairOptionRepository.UpdateRepairOptionOk(id, repairOptionRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    //return NotFound();
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a RepairOption Template failed on save.");
                }

                return Ok(repairOptionRepo);
                // return Content("{ \"message\":\"Form Updated\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }
        }

        // DELETE api/RepairOption/
        [HttpDelete("{id}", Name = "DeleteRepairOption")]
        public async Task<IActionResult> DeleteRepairOption(short id) // -----------------------------------------------------
        {
            try
            {
                var entity = _repairOptionRepository.GetRepairOptionByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound);
                    //return NotFound();
                }

                _repairOptionRepository.DeleteRepairOption(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a RepairOption Template failed on save.");
                }
                return Content("{ \"message\":\"RepairOption Deleted\",\"type\":\"success\" }", "application/json");
            }
            catch (Exception e)
            {
                string msg = "Unable to Process Delete Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }
        /// <summary>
        /// Get By Name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet("GetRepairOptionByName/{name}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetRepairOptionByName(string name) // ---------------------------------------------
        {
            try
            {

                _logger.LogInformation("GetByName");
                var repairOptionFromRepo = await _repairOptionRepository.GetRepairOptionByName(name);
                if (repairOptionFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetByName({name}) NOT FOUND", name);
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found By Name:-" + name);
                    //return NotFound();
                }
                return Ok(name);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetRepairOptionByName Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }


    }
}
