﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
//using System.Data.Entity.Validation;

namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class CurrenciesController : Controller
    {
        private readonly ICurrencyRepository _currencyRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const short maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;

        public CurrenciesController(ICurrencyRepository currencyRepository, IUnitOfWork unitOfWork, IMapper mapper,
            ILogger<CurrenciesController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;

            _currencyRepository = currencyRepository;

            _mapper = mapper;

            _logger = logger;

            _linkGenerator = linkGenerator;
        }



        /// <summary>
        /// List all public Currencies sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetCurrencies")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetCurrencies(string sortOrder, string searchString, int? page = 1, int? pageSize = maxPageSize)

        //public async Task<IActionResult> GetCurrencies(string sortOrder, int? contactId, int? leadId, string searchString, int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                ResponseObject resObject;

                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");

                    var dataFromRepo = await _currencyRepository.GetAllCurrencies();
                    var dataList = _mapper.Map<IEnumerable<CurrencyDto>>(dataFromRepo);

                    resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };

                    return Json(resObject);
                }

                // Sorting -------
                HttpContext.Session.SetString("NameSortParam", String.IsNullOrEmpty(sortOrder) ? "name_desc" : "");
                HttpContext.Session.SetString("NameSortParam", String.IsNullOrEmpty(sortOrder) ? "name_asc" : "");

                HttpContext.Session.SetString("EmailSortParam", String.IsNullOrEmpty(sortOrder) ? "email_desc" : "");
                HttpContext.Session.SetString("EmailSortParam", String.IsNullOrEmpty(sortOrder) ? "email_asc" : "");

                HttpContext.Session.SetString("PhoneSortParam", String.IsNullOrEmpty(sortOrder) ? "phone_desc" : "");
                HttpContext.Session.SetString("PhoneSortParam", String.IsNullOrEmpty(sortOrder) ? "phone_asc" : "");

                HttpContext.Session.SetString("ContactSortParam", String.IsNullOrEmpty(sortOrder) ? "contact_desc" : "");
                HttpContext.Session.SetString("ContactSortParam", String.IsNullOrEmpty(sortOrder) ? "contact_asc" : "");

                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_asc" : "");
                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_desc" : "");

                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;
                }

                IQueryable<Currency> data = null;

                //var filterSpecification = new CurrenciesFilterSpecification(currenciesCategoryId, makeId);

                data = await _currencyRepository.GetAllCurrencies(true);

                //currencies = await currencyRepository.GetAllCurrencies(true);

                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    data = data.Where(s => s.Name.Contains(searchString)
                                             || s.CurrencyCode.Contains(searchString));
                    //|| s.Model.Name.Contains(searchString)
                    //|| s.Model.Make.Name.Contains(searchString));
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "name_desc":
                        data = data.OrderByDescending(s => s.Name);
                        break;
                    case "price_desc":
                        data = data.OrderByDescending(s => s.CurrencyCode);
                        break;
                    case "price_asc":
                        data = data.OrderBy(s => s.CurrencyCode);
                        break;
                    //case "make_desc":
                    //    data = data.OrderByDescending(s => s.Make.Name);
                    //    break;
                    //case "make_asc":
                    //    data = data.OrderBy(s => s.Make.Name);
                    //    break;
                    case "id_asc":
                        data = data.OrderByDescending(s => s.CurrencyId);
                        break;
                    case "id_desc":
                        data = data.OrderByDescending(s => s.CurrencyId);
                        break;
                    default:
                        data = data.OrderBy(s => s.Name);
                        break;
                }//
                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount = await data.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetCurrencies", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetCurrencies", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";

                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));


                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();
                //.Select(eg => _expenseGroupFactory.CreateDataShapedObject(eg, lstOfFields))

                var pagedData = _mapper.Map<IEnumerable<CurrencyDto>>(items);

                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };

                return Json(resObject);

            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetItems Request";

                _logger.LogError(msg);

                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

                return Json(resObject);
            }
        }
       

        /// <summary>
        /// Get Contact Person By Id (with Contact attached)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetCurrencyById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetCurrencyById(short id)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;

                _logger.LogInformation("GetCurrencyById");

                var dataFromRepo = await _currencyRepository.GetCurrencyById(id);


                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetCollectionById({ID}) NOT FOUND", id);

                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };

                    return Json(resObject);
                }

                var data = _mapper.Map<CurrencyDto>(dataFromRepo);

                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };

                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetById Request";

                _logger.LogError(msg);

                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

                return Json(resObject);
            }

        }
        /// <summary>
        /// Add New Contact Person - POST api/Currencies
        /// </summary>
        /// <param name="currencies"></param>
        /// <returns></returns>

        [HttpPost(Name = "AddCurrencies")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddCurrencies([FromBody] CurrencyDto currencies)  // ---------------------
        {
            try
            {
                ResponseObject resObject;

                if (currencies == null)
                {
                    _logger.LogError("Owner object sent from client is null.");

                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = (CurrencyDto)null, HttpStatusCode = HttpStatusCode.NotFound };

                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");

                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model!",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var currenciesRepo = _mapper.Map<Currency>(currencies);

                await _currencyRepository.AddCurrency(currenciesRepo);

                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", currenciesRepo.CurrencyId);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a Currencies failed on save.");
                }

                //  var contactFromRepo = await currencyRepository.GetCurrenciesById(currenciesRepo.CurrenciesId);
                var contactFromRepo2 = CreatedAtAction(nameof(GetCurrencyById), new { id = currenciesRepo.CurrencyId }, currenciesRepo);

                var contactFromRepo = contactFromRepo2.Value;

                var data = _mapper.Map<CurrencyDto>(contactFromRepo);

                resObject = new ResponseObject
                {
                    Message = "Submitted Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Sever Error, Unable to Process Create Request";

                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

                return Json(resObject);
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        // PUT api/Currencies/5
        [HttpPut("{id}", Name = "UpdateCurrencies")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateCurrencies(short id, [FromBody] CurrencyDto currencies) // ------------
        {
            try
            {
                ResponseObject resObject;

                if (currencies == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = "Data not Found, BadRequest",
                        Status = "error",
                        Data = (CurrencyDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Data");

                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model, BadRequest",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var currenciesRepo = _mapper.Map<Currency>(currencies);

                if (!_currencyRepository.UpdateCurrencyOk(id, currenciesRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    // return StatusCode((int) HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a Currencies failed on save.");
                }

                //var contactFromRepo = await currencyRepository.GetCurrenciesById(currenciesRepo.CurrenciesId);
                var contactFromRepo2 = CreatedAtAction(nameof(GetCurrencyById), new { id = currenciesRepo.CurrencyId }, currenciesRepo);

                var contactFromRepo = contactFromRepo2.Value;

                var data = _mapper.Map<CurrencyDto>(contactFromRepo);

                resObject = new ResponseObject

                {
                    Message = "Saved Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }
        }

        /// <summary>
        /// DELETE api/Currencies/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}", Name = "DeleteCurrencies")]
        public async Task<IActionResult> DeleteCurrencies(short id) // -----------------------------------------------------
        {
            try
            {
                ResponseObject resObject;

                var entity = _currencyRepository.GetCurrencyByIdAsNoTracking(id);

                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");

                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                _currencyRepository.DeleteCurrency(id);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a Currencies failed on save.");
                }

                resObject = new ResponseObject
                {
                    Message = "Deleted Successfully",
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process Delete Request";
                _logger.LogError(msg);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }
        }

        [HttpGet("GetCurrenciesByName/{name}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetCurrenciesByName(string name) // ---------------------------------------------
        {
            try
            {
                ResponseObject resObject;

                _logger.LogInformation("GetByName");

                var currenciesFromRepo = await _currencyRepository.GetCurrencyByName(name);

                var currenciesList = _mapper.Map<IEnumerable<CurrencyDto>>(currenciesFromRepo);

                if (currenciesFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetByName({name}) NOT FOUND", name);

                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = name,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                resObject = new ResponseObject { Message = "Data Found!", Status = "success", Data = currenciesList, HttpStatusCode = HttpStatusCode.OK };

                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetCurrenciesByName Request";

                _logger.LogError(msg);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }

        }
    }

}

