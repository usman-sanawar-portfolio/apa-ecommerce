﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using DRS.APA.API.Helpers;
using DRS.APA.API.Services;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
//using System.Data.Entity.Validation;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class CartsController : Controller
    {
        private readonly ICartsRepository _cartRepository;

        private IWebSiteOrderingService _service;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const int maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;

        const string SessionName = "DRS.APAcart";

        public CartsController(ICartsRepository cartRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<CartsController> logger, LinkGenerator linkGenerator, IWebSiteOrderingService service)
        {
            _unitOfWork = unitOfWork;
            _cartRepository = cartRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
            _service = service;
        }               

        /// <summary>
        /// List all public Carts sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetCarts")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetCarts(int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var cartsFromRepo = await _cartRepository.GetAllCarts();
                    var cartslist = _mapper.Map<IEnumerable<NewCartDto>>(cartsFromRepo);
                    return Ok(cartslist);
                }

                IQueryable<NewCart> carts = null;
                carts = await _cartRepository.GetAllCarts(true);

                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount =await carts.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetCarts", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetCarts", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";
                
                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

               // carts = await _cartRepository.GetAllCarts(true);
                // return result
                var items = await carts
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();
                //.Select(eg => _expenseGroupFactory.CreateDataShapedObject(eg, lstOfFields))

                var pagedItems = _mapper.Map<IEnumerable<NewCartDto>>(items);
                return Ok(pagedItems);                

            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetItems Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }



        /// <summary>
        /// List all public Carts sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("items",Name = "GetCartItems")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetCartItems()
        {
            try
            {

                var legacyCookie = HttpContext.Request.Cookies["DRS.APAcartcookie"];
                if (legacyCookie != null)
                {

                    _logger.LogInformation("Generating List");
                    var cartsFromRepo = await _cartRepository.GetCartItems(legacyCookie.ToString());
                    var cartslist = _mapper.Map<IEnumerable<CartItemDto>>(cartsFromRepo);
                    return Ok(cartslist);

                }
                else
                {
                    return StatusCode((int)HttpStatusCode.InternalServerError, "cookie not found");
                }
               



            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetItems Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }


        

        [HttpGet("{id}", Name = "GetCartById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetCartById(int id)  // --------------------------------------------------
        {
            try
            {

                  
                _logger.LogInformation("GetCartById");
                var cartFromRepo = await _cartRepository.GetCartById(id);
                if (cartFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetCartById({ID}) NOT FOUND", id);
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found!-" + id);
                    //return NotFound();
                }
             
               var cart = _mapper.Map<NewCartDto>(cartFromRepo);
                return Ok(cart);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetById Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }

        // POST api/Cart
        //[HttpPost(Name = "AddCart")]
        //[ProducesResponseType(StatusCodes.Status201Created)]
        //[ProducesResponseType(StatusCodes.Status500InternalServerError)]
        //public async Task<IActionResult> AddCart([FromBody] NewCartDto cart)  // ---------------------
        //{
        //    try
        //    {

        //        if (cart == null)
        //        {
        //            _logger.LogError("Owner object sent from client is null.");
        //            return StatusCode((int)HttpStatusCode.BadRequest, cart);
        //        }
        //        if (!ModelState.IsValid)
        //        {
        //            _logger.LogError("Invalid owner object sent from client.");
        //            return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
        //            //return BadRequest(CartState);
        //        }

        //        var cartRepo = _mapper.Map<NewCart>(cart);
        //        await _cartRepository.AddCart(cartRepo);
        //        _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", cartRepo.CartId);
        //        if (!await _unitOfWork.CompleteAsync())
        //        {
        //            throw new Exception("Adding a Cart failed on save.");
        //        }
        //        return CreatedAtAction(nameof(GetCartById), new { id = cartRepo.CartId }, cartRepo);
        //        //return StatusCode((int)HttpStatusCode.Created);
        //        // return Content("{ \"message\":\"Form Submitted\",\"type\":\"success\" }", "application/json");
        //    }

        //    catch (Exception e)
        //    {
        //        string msg = "Unable to Process Create Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
        //        _logger.LogWarning(msg);
        //        return StatusCode((int)HttpStatusCode.InternalServerError, msg);
        //        //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
        //    }

        //}
        //---------------------------
        [HttpPost(Name = "AddToCart")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddToCart([FromBody] CartProductDto dto)  
        {
            if (dto.Add)
            {

                var createdCart =
                    await _service.ItemSelected(dto.ProductId, dto.Quantity, dto.Price,
                        "http://DRS.APA.com", dto.memebercookie, 0);

                if (createdCart == null)
                {
                    return NotFound();
                }

                //quantity = 1;
                try
                {
                    //var legacyCookie = HttpContext.Request.Cookies["DRS.APAcartcookie"].FirstOrDefault();
                    var legacyCookie = HttpContext.Request.Cookies["DRS.APAcartcookie"];
                    if (legacyCookie != null)
                    {
                        // sessionId = legacyCookie.ToString();
                        HttpContext.Session.SetString(SessionName, "DRS.APAcartsession");
                        //TempData["carttempdata"] = HttpContext.Session.GetString(SessionName);

                    }
                    else
                    {
                        var option = new CookieOptions();
                        option.Expires = DateTime.Now.AddDays(1);
                        Response.Cookies.Append("DRS.APAcartcookie", createdCart.CartCookie, option);
                        //CookieUtilities.BuildCartCookie(createdCart.CartCookie, DateTime.Now.AddDays(1));

                    }
                }
                catch (Exception)
                {
                    // ignored
                }
                //////var legacyCookie =HttpContext.Request.Cookies["DRS.APAcartcookie"].FirstOrDefault();

                ////// var cookiename = legacyCookie.ToString();
               

               

                //string sessionId = "";
                //if (legacyCookie != null)
                //{
                //    sessionId = legacyCookie.ToString();

                //}
                //else
                //{

                //    CookieUtilities.BuildCartCookie(cookiename, DateTime.Now.AddDays(1));

                //}

                return Ok();
            }
            else
            {


                //var cartproduct = _context.CartItems.SingleOrDefault(m => m.CartItemId == dto.ProductId);

                //if (cartproduct == null)
                //{
                //    // return HttpNotFound();
                //}

                //_context.CartItems.Remove(cartproduct);
                //_context.SaveChanges();
                return Ok();
            }

        }

        //------------------------
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        // PUT api/Cart/5
        [HttpPut("{id}", Name = "UpdateCart")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateCart(int id, [FromBody] NewCartDto cart) // ------------
        {
            try
            {

                if (cart == null)
                {
                    _logger.LogError("Object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, cart);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid object sent from client-CartState");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(CartState);
                }

                var cartRepo = _mapper.Map<NewCart>(cart);
                if (!_cartRepository.UpdateCartOk(id, cartRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    //return NotFound();
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a Cart Template failed on save.");
                }

                return Ok(cartRepo);
                // return Content("{ \"message\":\"Form Updated\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }
        }

        // DELETE api/Cart/
        [HttpDelete("{id}", Name = "DeleteCart")]
        public async Task<IActionResult> DeleteCart(int id) // -----------------------------------------------------
        {
            try
            {
                var entity = _cartRepository.GetCartByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound);
                    //return NotFound();
                }

                _cartRepository.DeleteCart(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a Cart Template failed on save.");
                }
                return Content("{ \"message\":\"Cart Deleted\",\"type\":\"success\" }", "application/json");
            }
            catch (Exception e)
            {
                string msg = "Unable to Process Delete Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }
        
        // PUT api/Cart/5
        [HttpPut("cart-item/{id}", Name = "UpdateCartItem")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateCartItem(int cartitemId, int quantity) // ------------
        {
            try
            {

               _cartRepository.UpdateCart(cartitemId,quantity);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a Cart Template failed on save.");
                }

                return Ok();
                // return Content("{ \"message\":\"Form Updated\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }
        }

        // DELETE api/Cart/
        [HttpDelete("cartitem/{id}", Name = "DeleteCartItem")]
        public async Task<IActionResult> DeleteCartItem(int id) // -----------------------------------------------------
        {
            try
            {
                var entity = _cartRepository.GetCartByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound);
                    //return NotFound();
                }

                _cartRepository.DeleteCart(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a Cart Template failed on save.");
                }
                return Content("{ \"message\":\"Cart Deleted\",\"type\":\"success\" }", "application/json");
            }
            catch (Exception e)
            {
                string msg = "Unable to Process Delete Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }

    }
}
