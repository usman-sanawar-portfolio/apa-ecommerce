using System;
using System.Collections.Generic;

using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class DiscountCustomerGroupsController : Controller
    {
        private readonly IDiscountCustomerGroupRepository _discountCustomerGroupRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const int maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;


        public DiscountCustomerGroupsController(IDiscountCustomerGroupRepository repairOptionRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<DiscountCustomerGroupsController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _discountCustomerGroupRepository = repairOptionRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }
        /// <summary>
        /// List all public DiscountCustomerGroups sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetDiscountCustomerGroups")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetDiscountCustomerGroups(string sortOrder, string searchString, int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                ResponseObject resObject;

                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");

                    var dataFromRepo = await _discountCustomerGroupRepository.GetAllDiscountCustomerGroups();
                    var dataList = _mapper.Map<IEnumerable<DiscountCustomerGroupDto>>(dataFromRepo);

                    resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };

                    return Json(resObject);
                }

                // Sorting -------
                HttpContext.Session.SetString("CustomerGroupNameSortParam", String.IsNullOrEmpty(sortOrder) ? "customerGroupName_desc" : "");
                HttpContext.Session.SetString("CustomerGroupNameSortParam", String.IsNullOrEmpty(sortOrder) ? "customerGroupName_asc" : "");

                HttpContext.Session.SetString("DiscountNameSortParam", String.IsNullOrEmpty(sortOrder) ? "discountName_desc" : "");
                HttpContext.Session.SetString("DiscountNameSortParam", String.IsNullOrEmpty(sortOrder) ? "discountName_asc" : "");

                HttpContext.Session.SetString("DiscountIdSortParam", String.IsNullOrEmpty(sortOrder) ? "discountId_desc" : "");
                HttpContext.Session.SetString("DiscountIdSortParam", String.IsNullOrEmpty(sortOrder) ? "discountId_asc" : "");


                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;
                }

                IQueryable<DiscountCustomerGroup> data = null;

                //var filterSpecification = new DiscountCustomerGroupFilterSpecification(contactId, leadId);

                data = await _discountCustomerGroupRepository.GetAllDiscountCustomerGroups(true);

                //discountCustomerGroups = await _discountCustomerGroupRepository.GetAllDiscountCustomerGroups(true);

                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);

                    data = data.Where(s => s.CustomerGroup.CustomerGroupName.Contains(searchString)
                                             || s.CustomerGroup.Description.Contains(searchString)
                                            

                    );

                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "customerGroupName_desc":
                        data = data.OrderByDescending(s => s.CustomerGroup.CustomerGroupName);
                        break;
                    case "discountName_desc":
                        data = data.OrderByDescending(s => s.Discount.Name);
                        break;
                    case "discountName_asc":
                        data = data.OrderBy(s => s.Discount.Name);
                        break;
                    
                    case "contactName_desc":
                        data = data.OrderByDescending(s => s.Discount.Name);
                        break;
                    case "contactName_asc":
                        data = data.OrderBy(s => s.Discount.Name);
                        break;
                    case "discountId_asc":
                        data = data.OrderByDescending(s => s.DiscountId);
                        break;
                    case "discountId_desc":
                        data = data.OrderByDescending(s => s.DiscountId);
                        break;
                    default:
                        data = data.OrderBy(s => s.CustomerGroup.CustomerGroupName);
                        break;
                }
                //
                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount = await data.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetDiscountCustomerGroups", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetDiscountCustomerGroups", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";

                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));


                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();
                //.Select(eg => _expenseGroupFactory.CreateDataShapedObject(eg, lstOfFields))

                var pagedData = _mapper.Map<IEnumerable<DiscountCustomerGroupDto>>(items);

                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };

                return Json(resObject);

            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetItems Request";

                _logger.LogError(msg);

                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

                return Json(resObject);
            }
        }

        /// <summary>
        /// GetDiscountCustomerGroupById
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetDiscountCustomerGroupById/{customerGroupId}/{discountId}", Name = "GetDiscountCustomerGroupById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetDiscountCustomerGroupById(short discountId, int customerGroupId)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;

                _logger.LogInformation("GetDiscountCustomerGroupById");

                var dataFromRepo = await _discountCustomerGroupRepository.GetDiscountCustomerGroupById(discountId,customerGroupId);


                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetCollectionById({discountId},{customerGroupId}) NOT FOUND", discountId, customerGroupId);

                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data =customerGroupId.ToString()+"/"+discountId.ToString(), HttpStatusCode = HttpStatusCode.NotFound };

                    return Json(resObject);
                }

                var data = _mapper.Map<DiscountCustomerGroupDto>(dataFromRepo);

                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };

                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetById Request";

                _logger.LogError(msg);

                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

                return Json(resObject);
            }

        }
        /// <summary>
        /// Add New Contact Person - POST api/DiscountCustomerGroup
        /// </summary>
        /// <param name="discountCustomerGroup"></param>
        /// <returns></returns>

        [HttpPost(Name = "AddDiscountCustomerGroup")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddDiscountCustomerGroup([FromBody] DiscountCustomerGroupDto discountCustomerGroup)  // ---------------------
        {
            try
            {
                ResponseObject resObject;

                if (discountCustomerGroup == null)
                {
                    _logger.LogError("Owner object sent from client is null.");

                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = (DiscountCustomerGroupDto)null, HttpStatusCode = HttpStatusCode.NotFound };

                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");

                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model!",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var discountCustomerGroupRepo = _mapper.Map<DiscountCustomerGroup>(discountCustomerGroup);

                await _discountCustomerGroupRepository.AddDiscountCustomerGroup(discountCustomerGroupRepo);

                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", discountCustomerGroupRepo.DiscountId);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a DiscountCustomerGroup failed on save.");
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetDiscountCustomerGroupById), new { id = discountCustomerGroupRepo.DiscountId }, discountCustomerGroupRepo);

                var contactFromRepo = contactFromRepo2.Value;

                var data = _mapper.Map<DiscountCustomerGroupDto>(contactFromRepo);

                resObject = new ResponseObject
                {
                    Message = "Submitted Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Sever Error, Unable to Process Create Request";

                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject {
                    Message = BuiltMessage(e, LoggingEvents.GetItem), 
                    Message2 = msg, Status = "error", 
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        /// <summary>
        /// DELETE api/DiscountCustomerGroup/5
        /// </summary>
        /// <param name="discountId"></param>
        /// <param name="customerGroupId"></param>
        /// <returns></returns>
        [HttpDelete("{discountId}/{customerGroupId}", Name = "DeleteDiscountCustomerGroup")]
        public async Task<IActionResult> DeleteDiscountCustomerGroup(short discountId,int customerGroupId) // -----------------------------------------------------
        {
            try
            {
                ResponseObject resObject;

                var entity = _discountCustomerGroupRepository.GetDiscountCustomerGroupByIdAsNoTracking(discountId,customerGroupId);

                if (entity == false)
                {
                    _logger.LogError($"entity with id: {discountId}, hasn't been found in db.");

                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = discountId,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                _discountCustomerGroupRepository.DeleteDiscountCustomerGroup(discountId,customerGroupId);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a DiscountCustomerGroup failed on save.");
                }

                resObject = new ResponseObject
                {
                    Message = "Deleted Successfully",
                    Status = "success",
                    Data = discountId,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process Delete Request";
                _logger.LogError(msg);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }
        }
    }
}

