using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared; 
using DRS.APA.API.Services;
using Microsoft.AspNetCore.Authorization;
//using System.Data.Entity.Validation;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class CustomerGroupsController : Controller
    {
        private readonly ICustomerGroupRepository _customerGroupRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const int maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;


        public CustomerGroupsController(ICustomerGroupRepository customerGroupRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<CustomerGroupsController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _customerGroupRepository = customerGroupRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }



        /// <summary>
        /// List all public CustomerGroups sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [Authorize(Policy = "CanViewCustomerGroup")]
        [HttpGet("{page}/{pageSize}", Name = "GetCustomerGroups")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetCustomerGroups(string sortOrder, bool? DisplayOnSite, string searchString, int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                ResponseObject resObject;

                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");

                    var dataFromRepo = await _customerGroupRepository.GetAllCustomerGroups();
                    if (DisplayOnSite == true)
                    {
                        dataFromRepo=dataFromRepo.Where(x => x.DisplayOnSite == true);
                    }
                    else if(DisplayOnSite==false)
                    {
                        dataFromRepo = dataFromRepo.Where(x => x.DisplayOnSite == false);
                    }
                    var dataList = _mapper.Map<IEnumerable<CustomerGroupDto>>(dataFromRepo);

                    resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };

                    return Json(resObject);
                }

                // Sorting -------
                HttpContext.Session.SetString("NameSortParam", String.IsNullOrEmpty(sortOrder) ? "name_desc" : "");
                HttpContext.Session.SetString("NameSortParam", String.IsNullOrEmpty(sortOrder) ? "name_asc" : "");

                HttpContext.Session.SetString("DescriptionSortParam", String.IsNullOrEmpty(sortOrder) ? "description_desc" : "");
                HttpContext.Session.SetString("DescriptionSortParam", String.IsNullOrEmpty(sortOrder) ? "description_asc" : "");

                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_asc" : "");
                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_desc" : "");

                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }

                IQueryable<CustomerGroup> data = null;
                data = await _customerGroupRepository.GetAllCustomerGroups(true);
                if (DisplayOnSite == true)
                {
                    data= data.Where(x => x.DisplayOnSite == true);
                }
                else if (DisplayOnSite == false)
                {
                    data = data.Where(x => x.DisplayOnSite == false);
                }

                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    data = data.Where(s => s.CustomerGroupName.Contains(searchString)
                    || s.Description.Contains(searchString));
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "name_desc":
                        data = data.OrderByDescending(s => s.CustomerGroupName);
                        break;
                    case "description_desc":
                        data = data.OrderByDescending(s => s.Description);
                        break;
                    case "description_asc":
                        data = data.OrderBy(s => s.Description);
                        break;
                 
                    case "id_asc":
                        data = data.OrderByDescending(s => s.CustomerGroupId);
                        break;
                    case "id_desc":
                        data = data.OrderByDescending(s => s.CustomerGroupId);
                        break;
                    default:
                        data = data.OrderBy(s => s.CustomerGroupName);
                        break;
                }//
                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount = await data.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetCustomerGroups", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetCustomerGroups", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";

                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));


                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();

                var pagedData = _mapper.Map<IEnumerable<CustomerGroupDto>>(items);

                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };

                return Json(resObject);

            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetItems Request";

                _logger.LogError(msg);

                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

                return Json(resObject);
            }
        }

        /// <summary>
        /// Get Contact Person By Id (with Contact attached)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Policy = "CanViewCustomerGroup")]
        [HttpGet("{id}", Name = "GetCustomerGroupById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetCustomerGroupById(int id)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;

                _logger.LogInformation("GetCustomerGroupById");

                var dataFromRepo = await _customerGroupRepository.GetCustomerGroupById(id);


                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetCollectionById({ID}) NOT FOUND", id);

                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };

                    return Json(resObject);
                }

                var data = _mapper.Map<CustomerGroupDto>(dataFromRepo);

                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };

                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetById Request";

                _logger.LogError(msg);

                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

                return Json(resObject);
            }

        }
        /// <summary>
        /// Add New Contact Person - POST api/CustomerGroup
        /// </summary>
        /// <param name="customerGroup"></param>
        /// <returns></returns>
        [Authorize(Policy = "CanCreateCustomerGroup")]
        [HttpPost(Name = "AddCustomerGroup")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddCustomerGroup([FromBody] CustomerGroupDto customerGroup)  // ---------------------
        {
            try
            {
                ResponseObject resObject;

                if (customerGroup == null)
                {
                    _logger.LogError("Customer Group object sent from client is null.");

                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = (CustomerGroupDto)null, HttpStatusCode = HttpStatusCode.NotFound };

                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");

                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model!",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var customerGroupRepo = _mapper.Map<CustomerGroup>(customerGroup);

                await _customerGroupRepository.AddCustomerGroup(customerGroupRepo);

                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", customerGroupRepo.CustomerGroupId);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a CustomerGroup failed on save.");
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetCustomerGroupById), new { id = customerGroupRepo.CustomerGroupId }, customerGroupRepo);

                var contactFromRepo = contactFromRepo2.Value;

                var data = _mapper.Map<CustomerGroupDto>(contactFromRepo);

                resObject = new ResponseObject
                {
                    Message = "Submitted Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Sever Error, Unable to Process Create Request";

                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        // PUT api/CustomerGroup/5
        [Authorize(Policy = "CanUpdateCustomerGroup")]
        [HttpPut("{id}", Name = "UpdateCustomerGroup")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateCustomerGroup(int id, [FromBody] CustomerGroupDto customerGroup) // ------------
        {
            try
            {
                ResponseObject resObject;

                if (customerGroup == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = "Data not Found, BadRequest",
                        Status = "error",
                        Data = (CustomerGroupDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Data");

                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model, BadRequest",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var customerGroupRepo = _mapper.Map<CustomerGroup>(customerGroup);

                if (!_customerGroupRepository.UpdateCustomerGroupOk(id, customerGroupRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    // return StatusCode((int) HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating CustomerGroup failed on save.");
                }

                //var contactFromRepo = await _customerGroupRepository.GetCustomerGroupById(customerGroupRepo.CustomerGroupId);
                var contactFromRepo2 = CreatedAtAction(nameof(GetCustomerGroupById), new { id = customerGroupRepo.CustomerGroupId }, customerGroupRepo);

                var contactFromRepo = contactFromRepo2.Value;

                var data = _mapper.Map<CustomerGroupDto>(contactFromRepo);

                resObject = new ResponseObject

                {
                    Message = "Saved Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }
        }

        /// <summary>
        /// DELETE api/CustomerGroup/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Policy = "CanDeleteCustomerGroup")]
        [HttpDelete("{id}", Name = "DeleteCustomerGroup")]
        public async Task<IActionResult> DeleteCustomerGroup(int id) // -----------------------------------------------------
        {
            try
            {
                ResponseObject resObject;

                var entity = _customerGroupRepository.GetCustomerGroupByIdAsNoTracking(id);

                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");

                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                _customerGroupRepository.DeleteCustomerGroup(id);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a CustomerGroup failed on save.");
                }

                resObject = new ResponseObject
                {
                    Message = "Deleted Successfully",
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process Delete Request";
                _logger.LogError(msg);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }
        }
    }
}
