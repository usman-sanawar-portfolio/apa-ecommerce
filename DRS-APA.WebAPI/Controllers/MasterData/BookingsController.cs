﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using FluentDateTime;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Repair;
using DRS.APA.Domain.Core.Domain.Schedule;
using DRS.APA.Domain.Core.Dtos.Schedule;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.Domain.Core.Specifications;
//using System.Data.Entity.Validation;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class BookingsController : Controller
    {
        private readonly IBookingRepository _bookingRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const int maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;
       
        public BookingsController(IBookingRepository bookingRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<BookingsController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _bookingRepository = bookingRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }               

        /// <summary>
        /// List all public Bookings sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetBookings")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetBookings(string sortOrder, int? scheduleId, DateTime? bookingDateFrom, DateTime? bookingDateTo, string searchString, int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var bookingsFromRepo = await _bookingRepository.GetAllBookings();
                    var bookingslist = _mapper.Map<IEnumerable<BookingDto>>(bookingsFromRepo);
                    return Ok(bookingslist);
                }

                // Sorting -------

                HttpContext.Session.SetString("NameSortParm", String.IsNullOrEmpty(sortOrder) ? "name_desc" : "");
                HttpContext.Session.SetString("NameSortParm", String.IsNullOrEmpty(sortOrder) ? "name_asc" : "");

                HttpContext.Session.SetString("ConfirmedSortParm", String.IsNullOrEmpty(sortOrder) ? "confirmed_desc" : "");
                HttpContext.Session.SetString("ConfirmedSortParm", String.IsNullOrEmpty(sortOrder) ? "confirmed_asc" : "");

                HttpContext.Session.SetString("DateSortParm", String.IsNullOrEmpty(sortOrder) ? "date_desc" : "");
                HttpContext.Session.SetString("DateSortParm", String.IsNullOrEmpty(sortOrder) ? "date_asc" : "");

                HttpContext.Session.SetString("IdSortParm", String.IsNullOrEmpty(sortOrder) ? "id_asc" : "");
                HttpContext.Session.SetString("IdSortParm", String.IsNullOrEmpty(sortOrder) ? "id_desc" : "");


                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;

                }
                //Search Items
                IQueryable<Booking> bookings = null;

                var filterSpecification = new BookingFilterSpecification(scheduleId, bookingDateFrom, bookingDateTo);

                bookings = await _bookingRepository.GetAllBookings(filterSpecification);

                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    bookings = bookings.Where(s => s.Title.Contains(searchString)
                                             || s.Remarks.Contains(searchString));
                    //|| s.Model.Name.Contains(searchString)
                    //|| s.Model.Make.Name.Contains(searchString));
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }

                //Sort Order
                switch (sortOrder)
                {
                    case "name_desc":
                        bookings = bookings.OrderByDescending(s => s.Title);
                        break;
                    case "date_desc":
                        bookings = bookings.OrderByDescending(s => s.BookingDate);
                        break;
                    case "date_asc":
                        bookings = bookings.OrderBy(s => s.BookingDate);
                        break;
                    case "confirmed_desc":
                        bookings = bookings.OrderByDescending(s => s.Confirmed);
                        break;
                    case "confirmed_asc":
                        bookings = bookings.OrderBy(s => s.Confirmed);
                        break;
                    case "id_asc":
                        bookings = bookings.OrderByDescending(s => s.BookingId);
                        break;
                    case "id_desc":
                        bookings = bookings.OrderByDescending(s => s.BookingId);
                        break;
                    default:
                        bookings = bookings.OrderBy(s => s.Title);
                        break;
                }
                //

                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount =await bookings.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetBookings", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetBookings", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";
                
                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

               // bookings = await _bookingRepository.GetAllBookings(true);
                // return result
                var items = await bookings
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();
                //.Select(eg => _expenseGroupFactory.CreateDataShapedObject(eg, lstOfFields))

                var pagedItems = _mapper.Map<IEnumerable<BookingDto>>(items);
                return Ok(pagedItems);                

            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetItems Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }

        [HttpGet("{id}", Name = "GetBookingById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetBookingById(int id)  // --------------------------------------------------
        {
            try
            {
                _logger.LogInformation("GetBookingById");
                var bookingFromRepo = await _bookingRepository.GetBookingById(id);
                if (bookingFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetBookingById({ID}) NOT FOUND", id);
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found!-" + id);
                    //return NotFound();
                }
             
               var booking = _mapper.Map<BookingDto>(bookingFromRepo);
                return Ok(booking);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetById Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }

        [HttpGet("schedules/{scheduleId}", Name = "GetAllBookingsBySchedule")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAllBookingsBySchedule(int scheduleId)  // --------------------------------------------------
        {
            try
            {
                _logger.LogInformation("GetBookingsByCategory");
                var bookingFromRepo = await _bookingRepository.GetAllBookingsBySchedule(scheduleId);
                if (bookingFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetBookingByMake({ID}) NOT FOUND", scheduleId);
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found!-" + scheduleId);
                    //return NotFound();
                }

                //var bookings = _mapper.Map<BookingDto>(bookingFromRepo); was creating error
                var bookingslist = _mapper.Map<IEnumerable<BookingDto>>(bookingFromRepo);
                return Ok(bookingslist);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetById Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }

        [HttpGet("day-slots/{startDate}/{scheduleId}", Name = "GetDaySlots")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetDaySlots(DateTime startDate, int? scheduleId)  // -----------------------------
        {
            try
            {
                TimeSpan timeIn = new TimeSpan(8, 00, 0);

                TimeSpan timeOut = new TimeSpan(18, 00, 0);

                DateTime clockin = startDate + timeIn;
                DateTime clockout = startDate + timeOut;

                ICollection<SlotDto> slotstoSend = new List<SlotDto>();
                
                //open bookings for the date
                var bookingsFromRepo = await _bookingRepository.GetAllBookingsByDate(startDate);
                
                var hours = new List<DateTime>();

                hours.Add(clockin);

                var next = new DateTime(clockin.Year, clockin.Month, clockin.Day,
                    clockin.Hour, 0, 0, clockin.Kind);

                while ((next = next.AddHours(2)) <= clockout)
                {
                    hours.Add(next);

                    SlotDto slot = new SlotDto();

                    slot.SlotDesc = next.ToString("HH:mm") + "-" + next.AddHours(2).ToString("HH:mm");

                    slot.ScheduleId = (int)scheduleId;

                    slot.FullDate = startDate;

                    slot.IsBooked = false;

                    DateTimeRange dtTimeRange =new DateTimeRange(next,next.AddHours(2));
                    
                    if (bookingsFromRepo != null)
                    {
                        slot.IsBooked = bookingsFromRepo.Any(d => d.Overlaps(dtTimeRange));

                        //foreach (var booking in bookingsFromRepo)
                        //{
                        //    slot.IsActive = !booking.Overlaps(dtTimeRange);
                        //}
                    }
                    
                    slotstoSend.Add(slot);

                    hours.Add(clockout);
                    
                }

                return Ok(slotstoSend);

            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetById Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }



        //https://stackoverflow.com/questions/51211646/the-input-was-not-valid-net-core-web-api
        // POST api/Booking
        [HttpPost(Name = "AddBooking")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddBooking([FromBody] BookingDto booking)  // ---------------------
        {
            try
            {

                if (booking == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, booking);
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(BookingState);
                }

                var bookingRepo = _mapper.Map<Booking>(booking);
                await _bookingRepository.AddBooking(bookingRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", bookingRepo.BookingId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a Booking failed on save.");
                }
                return CreatedAtAction(nameof(GetBookingById), new { id = bookingRepo.BookingId }, bookingRepo);
                //return StatusCode((int)HttpStatusCode.Created);
                // return Content("{ \"message\":\"Form Submitted\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process Create Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        // PUT api/Booking/5
        [HttpPut("{id}", Name = "UpdateBooking")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateBooking(int id, [FromBody] BookingDto booking) // ------------
        {
            try
            {

                if (booking == null)
                {
                    _logger.LogError("Object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, booking);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid object sent from client-BookingState");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(BookingState);
                }

                var bookingRepo = _mapper.Map<Booking>(booking);
                if (!_bookingRepository.UpdateBookingOk(id, bookingRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    //return NotFound();
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a Booking Template failed on save.");
                }

                return Ok(bookingRepo);
                // return Content("{ \"message\":\"Form Updated\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }
        }

        // DELETE api/Booking/
        [HttpDelete("{id}", Name = "DeleteBooking")]
        public async Task<IActionResult> DeleteBooking(int id) // -----------------------------------------------------
        {
            try
            {
                var entity = _bookingRepository.GetBookingByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound);
                    //return NotFound();
                }

                _bookingRepository.DeleteBooking(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a Booking Template failed on save.");
                }
                return Content("{ \"message\":\"Booking Deleted\",\"type\":\"success\" }", "application/json");
            }
            catch (Exception e)
            {
                string msg = "Unable to Process Delete Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }

        [HttpGet("GetBookingByName/{name}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetBookingByName(string name) // ---------------------------------------------
        {
            try
            {

                _logger.LogInformation("GetByName");
                var bookingFromRepo = await _bookingRepository.GetBookingByTitle(name);
                if (bookingFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetByName({name}) NOT FOUND", name);
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found By Name:-" + name);
                    //return NotFound();
                }
                return Ok(name);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetBookingByName Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }


    }
}
