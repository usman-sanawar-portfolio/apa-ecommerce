﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Repair;
using DRS.APA.Domain.Core.Dtos.Repair;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
//using System.Data.Entity.Validation;

//using DRSAPP.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class ResourceGroupsController : Controller
    {
        private readonly IResourceGroupRepository _resourceGroupRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const int maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;


        public ResourceGroupsController(IResourceGroupRepository resourceGroupRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<ResourceGroupsController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _resourceGroupRepository = resourceGroupRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }

        /// <summary>
        /// List all public ResourceGroups sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetResourceGroups")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetResourceGroups(int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var resourceGroupsFromRepo = await _resourceGroupRepository.GetAllResourceGroups();
                    var resourceGroupslist = _mapper.Map<IEnumerable<ResourceGroupDto>>(resourceGroupsFromRepo);
                    return Ok(resourceGroupslist);
                }

                IQueryable<ResourceGroup> resourceGroups = null;
                resourceGroups = await _resourceGroupRepository.GetAllResourceGroups(true);

                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount = await resourceGroups.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetResourceGroups", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetResourceGroups", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";

                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

                resourceGroups = await _resourceGroupRepository.GetAllResourceGroups(true);
                // return result
                var items = await resourceGroups
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();
                //.Select(eg => _expenseGroupFactory.CreateDataShapedObject(eg, lstOfFields))

                var pagedItems = _mapper.Map<IEnumerable<ResourceGroupDto>>(items);
                return Ok(pagedItems);

            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetItems Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }

        [HttpGet("{id}", Name = "GetResourceGroupById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetResourceGroupById(short id)  // --------------------------------------------------
        {
            try
            {
                _logger.LogInformation("GetResourceGroupById");
                var resourceGroupFromRepo = await _resourceGroupRepository.GetResourceGroupById(id);
                if (resourceGroupFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetResourceGroupById({ID}) NOT FOUND", id);
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found!-" + id);
                    //return NotFound();
                }

                var resourceGroup = _mapper.Map<ResourceGroupDto>(resourceGroupFromRepo);
                return Ok(resourceGroup);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetById Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }

        // POST api/ResourceGroup
        [HttpPost(Name = "AddResourceGroup")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddResourceGroup([FromBody] ResourceGroupDto resourceGroup)  // ---------------------
        {
            try
            {

                if (resourceGroup == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, resourceGroup);
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(ModelState);
                }

                var resourceGroupRepo = _mapper.Map<ResourceGroup>(resourceGroup);
                await _resourceGroupRepository.AddResourceGroup(resourceGroupRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", resourceGroupRepo.ResourceGroupId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a ResourceGroup failed on save.");
                }
                return CreatedAtAction(nameof(GetResourceGroupById), new { id = resourceGroupRepo.ResourceGroupId }, resourceGroupRepo);
                //return StatusCode((int)HttpStatusCode.Created);
                // return Content("{ \"message\":\"Form Submitted\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process Create Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        // PUT api/ResourceGroup/5
        [HttpPut("{id}", Name = "UpdateResourceGroup")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateResourceGroup(short id, [FromBody] ResourceGroupDto resourceGroup) // ------------
        {
            try
            {

                if (resourceGroup == null)
                {
                    _logger.LogError("Object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, resourceGroup);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid object sent from client-ModelState");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(ModelState);
                }

                var resourceGroupRepo = _mapper.Map<ResourceGroup>(resourceGroup);
                if (!_resourceGroupRepository.UpdateResourceGroupOk(id, resourceGroupRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    //return NotFound();
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a ResourceGroup Template failed on save.");
                }

                return Ok(resourceGroupRepo);
                // return Content("{ \"message\":\"Form Updated\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }
        }

        // DELETE api/ResourceGroup/
        [HttpDelete("{id}", Name = "DeleteResourceGroup")]
        public async Task<IActionResult> DeleteResourceGroup(short id) // -----------------------------------------------------
        {
            try
            {
                var entity = _resourceGroupRepository.GetResourceGroupByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound);
                    //return NotFound();
                }

                _resourceGroupRepository.DeleteResourceGroup(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a ResourceGroup Template failed on save.");
                }
                return Content("{ \"message\":\"ResourceGroup Deleted\",\"type\":\"success\" }", "application/json");
            }
            catch (Exception e)
            {
                string msg = "Unable to Process Delete Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }

        [HttpGet("GetResourceGroupByName/{name}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetResourceGroupByName(string name) // ---------------------------------------------
        {
            try
            {

                _logger.LogInformation("GetByName");
                var resourceGroupFromRepo = await _resourceGroupRepository.GetResourceGroupByName(name);
                if (resourceGroupFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetByName({name}) NOT FOUND", name);
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found By Name:-" + name);
                    //return NotFound();
                }
                return Ok(name);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetResourceGroupByName Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }


    }
}
