﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.Repair;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
//using System.Data.Entity.Validation;


[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class AccountsController : Controller
    {
        private readonly IAccountRepository _accountRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const int maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;

       
        public AccountsController(IAccountRepository accountRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<AccountsController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _accountRepository = accountRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }               

        /// <summary>
        /// List all public Accounts sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetAccounts")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAccounts(int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var accountsFromRepo = await _accountRepository.GetAllAccounts();
                    var accountslist = _mapper.Map<IEnumerable<AccountDto>>(accountsFromRepo);
                    return Ok(accountslist);
                }

                IQueryable<Account> accounts = null;
                accounts = await _accountRepository.GetAllAccounts(true);

                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount =await accounts.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetAccounts", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetAccounts", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";
                
                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

                accounts = await _accountRepository.GetAllAccounts(true);
                // return result
                var items = await accounts
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();
                //.Select(eg => _expenseGroupFactory.CreateDataShapedObject(eg, lstOfFields))

                var pagedItems = _mapper.Map<IEnumerable<AccountDto>>(items);
                return Ok(pagedItems);                

            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetItems Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }

        [HttpGet("{id}", Name = "GetAccountById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAccountById(short id)  // --------------------------------------------------
        {
            try
            {
                _logger.LogInformation("GetAccountById");
                var accountFromRepo = await _accountRepository.GetAccountById(id);
                if (accountFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetAccountById({ID}) NOT FOUND", id);
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found!-" + id);
                    //return NotFound();
                }

                var account = _mapper.Map<AccountDto>(accountFromRepo);
                return Ok(account);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetById Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }
     
        // POST api/Account
        [HttpPost(Name = "AddAccount")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddAccount([FromBody] AccountDto account)  // ---------------------
        {
            try
            {

                if (account == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, account);
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(ModelState);
                }

                var accountRepo = _mapper.Map<Account>(account);
                await _accountRepository.AddAccount(accountRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", accountRepo.AccountId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a Account failed on save.");
                }
                return CreatedAtAction(nameof(GetAccountById), new { id = accountRepo.AccountId }, accountRepo);
                //return StatusCode((int)HttpStatusCode.Created);
                // return Content("{ \"message\":\"Form Submitted\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process Create Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        // PUT api/Account/5
        [HttpPut("{id}", Name = "UpdateAccount")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateAccount(short id, [FromBody] AccountDto account) // ------------
        {
            try
            {

                if (account == null)
                {
                    _logger.LogError("Object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, account);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid object sent from client-ModelState");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(ModelState);
                }

                var accountRepo = _mapper.Map<Account>(account);
                if (!_accountRepository.UpdateAccountOk(id, accountRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    //return NotFound();
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a Account Template failed on save.");
                }

                return Ok(accountRepo);
                // return Content("{ \"message\":\"Form Updated\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }
        }

        // DELETE api/Account/
        [HttpDelete("{id}", Name = "DeleteAccount")]
        public async Task<IActionResult> DeleteAccount(short id) // -----------------------------------------------------
        {
            try
            {
                var entity = _accountRepository.GetAccountByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound);
                    //return NotFound();
                }

                _accountRepository.DeleteAccount(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a Account Template failed on save.");
                }
                return Content("{ \"message\":\"Account Deleted\",\"type\":\"success\" }", "application/json");
            }
            catch (Exception e)
            {
                string msg = "Unable to Process Delete Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }

        [HttpGet("GetAccountByName/{name}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAccountByName(string name) // ---------------------------------------------
        {
            try
            {

                _logger.LogInformation("GetByName");
                var accountFromRepo = await _accountRepository.GetAccountByName(name);
                if (accountFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetByName({name}) NOT FOUND", name);
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found By Name:-" + name);
                    //return NotFound();
                }
                return Ok(name);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetAccountByName Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }


    }
}
