﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.API.Services;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;


[assembly: ApiConventionType(typeof(DefaultApiConventions))]

namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class CollectionsController : Controller
    {
        private readonly ICollectionRepository _collectionRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const int maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;

        public CollectionsController(ICollectionRepository collectionRepository, IUnitOfWork unitOfWork, IMapper mapper,
            ILogger<CollectionsController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _collectionRepository = collectionRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }

//        /// <summary>
//        /// List all public Collections sorted by Id.
//        /// </summary>
//        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
//        [HttpGet("{page}/{pageSize}", Name = "GetCollections")]
//        [ProducesResponseType(StatusCodes.Status200OK)]
//        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//        public async Task<IActionResult> GetCollections(int? page = 1, int? pageSize = maxPageSize)
//        {
//            try
//            {
//               // ResponseObject resObject;
//                if (!page.HasValue || page.Value == 0)
//                {
//                    _logger.LogInformation("Generating List");
//                    var allCollections = await _collectionRepository.GetAllCollections();
//                    var collectionList = _mapper.Map<IEnumerable<CollectionDto>>(allCollections);
//                    //resObject = new ResponseObject { Message = "Data List!",Status = "success", Data = collectionList, HttpStatusCode = HttpStatusCode.OK };
//                    //return Ok(resObject);
//                    return Ok(allCollections);
//                }

//                IQueryable<Collection> jobtypes = null;
//                jobtypes = await _collectionRepository.GetAllCollections(true);

//                // ensure the page size isn't larger than the maximum.
//                if (pageSize > maxPageSize)
//                {
//                    pageSize = maxPageSize;
//                }

//                // calculate data for metadata
//                var totalCount = await jobtypes.CountAsync();
//                var totalPages = (int) Math.Ceiling((double) totalCount / (int) pageSize);

//                var prevLink = page > 1
//                    ? _linkGenerator.GetPathByAction(HttpContext, "GetCollections",
//                        values: new {page = page - 1, pageSize = pageSize})
//                    : "";

//                var nextLink = page < totalPages
//                    ? _linkGenerator.GetPathByAction(HttpContext, "GetCollections",
//                        values: new {page = page + 1, pageSize = pageSize})
//                    : "";

//                var paginationHeader = new
//                {
//                    currentPage = page,
//                    pageSize = pageSize,
//                    totalCount = totalCount,
//                    totalPages = totalPages,
//                    previousPageLink = prevLink,
//                    nextPageLink = nextLink
//                };

//                HttpContext.Response.Headers.Add("X-Pagination",
//                    Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

//                jobtypes = await _collectionRepository.GetAllCollections(true);
//                // return result
//                var items = await jobtypes.Skip((int) pageSize * ((int) page - 1)).Take((int) pageSize).ToListAsync();
//                //.Select(eg => _expenseGroupFactory.CreateDataShapedObject(eg, lstOfFields))

//                var pagedItems = _mapper.Map<IEnumerable<CollectionDto>>(items);

//                return Ok(pagedItems);
//               // resObject = new ResponseObject {Message = "Data List!",Status = "success",Data = pagedItems,HttpStatusCode = HttpStatusCode.OK };
//                //return Ok(resObject);
//            }
//            catch (Exception e)
//            {
//                string msg = "Unable to Process GetItems Request" ;
//                _logger.LogError(msg);
//                //var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = e.Message, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
//                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
//            }
//        }

//        [HttpGet("{id}", Name = "GetCollectionById")]
//        [ProducesResponseType(StatusCodes.Status200OK)]
//        [ProducesResponseType(StatusCodes.Status404NotFound)]
//        public async Task<IActionResult> GetCollectionById(short id) // --------------
//        {
//            try
//            {
//                ResponseObject resObject;
//                _logger.LogInformation("GetCollectionById");

//                var jobtypeFromRepo = await _collectionRepository.GetCollectionById(id);

//                if (jobtypeFromRepo == null)
//                {
//                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetCollectionById({ID}) NOT FOUND", id);
//                    _logger.LogInformation($"No record found for {id}");
//                    resObject = new ResponseObject {Message = "Data not found, BadRequest", Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound};
//                    return StatusCode((int)HttpStatusCode.BadRequest, resObject);
//                }

//                var jobtype = _mapper.Map<CollectionDto>(jobtypeFromRepo);
//                //resObject = new ResponseObject {Message = "Data List!", Status = "success", Data = jobtype, HttpStatusCode = HttpStatusCode.OK};
//                //return Ok(resObject);
//                return Ok(jobtype);
//            }

//            catch (Exception e)
//            {
//                string msg = "Unable to Process GetById Request";
//                _logger.LogError(msg);
//                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = e.Message, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
//                return StatusCode((int)HttpStatusCode.InternalServerError, resObject);
//            }
//        }

//        // POST api/Collection
//        [HttpPost(Name = "AddCollection")]
//        [ProducesResponseType(StatusCodes.Status201Created)]
//        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//        public async Task<IActionResult> AddCollection([FromBody] CollectionDto collection) // ---------------------
//        {
//            try
//            {
//                ResponseObject resObject;
//                if (collection == null)
//                {
//                    _logger.LogError("Data not Found!");
//                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = (CollectionDto) null, HttpStatusCode = HttpStatusCode.NotFound };
//                    return StatusCode((int)HttpStatusCode.BadRequest, resObject);
//                }

//                if (!ModelState.IsValid)
//                {
//                    _logger.LogError("Invalid Data");
//                    resObject = new ResponseObject
//                    {
//                        Message = "Invalid Data/Model!",
//                        Status = "error",
//                        Data = ModelState,
//                        HttpStatusCode = HttpStatusCode.BadRequest
//                    };
//                    return StatusCode((int) HttpStatusCode.BadRequest, resObject);
//                    //return BadRequest(ModelState);
//                }

//                var collectionRepo = _mapper.Map<Collection>(collection);
//                await _collectionRepository.AddCollection(collectionRepo);
//                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", collectionRepo.CollectionId);
//                if (!await _unitOfWork.CompleteAsync())
//                {
//                    throw new Exception("Adding a Collection failed on save.");
//                }
//                //resObject = new ResponseObject
//                //{
//                //    Message = "Submitted Successfully",
//                //    Status = "success",
//                //    Data = collectionRepo,
//                //    HttpStatusCode = HttpStatusCode.OK
//                //};
//                // Data = CreatedAtAction(nameof(GetCollectionById), new { id = collectionRepo.CollectionId }, collectionRepo)
//                // return Content(resObject.ToString());
//                //return Ok(resObject);
//                 return CreatedAtAction(nameof(GetCollectionById),  new {id = collectionRepo.CollectionId}, collectionRepo);
//                //return StatusCode((int)HttpStatusCode.Created);
//                // return Content("{ \"message\":\"Form Submitted\",\"type\":\"success\" }", "application/json");
//            }

//            catch (Exception e)
//            {
//                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = e.Message, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

//                _logger.LogWarning(e.Message);

//                return StatusCode((int) HttpStatusCode.InternalServerError, resObject);
//            }
//        }

//        private string BuiltMessage(Exception e, int eventtype)
//        {
//            if (e.InnerException != null)
//            {
//                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
//                {
//                    if (e.InnerException.InnerException != null)
//                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
//                        {
//                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " +
//                                   e.Message + ", " + e.InnerException.InnerException.Message;
//                        }
//                }

//                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
//            }
//            else
//            {
//                return eventtype.ToString() + " >> " + e.Message;
//            }
//        }

//        // PUT api/Collection/5
//        [HttpPut("{id}", Name = "UpdateCollection")]
//        [ProducesResponseType(StatusCodes.Status200OK)]
//        [ProducesResponseType(StatusCodes.Status404NotFound)]
//        [ProducesResponseType(StatusCodes.Status400BadRequest)]
//        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
//        public async Task<IActionResult> UpdateCollection(short id, [FromBody] CollectionDto collection) // ------------
//        {
//            try
//            {
//                ResponseObject resObject;
//                if (collection == null)
//                {
//                    _logger.LogError("Data not Found!");
//                    resObject = new ResponseObject
//                    {
//                        Message = "Data not Found, BadRequest",
//                        Status = "error",
//                        Data = (CollectionDto) null,
//                        HttpStatusCode = HttpStatusCode.BadRequest
//                    };
//                    return StatusCode((int) HttpStatusCode.BadRequest, resObject);
//                }

//                if (!ModelState.IsValid)
//                {
//                    _logger.LogError("Invalid Data");
//                    resObject = new ResponseObject
//                    {
//                        Message = "Invalid Data/Model, BadRequest",
//                        Status = "error",
//                        Data = ModelState,
//                        HttpStatusCode = HttpStatusCode.BadRequest
//                    };
//                   // return StatusCode((int) HttpStatusCode.BadRequest, resObject);
//                    return BadRequest(ModelState);
//                }

//                var collectionRepo = _mapper.Map<Collection>(collection);
//                if (!_collectionRepository.UpdateCollectionOk(id, collectionRepo))
//                {
//                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
//                    // return StatusCode((int) HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
//                    resObject = new ResponseObject
//                    {
//                        Message = "Data not found, BadRequest",
//                        Status = "error",
//                        Data = id,
//                        HttpStatusCode = HttpStatusCode.BadRequest
//                    };
//                    return StatusCode((int) HttpStatusCode.BadRequest, resObject);
//                }

//                if (!await _unitOfWork.CompleteAsync())
//                {
//                    throw new Exception("Updating a Collection Template failed on save.");
//                }

//                //return Ok(collectionRepo);
//                // return Content("{ \"message\":\"Form Updated\",\"type\":\"success\" }", "application/json");

//                resObject = new ResponseObject
//                {
//                    Message = "Saved Successfully",
//                    Status = "success",
//                    Data = collectionRepo,
//                    HttpStatusCode = HttpStatusCode.OK
//                };
//                return Ok(resObject);
//            }

//            catch (Exception e)
//            {
//                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
//                var resObject = new ResponseObject
//                {
//                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
//                    Message = msg,
//                    Status = "error",
//                    HttpStatusCode = HttpStatusCode.InternalServerError
//                };
//                _logger.LogWarning(e.Message);
//                return StatusCode((int) HttpStatusCode.InternalServerError, resObject);
//            }
//        }
//        /// <summary>
//        /// DELETE api
//        /// </summary>
//        /// <param name="id"></param>
//        /// <returns></returns>

//        [HttpDelete("{id}", Name = "DeleteCollection")]
//        public async Task<IActionResult> DeleteCollection(short id) 
//        {
//            try
//            {
//                ResponseObject resObject;
//                var entity = _collectionRepository.GetCollectionByIdAsNoTracking(id);

//                if (entity == false)
//                {
//                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");

//                    //return StatusCode((int) HttpStatusCode.NotFound);
//                    //return NotFound();

//                    // return StatusCode((int) HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
//                    resObject = new ResponseObject
//                    {
//                        Message = "Data not found, BadRequest",
//                        Status = "error",
//                        Data = id,
//                        HttpStatusCode = HttpStatusCode.BadRequest
//                    };

//                    return StatusCode((int) HttpStatusCode.BadRequest, resObject);
//                }

//                _collectionRepository.DeleteCollection(id);

//                if (!await _unitOfWork.CompleteAsync())
//                {
//                    throw new Exception("Deleting a Collection Template failed on save.");
//                }

//                resObject = new ResponseObject
//                {
//                    Message = "Deleted Successfully",
//                    Status = "success",
//                    Data = id,
//                    HttpStatusCode = HttpStatusCode.OK
//                };

//                return Ok(resObject);
//            }

//            catch (Exception e)

//            {
//                string msg = "InternalServerError, Unable to Process Delete Request";
//                _logger.LogError(msg);
//                var resObject = new ResponseObject
//                {
//                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
//                    Message = msg,
//                    Status = "error",
//                    HttpStatusCode = HttpStatusCode.InternalServerError
//                };
//                _logger.LogWarning(e.Message);
//                return StatusCode((int) HttpStatusCode.InternalServerError, resObject);
//            }
//        }

//        [HttpGet("GetCollectionByName/{name}")]
//        [ProducesResponseType(StatusCodes.Status200OK)]
//        [ProducesResponseType(StatusCodes.Status404NotFound)]
//        public async Task<IActionResult>
//            GetCollectionByName(string name) // ---------------------------------------------
//        {
//            try
//            {
//                ResponseObject resObject;
//                _logger.LogInformation("GetByName");
//                var collectionFromRepo = await _collectionRepository.GetCollectionByName(name);
//                if (collectionFromRepo == null)
//                {
//                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetByName({name}) NOT FOUND", name);
//                    resObject = new ResponseObject
//                    {
//                        Message = "Data not found, BadRequest",
//                        Status = "error",
//                        Data = name,
//                        HttpStatusCode = HttpStatusCode.BadRequest
//                    };

//                    return StatusCode((int)HttpStatusCode.BadRequest, resObject);
//                }

//                resObject = new ResponseObject { Message = "Record By Name!", Status = "success", Data = collectionFromRepo, HttpStatusCode = HttpStatusCode.OK };
//                return Ok(resObject);
//            }
//            catch (Exception e)
//            {
//                string msg = "Unable to Process GetCollectionByName Request"; 
//                _logger.LogError(msg);
//                var resObject = new ResponseObject
//                {
//                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
//                    Message = msg,
//                    Status = "error",
//                    HttpStatusCode = HttpStatusCode.InternalServerError
//                };
//                _logger.LogWarning(e.Message);
//                return StatusCode((int)HttpStatusCode.InternalServerError, resObject);
//            }
//        }
//    }
//}



/// <summary>
/// List all public Collections sorted by Id.
/// </summary>
/// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
[HttpGet("{page}/{pageSize}", Name = "GetCollections")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetCollections(string sortOrder,  string searchString, int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                ResponseObject resObject;

                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");

                    var dataFromRepo = await _collectionRepository.GetAllCollections();
                    var dataList = _mapper.Map<IEnumerable<CollectionDto>>(dataFromRepo);

                    resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };

                    return Json(resObject);
                }

                // Sorting -------
                HttpContext.Session.SetString("NameSortParam", String.IsNullOrEmpty(sortOrder) ? "name_desc" : "");
                HttpContext.Session.SetString("NameSortParam", String.IsNullOrEmpty(sortOrder) ? "name_asc" : "");

                HttpContext.Session.SetString("EmailSortParam", String.IsNullOrEmpty(sortOrder) ? "email_desc" : "");
                HttpContext.Session.SetString("EmailSortParam", String.IsNullOrEmpty(sortOrder) ? "email_asc" : "");

                HttpContext.Session.SetString("PhoneSortParam", String.IsNullOrEmpty(sortOrder) ? "phone_desc" : "");
                HttpContext.Session.SetString("PhoneSortParam", String.IsNullOrEmpty(sortOrder) ? "phone_asc" : "");

                HttpContext.Session.SetString("ContactSortParam", String.IsNullOrEmpty(sortOrder) ? "contact_desc" : "");
                HttpContext.Session.SetString("ContactSortParam", String.IsNullOrEmpty(sortOrder) ? "contact_asc" : "");

                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_asc" : "");
                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_desc" : "");

                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;
                }

                IQueryable<Collection> data = null;

                //var filterSpecification = new CollectionFilterSpecification(contactId, leadId);

                data = await _collectionRepository.GetAllCollections(true);

                //collections = await _collectionRepository.GetAllCollections(true);

                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);

                    data = data.Where(s => s.Name.Contains(searchString)
                                             || s.ThumbPath.Contains(searchString)
                                             || s.MetaKeywords.Contains(searchString)
                                             || s.MetaDescription.Contains(searchString)
                                             || s.MetaTitle.Contains(searchString)

                    );

                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "name_desc":
                        data = data.OrderByDescending(s => s.Name);
                        break;
                    case "email_desc":
                        data = data.OrderByDescending(s => s.ThumbPath);
                        break;
                    case "email_asc":
                        data = data.OrderBy(s => s.ThumbPath);
                        break;
                    case "mobilePhone_desc":
                        data = data.OrderByDescending(s => s.MetaTitle);
                        break;
                    case "mobilePhone_asc":
                        data = data.OrderBy(s => s.MetaTitle);
                        break;
                    case "contactName_desc":
                        data = data.OrderByDescending(s => s.MetaDescription);
                        break;
                    case "contactName_asc":
                        data = data.OrderBy(s => s.MetaDescription);
                        break;
                    case "id_asc":
                        data = data.OrderByDescending(s => s.CollectionId);
                        break;
                    case "id_desc":
                        data = data.OrderByDescending(s => s.CollectionId);
                        break;
                    default:
                        data = data.OrderBy(s => s.Name);
                        break;
                }
                //
                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount = await data.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetCollections", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetCollections", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";

                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));


                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();
                //.Select(eg => _expenseGroupFactory.CreateDataShapedObject(eg, lstOfFields))

                var pagedData = _mapper.Map<IEnumerable<CollectionDto>>(items);

                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };

                return Json(resObject);

            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetItems Request";

                _logger.LogError(msg);

                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

                return Json(resObject);
            }
        }
      

        /// <summary>
        /// Get Contact Person By Id (with Contact attached)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetCollectionById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetCollectionById(int id)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;

                _logger.LogInformation("GetCollectionById");

                var dataFromRepo = await _collectionRepository.GetCollectionById(id);


                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetCollectionById({ID}) NOT FOUND", id);

                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };

                    return Json(resObject);
                }

                var data = _mapper.Map<CollectionDto>(dataFromRepo);

                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };

                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetById Request";

                _logger.LogError(msg);

                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

                return Json(resObject);
            }

        }
        /// <summary>
        /// Add New Contact Person - POST api/Collection
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>

        [HttpPost(Name = "AddCollection")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddCollection([FromBody] CollectionDto collection)  // ---------------------
        {
            try
            {
                ResponseObject resObject;

                if (collection == null)
                {
                    _logger.LogError("Owner object sent from client is null.");

                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = (CollectionDto)null, HttpStatusCode = HttpStatusCode.NotFound };

                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");

                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model!",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var collectionRepo = _mapper.Map<Collection>(collection);

                await _collectionRepository.AddCollection(collectionRepo);

                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", collectionRepo.CollectionId);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a Collection failed on save.");
                }

                //var contactFromRepo = await _collectionRepository.GetCollectionById(collectionRepo.CollectionId);
                //var res1 = await GetCollectionById(collectionRepo.CollectionId);
                var contactFromRepo2 = CreatedAtAction(nameof(GetCollectionById), new { id = collectionRepo.CollectionId }, collectionRepo);

                var contactFromRepo = contactFromRepo2.Value;

                var data = _mapper.Map<CollectionDto>(contactFromRepo);

                resObject = new ResponseObject
                {
                    Message = "Submitted Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Sever Error, Unable to Process Create Request";

                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

                return Json(resObject);
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        // PUT api/Collection/5
        [HttpPut("{id}", Name = "UpdateCollection")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateCollection(int id, [FromBody] CollectionDto collection) // ------------
        {
            try
            {
                ResponseObject resObject;

                if (collection == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = "Data not Found, BadRequest",
                        Status = "error",
                        Data = (CollectionDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Data");

                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model, BadRequest",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var collectionRepo = _mapper.Map<Collection>(collection);

                if (!_collectionRepository.UpdateCollectionOk(id, collectionRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    // return StatusCode((int) HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a Collection failed on save.");
                }

                //var contactFromRepo = await _collectionRepository.GetCollectionById(collectionRepo.CollectionId);
                var contactFromRepo2 = CreatedAtAction(nameof(GetCollectionById), new { id = collectionRepo.CollectionId }, collectionRepo);

                var contactFromRepo = contactFromRepo2.Value;

                var data = _mapper.Map<CollectionDto>(contactFromRepo);

                resObject = new ResponseObject

                {
                    Message = "Saved Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }
        }

        /// <summary>
        /// DELETE api/Collection/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}", Name = "DeleteCollection")]
        public async Task<IActionResult> DeleteCollection(int id) // -----------------------------------------------------
        {
            try
            {
                ResponseObject resObject;

                var entity = _collectionRepository.GetCollectionByIdAsNoTracking(id);

                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");

                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                _collectionRepository.DeleteCollection(id);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a Collection failed on save.");
                }

                resObject = new ResponseObject
                {
                    Message = "Deleted Successfully",
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process Delete Request";
                _logger.LogError(msg);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }
        }

        [HttpGet("GetCollectionByName/{name}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetCollectionByName(string name) // ---------------------------------------------
        {
            try
            {
                ResponseObject resObject;

                _logger.LogInformation("GetByName");

                var collectionFromRepo = await _collectionRepository.GetCollectionByName(name);

                var collectionsList = _mapper.Map<IEnumerable<CollectionDto>>(collectionFromRepo);

                if (collectionFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetByName({name}) NOT FOUND", name);

                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = name,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                resObject = new ResponseObject { Message = "Data Found!", Status = "success", Data = collectionsList, HttpStatusCode = HttpStatusCode.OK };

                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetCollectionByName Request";

                _logger.LogError(msg);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }

        }

        ///// <summary>
        ///// UpdateIsPrimaryStatus, isprimary-status/{id}
        ///// </summary>
        //[HttpPut("isprimary-status/{id}", Name = "UpdateIsPrimaryStatus")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status500InternalServerError)]
        //public async Task<IActionResult> UpdateIsPrimaryStatus(int id, string status) // ------------
        //{
        //    try
        //    {
        //        ResponseObject resObject;

        //        if (status == null)
        //        {
        //            _logger.LogError("Object sent from client is null.");

        //            resObject = new ResponseObject
        //            {
        //                Message = "Data not Found, BadRequest",
        //                Status = "error",
        //                Data = (string)null,
        //                HttpStatusCode = HttpStatusCode.BadRequest
        //            };

        //            return Json(resObject);
        //        }


        //        if (!_collectionRepository.UpdateStatus(id, status))
        //        {
        //            _logger.LogError($"entity with id: {id}, hasn't been found in db.");

        //            resObject = new ResponseObject
        //            {
        //                Message = "Data not Found, BadRequest",
        //                Status = "error",
        //                Data = (string)null,
        //                HttpStatusCode = HttpStatusCode.BadRequest
        //            };

        //            return Json(resObject);
        //        }

        //        if (!await _unitOfWork.CompleteAsync())
        //        {
        //            throw new Exception("Updating a Client Contact IsPrimary failed on save.");
        //        }

        //        resObject = new ResponseObject
        //        {
        //            Message = "IsPrimary status successfully updated",
        //            Status = "success",
        //            Data = status,
        //            HttpStatusCode = HttpStatusCode.BadRequest
        //        };

        //        return Json(resObject);
        //    }

        //    catch (Exception e)
        //    {

        //        string msg = "Unable to Process PUT Request >> ";
        //        _logger.LogWarning(msg);

        //        var resObject = new ResponseObject
        //        {
        //            Message2 = BuiltMessage(e, LoggingEvents.GetItem),
        //            Message = msg,
        //            Status = "error",
        //            HttpStatusCode = HttpStatusCode.InternalServerError
        //        };

        //        return Json(resObject);
        //    }
        //}

    }
}

