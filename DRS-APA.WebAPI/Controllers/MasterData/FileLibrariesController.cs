using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.MasterData; 
using DRS.APA.Domain.Core.Shared; 
using DRS.APA.API.Services;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Microservice.API.Controllers.Common;
using Microsoft.AspNetCore.Authorization;


[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class FileLibrariesController : Controller
    {
        private readonly IFileLibraryRepository _fileLibraryRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const int maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;


        public FileLibrariesController(IFileLibraryRepository fileLibraryRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<FileLibrariesController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _fileLibraryRepository = fileLibraryRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }

    /// <summary>
    /// List all FileLibrary, by default sorted by FileLibraryName
    /// </summary>
    /// <remarks>With pagination, you can fetch up to x records per page.</remarks>
    [Authorize(Policy = "CanViewFile")]
    [HttpGet("{page}/{pageSize}", Name = "GetFileLibrary")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetFileLibrary(
            string sortOrder, string type, string Name, string FileName,
            string searchString, int? page = 1, int? pageSize = ConstantProps.maxPageSize)
        {
            try
            {
                ResponseObject resObject;
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var dataFromRepo = await _fileLibraryRepository.GetAllFileLibrary();

                    var dataList = _mapper.Map<IEnumerable<FileLibraryDto>>(dataFromRepo);
                    if (!String.IsNullOrEmpty(type))
                    {
                        dataList = dataList.Where(x => x.Type.Trim() == type.Trim());
                    }
                    resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                    return Json(resObject);
                }

                // Sorting -------
                HttpContext.Session.SetString("FileNameSortParam", String.IsNullOrEmpty(sortOrder) ? "fileName_desc" : "");
                HttpContext.Session.SetString("FileNameSortParam", String.IsNullOrEmpty(sortOrder) ? "fileName_asc" : "");
               
                HttpContext.Session.SetString("NameSortParam", String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "");
                HttpContext.Session.SetString("NameSortParam", String.IsNullOrEmpty(sortOrder) ? "Name_asc" : "");

                HttpContext.Session.SetString("TypeSortParam", String.IsNullOrEmpty(sortOrder) ? "type_desc" : "");
                HttpContext.Session.SetString("TypeSortParam", String.IsNullOrEmpty(sortOrder) ? "type_asc" : "");

        
                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;
                }
                IQueryable<FileLibrary> data = null;

                data = await _fileLibraryRepository.GetAllFileLibrary(true);

                if (!String.IsNullOrEmpty(type))
                {
                    data = data.Where(x => x.Type.Trim() == type.Trim());
                }
                
                if (!String.IsNullOrEmpty(Name))
                {
                    data = data.Where(x => x.Name.Trim() == Name.Trim());
                }
                if (!String.IsNullOrEmpty(FileName))
                {
                    data = data.Where(x => x.FileName.Trim() == FileName.Trim());
                }
                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    data = data.Where(s => s.FileName.Contains(searchString)
                                             || s.Name.Contains(searchString)
                                             || s.Type.Contains(searchString)
                                             );
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "fileName_desc":
                        data = data.OrderByDescending(s => s.FileName);
                        break;
                    case "name_desc":
                        data = data.OrderByDescending(s => s.Name);
                        break;
                    case "name_asc":
                        data = data.OrderBy(s => s.Name);
                        break;
                    case "type_desc":
                        data = data.OrderByDescending(s => s.Type);
                        break;
                    case "type_asc":
                        data = data.OrderBy(s => s.Type);
                        break;
               
                    default:
                        data = data.OrderBy(s => s.FileName);
                        break;
                }//


                // calculate data for metadata
                var totalCount = await data.CountAsync();
                PaggingAndSorting paggingSorting = new PaggingAndSorting(_linkGenerator);
                paggingSorting.PaggingAndSortingData(HttpContext, totalCount, "GetFileLibrarys", page, pageSize);

        
                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();

                var pagedData = _mapper.Map<IEnumerable<FileLibraryDto>>(items);

                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetFileLibrary");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

    /// <summary>
    /// Get FileLibrary By Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanViewFile")]
    [HttpGet("{id}", Name = "GetFileLibraryById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetFileLibraryById(short id)  // --------------------------------------------------
        {
            try
            {

                ResponseObject resObject;

                _logger.LogInformation("GetFileLibraryById");
                var dataFromRepo = await _fileLibraryRepository.GetFileLibraryById(id);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetFileLibraryById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                var data = _mapper.Map<FileLibraryDto>(dataFromRepo);


                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetFileLibraryById");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }


    /// <summary>
    /// Add FileLibrary - POST api/FileLibrary
    /// </summary>
    /// <param name="fileLibrary"></param>
    /// <returns></returns>

    [Authorize(Policy = "CanCreateFile")]
    [HttpPost(Name = "AddFileLibrary")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddFileLibrary([FromBody] FileLibraryDto fileLibrary)
        {
            try
            {
                ResponseObject resObject;
                if (fileLibrary == null)
                {
                    _logger.LogError(ConstantProps.NullObjectError("FileLibrary"));
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (FileLibraryDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidObjectError("FileLibrary"));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var fileLibraryRepo = _mapper.Map<FileLibrary>(fileLibrary);
                await _fileLibraryRepository.AddFileLibrary(fileLibraryRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "FileLibrary {ID} Created", fileLibraryRepo.FileLibraryId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Adding", "FileLibrary"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetFileLibraryById), new { id = fileLibraryRepo.FileLibraryId }, fileLibraryRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<FileLibraryDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SubmittedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Create");
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject { 
                    Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error", 
                    HttpStatusCode = HttpStatusCode.InternalServerError 
                };
                return Json(resObject);
            }
        }

    // PUT api/FileLibrary/5
    [Authorize(Policy = "CanUpdateFile")]
    [HttpPut("{id}", Name = "UpdateFileLibrary")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateFileLibrary(short id, [FromBody] FileLibraryDto fileLibrary)
        {
            try
            {
                ResponseObject resObject;
                if (fileLibrary == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = (FileLibraryDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidDataModelText);
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var fileLibraryRepo = _mapper.Map<FileLibrary>(fileLibrary);
                if (!_fileLibraryRepository.UpdateFileLibraryOk(id, fileLibraryRepo))
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Updating", "FileLibrary"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetFileLibraryById), new { id = fileLibraryRepo.FileLibraryId }, fileLibraryRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<FileLibraryDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SavedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

    /// <summary>
    /// DELETE api/FileLibrary/5
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanDeleteFile")]
    [HttpDelete("{id}", Name = "DeleteFileLibrary")]
        public async Task<IActionResult> DeleteFileLibrary(short id)
        {
            try
            {
                ResponseObject resObject;
                var entity = _fileLibraryRepository.GetFileLibraryByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                _fileLibraryRepository.DeleteFileLibrary(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Deleting", "FileLibrary"));
                }
                resObject = new ResponseObject
                {
                    Message = ConstantProps.DeletedSuccessfullyText,
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Delete");
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

    }
}

