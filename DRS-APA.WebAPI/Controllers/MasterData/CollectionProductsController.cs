﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;

//using System.Data.Entity.Validation;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class CollectionProductsController : Controller
    {
        private readonly ICollectionProductRepository _collectionProductRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const int maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;

       
        public CollectionProductsController(ICollectionProductRepository repairOptionRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<CollectionProductsController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _collectionProductRepository = repairOptionRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }

            /// <summary>
            /// List all public CollectionProducts sorted by Id.
            /// </summary>
            /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
            [HttpGet("{page}/{pageSize}", Name = "GetCollectionProducts")]
            [ProducesResponseType(StatusCodes.Status200OK)]
            [ProducesResponseType(StatusCodes.Status500InternalServerError)]
            public async Task<IActionResult> GetCollectionProducts(string sortOrder, string searchString, int? page = 1, int? pageSize = maxPageSize)
            {
                try
                {
                    ResponseObject resObject;

                    if (!page.HasValue || page.Value == 0)
                    {
                        _logger.LogInformation("Generating List");

                        var dataFromRepo = await _collectionProductRepository.GetAllCollectionProducts();
                        var dataList = _mapper.Map<IEnumerable<CollectionProductDto>>(dataFromRepo);

                        resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };

                        return Json(resObject);
                    }

                    // Sorting -------
                    HttpContext.Session.SetString("NameSortParam", String.IsNullOrEmpty(sortOrder) ? "name_desc" : "");
                    HttpContext.Session.SetString("NameSortParam", String.IsNullOrEmpty(sortOrder) ? "name_asc" : "");

                    HttpContext.Session.SetString("EmailSortParam", String.IsNullOrEmpty(sortOrder) ? "email_desc" : "");
                    HttpContext.Session.SetString("EmailSortParam", String.IsNullOrEmpty(sortOrder) ? "email_asc" : "");

                    HttpContext.Session.SetString("PhoneSortParam", String.IsNullOrEmpty(sortOrder) ? "phone_desc" : "");
                    HttpContext.Session.SetString("PhoneSortParam", String.IsNullOrEmpty(sortOrder) ? "phone_asc" : "");

                    HttpContext.Session.SetString("ContactSortParam", String.IsNullOrEmpty(sortOrder) ? "contact_desc" : "");
                    HttpContext.Session.SetString("ContactSortParam", String.IsNullOrEmpty(sortOrder) ? "contact_asc" : "");

                    HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_asc" : "");
                    HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_desc" : "");

                    //search string check
                    var previousSearchString = HttpContext.Session.GetString("searchString");

                    if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                    {
                        page = 1;
                    }
                    else
                    {
                        // searchString = currentFilter;
                    }

                    IQueryable<CollectionProduct> data = null;

                    //var filterSpecification = new CollectionProductFilterSpecification(contactId, leadId);

                    data = await _collectionProductRepository.GetAllCollectionProducts(true);

                    //collectionProducts = await _collectionProductRepository.GetAllCollectionProducts(true);

                    //search
                    if (!String.IsNullOrEmpty(searchString))
                    {
                        HttpContext.Session.SetString("searchString", searchString);

                        data = data.Where(s => s.Collection.Name.Contains(searchString)
                                                 || s.Product.MetaTagDescription.Contains(searchString)
                                                 || s.Product.MetaTagTitle.Contains(searchString)
                        //|| s.meta.Contains(searchString)
                        //|| s.Title.Contains(searchString)

                        );

                    }
                    else
                    {
                        HttpContext.Session.SetString("searchString", string.Empty);
                    }
                    //Sort Order
                    switch (sortOrder)
                    {
                        case "name_desc":
                            data = data.OrderByDescending(s => s.Collection.Name);
                            break;
                        case "email_desc":
                            data = data.OrderByDescending(s => s.Product.MetaTagTitle);
                            break;
                        case "email_asc":
                            data = data.OrderBy(s => s.Product.MetaTagTitle);
                            break;
                        case "mobilePhone_desc":
                            data = data.OrderByDescending(s => s.Product.MetaTagDescription);
                            break;
                        case "mobilePhone_asc":
                            data = data.OrderBy(s => s.Product.MetaTagDescription);
                            break;
                      
                        case "id_asc":
                            data = data.OrderByDescending(s => s.ProductId);
                            break;
                        case "id_desc":
                            data = data.OrderByDescending(s => s.ProductId);
                            break;
                        default:
                            data = data.OrderBy(s => s.Collection.Name);
                            break;
                    }
                    //
                    // ensure the page size isn't larger than the maximum.
                    if (pageSize > maxPageSize)
                    {
                        pageSize = maxPageSize;
                    }

                    // calculate data for metadata
                    var totalCount = await data.CountAsync();
                    var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                    var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetCollectionProducts", values: new
                    {
                        page = page - 1,
                        pageSize = pageSize

                    }) : "";

                    var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetCollectionProducts", values: new
                    {
                        page = page + 1,
                        pageSize = pageSize

                    }) : "";

                    var paginationHeader = new
                    {
                        currentPage = page,
                        pageSize = pageSize,
                        totalCount = totalCount,
                        totalPages = totalPages,
                        previousPageLink = prevLink,
                        nextPageLink = nextLink
                    };

                    HttpContext.Response.Headers.Add("X-Pagination",
                       Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));


                    // return result
                    var items = await data
                        .Skip((int)pageSize * ((int)page - 1))
                        .Take((int)pageSize)
                        .ToListAsync();
                    //.Select(eg => _expenseGroupFactory.CreateDataShapedObject(eg, lstOfFields))

                    var pagedData = _mapper.Map<IEnumerable<CollectionProductDto>>(items);

                    resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };

                    return Json(resObject);

                }
                catch (Exception e)
                {
                    string msg = "Internal Server Error, Unable to Process GetItems Request";

                    _logger.LogError(msg);

                    var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

                    return Json(resObject);
                }
            }
           
            /// <summary>
            /// Get Contact Person By Id (with Contact attached)
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            [HttpGet("{id}", Name = "GetCollectionProductById")]
            [ProducesResponseType(StatusCodes.Status200OK)]
            [ProducesResponseType(StatusCodes.Status404NotFound)]
            [ProducesResponseType(StatusCodes.Status500InternalServerError)]
            public async Task<IActionResult> GetCollectionProductById(int productId, int collectionId)  // --------------------------------------------------
            {
                try
                {
                    ResponseObject resObject;

                    _logger.LogInformation("GetCollectionProductById");

                    var dataFromRepo = await _collectionProductRepository.GetCollectionProductById(productId,collectionId);


                    if (dataFromRepo == null)
                    {
                        _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetCollectionById({ID}) NOT FOUND", productId);

                        resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data =productId, HttpStatusCode = HttpStatusCode.NotFound };

                        return Json(resObject);
                    }

                    var data = _mapper.Map<CollectionProductDto>(dataFromRepo);

                    resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };

                    return Json(resObject);
                }
                catch (Exception e)
                {
                    string msg = "Internal Server Error, Unable to Process GetById Request";

                    _logger.LogError(msg);

                    var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

                    return Json(resObject);
                }

            }
            /// <summary>
            /// Add New Contact Person - POST api/CollectionProduct
            /// </summary>
            /// <param name="collectionProduct"></param>
            /// <returns></returns>

            [HttpPost(Name = "AddCollectionProduct")]
            [ProducesResponseType(StatusCodes.Status201Created)]
            [ProducesResponseType(StatusCodes.Status500InternalServerError)]
            public async Task<IActionResult> AddCollectionProduct([FromBody] CollectionProductDto collectionProduct)  // ---------------------
            {
                try
                {
                    ResponseObject resObject;

                    if (collectionProduct == null)
                    {
                        _logger.LogError("Owner object sent from client is null.");

                        resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = (CollectionProductDto)null, HttpStatusCode = HttpStatusCode.NotFound };

                        return Json(resObject);
                    }

                    if (!ModelState.IsValid)
                    {
                        _logger.LogError("Invalid owner object sent from client.");

                        resObject = new ResponseObject
                        {
                            Message = "Invalid Data/Model!",
                            Status = "error",
                            Data = ModelState,
                            HttpStatusCode = HttpStatusCode.BadRequest
                        };

                        return Json(resObject);
                    }

                    var collectionProductRepo = _mapper.Map<CollectionProduct>(collectionProduct);

                    await _collectionProductRepository.AddCollectionProduct(collectionProductRepo);

                    _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", collectionProductRepo.ProductId);

                    if (!await _unitOfWork.CompleteAsync())
                    {
                        throw new Exception("Adding a CollectionProduct failed on save.");
                    }

                    //  var contactFromRepo = await _collectionProductRepository.GetCollectionProductById(collectionProductRepo.CollectionProductId);
                    var contactFromRepo2 = CreatedAtAction(nameof(GetCollectionProductById), new { id = collectionProductRepo.ProductId }, collectionProductRepo);

                    var contactFromRepo = contactFromRepo2.Value;

                    var data = _mapper.Map<CollectionProductDto>(contactFromRepo);

                    resObject = new ResponseObject
                    {
                        Message = "Submitted Successfully",
                        Status = "success",
                        Data = data,
                        HttpStatusCode = HttpStatusCode.OK
                    };

                    return Json(resObject);
                }

                catch (Exception e)
                {
                    string msg = "Sever Error, Unable to Process Create Request";

                    _logger.LogWarning(e.Message);

                    var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

                    return Json(resObject);
                }

            }
            private string BuiltMessage(Exception e, int eventtype)
            {
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                            }

                    }
                    return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
                }
                else
                {
                    return eventtype.ToString() + " >> " + e.Message;
                }
            }
            // PUT api/CollectionProduct/5
            [HttpPut("{id}", Name = "UpdateCollectionProduct")]
            [ProducesResponseType(StatusCodes.Status200OK)]
            [ProducesResponseType(StatusCodes.Status404NotFound)]
            [ProducesResponseType(StatusCodes.Status400BadRequest)]
            [ProducesResponseType(StatusCodes.Status500InternalServerError)]
            public async Task<IActionResult> UpdateCollectionProduct(int productId,int collectionId, [FromBody] CollectionProductDto collectionProduct) // ------------
            {
                try
                {
                    ResponseObject resObject;

                    if (collectionProduct == null)
                    {
                        resObject = new ResponseObject
                        {
                            Message = "Data not Found, BadRequest",
                            Status = "error",
                            Data = (CollectionProductDto)null,
                            HttpStatusCode = HttpStatusCode.BadRequest
                        };

                        return Json(resObject);
                    }

                    if (!ModelState.IsValid)
                    {
                        _logger.LogError("Invalid Data");

                        resObject = new ResponseObject
                        {
                            Message = "Invalid Data/Model, BadRequest",
                            Status = "error",
                            Data = ModelState,
                            HttpStatusCode = HttpStatusCode.BadRequest
                        };

                        return Json(resObject);
                    }

                    var collectionProductRepo = _mapper.Map<CollectionProduct>(collectionProduct);

                    if (!_collectionProductRepository.UpdateCollectionProductOk(productId,collectionId, collectionProductRepo))
                    {
                        _logger.LogError($"entity with id: {productId}, hasn't been found in db.");
                        // return StatusCode((int) HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                        resObject = new ResponseObject
                        {
                            Message = "Data not found, BadRequest",
                            Status = "error",
                            Data = productId,
                            HttpStatusCode = HttpStatusCode.BadRequest
                        };

                        return Json(resObject);
                    }

                    if (!await _unitOfWork.CompleteAsync())
                    {
                        throw new Exception("Updating a CollectionProduct failed on save.");
                    }

                    //var contactFromRepo = await _collectionProductRepository.GetCollectionProductById(collectionProductRepo.CollectionProductId);
                    var contactFromRepo2 = CreatedAtAction(nameof(GetCollectionProductById), new { id = collectionProductRepo.ProductId }, collectionProductRepo);

                    var contactFromRepo = contactFromRepo2.Value;

                    var data = _mapper.Map<CollectionProductDto>(contactFromRepo);

                    resObject = new ResponseObject

                    {
                        Message = "Saved Successfully",
                        Status = "success",
                        Data = data,
                        HttpStatusCode = HttpStatusCode.OK
                    };

                    return Json(resObject);
                }

                catch (Exception e)
                {
                    string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                    _logger.LogWarning(e.Message);

                    var resObject = new ResponseObject
                    {
                        Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                        Message = msg,
                        Status = "error",
                        HttpStatusCode = HttpStatusCode.InternalServerError
                    };

                    return Json(resObject);
                }
            }

            /// <summary>
            /// DELETE api/CollectionProduct/5
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            [HttpDelete("{id}", Name = "DeleteCollectionProduct")]
            public async Task<IActionResult> DeleteCollectionProduct(int productId,int collectionId) // -----------------------------------------------------
            {
                try
                {
                    ResponseObject resObject;

                    var entity = _collectionProductRepository.GetCollectionProductByIdAsNoTracking(productId,collectionId);

                    if (entity == false)
                    {
                        _logger.LogError($"entity with id: {productId}, hasn't been found in db.");

                        resObject = new ResponseObject
                        {
                            Message = "Data not found, BadRequest",
                            Status = "error",
                            Data = productId,
                            HttpStatusCode = HttpStatusCode.BadRequest
                        };

                        return Json(resObject);
                    }

                    _collectionProductRepository.DeleteCollectionProduct(productId,collectionId);

                    if (!await _unitOfWork.CompleteAsync())
                    {
                        throw new Exception("Deleting a CollectionProduct failed on save.");
                    }

                    resObject = new ResponseObject
                    {
                        Message = "Deleted Successfully",
                        Status = "success",
                        Data = productId,
                        HttpStatusCode = HttpStatusCode.OK
                    };

                    return Json(resObject);
                }

                catch (Exception e)
                {
                    string msg = "Internal Server Error, Unable to Process Delete Request";
                    _logger.LogError(msg);

                    var resObject = new ResponseObject
                    {
                        Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                        Message = msg,
                        Status = "error",
                        HttpStatusCode = HttpStatusCode.InternalServerError
                    };

                    return Json(resObject);
                }
            }
        }
    }

