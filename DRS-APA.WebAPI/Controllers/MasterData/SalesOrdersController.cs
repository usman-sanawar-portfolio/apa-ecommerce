﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Shop;
using DRS.APA.Domain.Core.Dtos.Shop;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Repositories.Shop;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.Domain.Core.Specifications;
//using System.Data.Entity.Validation;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class SalesOrdersController : Controller
    {
        private readonly ISalesOrderRepository _salesOrderRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const int maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;

       
        public SalesOrdersController(ISalesOrderRepository salesOrderRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<SalesOrdersController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _salesOrderRepository = salesOrderRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }               

        /// <summary>
        /// List all public SalesOrders sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetSalesOrders")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetSalesOrders(string sortOrder, int? customerId, short? deliveryMethodId, DateTime? dateFrom, DateTime? dateTo, string searchString, int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var salesOrdersFromRepo = await _salesOrderRepository.GetAllSalesOrders();
                    var salesOrderslist = _mapper.Map<IEnumerable<SalesOrderDto>>(salesOrdersFromRepo);
                    return Ok(salesOrderslist);
                }

                // Sorting -------

                HttpContext.Session.SetString("NameSortParm", String.IsNullOrEmpty(sortOrder) ? "name_desc" : "");
                HttpContext.Session.SetString("NameSortParm", String.IsNullOrEmpty(sortOrder) ? "name_asc" : "");

                HttpContext.Session.SetString("PriceSortParm", String.IsNullOrEmpty(sortOrder) ? "price_desc" : "");
                HttpContext.Session.SetString("PriceSortParm", String.IsNullOrEmpty(sortOrder) ? "price_asc" : "");

                HttpContext.Session.SetString("DateSortParm", String.IsNullOrEmpty(sortOrder) ? "date_desc" : "");
                HttpContext.Session.SetString("DateSortParm", String.IsNullOrEmpty(sortOrder) ? "date_asc" : "");

                HttpContext.Session.SetString("NoSortParm", String.IsNullOrEmpty(sortOrder) ? "no_desc" : "");
                HttpContext.Session.SetString("NoSortParm", String.IsNullOrEmpty(sortOrder) ? "no_asc" : "");

                HttpContext.Session.SetString("IdSortParm", String.IsNullOrEmpty(sortOrder) ? "id_asc" : "");
                HttpContext.Session.SetString("IdSortParm", String.IsNullOrEmpty(sortOrder) ? "id_desc" : "");


                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;

                }
                //Search Items
                IQueryable<SalesOrder> salesOrders = null;

                var filterSpecification = new SalesOrderFilterSpecification(customerId,deliveryMethodId, dateFrom, dateTo);

                salesOrders = await _salesOrderRepository.GetAllSalesOrders(filterSpecification);

                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    salesOrders = salesOrders.Where(s => s.Comment.Contains(searchString)
                                             || s.ShippingAddress.Street.Contains(searchString));
                    //|| s.Model.Name.Contains(searchString)
                    //|| s.Model.Make.Name.Contains(searchString));
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }

                //Sort Order
                switch (sortOrder)
                {
                    case "name_desc":
                        salesOrders = salesOrders.OrderByDescending(s => s.Customer);
                        break;
                    case "price_desc":
                        salesOrders = salesOrders.OrderByDescending(s => s.Total());
                        break;
                    case "price_asc":
                        salesOrders = salesOrders.OrderBy(s => s.Total());
                        break;
                    case "id_asc":
                        salesOrders = salesOrders.OrderByDescending(s => s.SalesOrderId);
                        break;
                    case "id_desc":
                        salesOrders = salesOrders.OrderByDescending(s => s.SalesOrderId);
                        break;
                    default:
                        salesOrders = salesOrders.OrderBy(s => s.Customer);
                        break;
                }
                //

                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount =await salesOrders.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetSalesOrders", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetSalesOrders", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";
                
                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

               // salesOrders = await _salesOrderRepository.GetAllSalesOrders(true);
                // return result
                var items = await salesOrders
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();
                //.Select(eg => _expenseGroupFactory.CreateDataShapedObject(eg, lstOfFields))

                var pagedItems = _mapper.Map<IEnumerable<SalesOrderDto>>(items);
                return Ok(pagedItems);                

            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetItems Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }

        [HttpGet("{id}", Name = "GetSalesOrderById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetSalesOrderById(int id)  // --------------------------------------------------
        {
            try
            {
                _logger.LogInformation("GetSalesOrderById");
                var salesOrderFromRepo = await _salesOrderRepository.GetSalesOrderById(id);
                if (salesOrderFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetSalesOrderById({ID}) NOT FOUND", id);
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found!-" + id);
                    //return NotFound();
                }
             
               var salesOrder = _mapper.Map<SalesOrderDto>(salesOrderFromRepo);
                return Ok(salesOrder);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetById Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }

        [HttpGet("salesOrdercategories/{makeId}", Name = "GetSalesOrdersByCategory")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetSalesOrdersByCategory(int makeId)  // --------------------------------------------------
        {
            try
            {
                _logger.LogInformation("GetSalesOrdersByCategory");
                var salesOrderFromRepo = await _salesOrderRepository.GetAllSalesOrdersByCategory(makeId);
                if (salesOrderFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetSalesOrderByMake({ID}) NOT FOUND", makeId);
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found!-" + makeId);
                    //return NotFound();
                }

                //var salesOrders = _mapper.Map<SalesOrderDto>(salesOrderFromRepo); was creating error
                var salesOrderslist = _mapper.Map<IEnumerable<SalesOrderDto>>(salesOrderFromRepo);
                return Ok(salesOrderslist);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetById Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }
        /// <summary>
        /// Get orders category based.
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [HttpGet("categories/{categoryId}", Name = "GetAllSalesOrdersByCategory")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAllSalesOrdersByCategory(int categoryId)  // --------------------------------------------------
        {
            try
            {
                _logger.LogInformation("GetSalesOrdersByCollection");
                var salesOrderFromRepo = await _salesOrderRepository.GetAllSalesOrdersByCategory(categoryId);
                if (salesOrderFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetSalesOrderByMake({ID}) NOT FOUND", categoryId);
                   // _logger.LogInformation();
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found!-" + categoryId);
                    //return NotFound();
                }

                //var salesOrders = _mapper.Map<SalesOrderDto>(salesOrderFromRepo); was creating error
                var salesOrderslist = _mapper.Map<IEnumerable<SalesOrderDto>>(salesOrderFromRepo);
                return Ok(salesOrderslist);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetById Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }

        /// <summary>
        /// Get orders category based.
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        [HttpGet("customers/{customerId}", Name = "GetAllSalesOrdersByCustomer")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAllSalesOrdersByCustomer(int customerId)  // --------------------------------------------------
        {
            try
            {
                _logger.LogInformation("GetSalesOrdersByCollection");
                var salesOrderFromRepo = await _salesOrderRepository.GetAllSalesOrdersByCustomer(customerId);
                if (salesOrderFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetSalesOrderByMake({ID}) NOT FOUND", customerId);
                    // _logger.LogInformation();
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found!-" + customerId);
                    //return NotFound();
                }

                //var salesOrders = _mapper.Map<SalesOrderDto>(salesOrderFromRepo); was creating error
                var salesOrderslist = _mapper.Map<IEnumerable<SalesOrderDto>>(salesOrderFromRepo);
                return Ok(salesOrderslist);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetById Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }






        //https://stackoverflow.com/questions/51211646/the-input-was-not-valid-net-core-web-api




        // POST api/SalesOrder
        [HttpPost(Name = "AddSalesOrder")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddSalesOrder([FromBody] SalesOrderDto salesOrder)  // ---------------------
        {
            try
            {

                if (salesOrder == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, salesOrder);
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(SalesOrderState);
                }

                var salesOrderRepo = _mapper.Map<SalesOrder>(salesOrder);
                await _salesOrderRepository.AddSalesOrder(salesOrderRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", salesOrderRepo.SalesOrderId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a SalesOrder failed on save.");
                }
                return CreatedAtAction(nameof(GetSalesOrderById), new { id = salesOrderRepo.SalesOrderId }, salesOrderRepo);
                //return StatusCode((int)HttpStatusCode.Created);
                // return Content("{ \"message\":\"Form Submitted\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process Create Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        // PUT api/SalesOrder/5
        [HttpPut("{id}", Name = "UpdateSalesOrder")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateSalesOrder(int id, [FromBody] SalesOrderDto salesOrder) // ------------
        {
            try
            {

                if (salesOrder == null)
                {
                    _logger.LogError("Object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, salesOrder);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid object sent from client-SalesOrderState");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(SalesOrderState);
                }

                var salesOrderRepo = _mapper.Map<SalesOrder>(salesOrder);
                if (!_salesOrderRepository.UpdateSalesOrderOk(id, salesOrderRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    //return NotFound();
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a SalesOrder Template failed on save.");
                }

                return Ok(salesOrderRepo);
                // return Content("{ \"message\":\"Form Updated\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }
        }

        // DELETE api/SalesOrder/
        [HttpDelete("{id}", Name = "DeleteSalesOrder")]
        public async Task<IActionResult> DeleteSalesOrder(int id) // -----------------------------------------------------
        {
            try
            {
                var entity = _salesOrderRepository.GetSalesOrderByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound);
                    //return NotFound();
                }

                _salesOrderRepository.DeleteSalesOrder(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a SalesOrder Template failed on save.");
                }
                return Content("{ \"message\":\"SalesOrder Deleted\",\"type\":\"success\" }", "application/json");
            }
            catch (Exception e)
            {
                string msg = "Unable to Process Delete Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }

    }
}
