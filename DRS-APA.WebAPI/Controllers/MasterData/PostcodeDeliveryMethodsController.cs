﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;

//using System.Data.Entity.Validation;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class PostcodeDeliveryMethodsController : Controller
    {
        private readonly IPostcodeDeliveryMethodRepository _postcodeDeliveryMethodRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const int maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;

       
        public PostcodeDeliveryMethodsController(IPostcodeDeliveryMethodRepository repairOptionRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<PostcodeDeliveryMethodsController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _postcodeDeliveryMethodRepository = repairOptionRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }               

        /// <summary>
        /// List all public PostcodeDeliveryMethodModels sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetPostcodeDeliveryMethodModels")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetPostcodeDeliveryMethods(int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var postcodeDeliveryMethodsFromRepo = await _postcodeDeliveryMethodRepository.GetAllPostcodeDeliveryMethods();
                    var postcodeDeliveryMethodsList = _mapper.Map<IEnumerable<PostcodeDeliveryMethodDto>>(postcodeDeliveryMethodsFromRepo);
                    return Ok(postcodeDeliveryMethodsList);
                }

                IQueryable<PostcodeDeliveryMethod> postcodeDeliveryMethods = null;
                postcodeDeliveryMethods = await _postcodeDeliveryMethodRepository.GetAllPostcodeDeliveryMethods(true);

                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount =await postcodeDeliveryMethods.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetPostcodeDeliveryMethods", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetPostcodeDeliveryMethods", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";
                
                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

                postcodeDeliveryMethods = await _postcodeDeliveryMethodRepository.GetAllPostcodeDeliveryMethods(true);
                // return result
                var items = await postcodeDeliveryMethods
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();
                //.Select(eg => _expenseGroupFactory.CreateDataShapedObject(eg, lstOfFields))

                var pagedItems = _mapper.Map<IEnumerable<PostcodeDeliveryMethodDto>>(items);
                return Ok(pagedItems);                

            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetItems Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }

        [HttpGet("postcodes/{postcodeId}/deliverymethods/{deliveryMethodId}", Name = "GetPostcodeDeliveryMethodById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetPostcodeDeliveryMethodById(short postcodeId, short deliveryMethodId)  // --------------------------------------------------
        {
            try
            {
                _logger.LogInformation("GetPostcodeDeliveryMethodById");
                var postcodeDeliveryMethodFromRepo = await _postcodeDeliveryMethodRepository.GetPostcodeDeliveryMethodById(postcodeId, deliveryMethodId);
                if (postcodeDeliveryMethodFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetPostcodeDeliveryMethodById({postcodeId},{deliveryMethodId}) NOT FOUND", postcodeId);
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found!-" + postcodeId + '-' + deliveryMethodId);
                    //return NotFound();
                }

                var postcodeDeliveryMethod = _mapper.Map<PostcodeDeliveryMethodDto>(postcodeDeliveryMethodFromRepo);
                return Ok(postcodeDeliveryMethod);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetById Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }



        /// <summary>
        /// GetPostcodeDeliveryMethods By Postcode
        /// </summary>
        /// <param name="postcodeId"></param>
        /// <returns></returns>
        [HttpGet("postcodes/{postcodeId}", Name = "GetPostcodeDeliveryMethodsByPostcode")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetPostcodeDeliveryMethodsByPostcode(short postcodeId)
        {
            try
            {
                _logger.LogInformation("Generating List");
                
                var postcodeDeliveryMethodsFromRepo = await _postcodeDeliveryMethodRepository.GetAllPostcodeDeliveryMethodsByPostcode(postcodeId);
                
                var postcodeDeliveryMethodsList = _mapper.Map<IEnumerable<PostcodeDeliveryMethodDto>>(postcodeDeliveryMethodsFromRepo);
                
                return Ok(postcodeDeliveryMethodsList);

            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetItems Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }







        // POST api/PostcodeDeliveryMethod
        [HttpPost(Name = "AddPostcodeDeliveryMethod")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddPostcodeDeliveryMethod([FromBody] PostcodeDeliveryMethodDto postcodeDeliveryMethod)  // ---------------------
        {
            try
            {

                if (postcodeDeliveryMethod == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, postcodeDeliveryMethod);
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(ModelState);
                }

                var postcodeDeliveryMethodRepo = _mapper.Map<PostcodeDeliveryMethod>(postcodeDeliveryMethod);
                await _postcodeDeliveryMethodRepository.AddPostcodeDeliveryMethod(postcodeDeliveryMethodRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", postcodeDeliveryMethodRepo.PostcodeId, postcodeDeliveryMethodRepo.DeliveryMethodId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a PostcodeDeliveryMethod failed on save.");
                }
                return CreatedAtAction(nameof(GetPostcodeDeliveryMethodById), new { ModelId = postcodeDeliveryMethodRepo.PostcodeId, RepairOptionId= postcodeDeliveryMethodRepo.DeliveryMethodId }, postcodeDeliveryMethodRepo);
                //return StatusCode((int)HttpStatusCode.Created);
                // return Content("{ \"message\":\"Form Submitted\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process Create Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        // PUT api/PostcodeDeliveryMethod/5
        [HttpPut("{id}", Name = "UpdatePostcodeDeliveryMethod")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdatePostcodeDeliveryMethod(short postcodeId, short deliveryMethodId, [FromBody] PostcodeDeliveryMethodDto postcodeDeliveryMethod) // ------------
        {
            try
            {
                if (postcodeDeliveryMethod == null)
                {
                    _logger.LogError("Object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, (PostcodeDeliveryMethodDto) null);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid object sent from client-ModelState");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(ModelState);
                }

                var postcodeDeliveryMethodRepo = _mapper.Map<PostcodeDeliveryMethod>(postcodeDeliveryMethod);
                if (!_postcodeDeliveryMethodRepository.UpdatePostcodeDeliveryMethodOk( postcodeId,  deliveryMethodId, postcodeDeliveryMethodRepo))
                {
                    _logger.LogError($"entity with id: {postcodeId}-{deliveryMethodId}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound, $"entity with id: {postcodeId}-{deliveryMethodId}, hasn't been found in db.");
                    //return NotFound();
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a PostcodeDeliveryMethod Template failed on save.");
                }

                return Ok(postcodeDeliveryMethodRepo);
                // return Content("{ \"message\":\"Form Updated\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }
        }

        // DELETE api/PostcodeDeliveryMethod/
        [HttpDelete("{id}", Name = "DeletePostcodeDeliveryMethod")]
        public async Task<IActionResult> DeletePostcodeDeliveryMethod(short postcodeId, short deliveryMethodId) // -----------------------------------------------------
        {
            try
            {
                var entity = _postcodeDeliveryMethodRepository.GetPostcodeDeliveryMethodByIdAsNoTracking(postcodeId, deliveryMethodId);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {postcodeId}-{deliveryMethodId}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound);
                    //return NotFound();
                }

                _postcodeDeliveryMethodRepository.DeletePostcodeDeliveryMethod(postcodeId, deliveryMethodId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a PostcodeDeliveryMethod Template failed on save.");
                }
                return Content("{ \"message\":\"PostcodeDeliveryMethod Deleted\",\"type\":\"success\" }", "application/json");
            }
            catch (Exception e)
            {
                string msg = "Unable to Process Delete Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }

    }
}
