﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;

//using System.Data.Entity.Validation;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class PostcodesController : Controller
    {
        private readonly IPostcodeRepository _postcodeRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const int maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;

       
        public PostcodesController(IPostcodeRepository jobtypeRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<PostcodesController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _postcodeRepository = jobtypeRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }               

        /// <summary>
        /// List all public Postcodes sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetPostcodes")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetPostcodes(int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var postcodesFromRepo = await _postcodeRepository.GetAllPostcodes();
                    var postcodesList = _mapper.Map<IEnumerable<PostcodeDto>>(postcodesFromRepo);
                    return Ok(postcodesList);
                }

                IQueryable<Postcode> postcodes = null;
                postcodes = await _postcodeRepository.GetAllPostcodes(true);

                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount =await postcodes.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetPostcodes", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetPostcodes", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";
                
                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

                postcodes = await _postcodeRepository.GetAllPostcodes(true);
                // return result
                var items = await postcodes
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();
                //.Select(eg => _expenseGroupFactory.CreateDataShapedObject(eg, lstOfFields))

                var pagedItems = _mapper.Map<IEnumerable<PostcodeDto>>(items);
                return Ok(pagedItems);                

            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetItems Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }

       





        [HttpGet("{id}", Name = "GetPostcodeById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]

        public async Task<IActionResult> GetPostcodeById(short id)  // --------------------------------------------------
        {
            try
            {
                _logger.LogInformation("GetPostcodeById");
                var postcodeFromRepo = await _postcodeRepository.GetPostcodeById(id);
                if (postcodeFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetPostcodeById({ID}) NOT FOUND", id);
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found!-" + id);
                    //return NotFound();
                }

                var postcode = _mapper.Map<PostcodeDto>(postcodeFromRepo);
                return Ok(postcode);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetById Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }
     
        // POST api/Postcode
        [HttpPost(Name = "AddPostcode")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddPostcode([FromBody] PostcodeDto postcode)  // ---------------------
        {
            try
            {

                if (postcode == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, postcode);
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(ModelState);
                }

                var postcodeRepo = _mapper.Map<Postcode>(postcode);
                await _postcodeRepository.AddPostcode(postcodeRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", postcodeRepo.PostcodeId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a Postcode failed on save.");
                }
                return CreatedAtAction(nameof(GetPostcodeById), new { id = postcodeRepo.PostcodeId }, postcodeRepo);
                //return StatusCode((int)HttpStatusCode.Created);
                // return Content("{ \"message\":\"Form Submitted\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process Create Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        // PUT api/Postcode/5
        [HttpPut("{id}", Name = "UpdatePostcode")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdatePostcode(short id, [FromBody] PostcodeDto postcode) // ------------
        {
            try
            {

                if (postcode == null)
                {
                    _logger.LogError("Object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, postcode);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid object sent from client-ModelState");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(ModelState);
                }

                var postcodeRepo = _mapper.Map<Postcode>(postcode);
                if (!_postcodeRepository.UpdatePostcodeOk(id, postcodeRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    //return NotFound();
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a Postcode Template failed on save.");
                }

                return Ok(postcodeRepo);
                // return Content("{ \"message\":\"Form Updated\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }
        }

        // DELETE api/Postcode/
        [HttpDelete("{id}", Name = "DeletePostcode")]
        public async Task<IActionResult> DeletePostcode(short id) // -----------------------------------------------------
        {
            try
            {
                var entity = _postcodeRepository.GetPostcodeByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound);
                    //return NotFound();
                }

                _postcodeRepository.DeletePostcode(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a Postcode Template failed on save.");
                }
                return Content("{ \"message\":\"Postcode Deleted\",\"type\":\"success\" }", "application/json");
            }
            catch (Exception e)
            {
                string msg = "Unable to Process Delete Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }

        /// <summary>
        /// Get By Name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet("getpostcodebyname/{name}", Name = "GetPostCodeByName")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetPostCodeByName(string name) // ---------------------------------------------
        {
            try
            {

                _logger.LogInformation("GetByName");
                var repairOptionFromRepo = await _postcodeRepository.GetPostcodeByName(name);
                if (repairOptionFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetByName({name}) NOT FOUND", name);
                    //return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found By Name:-" + name);
                    return Json((Postcode) null);
                }

                var postcode = _mapper.Map<PostcodeDto>(repairOptionFromRepo);
              
                return Ok(postcode);
                //return Json(repairOptionFromRepo);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetRepairOptionByName Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }
    }
}
