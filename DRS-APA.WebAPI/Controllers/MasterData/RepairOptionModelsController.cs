﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;

//using System.Data.Entity.Validation;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class RepairOptionModelsController : Controller
    {
        private readonly IRepairOptionModelRepository _repairOptionModelRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const int maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;

       
        public RepairOptionModelsController(IRepairOptionModelRepository repairOptionRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<RepairOptionModelsController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _repairOptionModelRepository = repairOptionRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }               

        /// <summary>
        /// List all public RepairOptionModelModels sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetRepairOptionModelModels")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetRepairOptionModels(int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var repairOptionModelsFromRepo = await _repairOptionModelRepository.GetAllRepairOptionModels();
                    var repairOptionModelsList = _mapper.Map<IEnumerable<RepairOptionModelDto>>(repairOptionModelsFromRepo);
                    return Ok(repairOptionModelsList);
                }

                IQueryable<RepairOptionModel> repairOptionModels = null;
                repairOptionModels = await _repairOptionModelRepository.GetAllRepairOptionModels(true);

                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount =await repairOptionModels.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetRepairOptionModels", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetRepairOptionModels", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";
                
                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

                repairOptionModels = await _repairOptionModelRepository.GetAllRepairOptionModels(true);
                // return result
                var items = await repairOptionModels
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();
                //.Select(eg => _expenseGroupFactory.CreateDataShapedObject(eg, lstOfFields))

                var pagedItems = _mapper.Map<IEnumerable<RepairOptionModelDto>>(items);
                return Ok(pagedItems);                

            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetItems Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }

        [HttpGet("repairoptions/{repairoptionId}/models/{modelId}", Name = "GetRepairOptionModelById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetRepairOptionModelById(short repairoptionId, int modelId)  // --------------------------------------------------
        {
            try
            {
                _logger.LogInformation("GetRepairOptionModelById");
                var repairOptionModelFromRepo = await _repairOptionModelRepository.GetRepairOptionModelById(repairoptionId,modelId);
                if (repairOptionModelFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetRepairOptionModelById({repairoptionId},{modelId}) NOT FOUND", repairoptionId);
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found!-" + repairoptionId + '-' + modelId);
                    //return NotFound();
                }

                var repairOptionModel = _mapper.Map<RepairOptionModelDto>(repairOptionModelFromRepo);
                return Ok(repairOptionModel);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetById Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }
     
        // POST api/RepairOptionModel
        [HttpPost(Name = "AddRepairOptionModel")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddRepairOptionModel([FromBody] RepairOptionModelDto repairOptionModel)  // ---------------------
        {
            try
            {

                if (repairOptionModel == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, repairOptionModel);
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(ModelState);
                }

                var repairOptionModelRepo = _mapper.Map<RepairOptionModel>(repairOptionModel);
                await _repairOptionModelRepository.AddRepairOptionModel(repairOptionModelRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", repairOptionModelRepo.RepairOptionId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a RepairOptionModel failed on save.");
                }
                return CreatedAtAction(nameof(GetRepairOptionModelById), new { ModelId = repairOptionModelRepo.ModelId, RepairOptionId= repairOptionModelRepo.RepairOptionId }, repairOptionModelRepo);
                //return StatusCode((int)HttpStatusCode.Created);
                // return Content("{ \"message\":\"Form Submitted\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process Create Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        // PUT api/RepairOptionModel/5
        [HttpPut("{id}", Name = "UpdateRepairOptionModel")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateRepairOptionModel(short repairoptionId, int modelId, [FromBody] RepairOptionModelDto repairOptionModel) // ------------
        {
            try
            {
                if (repairOptionModel == null)
                {
                    _logger.LogError("Object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, repairOptionModel);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid object sent from client-ModelState");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(ModelState);
                }

                var repairOptionModelRepo = _mapper.Map<RepairOptionModel>(repairOptionModel);
                if (!_repairOptionModelRepository.UpdateRepairOptionModelOk(repairoptionId, modelId, repairOptionModelRepo))
                {
                    _logger.LogError($"entity with id: {repairoptionId}-{modelId}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound, $"entity with id: {repairoptionId}-{modelId}, hasn't been found in db.");
                    //return NotFound();
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a RepairOptionModel Template failed on save.");
                }

                return Ok(repairOptionModelRepo);
                // return Content("{ \"message\":\"Form Updated\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }
        }

        // DELETE api/RepairOptionModel/
        [HttpDelete("{id}", Name = "DeleteRepairOptionModel")]
        public async Task<IActionResult> DeleteRepairOptionModel(short repairoptionId, int modelId) // -----------------------------------------------------
        {
            try
            {
                var entity = _repairOptionModelRepository.GetRepairOptionModelByIdAsNoTracking(repairoptionId, modelId);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {repairoptionId}-{modelId}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound);
                    //return NotFound();
                }

                _repairOptionModelRepository.DeleteRepairOptionModel(repairoptionId, modelId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a RepairOptionModel Template failed on save.");
                }
                return Content("{ \"message\":\"RepairOptionModel Deleted\",\"type\":\"success\" }", "application/json");
            }
            catch (Exception e)
            {
                string msg = "Unable to Process Delete Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }

        [HttpGet("GetRepairOptionModelByName/{name}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetRepairOptionModelByName(string name) // ---------------------------------------------
        {
            try
            {

                _logger.LogInformation("GetByName");
                var repairOptionModelFromRepo = await _repairOptionModelRepository.GetRepairOptionModelByName(name);
                if (repairOptionModelFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetByName({name}) NOT FOUND", name);
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found By Name:-" + name);
                    //return NotFound();
                }
                return Ok(name);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetRepairOptionModelByName Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }


    }
}
