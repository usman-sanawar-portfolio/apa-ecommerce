﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;


[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class DiscountProductsController : Controller
    {
        private readonly IDiscountProductRepository _discountProductRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const int maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;


        public DiscountProductsController(IDiscountProductRepository repairOptionRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<DiscountProductsController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _discountProductRepository = repairOptionRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }
        /// <summary>
        /// List all public DiscountProducts sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetDiscountProducts")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetDiscountProducts(string sortOrder, string searchString, int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                ResponseObject resObject;

                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");

                    var dataFromRepo = await _discountProductRepository.GetAllDiscountProducts();
                    var dataList = _mapper.Map<IEnumerable<DiscountProductDto>>(dataFromRepo);

                    resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };

                    return Json(resObject);
                }

                // Sorting -------
                HttpContext.Session.SetString("NameSortParam", String.IsNullOrEmpty(sortOrder) ? "name_desc" : "");
                HttpContext.Session.SetString("NameSortParam", String.IsNullOrEmpty(sortOrder) ? "name_asc" : "");

                HttpContext.Session.SetString("EmailSortParam", String.IsNullOrEmpty(sortOrder) ? "email_desc" : "");
                HttpContext.Session.SetString("EmailSortParam", String.IsNullOrEmpty(sortOrder) ? "email_asc" : "");

                HttpContext.Session.SetString("PhoneSortParam", String.IsNullOrEmpty(sortOrder) ? "phone_desc" : "");
                HttpContext.Session.SetString("PhoneSortParam", String.IsNullOrEmpty(sortOrder) ? "phone_asc" : "");

                HttpContext.Session.SetString("ContactSortParam", String.IsNullOrEmpty(sortOrder) ? "contact_desc" : "");
                HttpContext.Session.SetString("ContactSortParam", String.IsNullOrEmpty(sortOrder) ? "contact_asc" : "");

                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_asc" : "");
                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_desc" : "");

                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;
                }

                IQueryable<DiscountProduct> data = null;

                //var filterSpecification = new DiscountProductFilterSpecification(contactId, leadId);

                data = await _discountProductRepository.GetAllDiscountProducts(true);

                //discountProducts = await _discountProductRepository.GetAllDiscountProducts(true);

                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);

                    data = data.Where(s => s.Product.MetaTagTitle.Contains(searchString)
                                             || s.Product.MetaTagKeywords.Contains(searchString)
                                            

                    );

                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "name_desc":
                        data = data.OrderByDescending(s => s.Product.MetaTagTitle);
                        break;
                    case "email_desc":
                        data = data.OrderByDescending(s => s.Product.MetaTagKeywords);
                        break;
                    case "email_asc":
                        data = data.OrderBy(s => s.Product.MetaTagKeywords);
                        break;
                    case "mobilePhone_desc":
                        data = data.OrderByDescending(s => s.Product.MetaTagDescription);
                        break;
                    case "mobilePhone_asc":
                        data = data.OrderBy(s => s.Product.MetaTagDescription);
                        break;
                    case "contactName_desc":
                        data = data.OrderByDescending(s => s.Discount.Name);
                        break;
                    case "contactName_asc":
                        data = data.OrderBy(s => s.Discount.Name);
                        break;
                    case "id_asc":
                        data = data.OrderByDescending(s => s.DiscountId);
                        break;
                    case "id_desc":
                        data = data.OrderByDescending(s => s.DiscountId);
                        break;
                    default:
                        data = data.OrderBy(s => s.Product.MetaTagTitle);
                        break;
                }
                //
                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount = await data.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetDiscountProducts", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetDiscountProducts", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";

                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));


                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();

                var pagedData = _mapper.Map<IEnumerable<DiscountProductDto>>(items);

                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };

                return Json(resObject);

            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetItems Request";

                _logger.LogError(msg);

                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

                return Json(resObject);
            }
        }

        /// <summary>
        /// GetDiscountProductById
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetDiscountProductById/{productId}/{discountId}", Name = "GetDiscountProductById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetDiscountProductById(short discountId, int productId)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;

                _logger.LogInformation("GetDiscountProductById");

                var dataFromRepo = await _discountProductRepository.GetDiscountProductById(discountId,productId);


                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetCollectionById({discountId},{productId}) NOT FOUND", discountId, productId);

                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data =productId.ToString()+"/"+discountId.ToString(), HttpStatusCode = HttpStatusCode.NotFound };

                    return Json(resObject);
                }

                var data = _mapper.Map<DiscountProductDto>(dataFromRepo);

                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };

                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetById Request";

                _logger.LogError(msg);

                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

                return Json(resObject);
            }

        }
        /// <summary>
        /// Add New Contact Person - POST api/DiscountProduct
        /// </summary>
        /// <param name="discountProduct"></param>
        /// <returns></returns>

        [HttpPost(Name = "AddDiscountProduct")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddDiscountProduct([FromBody] DiscountProductDto discountProduct)  // ---------------------
        {
            try
            {
                ResponseObject resObject;

                if (discountProduct == null)
                {
                    _logger.LogError("Owner object sent from client is null.");

                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = (DiscountProductDto)null, HttpStatusCode = HttpStatusCode.NotFound };

                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");

                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model!",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var discountProductRepo = _mapper.Map<DiscountProduct>(discountProduct);

                await _discountProductRepository.AddDiscountProduct(discountProductRepo);

                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", discountProductRepo.DiscountId);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a DiscountProduct failed on save.");
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetDiscountProductById), new { id = discountProductRepo.DiscountId }, discountProductRepo);

                var contactFromRepo = contactFromRepo2.Value;

                var data = _mapper.Map<DiscountProductDto>(contactFromRepo);

                resObject = new ResponseObject
                {
                    Message = "Submitted Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Sever Error, Unable to Process Create Request";

                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject {
                    Message = BuiltMessage(e, LoggingEvents.GetItem), 
                    Message2 = msg, Status = "error", 
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        // PUT api/DiscountProduct/5
        [HttpPut("{discountId}/{productId}/DiscountProduct", Name = "UpdateDiscountProduct")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateDiscountProduct(short discountId, int productId, [FromBody] DiscountProductDto discountProduct) // ------------
        {
            try
            {
                ResponseObject resObject;

                if (discountProduct == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = "Data not Found, BadRequest",
                        Status = "error",
                        Data = (DiscountProductDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Data");

                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model, BadRequest",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var discountProductRepo = _mapper.Map<DiscountProduct>(discountProduct);

                if (!_discountProductRepository.UpdateDiscountProductOk(discountId, productId, discountProductRepo))
                {
                    _logger.LogError($"entity with id: {discountId}, hasn't been found in db.");
                    // return StatusCode((int) HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = discountId,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a DiscountProduct failed on save.");
                }

                //var contactFromRepo = await _discountProductRepository.GetDiscountProductById(discountProductRepo.DiscountProductId);
                var contactFromRepo2 = CreatedAtAction(nameof(GetDiscountProductById), new { id = discountProductRepo.DiscountId }, discountProductRepo);

                var contactFromRepo = contactFromRepo2.Value;

                var data = _mapper.Map<DiscountProductDto>(contactFromRepo);

                resObject = new ResponseObject

                {
                    Message = "Saved Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }
        }

        /// <summary>
        /// DELETE api/DiscountProduct/5
        /// </summary>
        /// <param name="discountId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpDelete("{discountId}/{productId}", Name = "DeleteDiscountProduct")]
        public async Task<IActionResult> DeleteDiscountProduct(short discountId,int productId) // -----------------------------------------------------
        {
            try
            {
                ResponseObject resObject;

                var entity = _discountProductRepository.GetDiscountProductByIdAsNoTracking(discountId,productId);

                if (entity == false)
                {
                    _logger.LogError($"entity with id: {discountId}, hasn't been found in db.");

                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = discountId,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                _discountProductRepository.DeleteDiscountProduct(discountId,productId);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a DiscountProduct failed on save.");
                }

                resObject = new ResponseObject
                {
                    Message = "Deleted Successfully",
                    Status = "success",
                    Data = discountId,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process Delete Request";
                _logger.LogError(msg);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }
        }
    }
}

