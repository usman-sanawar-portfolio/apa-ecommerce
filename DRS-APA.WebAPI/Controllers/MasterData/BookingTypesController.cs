﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Schedule;
using DRS.APA.Domain.Core.Dtos.Schedule;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.Domain.Core.Specifications;
//using System.Data.Entity.Validation;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class BookingTypesController : Controller
    {
        private readonly IBookingTypeRepository _bookingTypeRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const short maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;

       
        public BookingTypesController(IBookingTypeRepository bookingTypeRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<BookingTypesController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _bookingTypeRepository = bookingTypeRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }               

        /// <summary>
        /// List all public BookingTypes sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetBookingTypes")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetBookingTypes(string sortOrder, string searchString, short? page = 1, short? pageSize = maxPageSize)
        {
            try
            {
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var bookingTypesFromRepo = await _bookingTypeRepository.GetAllBookingTypes();
                    var bookingTypeslist = _mapper.Map<IEnumerable<BookingTypeDto>>(bookingTypesFromRepo);
                    return Ok(bookingTypeslist);
                }

                // Sorting -------

                HttpContext.Session.SetString("NameSortParm", String.IsNullOrEmpty(sortOrder) ? "name_desc" : "");
                HttpContext.Session.SetString("NameSortParm", String.IsNullOrEmpty(sortOrder) ? "name_asc" : "");

                HttpContext.Session.SetString("PriceSortParm", String.IsNullOrEmpty(sortOrder) ? "price_desc" : "");
                HttpContext.Session.SetString("PriceSortParm", String.IsNullOrEmpty(sortOrder) ? "price_asc" : "");

                HttpContext.Session.SetString("MakeSortParm", String.IsNullOrEmpty(sortOrder) ? "make_desc" : "");
                HttpContext.Session.SetString("MakeSortParm", String.IsNullOrEmpty(sortOrder) ? "make_asc" : "");

                HttpContext.Session.SetString("IdSortParm", String.IsNullOrEmpty(sortOrder) ? "id_asc" : "");
                HttpContext.Session.SetString("IdSortParm", String.IsNullOrEmpty(sortOrder) ? "id_desc" : "");


                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;

                }
                //Search Items
                IQueryable<BookingType> bookingTypes = null;

                //var filterSpecification = new BookingTypeFilterSpecification(bookingTypeCategoryId,  makeId);
                //bookingTypes = await _bookingTypeRepository.GetAllBookingTypes(filterSpecification);

                bookingTypes = await _bookingTypeRepository.GetAllBookingTypes(true);

                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    bookingTypes = bookingTypes.Where(s => s.Name.Contains(searchString)
                                             || s.Code.Contains(searchString));
                    //|| s.Model.Name.Contains(searchString)
                    //|| s.Model.Make.Name.Contains(searchString));
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }

                //Sort Order
                switch (sortOrder)
                {
                    case "name_desc":
                        bookingTypes = bookingTypes.OrderByDescending(s => s.Name);
                        break;
                    case "code_desc":
                        bookingTypes = bookingTypes.OrderByDescending(s => s.Code);
                        break;
                    case "code_asc":
                        bookingTypes = bookingTypes.OrderBy(s => s.Code);
                        break;
                    case "id_asc":
                        bookingTypes = bookingTypes.OrderByDescending(s => s.BookingTypeId);
                        break;
                    case "id_desc":
                        bookingTypes = bookingTypes.OrderByDescending(s => s.BookingTypeId);
                        break;
                    default:
                        bookingTypes = bookingTypes.OrderBy(s => s.Name);
                        break;
                }
                //

                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount =await bookingTypes.CountAsync();
                var totalPages = (short)Math.Ceiling((double)totalCount / (short)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetBookingTypes", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetBookingTypes", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";
                
                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

               // bookingTypes = await _bookingTypeRepository.GetAllBookingTypes(true);
                // return result
                var items = await bookingTypes
                    .Skip((short)pageSize * ((short)page - 1))
                    .Take((short)pageSize)
                    .ToListAsync();
                //.Select(eg => _expenseGroupFactory.CreateDataShapedObject(eg, lstOfFields))

                var pagedItems = _mapper.Map<IEnumerable<BookingTypeDto>>(items);
                return Ok(pagedItems);                

            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetItems Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((short)HttpStatusCode.InternalServerError, msg);
            }
        }

        [HttpGet("{id}", Name = "GetBookingTypeById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetBookingTypeById(short id)  // --------------------------------------------------
        {
            try
            {
                _logger.LogInformation("GetBookingTypeById");
                var bookingTypeFromRepo = await _bookingTypeRepository.GetBookingTypeById(id);
                if (bookingTypeFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetBookingTypeById({ID}) NOT FOUND", id);
                    return StatusCode((short)HttpStatusCode.NotFound, "Resource Not Found!-" + id);
                    //return NotFound();
                }
             
               var bookingType = _mapper.Map<BookingTypeDto>(bookingTypeFromRepo);
                return Ok(bookingType);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetById Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((short)HttpStatusCode.InternalServerError, msg);
            }

        }
        
        [HttpPost(Name = "AddBookingType")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddBookingType([FromBody] BookingTypeDto bookingType)  // ---------------------
        {
            try
            {

                if (bookingType == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return StatusCode((short)HttpStatusCode.BadRequest, bookingType);
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return StatusCode((short)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(BookingTypeState);
                }

                var bookingTypeRepo = _mapper.Map<BookingType>(bookingType);
                await _bookingTypeRepository.AddBookingType(bookingTypeRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", bookingTypeRepo.BookingTypeId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a BookingType failed on save.");
                }
                return CreatedAtAction(nameof(GetBookingTypeById), new { id = bookingTypeRepo.BookingTypeId }, bookingTypeRepo);
                //return StatusCode((short)HttpStatusCode.Created);
                // return Content("{ \"message\":\"Form Submitted\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process Create Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((short)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }

        }
        private string BuiltMessage(Exception e, short eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        // PUT api/BookingType/5
        [HttpPut("{id}", Name = "UpdateBookingType")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateBookingType(short id, [FromBody] BookingTypeDto bookingType) // ------------
        {
            try
            {

                if (bookingType == null)
                {
                    _logger.LogError("Object sent from client is null.");
                    return StatusCode((short)HttpStatusCode.BadRequest, bookingType);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid object sent from client-BookingTypeState");
                    return StatusCode((short)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(BookingTypeState);
                }

                var bookingTypeRepo = _mapper.Map<BookingType>(bookingType);
                if (!_bookingTypeRepository.UpdateBookingTypeOk(id, bookingTypeRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((short)HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    //return NotFound();
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a BookingType Template failed on save.");
                }

                return Ok(bookingTypeRepo);
                // return Content("{ \"message\":\"Form Updated\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((short)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }
        }

        // DELETE api/BookingType/
        [HttpDelete("{id}", Name = "DeleteBookingType")]
        public async Task<IActionResult> DeleteBookingType(short id) // -----------------------------------------------------
        {
            try
            {
                var entity = _bookingTypeRepository.GetBookingTypeByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((short)HttpStatusCode.NotFound);
                    //return NotFound();
                }

                _bookingTypeRepository.DeleteBookingType(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a BookingType Template failed on save.");
                }
                return Content("{ \"message\":\"BookingType Deleted\",\"type\":\"success\" }", "application/json");
            }
            catch (Exception e)
            {
                string msg = "Unable to Process Delete Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((short)HttpStatusCode.InternalServerError, msg);
            }

        }

        [HttpGet("GetBookingTypeByName/{name}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetBookingTypeByName(string name) // ---------------------------------------------
        {
            try
            {

                _logger.LogInformation("GetByName");
                var bookingTypeFromRepo = await _bookingTypeRepository.GetBookingTypeByName(name);
                if (bookingTypeFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetByName({name}) NOT FOUND", name);
                    return StatusCode((short)HttpStatusCode.NotFound, "Resource Not Found By Name:-" + name);
                    //return NotFound();
                }
                return Ok(name);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetBookingTypeByName Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((short)HttpStatusCode.InternalServerError, msg);
            }

        }


    }
}
