﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.Domain.Core.Specifications;
using DRS.APA.API.Services;
using DRS.APA.Domain.Core.Repositories;
using Microsoft.AspNetCore.Authorization;
//using System.Data.Entity.Validation;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class DeliveryMethodsController : Controller
    {
        private readonly IDeliveryMethodRepository _deliveryMethodRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const int maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;


        public DeliveryMethodsController(IDeliveryMethodRepository deliveryMethodRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<DeliveryMethodsController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _deliveryMethodRepository = deliveryMethodRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }



        /// <summary>
        /// List all public DeliveryMethods sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [Authorize(Policy = "CanViewDeliveryAndReturnMenu")]
        [HttpGet("{page}/{pageSize}", Name = "GetDeliveryMethods")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetDeliveryMethods(string sortOrder, string searchString, int? page = 1, int? pageSize = maxPageSize)

        //public async Task<IActionResult> GetDeliveryMethods(string sortOrder, int? contactId, int? leadId, string searchString, int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                ResponseObject resObject;

                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");

                    var dataFromRepo = await _deliveryMethodRepository.GetAllDeliveryMethods();
                    var dataList = _mapper.Map<IEnumerable<DeliveryMethodDto>>(dataFromRepo);

                    resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };

                    return Json(resObject);
                }

                // Sorting -------
                HttpContext.Session.SetString("NameSortParam", String.IsNullOrEmpty(sortOrder) ? "name_desc" : "");
                HttpContext.Session.SetString("NameSortParam", String.IsNullOrEmpty(sortOrder) ? "name_asc" : "");

                HttpContext.Session.SetString("EmailSortParam", String.IsNullOrEmpty(sortOrder) ? "email_desc" : "");
                HttpContext.Session.SetString("EmailSortParam", String.IsNullOrEmpty(sortOrder) ? "email_asc" : "");

                HttpContext.Session.SetString("PhoneSortParam", String.IsNullOrEmpty(sortOrder) ? "phone_desc" : "");
                HttpContext.Session.SetString("PhoneSortParam", String.IsNullOrEmpty(sortOrder) ? "phone_asc" : "");

                HttpContext.Session.SetString("ContactSortParam", String.IsNullOrEmpty(sortOrder) ? "contact_desc" : "");
                HttpContext.Session.SetString("ContactSortParam", String.IsNullOrEmpty(sortOrder) ? "contact_asc" : "");

                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_asc" : "");
                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_desc" : "");

                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;
                }

                IQueryable<DeliveryMethod> data = null;

                //var filterSpecification = new DeliveryMethodFilterSpecification(deliveryMethodCategoryId, makeId);

                data = await _deliveryMethodRepository.GetAllDeliveryMethods(true);


                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    data = data.Where(s => s.Name.Contains(searchString)
                                             || s.ThumbPath.Contains(searchString));
              
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "name_desc":
                        data = data.OrderByDescending(s => s.Name);
                        break;
                    case "price_desc":
                        data = data.OrderByDescending(s => s.ThumbPath);
                        break;
                    case "price_asc":
                        data = data.OrderBy(s => s.ThumbPath);
                        break;
                  
                    case "id_asc":
                        data = data.OrderByDescending(s => s.DeliveryMethodId);
                        break;
                    case "id_desc":
                        data = data.OrderByDescending(s => s.DeliveryMethodId);
                        break;
                    default:
                        data = data.OrderBy(s => s.Name);
                        break;
                }//
                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount = await data.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetDeliveryMethods", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetDeliveryMethods", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";

                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));


                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();

                var pagedData = _mapper.Map<IEnumerable<DeliveryMethodDto>>(items);

                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };

                return Json(resObject);

            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetItems Request";

                _logger.LogError(msg);

                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

                return Json(resObject);
            }
        }
       

        /// <summary>
        /// Get Contact Person By Id (with Contact attached)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Policy = "CanViewDeliveryAndReturnMenu")]
        [HttpGet("{id}", Name = "GetDeliveryMethodById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetDeliveryMethodById(short id)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;

                _logger.LogInformation("GetDeliveryMethodById");

                var dataFromRepo = await _deliveryMethodRepository.GetDeliveryMethodById(id);


                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetCollectionById({ID}) NOT FOUND", id);

                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };

                    return Json(resObject);
                }

                var data = _mapper.Map<DeliveryMethodDto>(dataFromRepo);

                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };

                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetById Request";

                _logger.LogError(msg);

                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };

                return Json(resObject);
            }

        }
        /// <summary>
        /// Add New Contact Person - POST api/DeliveryMethod
        /// </summary>
        /// <param name="deliveryMethod"></param>
        /// <returns></returns>
        [Authorize(Policy = "CanCreateDeliveryMethod")]
        [HttpPost(Name = "AddDeliveryMethod")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddDeliveryMethod([FromBody] DeliveryMethodDto deliveryMethod)  // ---------------------
        {
            try
            {
                ResponseObject resObject;

                if (deliveryMethod == null)
                {

                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = (DeliveryMethodDto)null, HttpStatusCode = HttpStatusCode.NotFound };

                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");

                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model!",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var deliveryMethodRepo = _mapper.Map<DeliveryMethod>(deliveryMethod);

                await _deliveryMethodRepository.AddDeliveryMethod(deliveryMethodRepo);

                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", deliveryMethodRepo.DeliveryMethodId);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a DeliveryMethod failed on save.");
                }

                //  var contactFromRepo = await _deliveryMethodRepository.GetDeliveryMethodById(deliveryMethodRepo.DeliveryMethodId);
                var contactFromRepo2 = CreatedAtAction(nameof(GetDeliveryMethodById), new { id = deliveryMethodRepo.DeliveryMethodId }, deliveryMethodRepo);

                var contactFromRepo = contactFromRepo2.Value;

                var data = _mapper.Map<DeliveryMethodDto>(contactFromRepo);

                resObject = new ResponseObject
                {
                    Message = "Submitted Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Sever Error, Unable to Process Create Request";

                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject { 
                    Message = BuiltMessage(e, LoggingEvents.GetItem), 
                    Message2 = msg, Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError 
                };

                return Json(resObject);
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        // PUT api/DeliveryMethod/5
        [Authorize(Policy = "CanUpdateDeliveryMethod")]
        [HttpPut("{id}", Name = "UpdateDeliveryMethod")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateDeliveryMethod(short id, [FromBody] DeliveryMethodDto deliveryMethod) // ------------
        {
            try
            {
                ResponseObject resObject;

                if (deliveryMethod == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = "Data not Found, BadRequest",
                        Status = "error",
                        Data = (DeliveryMethodDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Data");

                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model, BadRequest",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var deliveryMethodRepo = _mapper.Map<DeliveryMethod>(deliveryMethod);

                if (!_deliveryMethodRepository.UpdateDeliveryMethodOk(id, deliveryMethodRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    // return StatusCode((int) HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a DeliveryMethod failed on save.");
                }

                //var contactFromRepo = await _deliveryMethodRepository.GetDeliveryMethodById(deliveryMethodRepo.DeliveryMethodId);
                var contactFromRepo2 = CreatedAtAction(nameof(GetDeliveryMethodById), new { id = deliveryMethodRepo.DeliveryMethodId }, deliveryMethodRepo);

                var contactFromRepo = contactFromRepo2.Value;

                var data = _mapper.Map<DeliveryMethodDto>(contactFromRepo);

                resObject = new ResponseObject

                {
                    Message = "Saved Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }
        }

        /// <summary>
        /// DELETE api/DeliveryMethod/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Policy = "CanDeleteDeliveryMethod")]
        [HttpDelete("{id}", Name = "DeleteDeliveryMethod")]
        public async Task<IActionResult> DeleteDeliveryMethod(short id) // -----------------------------------------------------
        {
            try
            {
                ResponseObject resObject;

                var entity = _deliveryMethodRepository.GetDeliveryMethodByIdAsNoTracking(id);

                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");

                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                _deliveryMethodRepository.DeleteDeliveryMethod(id);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a DeliveryMethod failed on save.");
                }

                resObject = new ResponseObject
                {
                    Message = "Deleted Successfully",
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process Delete Request";
                _logger.LogError(msg);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }
        }
        [Authorize(Policy = "CanViewDeliveryMethod")]
        [HttpGet("GetDeliveryMethodByName/{name}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetDeliveryMethodByName(string name) // ---------------------------------------------
        {
            try
            {
                ResponseObject resObject;

                _logger.LogInformation("GetByName");

                var deliveryMethodFromRepo = await _deliveryMethodRepository.GetDeliveryMethodByName(name);

                var deliveryMethodsList = _mapper.Map<IEnumerable<DeliveryMethodDto>>(deliveryMethodFromRepo);

                if (deliveryMethodFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetByName({name}) NOT FOUND", name);

                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = name,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                resObject = new ResponseObject { Message = "Data Found!", Status = "success", Data = deliveryMethodsList, HttpStatusCode = HttpStatusCode.OK };

                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetDeliveryMethodByName Request";

                _logger.LogError(msg);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }

        }



    }
}



