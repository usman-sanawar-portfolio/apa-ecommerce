﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using FluentDateTime;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Domain.Schedule;
using DRS.APA.Domain.Core.Dtos.Schedule;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.Domain.Core.Specifications;
//using System.Data.Entity.Validation;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class SchedulesController : Controller
    {
        private readonly IScheduleRepository _scheduleRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        private readonly ILogger _logger;

        const int maxPageSize = 20;

        protected readonly LinkGenerator _linkGenerator;

       
        public SchedulesController(IScheduleRepository scheduleRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<SchedulesController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _scheduleRepository = scheduleRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }               

        /// <summary>
        /// List all public Schedules sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetSchedules")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetSchedules(string sortOrder, int? repairCategoryId, short? locationId,string searchString, int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var schedulesFromRepo = await _scheduleRepository.GetAllSchedules();
                    var scheduleslist = _mapper.Map<IEnumerable<ScheduleDto>>(schedulesFromRepo);
                    return Ok(scheduleslist);
                }

                // Sorting -------

                HttpContext.Session.SetString("NameSortParm", String.IsNullOrEmpty(sortOrder) ? "name_desc" : "");
                HttpContext.Session.SetString("NameSortParm", String.IsNullOrEmpty(sortOrder) ? "name_asc" : "");

                HttpContext.Session.SetString("PriceSortParm", String.IsNullOrEmpty(sortOrder) ? "price_desc" : "");
                HttpContext.Session.SetString("PriceSortParm", String.IsNullOrEmpty(sortOrder) ? "price_asc" : "");

                HttpContext.Session.SetString("MakeSortParm", String.IsNullOrEmpty(sortOrder) ? "make_desc" : "");
                HttpContext.Session.SetString("MakeSortParm", String.IsNullOrEmpty(sortOrder) ? "make_asc" : "");

                HttpContext.Session.SetString("IdSortParm", String.IsNullOrEmpty(sortOrder) ? "id_asc" : "");
                HttpContext.Session.SetString("IdSortParm", String.IsNullOrEmpty(sortOrder) ? "id_desc" : "");


                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;

                }
                //Search Items
                IQueryable<Schedule> schedules = null;

                var filterSpecification = new ScheduleFilterSpecification(repairCategoryId, locationId);

                schedules = await _scheduleRepository.GetAllSchedules(filterSpecification);

                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    schedules = schedules.Where(s => s.ScheduleType.Contains(searchString));
                    //|| s.Start.Contains(searchString));
                    //|| s.Model.Name.Contains(searchString)
                    //|| s.Model.Make.Name.Contains(searchString));
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }

                //Sort Order
                switch (sortOrder)
                {
                    case "name_desc":
                        schedules = schedules.OrderByDescending(s => s.Start);
                        break;
                    case "repairCategoryName_desc":
                        schedules = schedules.OrderByDescending(s => s.RepairCategory.Name);
                        break;
                    case "repairCategoryName_asc":
                        schedules = schedules.OrderBy(s => s.RepairCategory.Name);
                        break;
                    case "id_asc":
                        schedules = schedules.OrderByDescending(s => s.ScheduleId);
                        break;
                    case "id_desc":
                        schedules = schedules.OrderByDescending(s => s.ScheduleId);
                        break;
                    default:
                        schedules = schedules.OrderBy(s => s.Start);
                        break;
                }
                //

                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount =await schedules.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetSchedules", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetSchedules", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";
                
                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

               // schedules = await _scheduleRepository.GetAllSchedules(true);
                // return result
                var items = await schedules
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();
                //.Select(eg => _expenseGroupFactory.CreateDataShapedObject(eg, lstOfFields))

                var pagedItems = _mapper.Map<IEnumerable<ScheduleDto>>(items);
                return Ok(pagedItems);                

            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetItems Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }

        [HttpGet("{id}", Name = "GetScheduleById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetScheduleById(int id)  // --------------------------------------------------
        {
            try
            {
                _logger.LogInformation("GetScheduleById");
                var scheduleFromRepo = await _scheduleRepository.GetScheduleById(id);
                if (scheduleFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetScheduleById({ID}) NOT FOUND", id);
                    return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found!-" + id);
                    //return NotFound();
                }
             
               var schedule = _mapper.Map<ScheduleDto>(scheduleFromRepo);
                return Ok(schedule);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetById Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }

        /// <summary>
        /// GetWeeklySchedule (DateTime startDate, bool? next)
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        [HttpGet("weekly-schedule/{startDate}/{next}", Name = "GetWeeklySchedule")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetWeeklySchedule(DateTime startDate,bool next)  // -----------------------------
        {
            try
            {
                _logger.LogInformation("GetWeeklySchedule");

                Schedule scheduleFromRepo = null;

                if (next){
   
                    startDate = startDate.WeekAfter();
                }
                else
                {
                    scheduleFromRepo = await _scheduleRepository.GetWeeklySchedule(startDate);
                }
                
                ICollection<WeeklyScheduleDto> daystoSend = new List<WeeklyScheduleDto>();
               
                if (scheduleFromRepo == null)
                {

                    var schedule =new ScheduleDto();
                    var startofWeek = startDate.StartOfWeek(DayOfWeek.Monday);

                    schedule.Start = startofWeek;
                    var addDays = startofWeek.AddBusinessDays(4);
                    schedule.End = addDays;
                    schedule.ScheduleType = "Weekly";
                    schedule.SlotDuration = 2;
                    schedule.ResourcePerSlot = 1;
                    schedule.IsActive = true;
                    schedule.LocationId = 1;

                    scheduleFromRepo = _mapper.Map<Schedule>(schedule);
                    
                    await _scheduleRepository.AddSchedule(scheduleFromRepo);
                    if (!await _unitOfWork.CompleteAsync())
                    {
                        throw new Exception("Adding a Schedule failed on save.");
                    }

                }

                var totalDays = scheduleFromRepo.Duration();

                DateTime today = DateTime.Today;
                
                foreach (DateTime day in Utilities.EachDay(scheduleFromRepo.Start, scheduleFromRepo.End))
                {
                    if (day.Date >= today)
                    {
                        WeeklyScheduleDto item = new WeeklyScheduleDto();
                        item.Day = day.Date;
                        item.ScheduleId = scheduleFromRepo.ScheduleId;
                        item.FullDate = day.Date.ToString("yyyy-MM-dd ddd");
                        item.IsActive = true;
                        daystoSend.Add(item);
                    }
                    
                }

                return Ok(daystoSend);

            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetById Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }
        }

        //https://stackoverflow.com/questions/51211646/the-input-was-not-valid-net-core-web-api
        // POST api/Schedule
        [HttpPost(Name = "AddSchedule")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddSchedule([FromBody] ScheduleDto schedule)  // ---------------------
        {
            try
            {

                if (schedule == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, schedule);
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(ScheduleState);
                }

                var scheduleRepo = _mapper.Map<Schedule>(schedule);
                await _scheduleRepository.AddSchedule(scheduleRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", scheduleRepo.ScheduleId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a Schedule failed on save.");
                }
                return CreatedAtAction(nameof(GetScheduleById), new { id = scheduleRepo.ScheduleId }, scheduleRepo);
                //return StatusCode((int)HttpStatusCode.Created);
                // return Content("{ \"message\":\"Form Submitted\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process Create Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        // PUT api/Schedule/5
        [HttpPut("{id}", Name = "UpdateSchedule")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateSchedule(int id, [FromBody] ScheduleDto schedule) // ------------
        {
            try
            {

                if (schedule == null)
                {
                    _logger.LogError("Object sent from client is null.");
                    return StatusCode((int)HttpStatusCode.BadRequest, schedule);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid object sent from client-ScheduleState");
                    return StatusCode((int)HttpStatusCode.BadRequest, ModelState);
                    //return BadRequest(ScheduleState);
                }

                var scheduleRepo = _mapper.Map<Schedule>(schedule);
                if (!_scheduleRepository.UpdateScheduleOk(id, scheduleRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    //return NotFound();
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a Schedule Template failed on save.");
                }

                return Ok(scheduleRepo);
                // return Content("{ \"message\":\"Form Updated\",\"type\":\"success\" }", "application/json");
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
                //return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");
            }
        }

        // DELETE api/Schedule/
        [HttpDelete("{id}", Name = "DeleteSchedule")]
        public async Task<IActionResult> DeleteSchedule(int id) // -----------------------------------------------------
        {
            try
            {
                var entity = _scheduleRepository.GetScheduleByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    return StatusCode((int)HttpStatusCode.NotFound);
                    //return NotFound();
                }

                _scheduleRepository.DeleteSchedule(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a Schedule Template failed on save.");
                }
                return Content("{ \"message\":\"Schedule Deleted\",\"type\":\"success\" }", "application/json");
            }
            catch (Exception e)
            {
                string msg = "Unable to Process Delete Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogError(msg);
                return StatusCode((int)HttpStatusCode.InternalServerError, msg);
            }

        }

        //[HttpGet("GetScheduleByName/{name}")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //public async Task<IActionResult> GetScheduleByName(string name) // ---------------------------------------------
        //{
        //    try
        //    {

        //        _logger.LogInformation("GetByName");
        //        var scheduleFromRepo = await _scheduleRepository.GetScheduleByTitle(name);
        //        if (scheduleFromRepo == null)
        //        {
        //            _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetByName({name}) NOT FOUND", name);
        //            return StatusCode((int)HttpStatusCode.NotFound, "Resource Not Found By Name:-" + name);
        //            //return NotFound();
        //        }
        //        return Ok(name);
        //    }
        //    catch (Exception e)
        //    {
        //        string msg = "Unable to Process GetScheduleByName Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
        //        _logger.LogError(msg);
        //        return StatusCode((int)HttpStatusCode.InternalServerError, msg);
        //    }

        //}


    }
}
