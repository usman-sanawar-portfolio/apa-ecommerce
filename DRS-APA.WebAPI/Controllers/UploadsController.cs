﻿using System;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using DRS.APA.API.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ServiceManagement.API.Controllers
{
    [Produces("application/json")]
    [Route("api/hrm/companies/{companyId}/[controller]")]
    public class UploadsController : Controller
    {
        [HttpPost(Name = "Upload")]
        public async Task<IActionResult> Upload([FromForm]  MyModelWrapper model)
        {
            try
            {
                //-----------------
                var newFileName = string.Empty;

                if (HttpContext.Request.Form.Files != null)
                {
                    var fileName = string.Empty;
                    string PathDB = string.Empty;

                    var files = HttpContext.Request.Form.Files;

                    foreach (var file in files)
                    {
                        if (file.Length > 0)
                        {
                            //Getting FileName
                            fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

                            //Assigning Unique Filename (Guid)
                            //var myUniqueFileName = Convert.ToString(Guid.NewGuid());

                            //Getting file Extension
                            var FileExtension = Path.GetExtension(fileName);

                            // concating  FileName + FileExtension
                            //newFileName = myUniqueFileName + FileExtension;
                            // Combines two strings into a path.

                            //if (string.IsNullOrWhiteSpace(_environment.WebRootPath))
                            // {
                            string paths = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot", "Uploads");
                            // }

                            // Combines two strings into a path.

                            // fileName = Path.Combine(_environment.WebRootPath, "Uploads") + $@"\{newFileName}";
                            fileName = paths + $@"\{fileName}";
                            // if you want to store path of folder in database
                            PathDB = "Uploads/" + newFileName;

                            using (FileStream fs = System.IO.File.Create(fileName))
                            {
                                await file.CopyToAsync(fs);
                                fs.Flush();
                            }
                        }
                    }


                }

                //----------------
                return Content("{ \"message\":\"Form Submitted\",\"type\":\"success\" }", "application/json");
            }
            catch (DbEntityValidationException ex)
            {

                foreach (var entityValidationError in ex.EntityValidationErrors)
                {
                    foreach (var validationError in entityValidationError.ValidationErrors)
                    {
                        return Content("{ \"message\":\"" + validationError.PropertyName + " Error :" + validationError.ErrorMessage + "\",\"type\":\"error\" }", "application/json");
                    }
                }


            }
            catch (Exception e)
            {

                if (e.InnerException != null)
                    if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                    {
                        if (e.InnerException.InnerException != null)
                            if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                            {
                                return Content("{ \"message\":\"" + e.InnerException.InnerException.Message + "\",\"type\":\"error\" }", "application/json");
                            }
                    }
                    else
                    {
                        return Content("{ \"message\":\"" + e.InnerException.Message + "\",\"type\":\"error\" }", "application/json");
                    }
                else
                {
                    return Content("{ \"message\":\"" + e.Message + "\",\"type\":\"error\" }", "application/json");
                }

                return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");

            }
            return Content("{ \"message\":\"Exception Error\",\"type\":\"error\" }", "application/json");

            //return Content("{ \"message\":\"Organization Deleted\",\"type\":\"success\" }", "application/json");
        }
    }
}