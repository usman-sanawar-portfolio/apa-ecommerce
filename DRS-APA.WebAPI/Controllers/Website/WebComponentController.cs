﻿
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
//using System.Data.Entity.Validation;
using System.Threading.Tasks;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Repositories;
using Microsoft.AspNetCore.Http;
using DRS.APA.API.Helpers;
using Microsoft.Extensions.Logging;
using DRS.APA.Domain.Core;
using System.Net;
using System.Linq;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using DRS.APA.Microserivce.Domain.Core.Repositories.Website;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Domain.Core.Repositories.Website;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers
{
    [Route("api/DRS.APA/website/[controller]")]
    [SwaggerGroup("Website")]
    public class WebComponentController : Controller
    {
        private readonly IWebComponentRepository _webComponentRepository;
        private readonly IWebCardRepository _webCardRepository;
        private readonly IWebDocRepository _webDocRepository;
        private readonly IWebPhotoRepository _webPhotoRepository;
        private readonly IWebHtmlRepository _webHtmlRepository;
        private readonly IWebVideoRepository _webVideoRepository;

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        const int maxPageSize = 20;
        protected readonly LinkGenerator _linkGenerator;

        public WebComponentController(
            IWebComponentRepository webComponentRepository,
            IWebCardRepository webCardRepository,
            IWebDocRepository webDocRepository,
            IWebPhotoRepository webPhotoRepository,
            IWebHtmlRepository webHtmlRepository,
            IWebVideoRepository webVideoRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            ILogger<WebComponentController> logger,
            LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _webComponentRepository = webComponentRepository;
            _webCardRepository = webCardRepository;
            _webDocRepository = webDocRepository;
            _webPhotoRepository = webPhotoRepository;
            _webHtmlRepository = webHtmlRepository;
            _webVideoRepository = webVideoRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }


        /// <summary>
        /// List all public WebComponent sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetWebComponent")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebComponent(string sortOrder, string searchString, int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                ResponseObject resObject;
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var dataFromRepo = await _webComponentRepository.GetAllWebComponents();
                    var dataList = _mapper.Map<IEnumerable<WebComponentDto>>(dataFromRepo);
                    resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                    return Json(resObject);
                }

                // Sorting -------
                HttpContext.Session.SetString("TitleSortParam", String.IsNullOrEmpty(sortOrder) ? "title_desc" : "");
                HttpContext.Session.SetString("TitleSortParam", String.IsNullOrEmpty(sortOrder) ? "title_desc" : "");

                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_asc" : "");
                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_desc" : "");

                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;
                }

                IQueryable<WebComponent> data = null;
                data = await _webComponentRepository.GetAllWebComponents(true);
                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    data = data.Where(s => s.Title.Contains(searchString)
                                             || s.Type.Contains(searchString)
                    );

                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "title_desc":
                        data = data.OrderByDescending(s => s.Title);
                        break;

                    case "id_asc":
                        data = data.OrderByDescending(s => s.WebComponentId);
                        break;
                    case "id_desc":
                        data = data.OrderByDescending(s => s.WebComponentId);
                        break;
                    default:
                        data = data.OrderBy(s => s.Title);
                        break;
                }
                //
                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount = await data.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetWebComponent", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetWebComponent", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";

                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();

                var pagedData = _mapper.Map<IEnumerable<WebComponentDto>>(items);
                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetItems Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }


        /// <summary>
        /// Get Web Component By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetWebComponentById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebComponentById(short id)
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("GetWebComponentById");
                var dataFromRepo = await _webComponentRepository.GetWebComponentById(id);

                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebComponentById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                var data = _mapper.Map<WebComponentDto>(dataFromRepo);
                if (dataFromRepo.Type == "card")
                {
                    var dataFromCard = await _webCardRepository.GetWebCardByComponentId(dataFromRepo.WebComponentId);
                    if (dataFromCard != null)
                    {
                        data.WebCards = _mapper.Map<ICollection<WebCardDto>>(dataFromCard);
                    }
                }
                else if (dataFromRepo.Type == "doc")
                {
                    var dataFromDoc = await _webDocRepository.GetWebDocByComponentId(dataFromRepo.WebComponentId);
                    if (dataFromDoc != null)
                    {
                        data.WebDocs = _mapper.Map<ICollection<WebDocDto>>(dataFromDoc);
                    }
                }
                else if (dataFromRepo.Type == "html")
                {
                    var dataFromHtml = await _webHtmlRepository.GetWebHtmlByComponentId(dataFromRepo.WebComponentId);
                    if (dataFromHtml != null)
                    {
                        data.WebHtmls = _mapper.Map<ICollection<WebHtmlDto>>(dataFromHtml);
                    }
                }
                else if (dataFromRepo.Type == "photo")
                {
                    var dataFromPhoto = await _webPhotoRepository.GetWebPhotoByComponentId(dataFromRepo.WebComponentId);
                    if (dataFromPhoto != null)
                    {
                        data.WebPhotos = _mapper.Map<ICollection<WebPhotoDto>>(dataFromPhoto);
                    }
                }
                else if (dataFromRepo.Type == "video")
                {
                    var dataFromVideo = await _webVideoRepository.GetWebVideoByComponentId(dataFromRepo.WebComponentId);
                    if (dataFromVideo != null)
                    {
                        data.WebVideos = _mapper.Map<ICollection<WebVideoDto>>(dataFromVideo);
                    }
                }

                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetById Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }
        /// <summary>
        /// AddWebComponent - POST api/WebComponent
        /// </summary>
        /// <param name="webComponent"></param>
        /// <returns></returns>

        [HttpPost(Name = "AddWebComponent")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddWebComponent([FromBody] WebComponentDto webComponent)
        {
            try
            {
                ResponseObject resObject;
                if (webComponent == null)
                {
                    _logger.LogError("WebComponent object sent from client is null.");
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = (WebComponentDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid webComponent object sent from client.");
                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model!",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var webComponentRepo = _mapper.Map<WebComponent>(webComponent);

                if (webComponent.Type == "card")
                {
                    webComponentRepo.WebDocs = null;
                    webComponentRepo.WebHtmls = null;
                    webComponentRepo.WebPhotos = null;
                    webComponentRepo.WebVideos = null;
                }
                else if (webComponent.Type == "docs")
                {
                    webComponentRepo.WebCards = null;
                    webComponentRepo.WebHtmls = null;
                    webComponentRepo.WebPhotos = null;
                    webComponentRepo.WebVideos = null;
                }
                else if (webComponent.Type == "html")
                {
                    webComponentRepo.WebCards = null;
                    webComponentRepo.WebDocs = null;
                    webComponentRepo.WebPhotos = null;
                    webComponentRepo.WebVideos = null;
                }
                else if (webComponent.Type == "video")
                {
                    webComponentRepo.WebCards = null;
                    webComponentRepo.WebDocs = null;
                    webComponentRepo.WebPhotos = null;
                    webComponentRepo.WebHtmls = null;
                }
                else if (webComponent.Type == "photo")
                {
                    webComponentRepo.WebCards = null;
                    webComponentRepo.WebDocs = null;
                    webComponentRepo.WebVideos = null;
                    webComponentRepo.WebHtmls = null;
                }

                await _webComponentRepository.AddWebComponent(webComponentRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "WebComponent {ID} Created", webComponentRepo.WebComponentId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a WebComponent failed on save.");
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetWebComponentById), new { id = webComponentRepo.WebComponentId }, webComponentRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebComponentDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = "Submitted Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Sever Error, Unable to Process Create Request";
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject { Message = BuiltMessage(e, LoggingEvents.GetItem), Message2 = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
        // PUT api/WebComponent/5
        [HttpPut("{id}", Name = "UpdateWebComponent")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateWebComponent(short id, [FromBody] WebComponentDto webComponent)
        {
            try
            {
                ResponseObject resObject;
                if (webComponent == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = "Data not Found, BadRequest",
                        Status = "error",
                        Data = (WebComponentDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Data");

                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model, BadRequest",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var webComponentRepo = _mapper.Map<WebComponent>(webComponent);
                webComponentRepo.WebDocs = null;
                webComponentRepo.WebCards = null;
                webComponentRepo.WebHtmls = null;
                webComponentRepo.WebPhotos = null;
                webComponentRepo.WebVideos = null;

                if (!_webComponentRepository.UpdateWebComponentOk(id, webComponentRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    // return StatusCode((int) HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a WebComponent failed on save.");
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetWebComponentById), new { id = webComponentRepo.WebComponentId }, webComponentRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebComponentDto>(contactFromRepo);
                resObject = new ResponseObject

                {
                    Message = "Saved Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }
        }

        /// <summary>
        /// DELETE api/WebComponent/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}", Name = "DeleteWebComponent")]
        public async Task<IActionResult> DeleteWebComponent(short id)
        {
            try
            {
                ResponseObject resObject;
                var entity = _webComponentRepository.GetWebComponentByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                _webComponentRepository.DeleteWebComponent(id);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a WebComponent failed on save.");
                }
                resObject = new ResponseObject
                {
                    Message = "Deleted Successfully",
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process Delete Request";
                _logger.LogError(msg);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }
        }

        //[HttpGet("GetWebComponentByName/{name}")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //public async Task<IActionResult> GetWebComponentByName(string name) // ---------------------------------------------
        //{
        //    try
        //    {
        //        ResponseObject resObject;

        //        _logger.LogInformation("GetByName");

        //        var webComponentFromRepo = await _webComponentRepository.GetWebComponentByName(name);

        //        var WebComponentList = _mapper.Map<IEnumerable<WebComponentDto>>(webComponentFromRepo);

        //        if (webComponentFromRepo == null)
        //        {
        //            _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetByName({name}) NOT FOUND", name);

        //            resObject = new ResponseObject
        //            {
        //                Message = "Data not found, BadRequest",
        //                Status = "error",
        //                Data = name,
        //                HttpStatusCode = HttpStatusCode.BadRequest
        //            };

        //            return Json(resObject);
        //        }

        //        resObject = new ResponseObject { Message = "Data Found!", Status = "success", Data = WebComponentList, HttpStatusCode = HttpStatusCode.OK };

        //        return Json(resObject);
        //    }
        //    catch (Exception e)
        //    {
        //        string msg = "Unable to Process GetWebComponentByName Request";

        //        _logger.LogError(msg);

        //        var resObject = new ResponseObject
        //        {
        //            Message2 = BuiltMessage(e, LoggingEvents.GetItem),
        //            Message = msg,
        //            Status = "error",
        //            HttpStatusCode = HttpStatusCode.InternalServerError
        //        };

        //        return Json(resObject);
        //    }

        //}
    }
}
