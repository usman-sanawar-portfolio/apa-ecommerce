
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
//using System.Data.Entity.Validation;
using System.Threading.Tasks;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Repositories;
using Microsoft.AspNetCore.Http;
using DRS.APA.API.Helpers;
using Microsoft.Extensions.Logging;
using DRS.APA.Domain.Core;
using System.Net;
using System.Linq;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using Microsoft.Data.SqlClient;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website;
using DRS.APA.Domain.Core.Repositories.Website;
using Microsoft.AspNetCore.Authorization;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers
{
    [Route("api/DRS.APA/website/[controller]")]
    [SwaggerGroup("Website")]
    public class WebPageController : Controller
    {
        private readonly IWebPageRepository _WebPageRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        const int maxPageSize = 20;
        protected readonly LinkGenerator _linkGenerator;

        public WebPageController(
            IWebPageRepository WebPageRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            ILogger<WebPageController> logger,
            LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _WebPageRepository = WebPageRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }


    /// <summary>
    /// List all public WebPage sorted by Id.
    /// </summary>
    /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
    [Authorize(Policy = "CanViewWebPage")]
    [HttpGet("{page}/{pageSize}", Name = "GetWebPage")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebPage(string sortOrder, string searchString, int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                ResponseObject resObject;

                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");

                    var dataFromRepo = await _WebPageRepository.GetAllWebPages(false);
                    var dataList = _mapper.Map<IEnumerable<WebPageDto>>(dataFromRepo);

                    resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };

                    return Json(resObject);
                }

                // Sorting -------
                HttpContext.Session.SetString("TitleSortParam", String.IsNullOrEmpty(sortOrder) ? "title_desc" : "");
                HttpContext.Session.SetString("TitleSortParam", String.IsNullOrEmpty(sortOrder) ? "title_asc" : "");

                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_asc" : "");
                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_desc" : "");

                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;
                }
                IQueryable<WebPage> data = null;
                //var filterSpecification = new WebPageFilterSpecification(contactId, leadId);
                data = await _WebPageRepository.GetAllWebPages(true);

                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    data = data.Where(s => s.WebPageTitle.Contains(searchString) || s.Slug.Contains(searchString));
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "Title_desc":
                        data = data.OrderByDescending(s => s.WebPageTitle);
                        break;

                    case "id_asc":
                        data = data.OrderByDescending(s => s.WebPageId);
                        break;
                    case "id_desc":
                        data = data.OrderByDescending(s => s.WebPageId);
                        break;
                    default:
                        data = data.OrderBy(s => s.WebPageTitle);
                        break;
                }
                //
                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount = await data.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetWebPages", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetWebPages", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";

                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();

                var pagedData = _mapper.Map<IEnumerable<WebPageDto>>(items);
                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetItems Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }


    /// <summary>
    /// Get WebPage By Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanViewWebPage")]
    [HttpGet("{id}", Name = "GetWebPageById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebPageById(short id)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("GetWebPageById");
                var dataFromRepo = await _WebPageRepository.GetWebPageById(id);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebPageById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                var data = _mapper.Map<WebPageDto>(dataFromRepo);
                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetWebPageById Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }
    /// <summary>
    /// AddWebPage - POST api/WebPage
    /// </summary>
    /// <param name="WebPage"></param>
    /// <returns></returns>

    [Authorize(Policy = "CanCreateWebPage")]
    [HttpPost(Name = "AddWebPage")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddWebPage([FromBody] WebPageDto WebPage)  // ---------------------
        {
            try
            {
                ResponseObject resObject;
                if (WebPage == null)
                {
                    _logger.LogError("WebPage object sent from client is null.");
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = (WebPageDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid WebPage object sent from client.");
                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model!",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                var WebPageRepo = _mapper.Map<WebPage>(WebPage);
                await _WebPageRepository.AddWebPage(WebPageRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "Item {ID} Created", WebPageRepo.WebPageId);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a WebPage failed on save.");
                }
                var contactFromRepo2 = CreatedAtAction(nameof(GetWebPageById), new { id = WebPageRepo.WebPageId }, WebPageRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebPageDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = "Submitted Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Sever Error, Unable to Process Create Request";
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject { Message = BuiltMessage(e, LoggingEvents.GetItem), Message2 = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {

            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    //Changes-Babar:
                    if (e.InnerException is SqlException ex)
                    {
                        if (ex.Number == 2627 || ex.Number == 2601 || ex.Message.ToLower().Contains("duplicate key"))
                        {
                            return "Unable to process, duplicate data.";
                        }
                    }
                    //Changes-Babar:

                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }
                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }

    // PUT api/WebPage/5
    [Authorize(Policy = "CanUpdateWebPage")]
    [HttpPut("{id}", Name = "UpdateWebPage")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateWebPage(short id, [FromBody] WebPageDto WebPage) // ------------
        {
            try
            {
                ResponseObject resObject;
                if (WebPage == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = "Data not Found, BadRequest",
                        Status = "error",
                        Data = (WebPageDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Data");
                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model, BadRequest",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                var WebPageRepo = _mapper.Map<WebPage>(WebPage);
                if (!_WebPageRepository.UpdateWebPageOk(id, WebPageRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    // return StatusCode((int) HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a WebPage failed on save.");
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetWebPageById), new { id = WebPageRepo.WebPageId }, WebPageRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebPageDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = "Saved Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

    /// <summary>
    /// DELETE api/WebPage/5
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanDeleteWebPage")]
    [HttpDelete("{id}", Name = "DeleteWebPage")]
        public async Task<IActionResult> DeleteWebPage(short id) // -----------------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                var entity = _WebPageRepository.GetWebPageByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                _WebPageRepository.DeleteWebPage(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a WebPage failed on save.");
                }
                resObject = new ResponseObject
                {
                    Message = "Deleted Successfully",
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process Delete Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

    [Authorize(Policy = "CanViewWebPage")]
    [HttpGet("GetWebPageByTitle/{WebPageTitle}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> IsWebPageTitleExisting(string WebPageTitle) // ---------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("IsWebPageTitleExisting");
                var WebPageFromRepo = await _WebPageRepository.IsWebPageTitleExisting(WebPageTitle) ? true : false;
                resObject = new ResponseObject { Message = WebPageFromRepo == true ? "Data Found" : "Not Found", Status = WebPageFromRepo == false ? "success" : "error", Data = WebPageFromRepo, HttpStatusCode = WebPageFromRepo == true ? HttpStatusCode.OK : HttpStatusCode.NotFound };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process GetWebPageByTitle Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

    [Authorize(Policy = "CanViewWebPage")]
    [HttpGet("GetWebPageBySlug/{Slug}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> IsSlugExisting(string Slug) // ---------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("IsSlugExisting");
                var WebPageFromRepo = await _WebPageRepository.IsSlugExisting(Slug) ? true : false;
                resObject = new ResponseObject { Message = WebPageFromRepo == true ? "Data Found" : "Not Found", Status = "success", Data = WebPageFromRepo, HttpStatusCode = WebPageFromRepo == true ? HttpStatusCode.OK : HttpStatusCode.NotFound };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process Slug Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }
    }
}
