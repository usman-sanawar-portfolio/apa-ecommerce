using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using DRS.APA.Microservice.API.Controllers.Common;

using DRS.APA.Domain.Core.Repositories.Website;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using Microsoft.AspNetCore.Authorization;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/website/[controller]")]
    [SwaggerGroup("Website")]
    public class WebCarousalPhotosController : Controller
    {
        private readonly IWebCarousalPhotosRepository _WebCarousalPhotosRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        protected readonly LinkGenerator _linkGenerator;

        public WebCarousalPhotosController(IWebCarousalPhotosRepository WebCarousalPhotosRepository, IReviewRepository reviewRepository,
        IUnitOfWork unitOfWork, IMapper mapper, ILogger<WebCarousalPhotosController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _WebCarousalPhotosRepository = WebCarousalPhotosRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }

    /// <summary>
    /// List all WebCarousalPhotos
    /// </summary>
    /// <remarks>Without server side pagging/sorting.</remarks>
    [Authorize(Policy = "CanViewWebCarousalPhoto")]
    [HttpGet(Name = "GetWebCarousalPhotos")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebCarousalPhotos()
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("Generating List");
                var dataFromRepo = await _WebCarousalPhotosRepository.GetAllWebCarousalPhotos();
                var dataList = _mapper.Map<IEnumerable<WebCarousalPhotosDto>>(dataFromRepo);
                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);

            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetWebCarousalPhotos");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

    /// <summary>
    /// Get WebCarousalPhotos By Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanViewWebCarousalPhoto")]
    [HttpGet("{id}", Name = "GetWebCarousalPhotosById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebCarousalPhotosById(int id)
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("GetWebCarousalPhotosById");
                var dataFromRepo = await _WebCarousalPhotosRepository.GetWebCarousalPhotosById(id);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebCarousalPhotosById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                var data = _mapper.Map<WebCarousalPhotosDto>(dataFromRepo);
                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetWebCarousalPhotosById");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

    /// <summary>
    /// Add WebCarousalPhotos - POST api/WebCarousalPhotos
    /// </summary>
    /// <param name="WebCarousalPhotos"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanCreateWebCarousalPhoto")]
    [HttpPost(Name = "AddWebCarousalPhotos")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddWebCarousalPhotos([FromBody] WebCarousalPhotosDto WebCarousalPhotos)
        {
            try
            {
                ResponseObject resObject;
                if (WebCarousalPhotos == null)
                {
                    _logger.LogError(ConstantProps.NullObjectError("WebCarousalPhotos"));
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (WebCarousalPhotosDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidObjectError("WebCarousalPhotos"));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var WebCarousalPhotosRepo = _mapper.Map<WebCarousalPhotos>(WebCarousalPhotos);
                await _WebCarousalPhotosRepository.AddWebCarousalPhotos(WebCarousalPhotosRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "WebCarousalPhotos {ID} Created", WebCarousalPhotosRepo.WebCarousalPhotosId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Adding", "WebCarousalPhotos"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetWebCarousalPhotosById), new { id = WebCarousalPhotosRepo.WebCarousalPhotosId }, WebCarousalPhotosRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebCarousalPhotosDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SubmittedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Create");
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject { Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

    // PUT api/WebCarousalPhotos/5
    [Authorize(Policy = "CanUpdateWebCarousalPhoto")]
    [HttpPut("{id}", Name = "UpdateWebCarousalPhotos")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateWebCarousalPhotos(int id, [FromBody] WebCarousalPhotosDto WebCarousalPhotos)
        {
            try
            {
                ResponseObject resObject;
                if (WebCarousalPhotos == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = (WebCarousalPhotosDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidDataModelText);
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var WebCarousalPhotosRepo = _mapper.Map<WebCarousalPhotos>(WebCarousalPhotos);
                if (!_WebCarousalPhotosRepository.UpdateWebCarousalPhotosOk(id, WebCarousalPhotosRepo))
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Updating", "WebCarousalPhotos"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetWebCarousalPhotosById), new { id = WebCarousalPhotosRepo.WebCarousalPhotosId }, WebCarousalPhotosRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebCarousalPhotosDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SavedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

    /// <summary>
    /// DELETE api/WebCarousalPhotos/5
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanDeleteWebCarousalPhoto")]
    [HttpDelete("{id}", Name = "DeleteWebCarousalPhotos")]
        public async Task<IActionResult> DeleteWebCarousalPhotos(int id)
        {
            try
            {
                ResponseObject resObject;
                var entity = _WebCarousalPhotosRepository.GetWebCarousalPhotosByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                _WebCarousalPhotosRepository.DeleteWebCarousalPhotos(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Deleting", "WebCarousalPhotos"));
                }
                resObject = new ResponseObject
                {
                    Message = ConstantProps.DeletedSuccessfullyText,
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Delete");
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }
        
    }
}
