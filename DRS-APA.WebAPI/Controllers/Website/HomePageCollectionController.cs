using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.Domain.Core.Specifications;
using DRS.APA.API.Services;
using DRS.APA.Microservice.API.Controllers.Common;
using DRS.APA.Microserivce.Domain.Core.Dtos.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website;
using DRS.APA.Domain.Core.Repositories.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using AutoMapper.Configuration;
using Microsoft.AspNetCore.Authorization;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("Website")]
    public class HomePageCollectionController : Controller
    {
        private readonly IHomePageCollectionRepository _HomePageCollectionRepository;
        private readonly IReviewRepository _reviewRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        protected readonly LinkGenerator _linkGenerator;
        public HomePageCollectionController(IHomePageCollectionRepository HomePageCollectionRepository, IReviewRepository reviewRepository,
        IUnitOfWork unitOfWork, IMapper mapper, ILogger<HomePageCollectionController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _HomePageCollectionRepository = HomePageCollectionRepository;
            _reviewRepository = reviewRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }

    /// <summary>
    /// List all HomePageCollection
    /// </summary>
    /// <remarks></remarks>
     [Authorize(Policy = "CanViewStoreHomepage")]
  [HttpGet("{page}/{pageSize}", Name = "GetHomePageCollection")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetHomePageCollection()
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("Generating List");
                var dataFromRepo = await _HomePageCollectionRepository.GetAllHomePageCollection();
                var dataList = _mapper.Map<IEnumerable<HomePageCollectionDto>>(dataFromRepo);
                List<ProductCategoriesJunctionForHomeDto> ProductCategoriesJunctionForHomeDtoList;
                foreach (var v in dataList)
                {
                    ProductCategoriesJunctionForHomeDtoList = new List<ProductCategoriesJunctionForHomeDto>();
                    int p = 1;
                    foreach (var i in v.ProductCategoriesJunctionForHome)
                    {
                        if (p > ConstantProps.DefaultHomePageProducts)
                            break;
                        if (i.Product != null)
                        {
                            if (i.Product.IsDeleted == false)
                            {
                                ProductCategoriesJunctionForHomeDtoList.Add(i);
                                p++;
                            }
                        }
                        if (i.Product != null)
                        {
                            var TotalRating = await _reviewRepository.GetTotalReviewRatingByProductId(i.Product.ProductId);
                            var TotalCount = await _reviewRepository.GetTotalReviewCountByProductId(i.Product.ProductId);
                            i.TotalRating = TotalRating;
                            i.TotalReviews = TotalCount;
                        }
                      
                    }
                    if (ProductCategoriesJunctionForHomeDtoList.Count > 0)
                    {
                        v.ProductCategoriesJunctionForHome = ProductCategoriesJunctionForHomeDtoList;
                    }
                    else
                    {
                        v.ProductCategoriesJunctionForHome = null;
                    }
                }

                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetHomePageCollection");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }
    /// <summary>
    /// Get HomePageCollection By Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanViewStoreHomepage")]
    [HttpGet("{id}", Name = "GetHomePageCollectionById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetHomePageCollectionById(short id)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("GetHomePageCollectionById");
                var dataFromRepo = await _HomePageCollectionRepository.GetHomePageCollectionById(id);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetHomePageCollectionById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                var data = _mapper.Map<HomePageCollectionDto>(dataFromRepo);
                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetHomePageCollectionById");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }

    /// <summary>
    /// Add HomePageCollection - POST api/HomePageCollection
    /// </summary>
    /// <param name="HomePageCollection"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanCreateStoreHomepage")]
    [HttpPost(Name = "AddHomePageCollection")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddHomePageCollection([FromBody] HomePageCollectionDto HomePageCollection)
        {
            try
            {
                ResponseObject resObject;
                if (HomePageCollection == null)
                {
                    _logger.LogError(ConstantProps.NullObjectError("HomePageCollection"));
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (HomePageCollectionDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidObjectError("HomePageCollection"));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var HomePageCollectionRepo = _mapper.Map<HomePageCollection>(HomePageCollection);
                await _HomePageCollectionRepository.AddHomePageCollection(HomePageCollectionRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "HomePageCollection {ID} Created", HomePageCollectionRepo.HomePageCollectionId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Adding", "HomePageCollection"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetHomePageCollectionById), new { id = HomePageCollectionRepo.HomePageCollectionId }, HomePageCollectionRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<HomePageCollectionDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SubmittedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Create");
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject { Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message2 = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

    // PUT api/HomePageCollection/5
    [Authorize(Policy = "CanUpdateStoreHomepage")]
    [HttpPut("{id}", Name = "UpdateHomePageCollection")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateHomePageCollection(short id, [FromBody] HomePageCollectionDto HomePageCollection)
        {
            try
            {
                ResponseObject resObject;
                if (HomePageCollection == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = (HomePageCollectionDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidDataModelText);
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var HomePageCollectionRepo = _mapper.Map<HomePageCollection>(HomePageCollection);
                if (!_HomePageCollectionRepository.UpdateHomePageCollectionOk(id, HomePageCollectionRepo))
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Updating", "HomePageCollection"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetHomePageCollectionById), new { id = HomePageCollectionRepo.HomePageCollectionId }, HomePageCollectionRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<HomePageCollectionDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SavedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

    /// <summary>
    /// DELETE api/HomePageCollection/5
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanDeleteStoreHomepage")]
    [HttpDelete("{id}", Name = "DeleteHomePageCollection")]
        public async Task<IActionResult> DeleteHomePageCollection(short id)
        {
            try
            {
                ResponseObject resObject;
                var entity = _HomePageCollectionRepository.GetHomePageCollectionByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                _HomePageCollectionRepository.DeleteHomePageCollection(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Deleting", "HomePageCollection"));
                }
                resObject = new ResponseObject
                {
                    Message = ConstantProps.DeletedSuccessfullyText,
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Delete");
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }
    }
}
