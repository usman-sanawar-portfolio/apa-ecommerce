using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using DRS.APA.Microservice.API.Controllers.Common; 
using DRS.APA.Domain.Core.Repositories.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website;
using Microsoft.VisualStudio.Services.Organization.Client;
using Microsoft.AspNetCore.Authorization;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.Website
{
  [Route("api/DRS.APA/website/[controller]")]
  [SwaggerGroup("website")]
  public class BlogCategoryController : Controller
  {
    private readonly IBlogCategoryRepository _blogCategoryRepository;
     private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly ILogger _logger;
    protected readonly LinkGenerator _linkGenerator;

    public BlogCategoryController(IBlogCategoryRepository blogCategoryRepository, 
    IUnitOfWork unitOfWork, IMapper mapper, ILogger<BlogCategoryController> logger, LinkGenerator linkGenerator)
    {
      _unitOfWork = unitOfWork;
      _blogCategoryRepository = blogCategoryRepository;
       _mapper = mapper;
      _logger = logger;
      _linkGenerator = linkGenerator;
    }

    /// <summary>
    /// List all BlogCategory, by default sorted by CategoryName
    /// </summary>
    /// <remarks>With pagination, you can fetch up to x records per page.</remarks>
        [Authorize(Policy = "CanUpdateBlog")]
        [HttpGet("{page}/{pageSize}", Name = "GetBlogCategories")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetBlogCategories(
        string sortOrder, int? BlogCategoryId, string CategoryName, 
        string searchString, int? page = 1, int? pageSize = ConstantProps.maxPageSize)
    {
      try
      {
        ResponseObject resObject;
        if (!page.HasValue || page.Value == 0)
        {
          _logger.LogInformation("Generating List");
          var dataFromRepo = await _blogCategoryRepository.GetAllBlogCategories();
          var dataList = _mapper.Map<IEnumerable<BlogCategoryDto>>(dataFromRepo);
          if (BlogCategoryId.HasValue)
          {
            dataFromRepo = dataFromRepo.Where(x => x.BlogCategoryId == BlogCategoryId);
          }
          if (CategoryName != null)
          {
            dataFromRepo = dataFromRepo.Where(x => x.CategoryName == CategoryName);
          } 
          resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
          return Json(resObject);
        }

        // Sorting -------
 
        HttpContext.Session.SetString("CategoryNameSortParam", String.IsNullOrEmpty(sortOrder) ? "CategoryName_desc" : "");
        HttpContext.Session.SetString("CategoryNameSortParam", String.IsNullOrEmpty(sortOrder) ? "CategoryName_asc" : "");

        HttpContext.Session.SetString("BlogCategoryIdSortParam", String.IsNullOrEmpty(sortOrder) ? "blogCategoryId_asc" : "");
        HttpContext.Session.SetString("BlogCategoryIdSortParam", String.IsNullOrEmpty(sortOrder) ? "blogCategoryId_desc" : "");

        //search string check
        var previousSearchString = HttpContext.Session.GetString("searchString");

        if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
        {
          page = 1;
        }
        else
        {
          // searchString = currentFilter;
        }
        IQueryable<BlogCategory> data = null;
        data = await _blogCategoryRepository.GetAllBlogCategories(true);
        if(BlogCategoryId.HasValue)
        {
          data = data.Where(x => x.BlogCategoryId == BlogCategoryId);
        }
        if (CategoryName != null)
        {
          data = data.Where(x => x.CategoryName == CategoryName);
        }

        //search
        if (!String.IsNullOrEmpty(searchString))
        {
          HttpContext.Session.SetString("searchString", searchString);
          data = data.Where(s => s.CategoryName.Contains(searchString)
                                       );
        }
        else
        {
          HttpContext.Session.SetString("searchString", string.Empty);
        }
        //Sort Order
        switch (sortOrder)
        {
          case "CategoryName_asc":
            data = data.OrderBy(s => s.CategoryName);
            break;
          case "CategoryName_desc":
            data = data.OrderByDescending(s => s.CategoryName);
            break;
          case "blogCategoryId_asc":
            data = data.OrderBy(s => s.BlogCategoryId);
            break;
          case "blogCategoryId_desc":
            data = data.OrderByDescending(s => s.BlogCategoryId);
            break;
          default:
            data = data.OrderBy(s => s.BlogCategoryId);
            break;
        }
        var totalCount = await data.CountAsync();
        PaggingAndSorting paggingSorting = new PaggingAndSorting(_linkGenerator);
        paggingSorting.PaggingAndSortingData(HttpContext, totalCount, "GetBlogCategory", page, pageSize);

        // return result
        var items = await data
            .Skip((int)pageSize * ((int)page - 1))
            .Take((int)pageSize)
            .ToListAsync();

        var pagedData = _mapper.Map<IEnumerable<BlogCategoryDto>>(items);



        resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
        return Json(resObject);
      }
      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("GetBlogCategory");
        _logger.LogError(msg);
        var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
        return Json(resObject);
      }
    }

    /// <summary>
    /// Get BlogCategory By Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
        [Authorize(Policy = "CanUpdateBlog")]
        [HttpGet("{id}", Name = "GetBlogCategoryById")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetBlogCategoryById(int id)  // --------------------------------------------------
    {
      try
      {

        ResponseObject resObject;

        _logger.LogInformation("GetBlogCategoryById");
        var dataFromRepo = await _blogCategoryRepository.GetBlogCategoryById(id);

        if (dataFromRepo == null)
        {
          _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetBlogCategoryById({ID}) NOT FOUND", id);
          resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
          return Json(resObject);
        }

        var data = _mapper.Map<BlogCategoryDto>(dataFromRepo);

        resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
        return Json(resObject);
      }
      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("GetBlogCategoryById");
        _logger.LogError(msg);
        var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
        return Json(resObject);
      }

    }

        /// <summary>
        /// Add BlogCategory - POST api/BlogCategory
        /// </summary>
        /// <param name="blogCategory"></param>
        /// <returns></returns>
        [Authorize(Policy = "CanCreateBlogCategory")]
        [HttpPost(Name = "AddBlogCategory")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> AddBlogCategory([FromBody] BlogCategoryDto blogCategory)
    {
      try
      {
        ResponseObject resObject;
        if (blogCategory == null)
        {
          _logger.LogError(ConstantProps.NullObjectError("BlogCategory"));
          resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (BlogCategoryDto)null, HttpStatusCode = HttpStatusCode.NotFound };
          return Json(resObject);
        }

        if (!ModelState.IsValid)
        {
          _logger.LogError(ConstantProps.InvalidObjectError("BlogCategory"));
          resObject = new ResponseObject
          {
            Message = ConstantProps.InvalidDataModelText,
            Status = "error",
            Data = ModelState,
            HttpStatusCode = HttpStatusCode.BadRequest
          };

          return Json(resObject);
        }

        var blogCategoryRepo = _mapper.Map<BlogCategory>(blogCategory);
        await _blogCategoryRepository.AddBlogCategory(blogCategoryRepo);
        _logger.LogInformation(LoggingEvents.InsertItem, "BlogCategory {ID} Created", blogCategoryRepo.BlogCategoryId);
        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Adding", "BlogCategory"));
        }

        var contactFromRepo2 = CreatedAtAction(nameof(GetBlogCategoryById), new { id = blogCategoryRepo.BlogCategoryId }, blogCategoryRepo);
        var contactFromRepo = contactFromRepo2.Value;
        var data = _mapper.Map<BlogCategoryDto>(contactFromRepo);
        resObject = new ResponseObject
        {
          Message = ConstantProps.SubmittedSuccessfullyText,
          Status = "success",
          Data = data,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("Create");
        _logger.LogWarning(e.Message);
        var resObject = new ResponseObject { Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message2 = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
        return Json(resObject);
      }
    }

        // PUT api/BlogCategory/5
        [Authorize(Policy = "CanUpdateBlogCategory")]
        [HttpPut("{id}", Name = "UpdateBlogCategory")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> UpdateBlogCategory(int id, [FromBody] BlogCategoryDto blogCategory)
    {
      try
      {
        ResponseObject resObject;
        if (blogCategory == null)
        {
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = (BlogCategoryDto)null,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }

        if (!ModelState.IsValid)
        {
          _logger.LogError(ConstantProps.InvalidDataModelText);
          resObject = new ResponseObject
          {
            Message = ConstantProps.InvalidDataModelText,
            Status = "error",
            Data = ModelState,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }
        var blogCategoryRepo = _mapper.Map<BlogCategory>(blogCategory);
        if (!_blogCategoryRepository.UpdateBlogCategoryOk(id, blogCategoryRepo))
        {
          _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = id,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }

        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Updating", "BlogCategory"));
        }

        var contactFromRepo2 = CreatedAtAction(nameof(GetBlogCategoryById), new { id = blogCategoryRepo.BlogCategoryId }, blogCategoryRepo);
        var contactFromRepo = contactFromRepo2.Value;
        var data = _mapper.Map<BlogCategoryDto>(contactFromRepo);
        resObject = new ResponseObject
        {
          Message = ConstantProps.SavedSuccessfullyText,
          Status = "success",
          Data = data,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
        _logger.LogWarning(e.Message);

        var resObject = new ResponseObject
        {
          Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
          Message2 = msg,
          Status = "error",
          HttpStatusCode = HttpStatusCode.InternalServerError
        };
        return Json(resObject);
      }
    }

    /// <summary>
    /// DELETE api/BlogCategory/5
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
        [Authorize(Policy = "CanDeleteBlogCategory")]
        [HttpDelete("{id}", Name = "DeleteBlogCategory")]
    public async Task<IActionResult> DeleteBlogCategory(int id)
    {
      try
      {
        ResponseObject resObject;
        var entity = _blogCategoryRepository.GetBlogCategoryByIdAsNoTracking(id);
        if (entity == false)
        {
          _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = id,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }
        _blogCategoryRepository.DeleteBlogCategory(id);
        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Deleting", "BlogCategory"));
        }
        resObject = new ResponseObject
        {
          Message = ConstantProps.DeletedSuccessfullyText,
          Status = "success",
          Data = id,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("Delete");
        _logger.LogError(msg);
        var resObject = new ResponseObject
        {
          Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
          Message = msg,
          Status = "error",
          HttpStatusCode = HttpStatusCode.InternalServerError
        };
        return Json(resObject);
      }
    }


  }
}
