﻿
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
//using System.Data.Entity.Validation;
using System.Threading.Tasks;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Repositories;
using Microsoft.AspNetCore.Http;
using DRS.APA.API.Helpers;
using Microsoft.Extensions.Logging;
using DRS.APA.Domain.Core;
using System.Net;
using System.Linq;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using Microsoft.Data.SqlClient;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Domain.Core.Repositories.Website;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers
{
    [Route("api/DRS.APA/website/[controller]")]
    [SwaggerGroup("Website")]
    public class WebVideoController : Controller
    {
        private readonly IWebVideoRepository _WebVideoRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        const int maxPageSize = 20;
        protected readonly LinkGenerator _linkGenerator;

        public WebVideoController(
            IWebVideoRepository WebVideoRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            ILogger<WebVideoController> logger,
            LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _WebVideoRepository = WebVideoRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }


        /// <summary>
        /// List all WebVideo sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetWebVideo")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebVideo(string sortOrder, string searchString, int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                ResponseObject resObject;
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");

                    var dataFromRepo = await _WebVideoRepository.GetAllWebVideos(false);
                    var dataList = _mapper.Map<IEnumerable<WebVideoDto>>(dataFromRepo);
                    resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                    return Json(resObject);
                }

                // Sorting -------
                HttpContext.Session.SetString("HeadingSortParam", String.IsNullOrEmpty(sortOrder) ? "heading_desc" : "");
                HttpContext.Session.SetString("HeadingSortParam", String.IsNullOrEmpty(sortOrder) ? "heading_asc" : "");
                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_asc" : "");
                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_desc" : "");
                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;
                }
                IQueryable<WebVideo> data = null;
                //var filterSpecification = new WebVideoFilterSpecification(contactId, leadId);
                data = await _WebVideoRepository.GetAllWebVideos(true);

                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    data = data.Where(s => s.VideoUrl.Contains(searchString));
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "heading_desc":
                        data = data.OrderByDescending(s => s.VideoUrl);
                        break;

                    case "id_asc":
                        data = data.OrderByDescending(s => s.WebVideoId);
                        break;
                    case "id_desc":
                        data = data.OrderByDescending(s => s.WebVideoId);
                        break;
                    default:
                        data = data.OrderBy(s => s.VideoUrl);
                        break;
                }
                //
                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount = await data.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetWebVideos", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetWebVideos", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";

                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();

                var pagedData = _mapper.Map<IEnumerable<WebVideoDto>>(items);
                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetWebVideo Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

        /// <summary>
        /// Get WebVideos By ComponentId
        /// </summary>
        /// <param name="componentId"></param>
        /// <returns></returns>
        [HttpGet("GetWebVideoByComponentId/{componentId}", Name = "GetWebVideoByComponentId")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebVideoByComponentId(short componentId)
        {
            try
            {
                _logger.LogInformation("Generating GetWebVideoByComponentId List");
                ResponseObject resObject;
                var dataFromRepo = await _WebVideoRepository.GetWebVideoByComponentId(componentId);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebVideoByComponentId({ID}) NOT FOUND", componentId);
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = componentId, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                var dataList = _mapper.Map<IEnumerable<WebVideoDto>>(dataFromRepo);
                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetWebVideoByComponentId Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }
        /// <summary>
        /// Get WebVideo By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetWebVideoById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebVideoById(short id)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("GetWebVideoById");
                var dataFromRepo = await _WebVideoRepository.GetWebVideoById(id);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebVideoById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                var data = _mapper.Map<WebVideoDto>(dataFromRepo);
                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetWebVideoById Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }
        /// <summary>
        /// AddWebVideo - POST api/WebVideo
        /// </summary>
        /// <param name="WebVideo"></param>
        /// <returns></returns>

        [HttpPost(Name = "AddWebVideo")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddWebVideo([FromBody] WebVideoDto WebVideo)  // ---------------------
        {
            try
            {
                ResponseObject resObject;
                if (WebVideo == null)
                {
                    _logger.LogError("WebVideo object sent from client is null.");
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = (WebVideoDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid WebVideo object sent from client.");
                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model!",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var WebVideoRepo = _mapper.Map<WebVideo>(WebVideo);
                await _WebVideoRepository.AddWebVideo(WebVideoRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "WebVideo {ID} Created", WebVideoRepo.WebVideoId);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a WebVideo failed on save.");
                }
                var contactFromRepo2 = CreatedAtAction(nameof(GetWebVideoById), new { id = WebVideoRepo.WebVideoId }, WebVideoRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebVideoDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = "Submitted Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Sever Error, Unable to Process Create Request";
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject { Message = BuiltMessage(e, LoggingEvents.GetItem), Message2 = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {

            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    //Changes-Babar:
                    if (e.InnerException is SqlException ex)
                    {
                        if (ex.Number == 2627 || ex.Number == 2601 || ex.Message.ToLower().Contains("duplicate key"))
                        {
                            return "Unable to process, duplicate data.";
                        }
                    }
                    //Changes-Babar:

                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }
                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }

        // PUT api/WebVideo/5
        [HttpPut("{id}", Name = "UpdateWebVideo")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateWebVideo(short id, [FromBody] WebVideoDto WebVideo) // ------------
        {
            try
            {
                ResponseObject resObject;
                if (WebVideo == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = "Data not Found, BadRequest",
                        Status = "error",
                        Data = (WebVideoDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Data");
                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model, BadRequest",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var WebVideoRepo = _mapper.Map<WebVideo>(WebVideo);
                if (!_WebVideoRepository.UpdateWebVideoOk(id, WebVideoRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    // return StatusCode((int) HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a WebVideo failed on save.");
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetWebVideoById), new { id = WebVideoRepo.WebVideoId }, WebVideoRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebVideoDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = "Saved Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }
        /// <summary>
        /// DELETE api/WebVideo/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}", Name = "DeleteWebVideo")]
        public async Task<IActionResult> DeleteWebVideo(short id) // -----------------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                var entity = _WebVideoRepository.GetWebVideoByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                _WebVideoRepository.DeleteWebVideo(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a WebVideo failed on save.");
                }
                resObject = new ResponseObject
                {
                    Message = "Deleted Successfully",
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process Delete Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }        
    }
}
