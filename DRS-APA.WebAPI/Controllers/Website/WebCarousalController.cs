using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using DRS.APA.Microservice.API.Controllers.Common;

using DRS.APA.Domain.Core.Repositories.Website;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using Microsoft.AspNetCore.Authorization;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/website/[controller]")]
    [SwaggerGroup("Website")]
    public class WebCarousalController : Controller
    {
        private readonly IWebCarousalRepository _WebCarousalRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        protected readonly LinkGenerator _linkGenerator;

        public WebCarousalController(IWebCarousalRepository WebCarousalRepository, IReviewRepository reviewRepository,
        IUnitOfWork unitOfWork, IMapper mapper, ILogger<WebCarousalController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _WebCarousalRepository = WebCarousalRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }

    /// <summary>
    /// List all WebCarousals
    /// </summary>
    /// <remarks>Without server side pagging/sorting.</remarks>
    
     [Authorize(Policy = "CanViewWebCarousal")]
      [HttpGet(Name = "GetWebCarousal")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebCarousal()
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("Generating List");
                var dataFromRepo = await _WebCarousalRepository.GetAllWebCarousal();
                var dataList = _mapper.Map<IEnumerable<WebCarousalDto>>(dataFromRepo);
                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);

            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetWebCarousal");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

    /// <summary>
    /// Get WebCarousal By Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanViewWebCarousal")]
    [HttpGet("{id}", Name = "GetWebCarousalById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebCarousalById(int id)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("GetWebCarousalById");
                var dataFromRepo = await _WebCarousalRepository.GetWebCarousalById(id);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebCarousalById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                var data = _mapper.Map<WebCarousalDto>(dataFromRepo);
                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetWebCarousalById");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

    /// <summary>
    /// Add WebCarousal - POST api/WebCarousal
    /// </summary>
    /// <param name="WebCarousal"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanCreateWebCarousal")]
    [HttpPost(Name = "AddWebCarousal")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddWebCarousal([FromBody] WebCarousalDto webCarousal)
        {
            try
            {
                ResponseObject resObject;
                if (webCarousal == null)
                {
                    _logger.LogError(ConstantProps.NullObjectError("WebCarousal"));
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (WebCarousalDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidObjectError("WebCarousal"));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var WebCarousalRepo = _mapper.Map<WebCarousal>(webCarousal);
                await _WebCarousalRepository.AddWebCarousal(WebCarousalRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "WebCarousal {ID} Created", WebCarousalRepo.WebCarousalId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Adding", "WebCarousal"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetWebCarousalById), new { id = WebCarousalRepo.WebCarousalId }, WebCarousalRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebCarousalDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SubmittedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Create");
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

    // PUT api/WebCarousal/5
    [Authorize(Policy = "CanUpdateWebCarousal")]
    [HttpPut("{id}", Name = "UpdateWebCarousal")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateWebCarousal(int id, [FromBody] WebCarousalDto WebCarousal)
        {
            try
            {
                ResponseObject resObject;
                if (WebCarousal == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = (WebCarousalDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidDataModelText);
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var WebCarousalRepo = _mapper.Map<WebCarousal>(WebCarousal);
                if (!_WebCarousalRepository.UpdateWebCarousalOk(id, WebCarousalRepo))
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Updating", "WebCarousal"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetWebCarousalById), new { id = WebCarousalRepo.WebCarousalId }, WebCarousalRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebCarousalDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SavedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

    /// <summary>
    /// DELETE api/WebCarousal/5
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanDeleteWebCarousal")]
    [HttpDelete("{id}", Name = "DeleteWebCarousal")]
        public async Task<IActionResult> DeleteWebCarousal(int id)
        {
            try
            {
                ResponseObject resObject;
                var entity = _WebCarousalRepository.GetWebCarousalByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                _WebCarousalRepository.DeleteWebCarousal(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Deleting", "WebCarousal"));
                }
                resObject = new ResponseObject
                {
                    Message = ConstantProps.DeletedSuccessfullyText,
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Delete");
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }
        
    }
}
