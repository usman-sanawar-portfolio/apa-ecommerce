using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using DRS.APA.Microservice.API.Controllers.Common;
using DRS.APA.Domain.Core.Repositories.Website;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using Microsoft.AspNetCore.Authorization;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/website/[controller]")]
    [SwaggerGroup("Website")]
    public class WebBannerController : Controller
    {
        private readonly IWebBannerRepository _WebBannerRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        protected readonly LinkGenerator _linkGenerator;

        public WebBannerController(IWebBannerRepository WebBannerRepository, IReviewRepository reviewRepository,
        IUnitOfWork unitOfWork, IMapper mapper, ILogger<WebBannerController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _WebBannerRepository = WebBannerRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }

    /// <summary>
    /// List all WebBanners
    /// </summary>
    /// <remarks>Without server side pagging/sorting.</remarks>
    [Authorize(Policy = "CanViewWebBanner")]
    [HttpGet(Name = "GetWebBanner")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebBanner()
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("Generating List");
                var dataFromRepo = await _WebBannerRepository.GetAllWebBanner();
                var dataList = _mapper.Map<IEnumerable<WebBannerDto>>(dataFromRepo);
                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);

            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetWebBanner");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

    /// <summary>
    /// Get WebBanner By Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanViewWebBanner")]
    [HttpGet("{id}", Name = "GetWebBannerById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebBannerById(int id)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("GetWebBannerById");
                var dataFromRepo = await _WebBannerRepository.GetWebBannerById(id);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebBannerById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                var data = _mapper.Map<WebBannerDto>(dataFromRepo);
                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetWebBannerById");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

    /// <summary>
    /// Add WebBanner - POST api/WebBanner
    /// </summary>
    /// <param name="WebBanner"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanCreateWebBanner")]
    [HttpPost(Name = "AddWebBanner")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddWebBanner([FromBody] WebBannerDto WebBanner)
        {
            try
            {
                ResponseObject resObject;
                if (WebBanner == null)
                {
                    _logger.LogError(ConstantProps.NullObjectError("WebBanner"));
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (WebBannerDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidObjectError("WebBanner"));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var WebBannerRepo = _mapper.Map<WebBanner>(WebBanner);
                await _WebBannerRepository.AddWebBanner(WebBannerRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "WebBanner {ID} Created", WebBannerRepo.WebBannerId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Adding", "WebBanner"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetWebBannerById), new { id = WebBannerRepo.WebBannerId }, WebBannerRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebBannerDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SubmittedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Create");
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject {
                    Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), 
                    Message2 = msg,
                    Status = "error", 
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

    // PUT api/WebBanner/5
    [Authorize(Policy = "CanUpdateWebBanner")]
    [HttpPut("{id}", Name = "UpdateWebBanner")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateWebBanner(int id, [FromBody] WebBannerDto WebBanner)
        {
            try
            {
                ResponseObject resObject;
                if (WebBanner == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = (WebBannerDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidDataModelText);
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var WebBannerRepo = _mapper.Map<WebBanner>(WebBanner);
                if (!_WebBannerRepository.UpdateWebBannerOk(id, WebBannerRepo))
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Updating", "WebBanner"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetWebBannerById), new { id = WebBannerRepo.WebBannerId }, WebBannerRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebBannerDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SavedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

    /// <summary>
    /// DELETE api/WebBanner/5
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanDeleteWebBanner")]
    [HttpDelete("{id}", Name = "DeleteWebBanner")]
        public async Task<IActionResult> DeleteWebBanner(int id)
        {
            try
            {
                ResponseObject resObject;
                var entity = _WebBannerRepository.GetWebBannerByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                _WebBannerRepository.DeleteWebBanner(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Deleting", "WebBanner"));
                }
                resObject = new ResponseObject
                {
                    Message = ConstantProps.DeletedSuccessfullyText,
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Delete");
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }        
    }
}
