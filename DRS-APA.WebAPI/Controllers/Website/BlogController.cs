using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using DRS.APA.Microservice.API.Controllers.Common;
using DRS.APA.Domain.Core.Repositories.Website; 
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website;
using Microsoft.AspNetCore.Authorization;
using DRS.APA.Domain.Core.Repositories.Order;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.Website
{
    [Route("api/DRS.APA/website/[controller]")]
    [SwaggerGroup("website")]
    public class BlogController : Controller
    {
        private readonly IBlogRepository _blogRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        protected readonly LinkGenerator _linkGenerator;
    private readonly IUserRepository _userRepository;


    public BlogController(IBlogRepository blogRepository,   IUserRepository  userRepository,

    IUnitOfWork unitOfWork, IMapper mapper, ILogger<BlogController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _blogRepository = blogRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
          _userRepository = userRepository;
  }

        /// <summary>
        /// List all Blog, by default sorted by BlogName
        /// </summary>
        /// <remarks>With pagination, you can fetch up to x records per page.</remarks>
        [Authorize(Policy = "CanViewBlog")]
        [HttpGet("{page}/{pageSize}", Name = "GetBlogs")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetBlogs(
            string sortOrder, int? BlogId, string BlogTitle, int? CategoryId,
            string searchString, int? page = 1, int? pageSize = ConstantProps.maxPageSize)
        {
            try
            {
                ResponseObject resObject;
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var dataFromRepo = await _blogRepository.GetAllBlogs();
                    var dataList = _mapper.Map<IEnumerable<BlogDto>>(dataFromRepo);
                    if (BlogId.HasValue)
                    {
                        dataFromRepo = dataFromRepo.Where(x => x.BlogId == BlogId);
                    }
                    if (BlogTitle != null)
                    {
                        dataFromRepo = dataFromRepo.Where(x => x.BlogTitle == BlogTitle);
                    }
                    if (CategoryId.HasValue)
                    {
                        dataFromRepo = dataFromRepo.Where(x => x.BlogCategoryId == CategoryId);
                    }
                    resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                    return Json(resObject);
                }
                // Sorting -------

                HttpContext.Session.SetString("BlogTitleSortParam", String.IsNullOrEmpty(sortOrder) ? "BlogTitle_desc" : "");
                HttpContext.Session.SetString("BlogTitleSortParam", String.IsNullOrEmpty(sortOrder) ? "BlogTitle_asc" : "");

                HttpContext.Session.SetString("BlogIdSortParam", String.IsNullOrEmpty(sortOrder) ? "blogId_asc" : "");
                HttpContext.Session.SetString("BlogIdSortParam", String.IsNullOrEmpty(sortOrder) ? "blogId_desc" : "");

                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;
                }
                IQueryable<Blog> data = null;
                data = await _blogRepository.GetAllBlogs(true);
                if (BlogId.HasValue)
                {
                    data = data.Where(x => x.BlogId == BlogId);
                }
                if (BlogTitle != null)
                {
                    data = data.Where(x => x.BlogTitle == BlogTitle);
                }
                if (CategoryId.HasValue)
                {
                    data = data.Where(x => x.BlogCategoryId == CategoryId);
                }
                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    data = data.Where(s => s.BlogTitle.Contains(searchString)
                                             || s.BlogCategory.CategoryName.Contains(searchString)
                                                );
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "BlogTitle_asc":
                        data = data.OrderBy(s => s.BlogTitle);
                        break;
                    case "BlogTitle_desc":
                        data = data.OrderByDescending(s => s.BlogTitle);
                        break;
                    case "blogId_asc":
                        data = data.OrderBy(s => s.BlogId);
                        break;
                    case "blogId_desc":
                        data = data.OrderByDescending(s => s.BlogId);
                        break;
                    default:
                        data = data.OrderBy(s => s.BlogId);
                        break;
                }
                var totalCount = await data.CountAsync();
                PaggingAndSorting paggingSorting = new PaggingAndSorting(_linkGenerator);
                paggingSorting.PaggingAndSortingData(HttpContext, totalCount, "GetBlog", page, pageSize);

                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();
                var pagedData = _mapper.Map<IEnumerable<BlogDto>>(items);
                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetBlog");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

        /// <summary>
        /// Get Blog By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Policy = "CanViewBlog")]
        [HttpGet("{id}", Name = "GetBlogById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetBlogById(int id)  // --------------------------------------------------
        {
            try
            {

                ResponseObject resObject;

                _logger.LogInformation("GetBlogById");
                var dataFromRepo = await _blogRepository.GetBlogById(id);

                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetBlogById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                var data = _mapper.Map<BlogDto>(dataFromRepo);


                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetBlogById");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }


        /// <summary>
        /// Add Blog - POST api/Blog
        /// </summary>
        /// <param name="blog"></param>
        /// <returns></returns>
      [Authorize(Policy = "CanCreateBlog")]
        [HttpPost(Name = "AddBlog")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddBlog([FromBody] BlogDto blog)
        {
            try
            {
                ResponseObject resObject;
                if (blog == null)
                {
                    _logger.LogError(ConstantProps.NullObjectError("Blog"));
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (BlogDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidObjectError("Blog"));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var blogRepo = _mapper.Map<Blog>(blog);
                blogRepo.CustomerId = null;
                await _blogRepository.AddBlog(blogRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "Blog {ID} Created", blogRepo.BlogId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Adding", "Blog"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetBlogById), new { id = blogRepo.BlogId }, blogRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<BlogDto>(contactFromRepo);
        var Createdby = Int32.Parse(data.CreatedBy);
        var UpdatedBy = Int32.Parse(data.UpdatedBy);
         var createUser = _userRepository.GetUserCreatedById(Createdby);
         var updateUser = _userRepository.GetUserCreatedById(UpdatedBy);
         data.CreatedBy = createUser.Name; 
        data.UpdatedBy = updateUser.Name;
        resObject = new ResponseObject
                {
                    Message = ConstantProps.SubmittedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Create");
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject { Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message2 = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

        // PUT api/Blog/5
        [Authorize(Policy = "CanUpdateBlog")]
        [HttpPut("{id}", Name = "UpdateBlog")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateBlog(int id, [FromBody] BlogDto blog)
        {
            try
            {
                ResponseObject resObject;
                if (blog == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = (BlogDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidDataModelText);
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var blogRepo = _mapper.Map<Blog>(blog);
                if (!_blogRepository.UpdateBlogOk(id, blogRepo))
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Updating", "Blog"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetBlogById), new { id = blogRepo.BlogId }, blogRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<BlogDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SavedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

        /// <summary>
        /// DELETE api/Blog/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Policy = "CanDeleteBlog")]
        [HttpDelete("{id}", Name = "DeleteBlog")]
        public async Task<IActionResult> DeleteBlog(int id)
        {
            try
            {
                ResponseObject resObject;
                var entity = _blogRepository.GetBlogByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                _blogRepository.DeleteBlog(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Deleting", "Blog"));
                }
                resObject = new ResponseObject
                {
                    Message = ConstantProps.DeletedSuccessfullyText,
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Delete");
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }


    }
}
