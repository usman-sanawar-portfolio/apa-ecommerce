﻿
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
//using System.Data.Entity.Validation;
using System.Threading.Tasks;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Repositories;
using Microsoft.AspNetCore.Http;
using DRS.APA.API.Helpers;
using Microsoft.Extensions.Logging;
using DRS.APA.Domain.Core;
using System.Net;
using System.Linq;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using Microsoft.Data.SqlClient;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Domain.Core.Repositories.Website;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers
{
    [Route("api/DRS.APA/website/[controller]")]
    [SwaggerGroup("Website")]
    public class WebDocController : Controller
    {
        private readonly IWebDocRepository _WebDocRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        const int maxPageSize = 20;
        protected readonly LinkGenerator _linkGenerator;

        public WebDocController(
            IWebDocRepository WebDocRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            ILogger<WebDocController> logger,
            LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _WebDocRepository = WebDocRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }


        /// <summary>
        /// List all WebDoc sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetWebDoc")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebDoc(string sortOrder, string searchString, int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                ResponseObject resObject;
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");

                    var dataFromRepo = await _WebDocRepository.GetAllWebDocs(false);
                    var dataList = _mapper.Map<IEnumerable<WebDocDto>>(dataFromRepo);
                    resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                    return Json(resObject);
                }

                // Sorting -------
                HttpContext.Session.SetString("TitleSortParam", String.IsNullOrEmpty(sortOrder) ? "title_desc" : "");
                HttpContext.Session.SetString("TitleSortParam", String.IsNullOrEmpty(sortOrder) ? "title_asc" : "");
                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_asc" : "");
                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_desc" : "");
                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;
                }
                IQueryable<WebDoc> data = null;
                //var filterSpecification = new WebDocFilterSpecification(contactId, leadId);
                data = await _WebDocRepository.GetAllWebDocs(true);

                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    data = data.Where(s => s.Header.Contains(searchString));
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "heading_desc":
                        data = data.OrderByDescending(s => s.Header);
                        break;

                    case "id_asc":
                        data = data.OrderByDescending(s => s.WebDocId);
                        break;
                    case "id_desc":
                        data = data.OrderByDescending(s => s.WebDocId);
                        break;
                    default:
                        data = data.OrderBy(s => s.Header);
                        break;
                }
                //
                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount = await data.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetWebDocs", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetWebDocs", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";

                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();

                var pagedData = _mapper.Map<IEnumerable<WebDocDto>>(items);
                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetWebDoc Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

        /// <summary>
        /// Get WebDocs By ComponentId
        /// </summary>
        /// <param name="componentId"></param>
        /// <returns></returns>
        [HttpGet("GetWebDocByComponentId/{componentId}", Name = "GetWebDocByComponentId")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebDocByComponentId(short componentId)
        {
            try
            {
                _logger.LogInformation("Generating List");
                ResponseObject resObject;
                var dataFromRepo = await _WebDocRepository.GetWebDocByComponentId(componentId);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebDocByComponentId({ID}) NOT FOUND", componentId);
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = componentId, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                var dataList = _mapper.Map<IEnumerable<WebDocDto>>(dataFromRepo);
                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetWebDocByComponentId Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

        /// <summary>
        /// Get WebDoc By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetWebDocById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebDocById(short id)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("GetWebDocById");
                var dataFromRepo = await _WebDocRepository.GetWebDocById(id);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebDocById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                var data = _mapper.Map<WebDocDto>(dataFromRepo);
                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetWebDocById Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }
        /// <summary>
        /// AddWebDoc - POST api/WebDoc
        /// </summary>
        /// <param name="WebDoc"></param>
        /// <returns></returns>

        [HttpPost(Name = "AddWebDoc")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddWebDoc([FromBody] WebDocDto WebDoc)  // ---------------------
        {
            try
            {
                ResponseObject resObject;
                if (WebDoc == null)
                {
                    _logger.LogError("WebDoc object sent from client is null.");
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = (WebDocDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid WebDoc object sent from client.");
                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model!",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var WebDocRepo = _mapper.Map<WebDoc>(WebDoc);
                await _WebDocRepository.AddWebDoc(WebDocRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "WebDoc {ID} Created", WebDocRepo.WebDocId);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a WebDoc failed on save.");
                }
                var contactFromRepo2 = CreatedAtAction(nameof(GetWebDocById), new { id = WebDocRepo.WebDocId }, WebDocRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebDocDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = "Submitted Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Sever Error, Unable to Process Create Request";
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject { Message = BuiltMessage(e, LoggingEvents.GetItem), Message2 = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {

            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    //Changes-Babar:
                    if (e.InnerException is SqlException ex)
                    {
                        if (ex.Number == 2627 || ex.Number == 2601 || ex.Message.ToLower().Contains("duplicate key"))
                        {
                            return "Unable to process, duplicate data.";
                        }
                    }
                    //Changes-Babar:

                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }
                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }

        // PUT api/WebDoc/5
        [HttpPut("{id}", Name = "UpdateWebDoc")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateWebDoc(short id, [FromBody] WebDocDto WebDoc) // ------------
        {
            try
            {
                ResponseObject resObject;
                if (WebDoc == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = "Data not Found, BadRequest",
                        Status = "error",
                        Data = (WebDocDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Data");
                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model, BadRequest",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var WebDocRepo = _mapper.Map<WebDoc>(WebDoc);
                if (!_WebDocRepository.UpdateWebDocOk(id, WebDocRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    // return StatusCode((int) HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a WebDoc failed on save.");
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetWebDocById), new { id = WebDocRepo.WebDocId }, WebDocRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebDocDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = "Saved Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }
        /// <summary>
        /// DELETE api/WebDoc/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}", Name = "DeleteWebDoc")]
        public async Task<IActionResult> DeleteWebDoc(short id) // -----------------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                var entity = _WebDocRepository.GetWebDocByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                _WebDocRepository.DeleteWebDoc(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a WebDoc failed on save.");
                }
                resObject = new ResponseObject
                {
                    Message = "Deleted Successfully",
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process Delete Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }        
    }
}
