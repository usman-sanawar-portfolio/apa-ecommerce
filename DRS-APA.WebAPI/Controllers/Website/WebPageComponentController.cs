
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
//using System.Data.Entity.Validation;
using System.Threading.Tasks;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Repositories;
using Microsoft.AspNetCore.Http;
using DRS.APA.API.Helpers;
using Microsoft.Extensions.Logging;
using DRS.APA.Domain.Core;
using System.Net;
using System.Linq;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using DRS.APA.Microserivce.Domain.Core.Repositories.Website;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Domain.Core.Repositories.Website;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using Microsoft.AspNetCore.Authorization;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers
{
    [Route("api/DRS.APA/website/[controller]")]
    [SwaggerGroup("Website")]
    public class WebPageComponentController : Controller
    {
        private readonly IWebPageComponentRepository _webPageComponentRepository;

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        const int maxPageSize = 20;
        protected readonly LinkGenerator _linkGenerator;

        public WebPageComponentController(
            IWebPageComponentRepository webPageComponentRepository,

            IUnitOfWork unitOfWork,
            IMapper mapper,
            ILogger<WebPageComponentController> logger,
            LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _webPageComponentRepository = webPageComponentRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }


    /// <summary>
    /// List all public WebPageComponent sorted by Id.
    /// </summary>
    /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
    [Authorize(Policy = "CanViewPageComponent")]
    [HttpGet("{page}/{pageSize}", Name = "GetWebPageComponent")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebPageComponent(string sortOrder, string searchString, int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                ResponseObject resObject;
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating WebPageComponent List");
                    var dataFromRepo = await _webPageComponentRepository.GetAllWebPageComponents();
                    var dataList = _mapper.Map<IEnumerable<WebPageComponentDto>>(dataFromRepo);
                    resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                    return Json(resObject);
                }

                // Sorting -------
                HttpContext.Session.SetString("TitleSortParam", String.IsNullOrEmpty(sortOrder) ? "title_asc" : "");
                HttpContext.Session.SetString("TitleSortParam", String.IsNullOrEmpty(sortOrder) ? "title_desc" : "");

                HttpContext.Session.SetString("WebPageTitleSortParam", String.IsNullOrEmpty(sortOrder) ? "webPageTitle_asc" : "");
                HttpContext.Session.SetString("WebPageTitleSortParam", String.IsNullOrEmpty(sortOrder) ? "webPageTitle_desc" : "");

                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_asc" : "");
                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_desc" : "");

                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;
                }

                IQueryable<WebPageComponent> data = null;
                data = await _webPageComponentRepository.GetAllWebPageComponents(true);
                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    data = data.Where(s => s.WebComponent.Title.Contains(searchString)
                                             || s.WebPage.WebPageTitle.Contains(searchString)
                    );

                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "title_desc":
                        data = data.OrderByDescending(s => s.WebComponent.Title);
                        break;
                    case "webPageTitle_asc":
                        data = data.OrderBy(s => s.WebPage.WebPageTitle);
                        break;
                    case "webPageTitle_desc":
                        data = data.OrderByDescending(s => s.WebPage.WebPageTitle);
                        break;
                    case "id_asc":
                        data = data.OrderBy(s => s.WebComponentId).OrderByDescending(s => s.WebPageId);
                        break;
                    case "id_desc":
                        data = data.OrderByDescending(s => s.WebComponentId).OrderByDescending(s => s.WebPageId);
                        break;
                    default:
                        data = data.OrderBy(s => s.WebComponent.Title);
                        break;
                }
                //
                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount = await data.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetWebPageComponent", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetWebPageComponent", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";

                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();

                var pagedData = _mapper.Map<IEnumerable<WebPageComponentDto>>(items);
                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetItems Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }


    /// <summary>
    /// Get Web Component By componentId + pageId (Composite Key)
    /// </summary>
    /// <param name="componentId"></param>
    /// <param name="pageId"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanViewPageComponent")]
    [HttpGet("GetWebPageComponentById/{componentId}/{pageId}", Name = "GetWebPageComponentById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebPageComponentById(short componentId, short pageId)
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("GetWebPageComponentById");
                var dataFromRepo = await _webPageComponentRepository.GetWebPageComponentById(componentId, pageId);

                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebPageComponentById({compId},{pageId}) NOT FOUND", componentId, pageId);
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = componentId / pageId,
                        HttpStatusCode = HttpStatusCode.NotFound
                    };
                    return Json(resObject);
                }
                var data = _mapper.Map<WebPageComponentDto>(dataFromRepo);
                resObject = new ResponseObject
                {
                    Message = "Data List!",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetWebPageComponentById Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }

    //
    /// <summary>
    /// Get GetWebPageComponents By pageId
    /// </summary>
    /// <param name="pageId"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanViewPageComponent")]
    [HttpGet("GetWebPageComponentsOnPageId/{pageId}", Name = "GetWebPageComponentsOnPageId")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebPageComponentsOnPageId(short pageId)
        {
            try
            {
                _logger.LogInformation("Generating List");
                ResponseObject resObject;
                var dataFromRepo = await _webPageComponentRepository.GetWebPageComponentsOnPageId(pageId);

                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebPageComponentsOnPageId({ID}) NOT FOUND", pageId);
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = pageId, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                else if (dataFromRepo.Count <= 0)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebPageComponentsOnPageId({ID}) NOT FOUND", pageId);
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = pageId, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                var dataList = _mapper.Map<IEnumerable<WebPageComponentDto>>(dataFromRepo);
                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetWebPageComponentsOnPageId Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }


    /// <summary>
    /// Get GetWebPageComponents By page Slug
    /// </summary>
    /// <param name="slug"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanViewPageComponent")]
    [HttpGet("GetWebPageComponentsOnSlug/{slug}", Name = "GetWebPageComponentsOnSlug")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebPageComponentsOnSlug(string slug)
        {
            try
            {
                _logger.LogInformation("Generating List");
                ResponseObject resObject;
                var dataFromRepo = await _webPageComponentRepository.GetWebPageComponentsOnSlug(slug);

                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebPageComponentsOnSlug({ID}) NOT FOUND", slug);
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = slug, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                else if (dataFromRepo.Count <= 0)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebPageComponentsOnSlug({ID}) NOT FOUND", slug);
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = slug, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                var dataList = _mapper.Map<IEnumerable<WebPageComponentDto>>(dataFromRepo);
                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetWebPageComponentsOnSlug Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

    /// <summary>
    /// Get GetWebPageComponents By componentId
    /// </summary>
    /// <param name="componentId"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanViewPageComponent")]
    [HttpGet("GetWebPageComponentsOnComponentId/{componentId}", Name = "GetWebPageComponentsOnComponentId")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebPageComponentsOnComponentId(short componentId)
        {
            try
            {
                _logger.LogInformation("Generating List");
                ResponseObject resObject;
                var dataFromRepo = await _webPageComponentRepository.GetWebPageComponentsOnComponentId(componentId);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebPageComponentsOnComponentId({ID}) NOT FOUND", componentId);
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = componentId, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                else if (dataFromRepo.Count <= 0)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebPageComponentsOnComponentId({ID}) NOT FOUND", componentId);
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = componentId, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                var dataList = _mapper.Map<IEnumerable<WebPageComponentDto>>(dataFromRepo);
                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetWebPageComponentsOnComponentId Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

    /// <summary>
    /// AddWebPageComponent - POST api/WebPageComponent
    /// </summary>
    /// <param name="webPageComponent"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanCreatePageComponent")]
    [HttpPost(Name = "AddWebPageComponent")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddWebPageComponent([FromBody] ICollection<WebPageComponentDto> webPageComponent)
        {
            try
            {
                ResponseObject resObject;
                if (webPageComponent == null)
                {
                    _logger.LogError("WebPageComponent object sent from client is null.");
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = (WebPageComponentDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid webPageComponent object sent from client.");
                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model!",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                var webPageComponentRepo = _mapper.Map<ICollection<WebPageComponent>>(webPageComponent);

                await _webPageComponentRepository.AddWebPageComponent(webPageComponentRepo);
                //_logger.LogInformation(LoggingEvents.InsertItem, "WebPageComponent {ID} Created", webPageComponentRepo.WebPageComponentId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a WebPageComponent failed on save.");
                }
                ICollection<WebPageComponentDto> data = new List<WebPageComponentDto>();
                foreach (var v in webPageComponentRepo)
                {
                    var contactFromRepo2 = CreatedAtAction(
                        nameof(GetWebPageComponentById),
                        new { componentId = v.WebComponentId, pageId = v.WebPageId },
                        v);

                    var objWebPageComponentById = await GetWebPageComponentById(v.WebComponentId, v.WebPageId);
                    var contactFromRepo = contactFromRepo2.Value;
                    data.Add(_mapper.Map<WebPageComponentDto>(contactFromRepo));
                }
                resObject = new ResponseObject
                {
                    Message = "Submitted Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Sever Error, Unable to Process Create Request";
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject { Message = BuiltMessage(e, LoggingEvents.GetItem), Message2 = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {
            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }

                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }
    /// <summary>
    /// PUT api/WebPageComponent/UpdateWebPageComponent/5/1 ( Order and isActive can be updated only )
    /// </summary>
    /// <param name="componentId"></param>
    /// <param name="pageId"></param>
    /// <param name="webPageComponent"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanUpdatePageComponent")]
    [HttpPut("UpdateWebPageComponent/{componentId}/{pageId}", Name = "UpdateWebPageComponent")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateWebPageComponent(short componentId, short pageId, [FromBody] WebPageComponentDto webPageComponent)
        {
            try
            {
                ResponseObject resObject;
                if (webPageComponent == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = "Data not Found, BadRequest",
                        Status = "error",
                        Data = (WebPageComponentDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Data");

                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model, BadRequest",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var webPageComponentRepo = _mapper.Map<WebPageComponent>(webPageComponent);

                webPageComponentRepo.WebComponent = null;
                webPageComponentRepo.WebPage = null;

                if (!_webPageComponentRepository.UpdateWebPageComponentOk(componentId, pageId, webPageComponentRepo))
                {
                    _logger.LogError($"entity with id: {componentId / pageId}, hasn't been found in db.");
                    // return StatusCode((int) HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = componentId / pageId,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a WebPageComponent failed on save.");
                }
                var objWebPageComponentById = await GetWebPageComponentById(webPageComponentRepo.WebComponentId, webPageComponentRepo.WebPageId);

                var contactFromRepo2 = CreatedAtAction(nameof(GetWebPageComponentById), new { componentId = webPageComponentRepo.WebComponentId, pageId = webPageComponentRepo.WebPageId }, webPageComponentRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebPageComponentDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = "Saved Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };

                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }
        }

    /// <summary>
    /// DELETE api/WebPageComponent/DeleteWebPageComponent/5/1
    /// </summary>
    /// <param name="componentId"></param>
    /// <param name="pageId"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanDeletePageComponent")]
    [HttpDelete("DeleteWebPageComponent/{componentId}/{pageId}", Name = "DeleteWebPageComponent")]
        public async Task<IActionResult> DeleteWebPageComponent(short componentId, short pageId)
        {
            try
            {
                ResponseObject resObject;
                var entity = _webPageComponentRepository.GetWebPageComponentByIdAsNoTracking(componentId, pageId);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {componentId / pageId}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = componentId / pageId,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                _webPageComponentRepository.DeleteWebPageComponent(componentId, pageId);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a WebPageComponent failed on save.");
                }
                resObject = new ResponseObject
                {
                    Message = "Deleted Successfully",
                    Status = "success",
                    Data = componentId / pageId,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process Delete Request";
                _logger.LogError(msg);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };

                return Json(resObject);
            }
        }

    [Authorize(Policy = "CanViewPageComponent")]
    [HttpGet("WebPageComponentExists/{componentId}/{pageId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> WebPageComponentExists(short componentId, short pageId) // ---------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("IsWebPageTitleExisting");
                var WebPageFromRepo = await _webPageComponentRepository.WebPageComponentExists(componentId, pageId) ? true : false;
                resObject = new ResponseObject { Message = WebPageFromRepo == true ? "Data Found" : "Not Found", Status = WebPageFromRepo == false ? "success" : "error", Data = WebPageFromRepo, HttpStatusCode = WebPageFromRepo == true ? HttpStatusCode.OK : HttpStatusCode.NotFound };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process WebPageComponentExists Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }
    }
}
