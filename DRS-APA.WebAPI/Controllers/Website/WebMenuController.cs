
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DRS.APA.Core.Repositories;
using Microsoft.AspNetCore.Http;
using DRS.APA.API.Helpers;
using Microsoft.Extensions.Logging;
using System.Net;
using System.Linq;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using Microsoft.Data.SqlClient;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Repositories.Website;
using DRS.APA.Microservice.API.Controllers.Common;
using Microsoft.AspNetCore.Authorization;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers
{
    [Route("api/DRS.APA/website/[controller]")]
    [SwaggerGroup("Website")]
    public class WebMenuController : Controller
    {
        private readonly IWebMenuRepository _webMenuRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        //const int maxPageSize = 20;
        protected readonly LinkGenerator _linkGenerator;

        public WebMenuController(
            IWebMenuRepository WebMenuRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            ILogger<WebMenuController> logger,
            LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _webMenuRepository = WebMenuRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }
    /// <summary>
    /// List all WebMenu sorted by Id.
    /// </summary>
    /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
    [Authorize(Policy = "CanViewWebMenu")]
    [HttpGet("{page}/{pageSize}", Name = "GetWebMenu")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebMenu(string sortOrder, string searchString, short? webPageId,
            int? page = 1, int? pageSize = ConstantProps.maxPageSize)
        {
            try
            {
                ResponseObject resObject;
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");

                    var dataFromRepo = await _webMenuRepository.GetAllWebMenus(false);
                    var dataList = _mapper.Map<IEnumerable<WebMenuDto>>(dataFromRepo);
                    resObject = new ResponseObject { Message =ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                    return Json(resObject);
                }

                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
               
                IQueryable<WebMenu> data = null;
                data = await _webMenuRepository.GetAllWebMenus(true);

                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    data = data.Where(s => s.WebMenuTitle.Contains(searchString));
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                if (webPageId != null && webPageId > 0)
                {
                    data = data.Where(s => s.WebPageId == webPageId);
                }
                
                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();

                var pagedData = _mapper.Map<IEnumerable<WebMenuDto>>(items);
                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetWebMenu");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

    /// <summary>
    /// Get WebMenu By Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanViewWebMenu")]
    [HttpGet("{id}", Name = "GetWebMenuById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebMenuById(short id)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("GetWebMenuById");
                var dataFromRepo = await _webMenuRepository.GetWebMenuById(id);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebMenuById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                var data = _mapper.Map<WebMenuDto>(dataFromRepo);
                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetWebMenuById");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }
    /// <summary>
    /// AddWebMenu - POST api/WebMenu
    /// </summary>
    /// <param name="WebMenu"></param>
    /// <returns></returns>

    [Authorize(Policy = "CanCreateWebMenu")]
    [HttpPost(Name = "AddWebMenu")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddWebMenu([FromBody] WebMenuDto WebMenu)  // ---------------------
        {
            try
            {
                ResponseObject resObject;
                if (WebMenu == null)
                {                   
                    _logger.LogError(ConstantProps.NullObjectError("WebMenu"));
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (WebMenuDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidObjectError("WebMenu"));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var WebMenuRepo = _mapper.Map<WebMenu>(WebMenu);
                await _webMenuRepository.AddWebMenu(WebMenuRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "WebMenu {ID} Created", WebMenuRepo.WebMenuId);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Adding", "WebMenu"));
                }
                var contactFromRepo2 = CreatedAtAction(nameof(GetWebMenuById), new { id = WebMenuRepo.WebMenuId }, WebMenuRepo);
                var getWebMenuById = await GetWebMenuById(WebMenuRepo.WebMenuId);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebMenuDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SubmittedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Create");
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject { Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message2 = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

    // PUT api/WebMenu/5
    [Authorize(Policy = "CanUpdateWebMenu")]
    [HttpPut("{id}", Name = "UpdateWebMenu")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateWebMenu(short id, [FromBody] WebMenuDto WebMenu) // ------------
        {
            try
            {
                ResponseObject resObject;
                if (WebMenu == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = (WebMenuDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidDataModelText);
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var WebMenuRepo = _mapper.Map<WebMenu>(WebMenu);
                if (!_webMenuRepository.UpdateWebMenuOk(id, WebMenuRepo))
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Updating", "WebMenu"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetWebMenuById), new { id = WebMenuRepo.WebMenuId }, WebMenuRepo);
                var getWebMenuById = await GetWebMenuById(WebMenuRepo.WebMenuId);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebMenuDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SavedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message2= msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }
    /// <summary>
    /// DELETE api/WebMenu/5
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanDeleteWebMenu")]
    [HttpDelete("{id}", Name = "DeleteWebMenu")]
        public async Task<IActionResult> DeleteWebMenu(short id) // -----------------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                var entity = _webMenuRepository.GetWebMenuByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                _webMenuRepository.DeleteWebMenu(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Deleting", "WebMenu"));
                }
                resObject = new ResponseObject
                {
                    Message = ConstantProps.DeletedSuccessfullyText,
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Delete");
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }
    }
}
