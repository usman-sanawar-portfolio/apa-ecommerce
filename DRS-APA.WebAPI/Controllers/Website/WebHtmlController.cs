﻿
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
//using System.Data.Entity.Validation;
using System.Threading.Tasks;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain;
using DRS.APA.Domain.Core.Repositories;
using Microsoft.AspNetCore.Http;
using DRS.APA.API.Helpers;
using Microsoft.Extensions.Logging;
using DRS.APA.Domain.Core;
using System.Net;
using System.Linq;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using Microsoft.Data.SqlClient;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Domain.Core.Repositories.Website;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers
{
    [Route("api/DRS.APA/website/[controller]")]
    [SwaggerGroup("Website")]
    public class WebHtmlController : Controller
    {
        private readonly IWebHtmlRepository _WebHtmlRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        const int maxPageSize = 20;
        protected readonly LinkGenerator _linkGenerator;

        public WebHtmlController(
            IWebHtmlRepository WebHtmlRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            ILogger<WebHtmlController> logger,
            LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _WebHtmlRepository = WebHtmlRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }


        /// <summary>
        /// List all WebHtml sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetWebHtml")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebHtml(string sortOrder, int? page = 1, int? pageSize = maxPageSize)
        {
            try
            {
                ResponseObject resObject;
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");

                    var dataFromRepo = await _WebHtmlRepository.GetAllWebHtmls(false);
                    var dataList = _mapper.Map<IEnumerable<WebHtmlDto>>(dataFromRepo);
                    resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                    return Json(resObject);
                }

                // Sorting -------
                HttpContext.Session.SetString("HeadingSortParam", String.IsNullOrEmpty(sortOrder) ? "heading_desc" : "");
                HttpContext.Session.SetString("HeadingSortParam", String.IsNullOrEmpty(sortOrder) ? "heading_asc" : "");
                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_asc" : "");
                HttpContext.Session.SetString("IdSortParam", String.IsNullOrEmpty(sortOrder) ? "id_desc" : "");
                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                //if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                //{
                //    page = 1;
                //}
                //else
                //{
                //    // searchString = currentFilter;
                //}
                IQueryable<WebHtml> data = null;
                //var filterSpecification = new WebHtmlFilterSpecification(contactId, leadId);
                data = await _WebHtmlRepository.GetAllWebHtmls(true);

                //search
                //if (!String.IsNullOrEmpty(searchString))
                //{
                //    HttpContext.Session.SetString("searchString", searchString);
                //    //data = data.Where(s => s.MainHeading.Contains(searchString) || s.SubHeading.Contains(searchString));
                //}
                //else
                //{
                //    HttpContext.Session.SetString("searchString", string.Empty);
                //}
                HttpContext.Session.SetString("searchString", string.Empty);
                //Sort Order
                switch (sortOrder)
                {
                   
                    case "id_asc":
                        data = data.OrderByDescending(s => s.WebHtmlId);
                        break;
                    case "id_desc":
                        data = data.OrderByDescending(s => s.WebHtmlId);
                        break;
                    default:
                        data = data.OrderBy(s => s.Order);
                        break;
                }
                //
                // ensure the page size isn't larger than the maximum.
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                // calculate data for metadata
                var totalCount = await data.CountAsync();
                var totalPages = (int)Math.Ceiling((double)totalCount / (int)pageSize);

                var prevLink = page > 1 ? _linkGenerator.GetPathByAction(HttpContext, "GetWebHtmls", values: new
                {
                    page = page - 1,
                    pageSize = pageSize

                }) : "";

                var nextLink = page < totalPages ? _linkGenerator.GetPathByAction(HttpContext, "GetWebHtmls", values: new
                {
                    page = page + 1,
                    pageSize = pageSize

                }) : "";

                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLink = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Response.Headers.Add("X-Pagination",
                   Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();

                var pagedData = _mapper.Map<IEnumerable<WebHtmlDto>>(items);
                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetWebHtml Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

        /// <summary>
        /// Get WebHtmls By ComponentId
        /// </summary>
        /// <param name="componentId"></param>
        /// <returns></returns>
        [HttpGet("GetWebHtmlByComponentId/{componentId}", Name = "GetWebHtmlByComponentId")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebHtmlByComponentId(short componentId)
        {
            try
            {
                _logger.LogInformation("Generating List");
                ResponseObject resObject;
                var dataFromRepo = await _WebHtmlRepository.GetWebHtmlByComponentId(componentId);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebHtmlByComponentId({ID}) NOT FOUND", componentId);
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = componentId, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
                var dataList = _mapper.Map<IEnumerable<WebHtmlDto>>(dataFromRepo);
                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetWebHtmlByComponentId Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }
        /// <summary>
        /// Get WebHtml By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetWebHtmlById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebHtmlById(short id)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("GetWebHtmlById");
                var dataFromRepo = await _WebHtmlRepository.GetWebHtmlById(id);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebHtmlById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                var data = _mapper.Map<WebHtmlDto>(dataFromRepo);
                resObject = new ResponseObject { Message = "Data List!", Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process GetWebHtmlById Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }
        /// <summary>
        /// AddWebHtml - POST api/WebHtml
        /// </summary>
        /// <param name="WebHtml"></param>
        /// <returns></returns>

        [HttpPost(Name = "AddWebHtml")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddWebHtml([FromBody] WebHtmlDto WebHtml)  // ---------------------
        {
            try
            {
                ResponseObject resObject;
                if (WebHtml == null)
                {
                    _logger.LogError("WebHtml object sent from client is null.");
                    resObject = new ResponseObject { Message = "Data not found, BadRequest", Status = "error", Data = (WebHtmlDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid WebHtml object sent from client.");
                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model!",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var WebHtmlRepo = _mapper.Map<WebHtml>(WebHtml);
                await _WebHtmlRepository.AddWebHtml(WebHtmlRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "WebHtml {ID} Created", WebHtmlRepo.WebHtmlId);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Adding a WebHtml failed on save.");
                }
                var contactFromRepo2 = CreatedAtAction(nameof(GetWebHtmlById), new { id = WebHtmlRepo.WebHtmlId }, WebHtmlRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebHtmlDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = "Submitted Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = "Sever Error, Unable to Process Create Request";
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject { Message = BuiltMessage(e, LoggingEvents.GetItem), Message2 = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }
        private string BuiltMessage(Exception e, int eventtype)
        {

            if (e.InnerException != null)
            {
                if (!string.IsNullOrWhiteSpace(e.InnerException.Message))
                {
                    //Changes-Babar:
                    if (e.InnerException is SqlException ex)
                    {
                        if (ex.Number == 2627 || ex.Number == 2601 || ex.Message.ToLower().Contains("duplicate key"))
                        {
                            return "Unable to process, duplicate data.";
                        }
                    }
                    //Changes-Babar:

                    if (e.InnerException.InnerException != null)
                        if (!string.IsNullOrWhiteSpace(e.InnerException.InnerException.Message))
                        {
                            return eventtype.ToString() + " >> " + e.InnerException.InnerException.Message + " >> " + e.Message + ", " + e.InnerException.InnerException.Message;
                        }
                }
                return eventtype.ToString() + " >> " + e.Message + ", " + e.InnerException.Message;
            }
            else
            {
                return eventtype.ToString() + " >> " + e.Message;
            }
        }

        // PUT api/WebHtml/5
        [HttpPut("{id}", Name = "UpdateWebHtml")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateWebHtml(short id, [FromBody] WebHtmlDto WebHtml) // ------------
        {
            try
            {
                ResponseObject resObject;
                if (WebHtml == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = "Data not Found, BadRequest",
                        Status = "error",
                        Data = (WebHtmlDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Data");
                    resObject = new ResponseObject
                    {
                        Message = "Invalid Data/Model, BadRequest",
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var WebHtmlRepo = _mapper.Map<WebHtml>(WebHtml);
                if (!_WebHtmlRepository.UpdateWebHtmlOk(id, WebHtmlRepo))
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    // return StatusCode((int) HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Updating a WebHtml failed on save.");
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetWebHtmlById), new { id = WebHtmlRepo.WebHtmlId }, WebHtmlRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebHtmlDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = "Saved Successfully",
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Unable to Process PUT Request >> " + BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }
        /// <summary>
        /// DELETE api/WebHtml/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}", Name = "DeleteWebHtml")]
        public async Task<IActionResult> DeleteWebHtml(short id) // -----------------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                var entity = _WebHtmlRepository.GetWebHtmlByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError($"entity with id: {id}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = "Data not found, BadRequest",
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                _WebHtmlRepository.DeleteWebHtml(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception("Deleting a WebHtml failed on save.");
                }
                resObject = new ResponseObject
                {
                    Message = "Deleted Successfully",
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = "Internal Server Error, Unable to Process Delete Request";
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }        
    }
}
