﻿
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DRS.APA.Core.Repositories;
using Microsoft.AspNetCore.Http;
using DRS.APA.API.Helpers;
using Microsoft.Extensions.Logging;
using System.Net;
using System.Linq;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using Microsoft.Data.SqlClient;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Repositories.Website;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microservice.API.Controllers.Common;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers
{
    [Route("api/DRS.APA/website/[controller]")]
    [SwaggerGroup("Website")]
    public class WebSubMenuController : Controller
    {
        private readonly IWebSubMenuRepository _webMenuRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        //const int maxPageSize = 20;
        protected readonly LinkGenerator _linkGenerator;

        public WebSubMenuController(
            IWebSubMenuRepository WebSubMenuRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            ILogger<WebSubMenuController> logger,
            LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _webMenuRepository = WebSubMenuRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }


        /// <summary>
        /// List all WebSubMenu sorted by Id.
        /// </summary>
        /// <remarks>With pagination, you can fetch up to 20 records per page.</remarks>
        [HttpGet("{page}/{pageSize}", Name = "GetWebSubMenu")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebSubMenu(string sortOrder, string searchString,
            short? webMenuId, short? webPageId,
            int? page = 1, int? pageSize = ConstantProps.maxPageSize)
        {
            try
            {
                ResponseObject resObject;
                if (!page.HasValue || page.Value == 0)
                {
                    //_logger.LogInformation("Generating List");
                    var dataFromRepo = await _webMenuRepository.GetAllWebSubMenus(false);
                    var dataList = _mapper.Map<IEnumerable<WebSubMenuDto>>(dataFromRepo);
                    resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                    return Json(resObject);
                }

                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                
                IQueryable<WebSubMenu> data = null;
                data = await _webMenuRepository.GetAllWebSubMenus(true);

                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    data = data.Where(s => s.WebSubMenuTitle.Contains(searchString));
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                if (webMenuId != null && webMenuId > 0)
                {
                    data = data.Where(s => s.WebMenuId == webMenuId);
                }
                if (webPageId != null && webPageId > 0)
                {
                    data = data.Where(s => s.WebPageId == webPageId);
                }

                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();

                var pagedData = _mapper.Map<IEnumerable<WebSubMenuDto>>(items);
                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetWebSubMenu");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

        /// <summary>
        /// Get WebSubMenu By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetWebSubMenuById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetWebSubMenuById(short id)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("GetWebSubMenuById");
                var dataFromRepo = await _webMenuRepository.GetWebSubMenuById(id);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetWebSubMenuById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                var data = _mapper.Map<WebSubMenuDto>(dataFromRepo);
                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetWebSubMenuById");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }
        /// <summary>
        /// AddWebSubMenu - POST api/WebSubMenu
        /// </summary>
        /// <param name="WebSubMenu"></param>
        /// <returns></returns>

        [HttpPost(Name = "AddWebSubMenu")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddWebSubMenu([FromBody] WebSubMenuDto WebSubMenu)  // ---------------------
        {
            try
            {
                ResponseObject resObject;
                if (WebSubMenu == null)
                {
                    _logger.LogError(ConstantProps.NullObjectError("WebSubMenu"));
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (WebSubMenuDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidObjectError("WebSubMenu"));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var WebSubMenuRepo = _mapper.Map<WebSubMenu>(WebSubMenu);
                await _webMenuRepository.AddWebSubMenu(WebSubMenuRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "WebSubMenu {ID} Created", WebSubMenuRepo.WebSubMenuId);

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Adding", "WebSubMenu"));
                }
                var contactFromRepo2 = CreatedAtAction(nameof(GetWebSubMenuById), new { id = WebSubMenuRepo.WebSubMenuId }, WebSubMenuRepo);
                var getWebSubMenuById = await GetWebSubMenuById(WebSubMenuRepo.WebSubMenuId);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebSubMenuDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SubmittedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Create");
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject { Message= BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message2 = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

        // PUT api/WebSubMenu/5
        [HttpPut("{id}", Name = "UpdateWebSubMenu")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateWebSubMenu(short id, [FromBody] WebSubMenuDto WebSubMenu) // ------------
        {
            try
            {
                ResponseObject resObject;
                if (WebSubMenu == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = (WebSubMenuDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidDataModelText);
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var WebSubMenuRepo = _mapper.Map<WebSubMenu>(WebSubMenu);
                if (!_webMenuRepository.UpdateWebSubMenuOk(id, WebSubMenuRepo))
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    // return StatusCode((int) HttpStatusCode.NotFound, $"entity with id: {id}, hasn't been found in db.");
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Updating", "WebSubMenu"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetWebSubMenuById), new { id = WebSubMenuRepo.WebSubMenuId }, WebSubMenuRepo);
                var getWebSubMenuById = await GetWebSubMenuById(WebSubMenuRepo.WebSubMenuId);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<WebSubMenuDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SavedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }
        /// <summary>
        /// DELETE api/WebSubMenu/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}", Name = "DeleteWebSubMenu")]
        public async Task<IActionResult> DeleteWebSubMenu(short id)
        {
            try
            {
                ResponseObject resObject;
                var entity = _webMenuRepository.GetWebSubMenuByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                _webMenuRepository.DeleteWebSubMenu(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Deleting", "WebSubMenu"));
                }
                resObject = new ResponseObject
                {
                    Message = ConstantProps.DeletedSuccessfullyText,
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Delete");
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }
    }
}
