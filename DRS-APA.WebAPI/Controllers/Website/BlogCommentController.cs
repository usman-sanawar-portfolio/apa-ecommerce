using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using DRS.APA.Microservice.API.Controllers.Common;
using DRS.APA.Domain.Core.Repositories.Website;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website;
using Microsoft.AspNetCore.Authorization;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.Website
{
    [Route("api/DRS.APA/website/[controller]")]
    [SwaggerGroup("website")]
    public class BlogCommentController : Controller
    {
        private readonly IBlogCommentRepository _blogCommentRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        protected readonly LinkGenerator _linkGenerator;

        public BlogCommentController(IBlogCommentRepository blogCommentRepository,
        IUnitOfWork unitOfWork, IMapper mapper, ILogger<BlogCommentController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _blogCommentRepository = blogCommentRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }

        /// <summary>
        /// List all BlogComment, by default sorted by Comment
        /// </summary>
        /// <remarks>With pagination, you can fetch up to x records per page.</remarks>
        [Authorize(Policy = "CanViewBlogComment")]
        [HttpGet("{page}/{pageSize}", Name = "GetBlogComments")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetBlogComments(
            string sortOrder, int? BlogCommentId, string Comment, int? BlogId,
            string searchString, int? page = 1, int? pageSize = ConstantProps.maxPageSize)
        {
            try
            {
                ResponseObject resObject;
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var dataFromRepo = await _blogCommentRepository.GetAllBlogComments();
                    var dataList = _mapper.Map<IEnumerable<BlogCommentDto>>(dataFromRepo);
                    if (BlogCommentId.HasValue)
                    {
                        dataFromRepo = dataFromRepo.Where(x => x.BlogCommentId == BlogCommentId);
                    }
                    if (Comment != null)
                    {
                        dataFromRepo = dataFromRepo.Where(x => x.Comment == Comment);
                    }
                    if (BlogId.HasValue)
                    {
                        dataFromRepo = dataFromRepo.Where(x => x.BlogId == BlogId);
                    }
                    resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                    return Json(resObject);
                }

                // Sorting -------

                HttpContext.Session.SetString("CommentSortParam", String.IsNullOrEmpty(sortOrder) ? "Comment_desc" : "");
                HttpContext.Session.SetString("CommentSortParam", String.IsNullOrEmpty(sortOrder) ? "Comment_asc" : "");

                HttpContext.Session.SetString("BlogCommentIdSortParam", String.IsNullOrEmpty(sortOrder) ? "blogCommentId_asc" : "");
                HttpContext.Session.SetString("BlogCommentIdSortParam", String.IsNullOrEmpty(sortOrder) ? "blogCommentId_desc" : "");

                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
                else
                {
                    // searchString = currentFilter;
                }
                IQueryable<BlogComment> data = null;
                data = await _blogCommentRepository.GetAllBlogComments(true);
                if (BlogCommentId.HasValue)
                {
                    data = data.Where(x => x.BlogCommentId == BlogCommentId);
                }
                if (Comment != null)
                {
                    data = data.Where(x => x.Comment == Comment);
                }
                if (BlogId.HasValue)
                {
                    data = data.Where(x => x.BlogId == BlogId);
                }
                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    data = data.Where(s => s.Comment.Contains(searchString));
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "Comment_asc":
                        data = data.OrderBy(s => s.Comment);
                        break;
                    case "Comment_desc":
                        data = data.OrderByDescending(s => s.Comment);
                        break;
                    case "blogCommentId_asc":
                        data = data.OrderBy(s => s.BlogCommentId);
                        break;
                    case "blogCommentId_desc":
                        data = data.OrderByDescending(s => s.BlogCommentId);
                        break;
                    default:
                        data = data.OrderBy(s => s.BlogCommentId);
                        break;
                }
                var totalCount = await data.CountAsync();
                PaggingAndSorting paggingSorting = new PaggingAndSorting(_linkGenerator);
                paggingSorting.PaggingAndSortingData(HttpContext, totalCount, "GetBlogComment", page, pageSize);
                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();
                var pagedData = _mapper.Map<IEnumerable<BlogCommentDto>>(items);
                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetBlogComment");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

        /// <summary>
        /// Get BlogComment By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Policy = "CanViewBlogComment")]
        [HttpGet("{id}", Name = "GetBlogCommentById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetBlogCommentById(int id)  // --------------------------------------------------
        {
            try
            {

                ResponseObject resObject;

                _logger.LogInformation("GetBlogCommentById");
                var dataFromRepo = await _blogCommentRepository.GetBlogCommentById(id);

                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetBlogCommentById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                var data = _mapper.Map<BlogCommentDto>(dataFromRepo);

                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetBlogCommentById");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }

        /// <summary>
        /// Add BlogComment - POST api/BlogComment
        /// </summary>
        /// <param name="blogComment"></param>
        /// <returns></returns>
        [Authorize(Policy = "CanCreateBlogComment")]
        [HttpPost(Name = "AddBlogComment")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddBlogComment([FromBody] BlogCommentDto blogComment)
        {
            try
            {
                ResponseObject resObject;
                if (blogComment == null)
                {
                    _logger.LogError(ConstantProps.NullObjectError("BlogComment"));
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (BlogCommentDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidObjectError("BlogComment"));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var blogCommentRepo = _mapper.Map<BlogComment>(blogComment);
                await _blogCommentRepository.AddBlogComment(blogCommentRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "BlogComment {ID} Created", blogCommentRepo.BlogCommentId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Adding", "BlogComment"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetBlogCommentById), new { id = blogCommentRepo.BlogCommentId }, blogCommentRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<BlogCommentDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SubmittedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Create");
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject { Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message2 = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }

        // PUT api/BlogComment/5
        [Authorize(Policy = "CanUpdateBlogComment")]
        [HttpPut("{id}", Name = "UpdateBlogComment")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateBlogComment(int id, [FromBody] BlogCommentDto blogComment)
        {
            try
            {
                ResponseObject resObject;
                if (blogComment == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = (BlogCommentDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidDataModelText);
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var blogCommentRepo = _mapper.Map<BlogComment>(blogComment);
                if (!_blogCommentRepository.UpdateBlogCommentOk(id, blogCommentRepo))
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Updating", "BlogComment"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetBlogCommentById), new { id = blogCommentRepo.BlogCommentId }, blogCommentRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<BlogCommentDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SavedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

        /// <summary>
        /// DELETE api/BlogComment/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Policy = "CanDeleteBlogComment")]

        [HttpDelete("{id}", Name = "DeleteBlogComment")]
        public async Task<IActionResult> DeleteBlogComment(int id)
        {
            try
            {
                ResponseObject resObject;
                var entity = _blogCommentRepository.GetBlogCommentByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                _blogCommentRepository.DeleteBlogComment(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Deleting", "BlogComment"));
                }
                resObject = new ResponseObject
                {
                    Message = ConstantProps.DeletedSuccessfullyText,
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Delete");
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }


    }
}
