﻿using System.Threading.Tasks;
using DRS.APA.Core.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ServiceManagement.API.Controllers
{
    [Route("api/hrm/companies/{companyId}/[controller]")]
    public class DashboardsController : Controller
    {
        public readonly IUnitOfWork _unitOfWork;
        public readonly IDashBoardRepository _dashboard;

        public DashboardsController(IDashBoardRepository dashboard, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _dashboard = dashboard;
        }
        [HttpGet(Name = "GetSummary")]
        public async Task<IActionResult> GetSummary(int companyId)
        {
            var applicantsFromRepo = await _dashboard.GetSummary(companyId);
            return Ok(applicantsFromRepo);
        }
        [HttpGet("LeaveRequests",Name = "GetLeaveRequestsSummary")]
        public async Task<IActionResult> GetLeaveRequestsSummary(int companyId)
        {
            var applicantsFromRepo = await _dashboard.GetLeaveRequestsSummary(companyId);
            return Ok(applicantsFromRepo);
        }
        [HttpGet("AdvanceRequests", Name = "GetAdvanceRequestsSummary")]
        public async Task<IActionResult> GetAdvanceRequestsSummary(int companyId)
        {
            var applicantsFromRepo = await _dashboard.GetAdvanceRequestsSummary(companyId);
            return Ok(applicantsFromRepo);
        }
        [HttpGet("UpcomingEvents", Name = "GetUpcomingEvents")]
        public async Task<IActionResult> GetUpcomingEvents(int companyId)
        {
            var applicantsFromRepo = await _dashboard.UpcomingEventsSummary(companyId);
            return Ok(applicantsFromRepo);
        }
    }
}