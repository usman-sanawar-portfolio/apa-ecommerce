using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories; 
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using DRS.APA.Microservice.API.Controllers.Common;
using DRS.APA.Microserivce.Domain.Core.Repositories.Shipment;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using DRS.APA.Microserivce.Domain.Core.Dtos.Shipment;
using Microsoft.AspNetCore.Authorization;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.Microservice.API.Controllers.Shipment
{
  [Route("api/DRS.APA/Shipment/[controller]")]
  [SwaggerGroup("Shipment")]
  public class BarriersPerRackController : Controller
  {
    private readonly IBarriersPerRackRepository _barriersPerRackRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly ILogger _logger;
    protected readonly LinkGenerator _linkGenerator;

    public BarriersPerRackController(IBarriersPerRackRepository barriersPerRackRepository,
    IUnitOfWork unitOfWork, IMapper mapper, ILogger<BarriersPerRackController> logger, LinkGenerator linkGenerator)
    {
      _unitOfWork = unitOfWork;
      _barriersPerRackRepository = barriersPerRackRepository;
      _mapper = mapper;
      _logger = logger;
      _linkGenerator = linkGenerator;
    }

    /// <summary>
    /// List all BarriersPerRack, by default sorted by BarriersPerRackCost
    /// </summary>
    /// <remarks>With pagination, you can fetch up to x records per page.</remarks>
    [AllowAnonymous]
    [HttpGet("{page}/{pageSize}", Name = "GetBarriersPerRack")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetBarriersPerRack(
        string sortOrder, int? BarrierPerRackQty, int? BarriersPerRackId, 
        string searchString, int? page = 1, int? pageSize = ConstantProps.maxPageSize)
    {
      try
      {
        ResponseObject resObject;
        if (!page.HasValue || page.Value == 0)
        {
          _logger.LogInformation("Generating List");
          var dataFromRepo = await _barriersPerRackRepository.GetAllBarriersPerRack();
          var dataList = _mapper.Map<IEnumerable<BarriersPerRackDto>>(dataFromRepo);
          if (BarriersPerRackId.HasValue)
          {
            dataFromRepo = dataFromRepo.Where(x => x.BarriersPerRackId == BarriersPerRackId);
          }
          if (BarrierPerRackQty.HasValue)
          {
            dataFromRepo = dataFromRepo.Where(x => x.BarrierPerRackQty == BarrierPerRackQty);
          } 
          resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
          return Json(resObject);
        }

        // Sorting -------
        HttpContext.Session.SetString("BarriersPerRackIdSortParam", String.IsNullOrEmpty(sortOrder) ? "BarriersPerRackId_desc" : "");
        HttpContext.Session.SetString("BarriersPerRackIdSortParam", String.IsNullOrEmpty(sortOrder) ? "BarriersPerRackId_asc" : "");

        HttpContext.Session.SetString("BarrierPerRackQtySortParam", String.IsNullOrEmpty(sortOrder) ? "BarrierPerRackQty_desc" : "");
        HttpContext.Session.SetString("BarrierPerRackQtySortParam", String.IsNullOrEmpty(sortOrder) ? "BarrierPerRackQty_asc" : "");
 
        //search string check
        var previousSearchString = HttpContext.Session.GetString("searchString");

        if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
        {
          page = 1;
        }
        else
        {
          // searchString = currentFilter;
        }
        IQueryable<BarriersPerRack> data = null;
        data = await _barriersPerRackRepository.GetAllBarriersPerRack(true);
        if (BarriersPerRackId.HasValue)
        {
          data = data.Where(x => x.BarriersPerRackId == BarriersPerRackId);
        }
        if (BarrierPerRackQty.HasValue)
        {
          data = data.Where(x => x.BarrierPerRackQty == BarrierPerRackQty);
         } 

        //search
        if (!String.IsNullOrEmpty(searchString))
        {

          HttpContext.Session.SetString("searchString", searchString);
          data = data.Where(s => s.BarrierPerRackQty.ToString().Contains(searchString));
        }

        else
        {
          HttpContext.Session.SetString("searchString", string.Empty);
        }
        //Sort Order
        switch (sortOrder)
        {
          case "BarrierPerRackQty_desc":
            data = data.OrderByDescending(s => s.BarrierPerRackQty);
            break;
          case "BarrierPerRackQty_asc":
            data = data.OrderBy(s => s.BarrierPerRackQty);
            break; 
          case "BarriersPerRackId_asc":
            data = data.OrderBy(s => s.BarriersPerRackId);
            break;
          case "BarriersPerRackId_desc":
            data = data.OrderByDescending(s => s.BarriersPerRackId);
            break;
          default:
            data = data.OrderBy(s => s.BarriersPerRackId);
            break;
        }
        var totalCount = await data.CountAsync();
        PaggingAndSorting paggingSorting = new PaggingAndSorting(_linkGenerator);
        paggingSorting.PaggingAndSortingData(HttpContext, totalCount, "GetBarriersPerRack", page, pageSize);

        // return result
        var items = await data
            .Skip((int)pageSize * ((int)page - 1))
            .Take((int)pageSize)
            .ToListAsync();

        var pagedData = _mapper.Map<IEnumerable<BarriersPerRackDto>>(items);
        resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
        return Json(resObject);
      }
      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("GetBarriersPerRack");
        _logger.LogError(msg);
        var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
        return Json(resObject);
      }
    }

    /// <summary>
    /// Get BarriersPerRack By Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet("{id}", Name = "GetBarriersPerRackById")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetBarriersPerRackById(int id)  // --------------------------------------------------
    {
      try
      {
        ResponseObject resObject;
        _logger.LogInformation("GetBarriersPerRackById");
        var dataFromRepo = await _barriersPerRackRepository.GetBarriersPerRackById(id);

        if (dataFromRepo == null)
        {
          _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetBarriersPerRackById({ID}) NOT FOUND", id);
          resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
          return Json(resObject);
        }
        var data = _mapper.Map<BarriersPerRackDto>(dataFromRepo);

        resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
        return Json(resObject);
      }
      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("GetBarriersPerRackById");
        _logger.LogError(msg);
        var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
        return Json(resObject);
      }
    }
    /// <summary>
    /// Add BarriersPerRack - POST api/BarriersPerRack
    /// </summary>
    /// <param name="barriersPerRack"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanCreateBarrierPerRack")]
    [HttpPost(Name = "AddBarriersPerRack")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> AddBarriersPerRack([FromBody] BarriersPerRackDto barriersPerRack)
    {
      try
      {
        ResponseObject resObject;
        if (barriersPerRack == null)
        {
          _logger.LogError(ConstantProps.NullObjectError("BarriersPerRack"));
          resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (BarriersPerRackDto)null, HttpStatusCode = HttpStatusCode.NotFound };
          return Json(resObject);
        }

        if (!ModelState.IsValid)
        {
          _logger.LogError(ConstantProps.InvalidObjectError("BarriersPerRack"));
          resObject = new ResponseObject
          {
            Message = ConstantProps.InvalidDataModelText,
            Status = "error",
            Data = ModelState,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }
        var barriersPerRackRepo = _mapper.Map<BarriersPerRack>(barriersPerRack);
        await _barriersPerRackRepository.AddBarriersPerRack(barriersPerRackRepo);
        _logger.LogInformation(LoggingEvents.InsertItem, "BarriersPerRack {ID} Created", barriersPerRackRepo.BarriersPerRackId);
        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Adding", "BarriersPerRack"));
        }

        var contactFromRepo2 = CreatedAtAction(nameof(GetBarriersPerRackById), new { id = barriersPerRackRepo.BarriersPerRackId }, barriersPerRackRepo);
        var contactFromRepo = contactFromRepo2.Value;
        var data = _mapper.Map<BarriersPerRackDto>(contactFromRepo);
        resObject = new ResponseObject
        {
          Message = ConstantProps.SubmittedSuccessfullyText,
          Status = "success",
          Data = data,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("Create");
        _logger.LogWarning(e.Message);
        var resObject = new ResponseObject
        {
          Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
          Message2 = msg,
          Status = "error",
          HttpStatusCode = HttpStatusCode.InternalServerError
        };
        return Json(resObject);
      }
    }

    // PUT api/BarriersPerRack/5
    [Authorize(Policy = "CanUpdateBarrierPerRack")]
    [HttpPut("{id}", Name = "UpdateBarriersPerRack")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> UpdateBarriersPerRack(int id, [FromBody] BarriersPerRackDto barriersPerRack)
    {
      try
      {
        ResponseObject resObject;
        if (barriersPerRack == null)
        {
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = (BarriersPerRackDto)null,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }

        if (!ModelState.IsValid)
        {
          _logger.LogError(ConstantProps.InvalidDataModelText);
          resObject = new ResponseObject
          {
            Message = ConstantProps.InvalidDataModelText,
            Status = "error",
            Data = ModelState,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }
        var barriersPerRackRepo = _mapper.Map<BarriersPerRack>(barriersPerRack);
        if (!_barriersPerRackRepository.UpdateBarriersPerRackOk(id, barriersPerRackRepo))
        {
          _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = id,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }

        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Updating", "BarriersPerRack"));
        }

        var contactFromRepo2 = CreatedAtAction(nameof(GetBarriersPerRackById), new { id = barriersPerRackRepo.BarriersPerRackId }, barriersPerRackRepo);
        var contactFromRepo = contactFromRepo2.Value;
        var data = _mapper.Map<BarriersPerRackDto>(contactFromRepo);
        resObject = new ResponseObject
        {
          Message = ConstantProps.SavedSuccessfullyText,
          Status = "success",
          Data = data,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
        _logger.LogWarning(e.Message);

        var resObject = new ResponseObject
        {
          Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
          Message2 = msg,
          Status = "error",
          HttpStatusCode = HttpStatusCode.InternalServerError
        };
        return Json(resObject);
      }
    }

    /// <summary>
    /// DELETE api/BarriersPerRack/5
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanDeleteBarrierPerRack")]
    [HttpDelete("{id}", Name = "DeleteBarriersPerRack")]
    public async Task<IActionResult> DeleteBarriersPerRack(int id)
    {
      try
      {
        ResponseObject resObject;
        var entity = _barriersPerRackRepository.GetBarriersPerRackByIdAsNoTracking(id);
        if (entity == false)
        {
          _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = id,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }
        _barriersPerRackRepository.DeleteBarriersPerRack(id);
        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Deleting", "BarriersPerRack"));
        }
        resObject = new ResponseObject
        {
          Message = ConstantProps.DeletedSuccessfullyText,
          Status = "success",
          Data = id,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("Delete");
        _logger.LogError(msg);
        var resObject = new ResponseObject
        {
          Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
          Message = msg,
          Status = "error",
          HttpStatusCode = HttpStatusCode.InternalServerError
        };
        return Json(resObject);
      }
    }
  }
}

