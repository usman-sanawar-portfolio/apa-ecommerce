using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using DRS.APA.Microservice.API.Controllers.Common;
using DRS.APA.Microserivce.Domain.Core.Repositories.Shipment;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using DRS.APA.Microserivce.Domain.Core.Dtos.Shipment;
using Microsoft.AspNetCore.Authorization;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.Microservice.API.Controllers.Shipment
{
  [Route("api/DRS.APA/Shipment/[controller]")]
  [SwaggerGroup("Shipment")]
  public class UK_ExpressDataBagDeliveryController : Controller
  {
    private readonly IUK_ExpressDataBagDeliveryRepository _uk_ExpressDataBagDeliveryRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly ILogger _logger; 
    protected readonly LinkGenerator _linkGenerator;

    public UK_ExpressDataBagDeliveryController(IUK_ExpressDataBagDeliveryRepository uk_ExpressDataBagDeliveryRepository,
    IUnitOfWork unitOfWork, IMapper mapper, ILogger<UK_ExpressDataBagDeliveryController> logger, LinkGenerator linkGenerator)
    {
      _unitOfWork = unitOfWork;
      _uk_ExpressDataBagDeliveryRepository = uk_ExpressDataBagDeliveryRepository;
      _mapper = mapper;
      _logger = logger;
      _linkGenerator = linkGenerator;
    }

    /// <summary>
    /// List all UK_ExpressDataBagDelivery, by default sorted by UK_ExpressDataBagDeliveryDuration
    /// </summary>
    /// <remarks>With pagination, you can fetch up to x records per page.</remarks>
    [AllowAnonymous]
    [HttpGet("{page}/{pageSize}", Name = "GetUK_ExpressDataBagDelivery")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetUK_ExpressDataBagDelivery(
        string sortOrder, string Duration, int? UK_ExpressDataBagDeliveryId, decimal? Price, decimal? AdditionalPricePerKg, int? DataBagStandardWeight,
       string Notes, string searchString, int? page = 1, int? pageSize = ConstantProps.maxPageSize)
    {
      try
      {
        ResponseObject resObject;
        if (!page.HasValue || page.Value == 0)
        {
          _logger.LogInformation("Generating List");
          var dataFromRepo = await _uk_ExpressDataBagDeliveryRepository.GetAllUK_ExpressDataBagDelivery();
          var dataList = _mapper.Map<IEnumerable<UK_ExpressDataBagDeliveryDto>>(dataFromRepo);
          if (UK_ExpressDataBagDeliveryId.HasValue)
          {
            dataFromRepo = dataFromRepo.Where(x => x.UK_ExpressDataBagDeliveryId == UK_ExpressDataBagDeliveryId);
          }
          if (Duration !=null)
          {
            dataFromRepo = dataFromRepo.Where(x => x.Duration == Duration);
          }
          if (Price.HasValue)
          {
            dataFromRepo = dataFromRepo.Where(x => x.Price == Price);
          }
          if (Notes != null)
          {
            dataFromRepo = dataFromRepo.Where(x => x.Notes == Notes);
          }
          if (AdditionalPricePerKg.HasValue)
          {
            dataFromRepo = dataFromRepo.Where(x => x.AdditionalPricePerKg == AdditionalPricePerKg);
          }
          resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
          return Json(resObject);
        }

        // Sorting -------
        HttpContext.Session.SetString("UK_ExpressDataBagDeliveryIdSortParam", String.IsNullOrEmpty(sortOrder) ? "UK_ExpressDataBagDeliveryId_desc" : "");
        HttpContext.Session.SetString("UK_ExpressDataBagDeliveryIdSortParam", String.IsNullOrEmpty(sortOrder) ? "UK_ExpressDataBagDeliveryId_asc" : "");

        HttpContext.Session.SetString("DurationSortParam", String.IsNullOrEmpty(sortOrder) ? "Duration_desc" : "");
        HttpContext.Session.SetString("DurationSortParam", String.IsNullOrEmpty(sortOrder) ? "Duration_asc" : "");

        HttpContext.Session.SetString("PriceSortParam", String.IsNullOrEmpty(sortOrder) ? "Price_desc" : "");
        HttpContext.Session.SetString("PriceSortParam", String.IsNullOrEmpty(sortOrder) ? "Price_asc" : "");

        HttpContext.Session.SetString("AdditionalPricePerKgSortParam", String.IsNullOrEmpty(sortOrder) ? "AdditionalPricePerKg_desc" : "");
        HttpContext.Session.SetString("AdditionalPricePerKgSortParam", String.IsNullOrEmpty(sortOrder) ? "AdditionalPricePerKg_asc" : "");

        HttpContext.Session.SetString("NotesSortParam", String.IsNullOrEmpty(sortOrder) ? "Notes_desc" : "");
        HttpContext.Session.SetString("NotesSortParam", String.IsNullOrEmpty(sortOrder) ? "Notes_asc" : "");
        //search string check
        var previousSearchString = HttpContext.Session.GetString("searchString");

        if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
        {
          page = 1;
        }
        else
        {
          // searchString = currentFilter;
        }
        IQueryable<UK_ExpressDataBagDelivery> data = null;
        data = await _uk_ExpressDataBagDeliveryRepository.GetAllUK_ExpressDataBagDelivery(true);
        if (UK_ExpressDataBagDeliveryId.HasValue)
        {
          data = data.Where(x => x.UK_ExpressDataBagDeliveryId == UK_ExpressDataBagDeliveryId);
        }
        if (Duration!=null)
        {
          data = data.Where(x => x.Duration == Duration);

        }
        if (Price.HasValue)
        {
          data = data.Where(x => x.Price == Price);

        }
        if (Notes != null)
        {
          data = data.Where(x => x.Notes == Notes);

        }
        if (AdditionalPricePerKg != null)
        {
          data = data.Where(x => x.AdditionalPricePerKg == AdditionalPricePerKg);

        }

        //search
        if (!String.IsNullOrEmpty(searchString))
        {

          HttpContext.Session.SetString("searchString", searchString);
          data = data.Where(s => s.Duration.Contains(searchString));
        }

        else
        {
          HttpContext.Session.SetString("searchString", string.Empty);
        }
        //Sort Order
        switch (sortOrder)
        {
          case "Duration_desc":
            data = data.OrderByDescending(s => s.Duration);
            break;
          case "Duration_asc":
            data = data.OrderBy(s => s.Duration);
            break;
          case "Price_desc":
            data = data.OrderByDescending(s => s.Price);
            break;
          case "Price_asc":
            data = data.OrderBy(s => s.Price);
            break;
          case "Notes_desc":
            data = data.OrderByDescending(s => s.Notes);
            break;
          case "Notes_asc":
            data = data.OrderBy(s => s.Notes);
            break;
          case "UK_ExpressDataBagDeliveryId_asc":
            data = data.OrderBy(s => s.UK_ExpressDataBagDeliveryId);
            break;
          case "UK_ExpressDataBagDeliveryId_desc":
            data = data.OrderByDescending(s => s.UK_ExpressDataBagDeliveryId);
            break;
          default:
            data = data.OrderBy(s => s.UK_ExpressDataBagDeliveryId);
            break;
        }
        var totalCount = await data.CountAsync();
        PaggingAndSorting paggingSorting = new PaggingAndSorting(_linkGenerator);
        paggingSorting.PaggingAndSortingData(HttpContext, totalCount, "GetUK_ExpressDataBagDelivery", page, pageSize);

        // return result
        var items = await data
            .Skip((int)pageSize * ((int)page - 1))
            .Take((int)pageSize)
            .ToListAsync();

        var pagedData = _mapper.Map<IEnumerable<UK_ExpressDataBagDeliveryDto>>(items);
        resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
        return Json(resObject);
      }
      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("GetUK_ExpressDataBagDelivery");
        _logger.LogError(msg);
        var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
        return Json(resObject);
      }
    }

    /// <summary>
    /// Get UK_ExpressDataBagDelivery By Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet("{id}", Name = "GetUK_ExpressDataBagDeliveryById")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetUK_ExpressDataBagDeliveryById(int id)  // --------------------------------------------------
    {
      try
      {
        ResponseObject resObject;
        _logger.LogInformation("GetUK_ExpressDataBagDeliveryById");
        var dataFromRepo = await _uk_ExpressDataBagDeliveryRepository.GetUK_ExpressDataBagDeliveryById(id);

        if (dataFromRepo == null)
        {
          _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetUK_ExpressDataBagDeliveryById({ID}) NOT FOUND", id);
          resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
          return Json(resObject);
        }
        var data = _mapper.Map<UK_ExpressDataBagDeliveryDto>(dataFromRepo);

        resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
        return Json(resObject);
      }
      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("GetUK_ExpressDataBagDeliveryById");
        _logger.LogError(msg);
        var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
        return Json(resObject);
      }
    }
    /// <summary>
    /// Add UK_ExpressDataBagDelivery - POST api/UK_ExpressDataBagDelivery
    /// </summary>
    /// <param name="uk_ExpressDataBagDelivery"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanCreateExpressDeliveryBag")]
    [HttpPost(Name = "AddUK_ExpressDataBagDelivery")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> AddUK_ExpressDataBagDelivery([FromBody] UK_ExpressDataBagDeliveryDto uk_ExpressDataBagDelivery)
    {
      try
      {
        ResponseObject resObject;
        if (uk_ExpressDataBagDelivery == null)
        {
          _logger.LogError(ConstantProps.NullObjectError("UK_ExpressDataBagDelivery"));
          resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (UK_ExpressDataBagDeliveryDto)null, HttpStatusCode = HttpStatusCode.NotFound };
          return Json(resObject);
        }

        if (!ModelState.IsValid)
        {
          _logger.LogError(ConstantProps.InvalidObjectError("UK_ExpressDataBagDelivery"));
          resObject = new ResponseObject
          {
            Message = ConstantProps.InvalidDataModelText,
            Status = "error",
            Data = ModelState,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }
        var uk_ExpressDataBagDeliveryRepo = _mapper.Map<UK_ExpressDataBagDelivery>(uk_ExpressDataBagDelivery);
        await _uk_ExpressDataBagDeliveryRepository.AddUK_ExpressDataBagDelivery(uk_ExpressDataBagDeliveryRepo);
        _logger.LogInformation(LoggingEvents.InsertItem, "UK_ExpressDataBagDelivery {ID} Created", uk_ExpressDataBagDeliveryRepo.UK_ExpressDataBagDeliveryId);
        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Adding", "UK_ExpressDataBagDelivery"));
        }

        var contactFromRepo2 = CreatedAtAction(nameof(GetUK_ExpressDataBagDeliveryById), new { id = uk_ExpressDataBagDeliveryRepo.UK_ExpressDataBagDeliveryId }, uk_ExpressDataBagDeliveryRepo);
        var contactFromRepo = contactFromRepo2.Value;
        var data = _mapper.Map<UK_ExpressDataBagDeliveryDto>(contactFromRepo);
        resObject = new ResponseObject
        {
          Message = ConstantProps.SubmittedSuccessfullyText,
          Status = "success",
          Data = data,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("Create");
        _logger.LogWarning(e.Message);
        var resObject = new ResponseObject
        {
          Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
          Message2 = msg,
          Status = "error",
          HttpStatusCode = HttpStatusCode.InternalServerError
        };
        return Json(resObject);
      }
    }

    // PUT api/UK_ExpressDataBagDelivery/5
    [Authorize(Policy = "CanUpdateExpressDeliveryBag")]
    [HttpPut("{id}", Name = "UpdateUK_ExpressDataBagDelivery")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> UpdateUK_ExpressDataBagDelivery(int id, [FromBody] UK_ExpressDataBagDeliveryDto uk_ExpressDataBagDelivery)
    {
      try
      {
        ResponseObject resObject;
        if (uk_ExpressDataBagDelivery == null)
        {
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = (UK_ExpressDataBagDeliveryDto)null,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }

        if (!ModelState.IsValid)
        {
          _logger.LogError(ConstantProps.InvalidDataModelText);
          resObject = new ResponseObject
          {
            Message = ConstantProps.InvalidDataModelText,
            Status = "error",
            Data = ModelState,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }
        var uk_ExpressDataBagDeliveryRepo = _mapper.Map<UK_ExpressDataBagDelivery>(uk_ExpressDataBagDelivery);
        if (!_uk_ExpressDataBagDeliveryRepository.UpdateUK_ExpressDataBagDeliveryOk(id, uk_ExpressDataBagDeliveryRepo))
        {
          _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = id,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }

        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Updating", "UK_ExpressDataBagDelivery"));
        }

        var contactFromRepo2 = CreatedAtAction(nameof(GetUK_ExpressDataBagDeliveryById), new { id = uk_ExpressDataBagDeliveryRepo.UK_ExpressDataBagDeliveryId }, uk_ExpressDataBagDeliveryRepo);
        var contactFromRepo = contactFromRepo2.Value;
        var data = _mapper.Map<UK_ExpressDataBagDeliveryDto>(contactFromRepo);
        resObject = new ResponseObject
        {
          Message = ConstantProps.SavedSuccessfullyText,
          Status = "success",
          Data = data,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
        _logger.LogWarning(e.Message);

        var resObject = new ResponseObject
        {
          Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
          Message2 = msg,
          Status = "error",
          HttpStatusCode = HttpStatusCode.InternalServerError
        };
        return Json(resObject);
      }
    }

    /// <summary>
    /// DELETE api/UK_ExpressDataBagDelivery/5
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanDeleteExpressDeliveryBag")]
    [HttpDelete("{id}", Name = "DeleteUK_ExpressDataBagDelivery")]
    public async Task<IActionResult> DeleteUK_ExpressDataBagDelivery(int id)
    {
      try
      {
        ResponseObject resObject;
        var entity = _uk_ExpressDataBagDeliveryRepository.GetUK_ExpressDataBagDeliveryByIdAsNoTracking(id);
        if (entity == false)
        {
          _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = id,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }
        _uk_ExpressDataBagDeliveryRepository.DeleteUK_ExpressDataBagDelivery(id);
        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Deleting", "UK_ExpressDataBagDelivery"));
        }
        resObject = new ResponseObject
        {
          Message = ConstantProps.DeletedSuccessfullyText,
          Status = "success",
          Data = id,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("Delete");
        _logger.LogError(msg);
        var resObject = new ResponseObject
        {
          Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
          Message = msg,
          Status = "error",
          HttpStatusCode = HttpStatusCode.InternalServerError
        };
        return Json(resObject);
      }
    }


  }
}

