using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using DRS.APA.Microservice.API.Controllers.Common;
using DRS.APA.Microserivce.Domain.Core.Repositories.Shipment;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using DRS.APA.Microserivce.Domain.Core.Dtos.Shipment;
using Microsoft.AspNetCore.Authorization;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.Microservice.API.Controllers.Shipment
{
  [Route("api/DRS.APA/Shipment/[controller]")]
  [SwaggerGroup("Shipment")]
  public class UK_BarrierDeliveryPricesController : Controller
  {
    private readonly IUK_BarrierDeliveryPricesRepository _uk_BarrierDeliveryPricesRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly ILogger _logger;
    protected readonly LinkGenerator _linkGenerator;

    public UK_BarrierDeliveryPricesController(IUK_BarrierDeliveryPricesRepository uk_BarrierDeliveryPricesRepository,
    IUnitOfWork unitOfWork, IMapper mapper, ILogger<UK_BarrierDeliveryPricesController> logger, LinkGenerator linkGenerator)
    {
      _unitOfWork = unitOfWork;
      _uk_BarrierDeliveryPricesRepository = uk_BarrierDeliveryPricesRepository;
      _mapper = mapper;
      _logger = logger;
      _linkGenerator = linkGenerator;
    }

    /// <summary>
    /// List all UK_BarrierDeliveryPrices, by default sorted by UK_BarrierDeliveryPricesCost
    /// </summary>
    /// <remarks>With pagination, you can fetch up to x records per page.</remarks>
    [AllowAnonymous]
    [HttpGet("{page}/{pageSize}", Name = "GetUK_BarrierDeliveryPrices")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetUK_BarrierDeliveryPrices(
        string sortOrder, int? Qty, int? UK_BarrierDeliveryPricesId, decimal? Price, string Notes,
        string searchString, int? page = 1, int? pageSize = ConstantProps.maxPageSize)
    {
      try
      {
        ResponseObject resObject;
        if (!page.HasValue || page.Value == 0)
        {
          _logger.LogInformation("Generating List");
          var dataFromRepo = await _uk_BarrierDeliveryPricesRepository.GetAllUK_BarrierDeliveryPrices();
          var dataList = _mapper.Map<IEnumerable<UK_BarrierDeliveryPricesDto>>(dataFromRepo);
          if (UK_BarrierDeliveryPricesId.HasValue)
          {
            dataFromRepo = dataFromRepo.Where(x => x.UK_BarrierDeliveryPricesId == UK_BarrierDeliveryPricesId);
          }
          if (Qty.HasValue)
          {
            dataFromRepo = dataFromRepo.Where(x => x.Qty == Qty);
          }
          if (Price.HasValue)
          {
            dataFromRepo = dataFromRepo.Where(x => x.Price == Price);
          }
          if (Notes != null)
          {
            dataFromRepo = dataFromRepo.Where(x => x.Notes == Notes);
          }
          resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
          return Json(resObject);
        }

        // Sorting -------
        HttpContext.Session.SetString("UK_BarrierDeliveryPricesIdSortParam", String.IsNullOrEmpty(sortOrder) ? "UK_BarrierDeliveryPricesId_desc" : "");
        HttpContext.Session.SetString("UK_BarrierDeliveryPricesIdSortParam", String.IsNullOrEmpty(sortOrder) ? "UK_BarrierDeliveryPricesId_asc" : "");

        HttpContext.Session.SetString("QtySortParam", String.IsNullOrEmpty(sortOrder) ? "Qty_desc" : "");
        HttpContext.Session.SetString("QtySortParam", String.IsNullOrEmpty(sortOrder) ? "Qty_asc" : "");

        HttpContext.Session.SetString("PriceSortParam", String.IsNullOrEmpty(sortOrder) ? "Price_desc" : "");
        HttpContext.Session.SetString("PriceSortParam", String.IsNullOrEmpty(sortOrder) ? "Price_asc" : "");
 
        HttpContext.Session.SetString("NotesSortParam", String.IsNullOrEmpty(sortOrder) ? "Notes_desc" : "");
        HttpContext.Session.SetString("NotesSortParam", String.IsNullOrEmpty(sortOrder) ? "Notes_asc" : "");
        //search string check
        var previousSearchString = HttpContext.Session.GetString("searchString");

        if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
        {
          page = 1;
        }
        else
        {
          // searchString = currentFilter;
        }
        IQueryable<UK_BarrierDeliveryPrices> data = null;
        data = await _uk_BarrierDeliveryPricesRepository.GetAllUK_BarrierDeliveryPrices(true);
        if (UK_BarrierDeliveryPricesId.HasValue)
        {
          data = data.Where(x => x.UK_BarrierDeliveryPricesId == UK_BarrierDeliveryPricesId);
        }
        if (Qty.HasValue)
        {
          data = data.Where(x => x.Qty == Qty);

        }
        if (Price.HasValue)
        {
          data = data.Where(x => x.Price == Price);

        }
        if (Notes != null)
        {
          data = data.Where(x => x.Notes == Notes);

        }

        //search
        if (!String.IsNullOrEmpty(searchString))
        {

          HttpContext.Session.SetString("searchString", searchString);
          data = data.Where(s => s.Notes.Contains(searchString));
        }

        else
        {
          HttpContext.Session.SetString("searchString", string.Empty);
        }
        //Sort Order
        switch (sortOrder)
        {
          case "Qty_desc":
            data = data.OrderByDescending(s => s.Qty);
            break;
          case "Qty_asc":
            data = data.OrderBy(s => s.Qty);
            break;
          case "Price_desc":
            data = data.OrderByDescending(s => s.Price);
            break;
          case "Price_asc":
            data = data.OrderBy(s => s.Price);
            break;
          case "Notes_desc":
            data = data.OrderByDescending(s => s.Notes);
            break;
          case "Notes_asc":
            data = data.OrderBy(s => s.Notes);
            break;
          case "UK_BarrierDeliveryPricesId_asc":
            data = data.OrderBy(s => s.UK_BarrierDeliveryPricesId);
            break;
          case "UK_BarrierDeliveryPricesId_desc":
            data = data.OrderByDescending(s => s.UK_BarrierDeliveryPricesId);
            break;
          default:
            data = data.OrderBy(s => s.UK_BarrierDeliveryPricesId);
            break;
        }
        var totalCount = await data.CountAsync();
        PaggingAndSorting paggingSorting = new PaggingAndSorting(_linkGenerator);
        paggingSorting.PaggingAndSortingData(HttpContext, totalCount, "GetUK_BarrierDeliveryPrices", page, pageSize);

        // return result
        var items = await data
            .Skip((int)pageSize * ((int)page - 1))
            .Take((int)pageSize)
            .ToListAsync();

        var pagedData = _mapper.Map<IEnumerable<UK_BarrierDeliveryPricesDto>>(items);
        resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
        return Json(resObject);
      }
      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("GetUK_BarrierDeliveryPrices");
        _logger.LogError(msg);
        var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
        return Json(resObject);
      }
    }

    /// <summary>
    /// Get UK_BarrierDeliveryPrices By Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet("{id}", Name = "GetUK_BarrierDeliveryPricesById")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetUK_BarrierDeliveryPricesById(int id)  // --------------------------------------------------
    {
      try
      {
        ResponseObject resObject;
        _logger.LogInformation("GetUK_BarrierDeliveryPricesById");
        var dataFromRepo = await _uk_BarrierDeliveryPricesRepository.GetUK_BarrierDeliveryPricesById(id);

        if (dataFromRepo == null)
        {
          _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetUK_BarrierDeliveryPricesById({ID}) NOT FOUND", id);
          resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
          return Json(resObject);
        }
        var data = _mapper.Map<UK_BarrierDeliveryPricesDto>(dataFromRepo);

        resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
        return Json(resObject);
      }
      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("GetUK_BarrierDeliveryPricesById");
        _logger.LogError(msg);
        var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
        return Json(resObject);
      }
    }
    /// <summary>
    /// Add UK_BarrierDeliveryPrices - POST api/UK_BarrierDeliveryPrices
    /// </summary>
    /// <param name="uk_BarrierDeliveryPrices"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanCreateBarrierDeliveryPrice")]
    [HttpPost(Name = "AddUK_BarrierDeliveryPrices")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> AddUK_BarrierDeliveryPrices([FromBody] UK_BarrierDeliveryPricesDto uk_BarrierDeliveryPrices)
    {
      try
      {
        ResponseObject resObject;
        if (uk_BarrierDeliveryPrices == null)
        {
          _logger.LogError(ConstantProps.NullObjectError("UK_BarrierDeliveryPrices"));
          resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (UK_BarrierDeliveryPricesDto)null, HttpStatusCode = HttpStatusCode.NotFound };
          return Json(resObject);
        }

        if (!ModelState.IsValid)
        {
          _logger.LogError(ConstantProps.InvalidObjectError("UK_BarrierDeliveryPrices"));
          resObject = new ResponseObject
          {
            Message = ConstantProps.InvalidDataModelText,
            Status = "error",
            Data = ModelState,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }
        var uk_BarrierDeliveryPricesRepo = _mapper.Map<UK_BarrierDeliveryPrices>(uk_BarrierDeliveryPrices);
        await _uk_BarrierDeliveryPricesRepository.AddUK_BarrierDeliveryPrices(uk_BarrierDeliveryPricesRepo);
        _logger.LogInformation(LoggingEvents.InsertItem, "UK_BarrierDeliveryPrices {ID} Created", uk_BarrierDeliveryPricesRepo.UK_BarrierDeliveryPricesId);
        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Adding", "UK_BarrierDeliveryPrices"));
        }

        var contactFromRepo2 = CreatedAtAction(nameof(GetUK_BarrierDeliveryPricesById), new { id = uk_BarrierDeliveryPricesRepo.UK_BarrierDeliveryPricesId }, uk_BarrierDeliveryPricesRepo);
        var contactFromRepo = contactFromRepo2.Value;
        var data = _mapper.Map<UK_BarrierDeliveryPricesDto>(contactFromRepo);
        resObject = new ResponseObject
        {
          Message = ConstantProps.SubmittedSuccessfullyText,
          Status = "success",
          Data = data,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("Create");
        _logger.LogWarning(e.Message);
        var resObject = new ResponseObject
        {
          Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
          Message2 = msg,
          Status = "error",
          HttpStatusCode = HttpStatusCode.InternalServerError
        };
        return Json(resObject);
      }
    }

    // PUT api/UK_BarrierDeliveryPrices/5
    [Authorize(Policy = "CanUpdateBarrierDeliveryPrice")]
    [HttpPut("{id}", Name = "UpdateUK_BarrierDeliveryPrices")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> UpdateUK_BarrierDeliveryPrices(int id, [FromBody] UK_BarrierDeliveryPricesDto uk_BarrierDeliveryPrices)
    {
      try
      {
        ResponseObject resObject;
        if (uk_BarrierDeliveryPrices == null)
        {
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = (UK_BarrierDeliveryPricesDto)null,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }

        if (!ModelState.IsValid)
        {
          _logger.LogError(ConstantProps.InvalidDataModelText);
          resObject = new ResponseObject
          {
            Message = ConstantProps.InvalidDataModelText,
            Status = "error",
            Data = ModelState,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }
        var uk_BarrierDeliveryPricesRepo = _mapper.Map<UK_BarrierDeliveryPrices>(uk_BarrierDeliveryPrices);
        if (!_uk_BarrierDeliveryPricesRepository.UpdateUK_BarrierDeliveryPricesOk(id, uk_BarrierDeliveryPricesRepo))
        {
          _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = id,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }

        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Updating", "UK_BarrierDeliveryPrices"));
        }

        var contactFromRepo2 = CreatedAtAction(nameof(GetUK_BarrierDeliveryPricesById), new { id = uk_BarrierDeliveryPricesRepo.UK_BarrierDeliveryPricesId }, uk_BarrierDeliveryPricesRepo);
        var contactFromRepo = contactFromRepo2.Value;
        var data = _mapper.Map<UK_BarrierDeliveryPricesDto>(contactFromRepo);
        resObject = new ResponseObject
        {
          Message = ConstantProps.SavedSuccessfullyText,
          Status = "success",
          Data = data,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
        _logger.LogWarning(e.Message);

        var resObject = new ResponseObject
        {
          Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
          Message2 = msg,
          Status = "error",
          HttpStatusCode = HttpStatusCode.InternalServerError
        };
        return Json(resObject);
      }
    }

    /// <summary>
    /// DELETE api/UK_BarrierDeliveryPrices/5
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanDeleteBarrierDeliveryPrice")]
    [HttpDelete("{id}", Name = "DeleteUK_BarrierDeliveryPrices")]
    public async Task<IActionResult> DeleteUK_BarrierDeliveryPrices(int id)
    {
      try
      {
        ResponseObject resObject;
        var entity = _uk_BarrierDeliveryPricesRepository.GetUK_BarrierDeliveryPricesByIdAsNoTracking(id);
        if (entity == false)
        {
          _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = id,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }
        _uk_BarrierDeliveryPricesRepository.DeleteUK_BarrierDeliveryPrices(id);
        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Deleting", "UK_BarrierDeliveryPrices"));
        }
        resObject = new ResponseObject
        {
          Message = ConstantProps.DeletedSuccessfullyText,
          Status = "success",
          Data = id,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("Delete");
        _logger.LogError(msg);
        var resObject = new ResponseObject
        {
          Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
          Message = msg,
          Status = "error",
          HttpStatusCode = HttpStatusCode.InternalServerError
        };
        return Json(resObject);
      }
    }


  }
}

