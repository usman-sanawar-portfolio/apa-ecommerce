using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.Domain.Core.Specifications;
using DRS.APA.API.Services;
using DRS.APA.Microservice.API.Controllers.Common;
using DRS.APA.Microserivce.Domain.Core.Dtos.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Dtos.Shipment;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using Microsoft.AspNetCore.Authorization;
//using System.Data.Entity.Validation;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class UK_SpecialDeliveryPricessController : Controller
    {
        private readonly IUK_SpecialDeliveryPricesRepository _specialDeliveryPrices_UKRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        protected readonly LinkGenerator _linkGenerator;

        public UK_SpecialDeliveryPricessController(IUK_SpecialDeliveryPricesRepository specialDeliveryPrices_UKRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<UK_SpecialDeliveryPricessController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _specialDeliveryPrices_UKRepository = specialDeliveryPrices_UKRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }

    /// <summary>
    /// List all UK_SpecialDeliveryPricess, by default sorted by UK_SpecialDeliveryPricesName
    /// </summary>
    /// <remarks>With pagination, you can fetch up to x records per page.</remarks>
    [AllowAnonymous]
    [HttpGet("{page}/{pageSize}", Name = "GetUK_SpecialDeliveryPricess")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetUK_SpecialDeliveryPricess(
            string sortOrder, string searchString, int? page = 1, int? pageSize = ConstantProps.maxPageSize)
        {
            try
            {
                ResponseObject resObject;
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var dataFromRepo = await _specialDeliveryPrices_UKRepository.GetAllUK_SpecialDeliveryPrices();
                    var dataList = _mapper.Map<IEnumerable<UK_SpecialDeliveryPricesDto>>(dataFromRepo);
                 resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                    return Json(resObject);
                }

                // Sorting -------
                HttpContext.Session.SetString("NotesSortParam", String.IsNullOrEmpty(sortOrder) ? "notes_desc" : "");
                HttpContext.Session.SetString("NotesSortParam", String.IsNullOrEmpty(sortOrder) ? "notes_asc" : "");
                HttpContext.Session.SetString("RetrofitDeliveryCostSortParam", String.IsNullOrEmpty(sortOrder) ? "retrofitDeliveryCost_desc" : "");
                HttpContext.Session.SetString("RetrofitDeliveryCostSortParam", String.IsNullOrEmpty(sortOrder) ? "retrofitDeliveryCost_asc" : "");

                HttpContext.Session.SetString("OperatorDeliveryCostSortParam", String.IsNullOrEmpty(sortOrder) ? "operatorDeliveryCost_desc" : "");
                HttpContext.Session.SetString("OperatorDeliveryCostSortParam", String.IsNullOrEmpty(sortOrder) ? "operatorDeliveryCost_asc" : "");

                HttpContext.Session.SetString("ProductNameSortParam", String.IsNullOrEmpty(sortOrder) ? "productName_desc" : "");
                HttpContext.Session.SetString("ProductNameSortParam", String.IsNullOrEmpty(sortOrder) ? "productName_asc" : "");

                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
               
                IQueryable<UK_SpecialDeliveryPrices> data = null;
                data = await _specialDeliveryPrices_UKRepository.GetAllUK_SpecialDeliveryPrices(true);

                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    data = data.Where(s => s.Notes.Contains(searchString)
                    ||s.OperatorDeliveryCost.ToString().Contains(searchString)
                    );
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "notes_desc":
                        data = data.OrderByDescending(s => s.Notes);
                        break;
                   
                    case "operatorDeliveryCost_desc":
                        data = data.OrderByDescending(s => s.OperatorDeliveryCost);
                        break;
                    case "operatorDeliveryCost_asc":
                        data = data.OrderBy(s => s.OperatorDeliveryCost);
                        break;
                  
                    case "retrofitDeliveryCost_desc":
                        data = data.OrderByDescending(s => s.RetrofitDeliveryCost);
                        break;
                    case "retrofitDeliveryCost_asc":
                        data = data.OrderBy(s => s.RetrofitDeliveryCost);
                        break;
                  
                    default:
                        data = data.OrderBy(s => s.Notes);
                        break;
                }//
                // ensure the page size isn't larger than the maximum.
                //if (pageSize > maxPageSize)
                //{
                //    pageSize = maxPageSize;
                //}

                // calculate data for metadata
                var totalCount = await data.CountAsync();
                PaggingAndSorting paggingSorting = new PaggingAndSorting(_linkGenerator);
                paggingSorting.PaggingAndSortingData(HttpContext, totalCount, "GetUK_SpecialDeliveryPricess", page, pageSize);
              
                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();

                var pagedData = _mapper.Map<IEnumerable<UK_SpecialDeliveryPricesDto>>(items);
                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetUK_SpecialDeliveryPricess");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }


    /// <summary>
    /// Get UK_SpecialDeliveryPrices By Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet("{id}", Name = "GetUK_SpecialDeliveryPricesById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetUK_SpecialDeliveryPricesById(int id)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("GetUK_SpecialDeliveryPricesById");
                var dataFromRepo = await _specialDeliveryPrices_UKRepository.GetUK_SpecialDeliveryPricesById(id);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetUK_SpecialDeliveryPricesById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
              
                var data = _mapper.Map<UK_SpecialDeliveryPricesDto>(dataFromRepo);

                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetUK_SpecialDeliveryPricesById");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }
    /// <summary>
    /// Add UK_SpecialDeliveryPrices - POST api/UK_SpecialDeliveryPrices
    /// </summary>
    /// <param name="specialDeliveryPrices_UK"></param>
    /// <returns></returns>

    [Authorize(Policy = "CanCreateSpecialDelivery")]
    [HttpPost(Name = "AddUK_SpecialDeliveryPrices")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddUK_SpecialDeliveryPrices([FromBody] UK_SpecialDeliveryPricesDto specialDeliveryPrices_UK)  // ---------------------
        {
            try
            {
                ResponseObject resObject;
                if (specialDeliveryPrices_UK == null)
                {
                    _logger.LogError(ConstantProps.NullObjectError("UK_SpecialDeliveryPrices"));
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (UK_SpecialDeliveryPricesDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidObjectError("UK_SpecialDeliveryPrices"));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var specialDeliveryPrices_UKRepo = _mapper.Map<UK_SpecialDeliveryPrices>(specialDeliveryPrices_UK);
                await _specialDeliveryPrices_UKRepository.AddUK_SpecialDeliveryPrices(specialDeliveryPrices_UKRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "UK_SpecialDeliveryPrices {ID} Created", specialDeliveryPrices_UKRepo.UK_SpecialDeliveryPricesId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Adding", "UK_SpecialDeliveryPrices"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetUK_SpecialDeliveryPricesById), new { id = specialDeliveryPrices_UKRepo.UK_SpecialDeliveryPricesId }, specialDeliveryPrices_UKRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<UK_SpecialDeliveryPricesDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SubmittedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Create");
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject {
                    Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), 
                    Message2 = msg, 
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError 
                };
                return Json(resObject);
            }

        }

    // PUT api/UK_SpecialDeliveryPrices/5
    [Authorize(Policy = "CanUpdateSpecialDelivery")]
    [HttpPut("{id}", Name = "UpdateUK_SpecialDeliveryPrices")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateUK_SpecialDeliveryPrices(int id, [FromBody] UK_SpecialDeliveryPricesDto specialDeliveryPrices_UK) // ------------
        {
            try
            {
                ResponseObject resObject;
                if (specialDeliveryPrices_UK == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = (UK_SpecialDeliveryPricesDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidDataModelText);
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var specialDeliveryPrices_UKRepo = _mapper.Map<UK_SpecialDeliveryPrices>(specialDeliveryPrices_UK);
                if (!_specialDeliveryPrices_UKRepository.UpdateUK_SpecialDeliveryPricesOk(id, specialDeliveryPrices_UKRepo))
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Updating", "UK_SpecialDeliveryPrices"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetUK_SpecialDeliveryPricesById), new { id = specialDeliveryPrices_UKRepo.UK_SpecialDeliveryPricesId }, specialDeliveryPrices_UKRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<UK_SpecialDeliveryPricesDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SavedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

    /// <summary>
    /// DELETE api/UK_SpecialDeliveryPrices/5
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanDeleteSpecialDelivery")]
    [HttpDelete("{id}", Name = "DeleteUK_SpecialDeliveryPrices")]
        public async Task<IActionResult> DeleteUK_SpecialDeliveryPrices(int id)
        {
            try
            {
                ResponseObject resObject;
                var entity = _specialDeliveryPrices_UKRepository.GetUK_SpecialDeliveryPricesByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                _specialDeliveryPrices_UKRepository.DeleteUK_SpecialDeliveryPrices(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Deleting", "UK_SpecialDeliveryPrices"));
                }
                resObject = new ResponseObject
                {
                    Message = ConstantProps.DeletedSuccessfullyText,
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Delete");
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }
    }
}
