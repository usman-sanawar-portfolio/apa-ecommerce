using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.API.Services;
using DRS.APA.Microservice.API.Controllers.Common;
using DRS.APA.Microserivce.Domain.Core.Repositories.Shipment;
using DRS.APA.Microserivce.Domain.Core.Dtos.Shipment;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.Microservice.API.Controllers.Shipment
{
    [Route("api/DRS.APA/Shipment/[controller]")]
    [SwaggerGroup("Shipment")]
    public class UK_AllShipmentController : Controller
    {
        private readonly IBarriersPerRackRepository _barriersPerRackRepository;
        private readonly IPricePerRackRepository _PricePerRackRepository;
        private readonly IUK_ExpressDataBagDeliveryRepository _uk_ExpressDataBagDeliveryRepository;
        private readonly IUK_FreeDeliveryPricesRepository _freeDeliveryPrices_UKRepository;
        private readonly IUK_ParcelDeliveryRepository _ParcelDeliveriesRepository;
        private readonly IUK_SpecialDeliveryPricesRepository _specialDeliveryPrices_UKRepository;
        private readonly IUK_BarrierDeliveryPricesRepository _uk_BarrierDeliveryPricesRepository;

        private readonly IMapper _mapper;
        protected readonly LinkGenerator _linkGenerator;

        public UK_AllShipmentController(
            IBarriersPerRackRepository barriersPerRackRepository,
            //IPricePerRackRepository PricePerRackRepository,
            //IUK_ExpressDataBagDeliveryRepository uk_ExpressDataBagDeliveryRepository,
            IUK_FreeDeliveryPricesRepository freeDeliveryPrices_UKRepository,
            IUK_ParcelDeliveryRepository ParcelDeliveriesRepository,
            IUK_SpecialDeliveryPricesRepository specialDeliveryPrices_UKRepository,
            IUK_BarrierDeliveryPricesRepository uk_BarrierDeliveryPricesRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<BarriersPerRackController> logger, LinkGenerator linkGenerator)
        {
            _barriersPerRackRepository = barriersPerRackRepository;
            //_PricePerRackRepository = PricePerRackRepository;
            //_uk_ExpressDataBagDeliveryRepository = uk_ExpressDataBagDeliveryRepository;
            _freeDeliveryPrices_UKRepository = freeDeliveryPrices_UKRepository;
            _ParcelDeliveriesRepository = ParcelDeliveriesRepository;
            _specialDeliveryPrices_UKRepository = specialDeliveryPrices_UKRepository;
            _uk_BarrierDeliveryPricesRepository = uk_BarrierDeliveryPricesRepository;

            _mapper = mapper;
            _linkGenerator = linkGenerator;
        }
        [HttpGet(Name = "GetAllUKShipmentServices")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllUKShipmentServices()
        {
            try
            {
                ResponseObject resObject;
                var dataBarrierPerRackFromRepo = await _barriersPerRackRepository.GetAllBarriersPerRack();
                var dataBarrierPerRackList = _mapper.Map<IEnumerable<BarriersPerRackDto>>(dataBarrierPerRackFromRepo);

                //var dataPricePerRackFromRepo = await _PricePerRackRepository.GetAllPricePerRack();
                //var dataPricePerRackList = _mapper.Map<IEnumerable<PricePerRackDto>>(dataPricePerRackFromRepo);

                //var dataExpressDataBagFromRepo = await _uk_ExpressDataBagDeliveryRepository.GetAllUK_ExpressDataBagDelivery();
                //var dataExpressDataBagList = _mapper.Map<IEnumerable<UK_ExpressDataBagDeliveryDto>>(dataExpressDataBagFromRepo);

                var datafreeDeliveryFromRepo = await _freeDeliveryPrices_UKRepository.GetAllUK_FreeDeliveryPrices();
                var datafreeDeliveryList = _mapper.Map<IEnumerable<UK_FreeDeliveryPricesDto>>(datafreeDeliveryFromRepo);

                var dataParcelDeliveriesFromRepo = await _ParcelDeliveriesRepository.GetAllUK_ParcelDeliveries();
                var dataParcelDeliveriesList = _mapper.Map<IEnumerable<UK_ParcelDeliveryDto>>(dataParcelDeliveriesFromRepo);

                var dataSpecialDeliveryPricesFromRepo = await _specialDeliveryPrices_UKRepository.GetAllUK_SpecialDeliveryPrices();
                var dataSpecialDeliveryPricesList = _mapper.Map<IEnumerable<UK_SpecialDeliveryPricesDto>>(dataSpecialDeliveryPricesFromRepo);

                var dataBarrierDeliveryPricesFromRepo = await _uk_BarrierDeliveryPricesRepository.GetAllUK_BarrierDeliveryPrices();
                var dataBarrierDeliveryPricesList = _mapper.Map<IEnumerable<UK_BarrierDeliveryPricesDto>>(dataBarrierDeliveryPricesFromRepo);

                UK_AllShipmentDto objAllShipmentDto = new UK_AllShipmentDto();

                objAllShipmentDto.BarriersPerRack = dataBarrierPerRackList;
                //objAllShipmentDto.PricePerRack = dataPricePerRackList;
                //objAllShipmentDto.UK_ExpressDataBagDelivery = dataExpressDataBagList;
                objAllShipmentDto.UK_FreeDeliveryPrices = datafreeDeliveryList;
                objAllShipmentDto.UK_ParcelDelivery = dataParcelDeliveriesList;
                objAllShipmentDto.UK_SpecialDeliveryPrices = dataSpecialDeliveryPricesList;
                objAllShipmentDto.UK_BarrierDeliveryPrices = dataBarrierDeliveryPricesList;

                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = objAllShipmentDto, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetAllUKShipmentServices");
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }
    }
}

