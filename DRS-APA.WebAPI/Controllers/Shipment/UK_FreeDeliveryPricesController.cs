using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.Domain.Core.Specifications;
using DRS.APA.API.Services;
using DRS.APA.Microservice.API.Controllers.Common;
using DRS.APA.Microserivce.Domain.Core.Dtos.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Dtos.Shipment;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using Microsoft.AspNetCore.Authorization;
//using System.Data.Entity.Validation;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class UK_FreeDeliveryPricesController : Controller
    {
        private readonly IUK_FreeDeliveryPricesRepository _freeDeliveryPrices_UKRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        protected readonly LinkGenerator _linkGenerator;

        public UK_FreeDeliveryPricesController(IUK_FreeDeliveryPricesRepository freeDeliveryPrices_UKRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<UK_FreeDeliveryPricesController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _freeDeliveryPrices_UKRepository = freeDeliveryPrices_UKRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }

    /// <summary>
    /// List all UK_FreeDeliveryPrices, by default sorted by UK_FreeDeliveryPricesName
    /// </summary>
    /// <remarks>With pagination, you can fetch up to x records per page.</remarks>
    [AllowAnonymous]
    [HttpGet("{page}/{pageSize}", Name = "GetUK_FreeDeliveryPrices")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetUK_FreeDeliveryPrices(
            string sortOrder, string searchString,int? page = 1, int? pageSize = ConstantProps.maxPageSize)
        {
            try
            {
                ResponseObject resObject;
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var dataFromRepo = await _freeDeliveryPrices_UKRepository.GetAllUK_FreeDeliveryPrices();
                    var dataList = _mapper.Map<IEnumerable<UK_FreeDeliveryPricesDto>>(dataFromRepo);
                    resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                    return Json(resObject);
                }

                // Sorting -------
                HttpContext.Session.SetString("NotesSortParam", String.IsNullOrEmpty(sortOrder) ? "notes_desc" : "");
                HttpContext.Session.SetString("NotesSortParam", String.IsNullOrEmpty(sortOrder) ? "notes_asc" : "");

                HttpContext.Session.SetString("FreeOrderStartsFromSortParam", String.IsNullOrEmpty(sortOrder) ? "freeOrderStartsFrom_desc" : "");
                HttpContext.Session.SetString("FreeOrderStartsFromSortParam", String.IsNullOrEmpty(sortOrder) ? "freeOrderStartsFrom_asc" : "");

                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
               
                IQueryable<UK_FreeDeliveryPrices> data = null;
                data = await _freeDeliveryPrices_UKRepository.GetAllUK_FreeDeliveryPrices(true);

             
                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    data = data.Where(s => s.Notes.Contains(searchString)
                    ||s.FreeOrderStartsFrom.ToString().Contains(searchString)
                    );
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "notes_desc":
                        data = data.OrderByDescending(s => s.Notes);
                        break;
                   
                    case "freeOrderStartsFrom_desc":
                        data = data.OrderByDescending(s => s.FreeOrderStartsFrom);
                        break;
                    case "freeOrderStartsFrom_asc":
                        data = data.OrderBy(s => s.FreeOrderStartsFrom);
                        break;
                  
                  
                    default:
                        data = data.OrderBy(s => s.Notes);
                        break;
                }
      

                var totalCount = await data.CountAsync();
                PaggingAndSorting paggingSorting = new PaggingAndSorting(_linkGenerator);
                paggingSorting.PaggingAndSortingData(HttpContext, totalCount, "GetUK_FreeDeliveryPrices", page, pageSize);
              
                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();

                var pagedData = _mapper.Map<IEnumerable<UK_FreeDeliveryPricesDto>>(items);
                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetUK_FreeDeliveryPrices");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }


    /// <summary>
    /// Get UK_FreeDeliveryPrices By Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet("{id}", Name = "GetUK_FreeDeliveryPricesById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetUK_FreeDeliveryPricesById(int id)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("GetUK_FreeDeliveryPricesById");
                var dataFromRepo = await _freeDeliveryPrices_UKRepository.GetUK_FreeDeliveryPricesById(id);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetUK_FreeDeliveryPricesById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
              
                var data = _mapper.Map<UK_FreeDeliveryPricesDto>(dataFromRepo);

                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetUK_FreeDeliveryPricesById");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }
    /// <summary>
    /// Add UK_FreeDeliveryPrices - POST api/UK_FreeDeliveryPrices
    /// </summary>
    /// <param name="freeDeliveryPrices_UK"></param>
    /// <returns></returns>

    [Authorize(Policy = "CanCreateFreeDeliveryPrice")]
    [HttpPost(Name = "AddUK_FreeDeliveryPrices")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddUK_FreeDeliveryPrices([FromBody] UK_FreeDeliveryPricesDto freeDeliveryPrices_UK)  // ---------------------
        {
            try
            {
                ResponseObject resObject;
                if (freeDeliveryPrices_UK == null)
                {
                    _logger.LogError(ConstantProps.NullObjectError("UK_FreeDeliveryPrices"));
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (UK_FreeDeliveryPricesDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidObjectError("UK_FreeDeliveryPrices"));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var freeDeliveryPrices_UKRepo = _mapper.Map<UK_FreeDeliveryPrices>(freeDeliveryPrices_UK);
                await _freeDeliveryPrices_UKRepository.AddUK_FreeDeliveryPrices(freeDeliveryPrices_UKRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "UK_FreeDeliveryPrices {ID} Created", freeDeliveryPrices_UKRepo.UK_FreeDeliveryPricesId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Adding", "UK_FreeDeliveryPrices"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetUK_FreeDeliveryPricesById), new { id = freeDeliveryPrices_UKRepo.UK_FreeDeliveryPricesId }, freeDeliveryPrices_UKRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<UK_FreeDeliveryPricesDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SubmittedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Create");
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject {
                    Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), 
                    Message2 = msg, 
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError 
                };
                return Json(resObject);
            }

        }

    // PUT api/UK_FreeDeliveryPrices/5
    [Authorize(Policy = "CanUpdateFreeDeliveryPrice")]
    [HttpPut("{id}", Name = "UpdateUK_FreeDeliveryPrices")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateUK_FreeDeliveryPrices(int id, [FromBody] UK_FreeDeliveryPricesDto freeDeliveryPrices_UK) // ------------
        {
            try
            {
                ResponseObject resObject;
                if (freeDeliveryPrices_UK == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = (UK_FreeDeliveryPricesDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidDataModelText);
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var freeDeliveryPrices_UKRepo = _mapper.Map<UK_FreeDeliveryPrices>(freeDeliveryPrices_UK);
                if (!_freeDeliveryPrices_UKRepository.UpdateUK_FreeDeliveryPricesOk(id, freeDeliveryPrices_UKRepo))
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Updating", "UK_FreeDeliveryPrices"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetUK_FreeDeliveryPricesById), new { id = freeDeliveryPrices_UKRepo.UK_FreeDeliveryPricesId }, freeDeliveryPrices_UKRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<UK_FreeDeliveryPricesDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SavedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

    /// <summary>
    /// DELETE api/UK_FreeDeliveryPrices/5
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanDeleteFreeDeliveryPrice")]
    [HttpDelete("{id}", Name = "DeleteUK_FreeDeliveryPrices")]
        public async Task<IActionResult> DeleteUK_FreeDeliveryPrices(int id)
        {
            try
            {
                ResponseObject resObject;
                var entity = _freeDeliveryPrices_UKRepository.GetUK_FreeDeliveryPricesByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                _freeDeliveryPrices_UKRepository.DeleteUK_FreeDeliveryPrices(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Deleting", "UK_FreeDeliveryPrices"));
                }
                resObject = new ResponseObject
                {
                    Message = ConstantProps.DeletedSuccessfullyText,
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Delete");
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }
    }
}
