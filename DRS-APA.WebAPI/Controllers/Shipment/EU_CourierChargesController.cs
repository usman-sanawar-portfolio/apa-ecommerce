using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories; 
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using DRS.APA.Microservice.API.Controllers.Common;
using DRS.APA.Microserivce.Domain.Core.Repositories.Shipment;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using DRS.APA.Microserivce.Domain.Core.Dtos.Shipment;
using Microsoft.AspNetCore.Authorization;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.Microservice.API.Controllers.Shipment
{
  [Route("api/DRS.APA/Shipment/[controller]")]
  [SwaggerGroup("Shipment")]
  public class EU_CourierChargesController : Controller
  {
    private readonly IEU_CourierChargesRepository _eu_CourierChargesRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly ILogger _logger;
    protected readonly LinkGenerator _linkGenerator;

    public EU_CourierChargesController(IEU_CourierChargesRepository eu_CourierChargesRepository,
    IUnitOfWork unitOfWork, IMapper mapper, ILogger<EU_CourierChargesController> logger, LinkGenerator linkGenerator)
    {
      _unitOfWork = unitOfWork;
      _eu_CourierChargesRepository = eu_CourierChargesRepository;
      _mapper = mapper;
      _logger = logger;
      _linkGenerator = linkGenerator;
    }

    /// <summary>
    /// List all EU_CourierCharges, by default sorted by EU_CourierChargesCost
    /// </summary>
    /// <remarks>With pagination, you can fetch up to x records per page.</remarks>
    [AllowAnonymous]
    [HttpGet("{page}/{pageSize}", Name = "GetEU_CourierCharges")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetEU_CourierCharges(
        string sortOrder, int? First20KgPrice, int? EU_CourierChargesId, 
        string searchString, int? page = 1, int? pageSize = ConstantProps.maxPageSize)
    {
      try
      {
        ResponseObject resObject;
        if (!page.HasValue || page.Value == 0)
        {
          _logger.LogInformation("Generating List");
          var dataFromRepo = await _eu_CourierChargesRepository.GetAllEU_CourierCharges();
          var dataList = _mapper.Map<IEnumerable<EU_CourierChargesDto>>(dataFromRepo);
          if (EU_CourierChargesId.HasValue)
          {
            dataFromRepo = dataFromRepo.Where(x => x.EU_CourierChargesId == EU_CourierChargesId);
          }
          if (First20KgPrice.HasValue)
          {
            dataFromRepo = dataFromRepo.Where(x => x.First20KgPrice == First20KgPrice);
          } 
          resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
          return Json(resObject);
        }

        // Sorting -------
        HttpContext.Session.SetString("EU_CourierChargesIdSortParam", String.IsNullOrEmpty(sortOrder) ? "eU_CourierChargesId_desc" : "");
        HttpContext.Session.SetString("EU_CourierChargesIdSortParam", String.IsNullOrEmpty(sortOrder) ? "eU_CourierChargesId_asc" : "");

        HttpContext.Session.SetString("First20KgPriceSortParam", String.IsNullOrEmpty(sortOrder) ? "first20KgPrice_desc" : "");
        HttpContext.Session.SetString("First20KgPriceSortParam", String.IsNullOrEmpty(sortOrder) ? "first20KgPrice_asc" : "");
 
        //search string check
        var previousSearchString = HttpContext.Session.GetString("searchString");

        if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
        {
          page = 1;
        }
        else
        {
          // searchString = currentFilter;
        }
        IQueryable<EU_CourierCharges> data = null;
        data = await _eu_CourierChargesRepository.GetAllEU_CourierCharges(true);
        if (EU_CourierChargesId.HasValue)
        {
          data = data.Where(x => x.EU_CourierChargesId == EU_CourierChargesId);
        }
        if (First20KgPrice.HasValue)
        {
          data = data.Where(x => x.First20KgPrice == First20KgPrice);
         } 

        //search
        if (!String.IsNullOrEmpty(searchString))
        {

          HttpContext.Session.SetString("searchString", searchString);
          data = data.Where(s => s.First20KgPrice.ToString().Contains(searchString));
        }

        else
        {
          HttpContext.Session.SetString("searchString", string.Empty);
        }
        //Sort Order
        switch (sortOrder)
        {
          case "first20KgPrice_desc":
            data = data.OrderByDescending(s => s.First20KgPrice);
            break;
          case "first20KgPrice_asc":
            data = data.OrderBy(s => s.First20KgPrice);
            break; 
          case "eU_CourierChargesId_asc":
            data = data.OrderBy(s => s.EU_CourierChargesId);
            break;
          case "eU_CourierChargesId_desc":
            data = data.OrderByDescending(s => s.EU_CourierChargesId);
            break;
          default:
            data = data.OrderBy(s => s.EU_CourierChargesId);
            break;
        }
        var totalCount = await data.CountAsync();
        PaggingAndSorting paggingSorting = new PaggingAndSorting(_linkGenerator);
        paggingSorting.PaggingAndSortingData(HttpContext, totalCount, "GetEU_CourierCharges", page, pageSize);

        // return result
        var items = await data
            .Skip((int)pageSize * ((int)page - 1))
            .Take((int)pageSize)
            .ToListAsync();

        var pagedData = _mapper.Map<IEnumerable<EU_CourierChargesDto>>(items);
        resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
        return Json(resObject);
      }
      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("GetEU_CourierCharges");
        _logger.LogError(msg);
        var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
        return Json(resObject);
      }
    }

    /// <summary>
    /// Get EU_CourierCharges By Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet("{id}", Name = "GetEU_CourierChargesById")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetEU_CourierChargesById(int id)  // --------------------------------------------------
    {
      try
      {
        ResponseObject resObject;
        _logger.LogInformation("GetEU_CourierChargesById");
        var dataFromRepo = await _eu_CourierChargesRepository.GetEU_CourierChargesById(id);

        if (dataFromRepo == null)
        {
          _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetEU_CourierChargesById({ID}) NOT FOUND", id);
          resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
          return Json(resObject);
        }
        var data = _mapper.Map<EU_CourierChargesDto>(dataFromRepo);

        resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
        return Json(resObject);
      }
      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("GetEU_CourierChargesById");
        _logger.LogError(msg);
        var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
        return Json(resObject);
      }
    }
    /// <summary>
    /// Add EU_CourierCharges - POST api/EU_CourierCharges
    /// </summary>
    /// <param name="eu_CourierCharges"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanCreateEUCourierCharges")]
    [HttpPost(Name = "AddEU_CourierCharges")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> AddEU_CourierCharges([FromBody] EU_CourierChargesDto eu_CourierCharges)
    {
      try
      {
        ResponseObject resObject;
        if (eu_CourierCharges == null)
        {
          _logger.LogError(ConstantProps.NullObjectError("EU_CourierCharges"));
          resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (EU_CourierChargesDto)null, HttpStatusCode = HttpStatusCode.NotFound };
          return Json(resObject);
        }

        if (!ModelState.IsValid)
        {
          _logger.LogError(ConstantProps.InvalidObjectError("EU_CourierCharges"));
          resObject = new ResponseObject
          {
            Message = ConstantProps.InvalidDataModelText,
            Status = "error",
            Data = ModelState,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }
        var eu_CourierChargesRepo = _mapper.Map<EU_CourierCharges>(eu_CourierCharges);
        await _eu_CourierChargesRepository.AddEU_CourierCharges(eu_CourierChargesRepo);
        _logger.LogInformation(LoggingEvents.InsertItem, "EU_CourierCharges {ID} Created", eu_CourierChargesRepo.EU_CourierChargesId);
        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Adding", "EU_CourierCharges"));
        }

        var contactFromRepo2 = CreatedAtAction(nameof(GetEU_CourierChargesById), new { id = eu_CourierChargesRepo.EU_CourierChargesId }, eu_CourierChargesRepo);
        var contactFromRepo = contactFromRepo2.Value;
        var data = _mapper.Map<EU_CourierChargesDto>(contactFromRepo);
        resObject = new ResponseObject
        {
          Message = ConstantProps.SubmittedSuccessfullyText,
          Status = "success",
          Data = data,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("Create");
        _logger.LogWarning(e.Message);
        var resObject = new ResponseObject
        {
          Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
          Message2 = msg,
          Status = "error",
          HttpStatusCode = HttpStatusCode.InternalServerError
        };
        return Json(resObject);
      }
    }

    // PUT api/EU_CourierCharges/5
    [Authorize(Policy = "CanUpdateEUCourierCharges")]
    [HttpPut("{id}", Name = "UpdateEU_CourierCharges")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> UpdateEU_CourierCharges(int id, [FromBody] EU_CourierChargesDto eu_CourierCharges)
    {
      try
      {
        ResponseObject resObject;
        if (eu_CourierCharges == null)
        {
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = (EU_CourierChargesDto)null,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }

        if (!ModelState.IsValid)
        {
          _logger.LogError(ConstantProps.InvalidDataModelText);
          resObject = new ResponseObject
          {
            Message = ConstantProps.InvalidDataModelText,
            Status = "error",
            Data = ModelState,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }
        var eu_CourierChargesRepo = _mapper.Map<EU_CourierCharges>(eu_CourierCharges);
        if (!_eu_CourierChargesRepository.UpdateEU_CourierChargesOk(id, eu_CourierChargesRepo))
        {
          _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = id,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }

        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Updating", "EU_CourierCharges"));
        }

        var contactFromRepo2 = CreatedAtAction(nameof(GetEU_CourierChargesById), new { id = eu_CourierChargesRepo.EU_CourierChargesId }, eu_CourierChargesRepo);
        var contactFromRepo = contactFromRepo2.Value;
        var data = _mapper.Map<EU_CourierChargesDto>(contactFromRepo);
        resObject = new ResponseObject
        {
          Message = ConstantProps.SavedSuccessfullyText,
          Status = "success",
          Data = data,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
        _logger.LogWarning(e.Message);

        var resObject = new ResponseObject
        {
          Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
          Message2 = msg,
          Status = "error",
          HttpStatusCode = HttpStatusCode.InternalServerError
        };
        return Json(resObject);
      }
    }

    /// <summary>
    /// DELETE api/EU_CourierCharges/5
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanDeleteEUCourierCharges")]
    [HttpDelete("{id}", Name = "DeleteEU_CourierCharges")]
    public async Task<IActionResult> DeleteEU_CourierCharges(int id)
    {
      try
      {
        ResponseObject resObject;
        var entity = _eu_CourierChargesRepository.GetEU_CourierChargesByIdAsNoTracking(id);
        if (entity == false)
        {
          _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = id,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }
        _eu_CourierChargesRepository.DeleteEU_CourierCharges(id);
        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Deleting", "EU_CourierCharges"));
        }
        resObject = new ResponseObject
        {
          Message = ConstantProps.DeletedSuccessfullyText,
          Status = "success",
          Data = id,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("Delete");
        _logger.LogError(msg);
        var resObject = new ResponseObject
        {
          Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
          Message = msg,
          Status = "error",
          HttpStatusCode = HttpStatusCode.InternalServerError
        };
        return Json(resObject);
      }
    }
  }
}

