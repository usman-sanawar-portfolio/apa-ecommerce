using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.Domain.Core.Specifications;
using DRS.APA.API.Services;
using DRS.APA.Microservice.API.Controllers.Common;
using DRS.APA.Microserivce.Domain.Core.Dtos.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Dtos.Shipment;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using Microsoft.AspNetCore.Authorization;
//using System.Data.Entity.Validation;

//using DRS.APA.API.Filters;

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.API.Controllers.MasterData
{
    [Route("api/DRS.APA/masterdata/[controller]")]
    [SwaggerGroup("MasterData")]
    public class UK_ParcelDeliveriesController : Controller
    {
        private readonly IUK_ParcelDeliveryRepository _reviewRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        protected readonly LinkGenerator _linkGenerator;

        public UK_ParcelDeliveriesController(IUK_ParcelDeliveryRepository reviewRepository,
            IUnitOfWork unitOfWork, IMapper mapper, ILogger<UK_ParcelDeliveriesController> logger, LinkGenerator linkGenerator)
        {
            _unitOfWork = unitOfWork;
            _reviewRepository = reviewRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }

    /// <summary>
    /// List all UK_ParcelDeliveries, by default sorted by UK_ParcelDeliveryName
    /// </summary>
    /// <remarks>With pagination, you can fetch up to x records per page.</remarks>
    [AllowAnonymous]
    [HttpGet("{page}/{pageSize}", Name = "GetUK_ParcelDeliveries")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetUK_ParcelDeliveries(
            string sortOrder, string searchString, int? page = 1, int? pageSize = ConstantProps.maxPageSize)
        {
            try
            {
                ResponseObject resObject;
                if (!page.HasValue || page.Value == 0)
                {
                    _logger.LogInformation("Generating List");
                    var dataFromRepo = await _reviewRepository.GetAllUK_ParcelDeliveries();
                    var dataList = _mapper.Map<IEnumerable<UK_ParcelDeliveryDto>>(dataFromRepo);

                    resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
                    return Json(resObject);
                }

                // Sorting -------
                HttpContext.Session.SetString("NotesSortParam", String.IsNullOrEmpty(sortOrder) ? "notes_desc" : "");
                HttpContext.Session.SetString("NotesSortParam", String.IsNullOrEmpty(sortOrder) ? "notes_asc" : "");
                HttpContext.Session.SetString("DurationSortParam", String.IsNullOrEmpty(sortOrder) ? "duration_desc" : "");
                HttpContext.Session.SetString("DurationSortParam", String.IsNullOrEmpty(sortOrder) ? "duration_asc" : "");

                HttpContext.Session.SetString("CustomKgMaxSortParam", String.IsNullOrEmpty(sortOrder) ? "customKgMax_desc" : "");
                HttpContext.Session.SetString("CustomKgMaxSortParam", String.IsNullOrEmpty(sortOrder) ? "customKgMax_asc" : "");
                HttpContext.Session.SetString("CustomKgMinSortParam", String.IsNullOrEmpty(sortOrder) ? "customKgMin_desc" : "");
                HttpContext.Session.SetString("CustomKgMinSortParam", String.IsNullOrEmpty(sortOrder) ? "customKgMin_asc" : "");

                //search string check
                var previousSearchString = HttpContext.Session.GetString("searchString");

                if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
                {
                    page = 1;
                }
               
                IQueryable<UK_ParcelDelivery> data = null;
                data = await _reviewRepository.GetAllUK_ParcelDeliveries(true);
                //search
                if (!String.IsNullOrEmpty(searchString))
                {
                    HttpContext.Session.SetString("searchString", searchString);
                    data = data.Where(s => s.Duration.Contains(searchString)
                    ||s.Notes.Contains(searchString)
                    );
                }
                else
                {
                    HttpContext.Session.SetString("searchString", string.Empty);
                }
                //Sort Order
                switch (sortOrder)
                {
                    case "taxt_desc":
                        data = data.OrderByDescending(s => s.Notes);
                        break;
                   
                    case "duration_desc":
                        data = data.OrderByDescending(s => s.Duration);
                        break;
                    case "duration_asc":
                        data = data.OrderBy(s => s.Duration);
                        break;
                  
                    case "customKgMax_desc":
                        data = data.OrderByDescending(s => s.CustomKgMax);
                        break;
                    case "customKgMax_asc":
                        data = data.OrderBy(s => s.CustomKgMax);
                        break;
                    case "customKgMin_desc":
                        data = data.OrderByDescending(s => s.CustomKgMin);
                        break;
                    case "customKgMin_asc":
                        data = data.OrderBy(s => s.CustomKgMin);
                        break;
                    default:
                        data = data.OrderBy(s => s.Notes);
                        break;
                }

                var totalCount = await data.CountAsync();
                PaggingAndSorting paggingSorting = new PaggingAndSorting(_linkGenerator);
                paggingSorting.PaggingAndSortingData(HttpContext, totalCount, "GetUK_ParcelDeliveries", page, pageSize);
              
                // return result
                var items = await data
                    .Skip((int)pageSize * ((int)page - 1))
                    .Take((int)pageSize)
                    .ToListAsync();

                var pagedData = _mapper.Map<IEnumerable<UK_ParcelDeliveryDto>>(items);
                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetUK_ParcelDeliveries");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }
        }


    /// <summary>
    /// Get UK_ParcelDelivery By Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet("{id}", Name = "GetUK_ParcelDeliveryById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetUK_ParcelDeliveryById(int id)  // --------------------------------------------------
        {
            try
            {
                ResponseObject resObject;
                _logger.LogInformation("GetUK_ParcelDeliveryById");
                var dataFromRepo = await _reviewRepository.GetUK_ParcelDeliveryById(id);
                if (dataFromRepo == null)
                {
                    _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetUK_ParcelDeliveryById({ID}) NOT FOUND", id);
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }
              
                var data = _mapper.Map<UK_ParcelDeliveryDto>(dataFromRepo);

                resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("GetUK_ParcelDeliveryById");
                _logger.LogError(msg);
                var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
                return Json(resObject);
            }

        }
    /// <summary>
    /// Add UK_ParcelDelivery - POST api/UK_ParcelDelivery
    /// </summary>
    /// <param name="review"></param>
    /// <returns></returns>

    [Authorize(Policy = "CanCreateParcelDelivery")]
    [HttpPost(Name = "AddUK_ParcelDelivery")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddUK_ParcelDelivery([FromBody] UK_ParcelDeliveryDto review)  // ---------------------
        {
            try
            {
                ResponseObject resObject;
                if (review == null)
                {
                    _logger.LogError(ConstantProps.NullObjectError("UK_ParcelDelivery"));
                    resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (UK_ParcelDeliveryDto)null, HttpStatusCode = HttpStatusCode.NotFound };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidObjectError("UK_ParcelDelivery"));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };

                    return Json(resObject);
                }

                var reviewRepo = _mapper.Map<UK_ParcelDelivery>(review);
                await _reviewRepository.AddUK_ParcelDelivery(reviewRepo);
                _logger.LogInformation(LoggingEvents.InsertItem, "UK_ParcelDelivery {ID} Created", reviewRepo.UK_ParcelDeliveryId);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Adding", "UK_ParcelDelivery"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetUK_ParcelDeliveryById), new { id = reviewRepo.UK_ParcelDeliveryId }, reviewRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<UK_ParcelDeliveryDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SubmittedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Create");
                _logger.LogWarning(e.Message);
                var resObject = new ResponseObject {
                    Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), 
                    Message2 = msg, 
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError 
                };
                return Json(resObject);
            }

        }

    // PUT api/UK_ParcelDelivery/5
    [Authorize(Policy = "CanUpdateParcelDelivery")]
    [HttpPut("{id}", Name = "UpdateUK_ParcelDelivery")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateUK_ParcelDelivery(int id, [FromBody] UK_ParcelDeliveryDto review) // ------------
        {
            try
            {
                ResponseObject resObject;
                if (review == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = (UK_ParcelDeliveryDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(ConstantProps.InvalidDataModelText);
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.InvalidDataModelText,
                        Status = "error",
                        Data = ModelState,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                var reviewRepo = _mapper.Map<UK_ParcelDelivery>(review);
                if (!_reviewRepository.UpdateUK_ParcelDeliveryOk(id, reviewRepo))
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Updating", "UK_ParcelDelivery"));
                }

                var contactFromRepo2 = CreatedAtAction(nameof(GetUK_ParcelDeliveryById), new { id = reviewRepo.UK_ParcelDeliveryId }, reviewRepo);
                var contactFromRepo = contactFromRepo2.Value;
                var data = _mapper.Map<UK_ParcelDeliveryDto>(contactFromRepo);
                resObject = new ResponseObject
                {
                    Message = ConstantProps.SavedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }

    /// <summary>
    /// DELETE api/UK_ParcelDelivery/5
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Policy = "CanDeleteParcelDelivery")]
    [HttpDelete("{id}", Name = "DeleteUK_ParcelDelivery")]
        public async Task<IActionResult> DeleteUK_ParcelDelivery(int id)
        {
            try
            {
                ResponseObject resObject;
                var entity = _reviewRepository.GetUK_ParcelDeliveryByIdAsNoTracking(id);
                if (entity == false)
                {
                    _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = id,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                _reviewRepository.DeleteUK_ParcelDelivery(id);
                if (!await _unitOfWork.CompleteAsync())
                {
                    throw new Exception(ConstantProps.FailedOnSave("Deleting", "UK_ParcelDelivery"));
                }
                resObject = new ResponseObject
                {
                    Message = ConstantProps.DeletedSuccessfullyText,
                    Status = "success",
                    Data = id,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }

            catch (Exception e)
            {
                string msg = ConstantProps.InternalServerError("Delete");
                _logger.LogError(msg);
                var resObject = new ResponseObject
                {
                    Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
        }
    }
}
