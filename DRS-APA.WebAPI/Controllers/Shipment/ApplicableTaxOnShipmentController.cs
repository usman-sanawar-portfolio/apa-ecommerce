using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DRS.APA.API.Helpers;
using DRS.APA.Core.Repositories; 
using DRS.APA.Domain.Core.Shared;
using DRS.APA.API.Services;
using DRS.APA.Microservice.API.Controllers.Common;
using DRS.APA.Microserivce.Domain.Core.Repositories.Shipment;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using DRS.APA.Microserivce.Domain.Core.Dtos.Shipment; 

[assembly: ApiConventionType(typeof(DefaultApiConventions))]
namespace DRS.APA.Microservice.API.Controllers.Shipment
{
  [Route("api/DRS.APA/Shipment/[controller]")]
  [SwaggerGroup("Shipment")]
  public class ApplicableTaxOnShipmentController : Controller
  {
    private readonly IApplicableTaxOnShipmentRepository _applicableTaxOnShipmentRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly ILogger _logger;
    protected readonly LinkGenerator _linkGenerator;

    public ApplicableTaxOnShipmentController(IApplicableTaxOnShipmentRepository applicableTaxOnShipmentRepository,
    IUnitOfWork unitOfWork, IMapper mapper, ILogger<ApplicableTaxOnShipmentController> logger, LinkGenerator linkGenerator)
    {
      _unitOfWork = unitOfWork;
      _applicableTaxOnShipmentRepository = applicableTaxOnShipmentRepository;
      _mapper = mapper;
      _logger = logger;
      _linkGenerator = linkGenerator;
    }

    /// <summary>
    /// List all ApplicableTaxOnShipment, by default sorted by ApplicableTaxOnShipmentCost
    /// </summary>
    /// <remarks>With pagination, you can fetch up to x records per page.</remarks>
     [HttpGet("{page}/{pageSize}", Name = "GetApplicableTaxOnShipment")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetApplicableTaxOnShipment(
        string sortOrder,  int? ApplicableTaxOnShipmentId, 
        string searchString, int? page = 1, int? pageSize = ConstantProps.maxPageSize)
    {
      try
      {
        ResponseObject resObject;
        if (!page.HasValue || page.Value == 0)
        {
          _logger.LogInformation("Generating List");
          var dataFromRepo = await _applicableTaxOnShipmentRepository.GetAllApplicableTaxOnShipment();
          var dataList = _mapper.Map<IEnumerable<ApplicableTaxOnShipmentDto>>(dataFromRepo);
          if (ApplicableTaxOnShipmentId.HasValue)
          {
            dataFromRepo = dataFromRepo.Where(x => x.ApplicableTaxOnShipmentId == ApplicableTaxOnShipmentId);
          } 
          resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = dataList, HttpStatusCode = HttpStatusCode.OK };
          return Json(resObject);
        }

        // Sorting -------
        HttpContext.Session.SetString("ApplicableTaxOnShipmentIdSortParam", String.IsNullOrEmpty(sortOrder) ? "ApplicableTaxOnShipmentId_desc" : "");
        HttpContext.Session.SetString("ApplicableTaxOnShipmentIdSortParam", String.IsNullOrEmpty(sortOrder) ? "ApplicableTaxOnShipmentId_asc" : "");

        //search string check
        var previousSearchString = HttpContext.Session.GetString("searchString");

        if (searchString != null && searchString != HttpContext.Session.GetString("searchString"))
        {
          page = 1;
        }
        else
        {
          // searchString = currentFilter;
        }
        IQueryable<ApplicableTaxOnShipment> data = null;
        data = await _applicableTaxOnShipmentRepository.GetAllApplicableTaxOnShipment(true);
        if (ApplicableTaxOnShipmentId.HasValue)
        {
          data = data.Where(x => x.ApplicableTaxOnShipmentId == ApplicableTaxOnShipmentId);
        } 

        //search
        if (!String.IsNullOrEmpty(searchString))
        {

          HttpContext.Session.SetString("searchString", searchString);
          data = data.Where(s => s.TaxClass.TaxClassTitle.ToString().Contains(searchString));
        }

        else
        {
          HttpContext.Session.SetString("searchString", string.Empty);
        }
        //Sort Order
        switch (sortOrder)
        { 
          case "ApplicableTaxOnShipmentId_asc":
            data = data.OrderBy(s => s.ApplicableTaxOnShipmentId);
            break;
          case "ApplicableTaxOnShipmentId_desc":
            data = data.OrderByDescending(s => s.ApplicableTaxOnShipmentId);
            break;
          default:
            data = data.OrderBy(s => s.ApplicableTaxOnShipmentId);
            break;
        }
        var totalCount = await data.CountAsync();
        PaggingAndSorting paggingSorting = new PaggingAndSorting(_linkGenerator);
        paggingSorting.PaggingAndSortingData(HttpContext, totalCount, "GetApplicableTaxOnShipment", page, pageSize);

        // return result
        var items = await data
            .Skip((int)pageSize * ((int)page - 1))
            .Take((int)pageSize)
            .ToListAsync();

        var pagedData = _mapper.Map<IEnumerable<ApplicableTaxOnShipmentDto>>(items);
        resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = pagedData, HttpStatusCode = HttpStatusCode.OK };
        return Json(resObject);
      }
      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("GetApplicableTaxOnShipment");
        _logger.LogError(msg);
        var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
        return Json(resObject);
      }
    }

    /// <summary>
    /// Get ApplicableTaxOnShipment By Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
     [HttpGet("{id}", Name = "GetApplicableTaxOnShipmentById")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetApplicableTaxOnShipmentById(int id)  // --------------------------------------------------
    {
      try
      {
        ResponseObject resObject;
        _logger.LogInformation("GetApplicableTaxOnShipmentById");
        var dataFromRepo = await _applicableTaxOnShipmentRepository.GetApplicableTaxOnShipmentById(id);

        if (dataFromRepo == null)
        {
          _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetApplicableTaxOnShipmentById({ID}) NOT FOUND", id);
          resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = id, HttpStatusCode = HttpStatusCode.NotFound };
          return Json(resObject);
        }
        var data = _mapper.Map<ApplicableTaxOnShipmentDto>(dataFromRepo);

        resObject = new ResponseObject { Message = ConstantProps.dataListText, Status = "success", Data = data, HttpStatusCode = HttpStatusCode.OK };
        return Json(resObject);
      }
      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("GetApplicableTaxOnShipmentById");
        _logger.LogError(msg);
        var resObject = new ResponseObject { Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem), Message = msg, Status = "error", HttpStatusCode = HttpStatusCode.InternalServerError };
        return Json(resObject);
      }
    }
    /// <summary>
    /// Add ApplicableTaxOnShipment - POST api/ApplicableTaxOnShipment
    /// </summary>
    /// <param name="applicableTaxOnShipment"></param>
    /// <returns></returns>
     [HttpPost(Name = "AddApplicableTaxOnShipment")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> AddApplicableTaxOnShipment([FromBody] ApplicableTaxOnShipmentDto applicableTaxOnShipment)
    {
      try
      {
        ResponseObject resObject;
        if (applicableTaxOnShipment == null)
        {
          _logger.LogError(ConstantProps.NullObjectError("ApplicableTaxOnShipment"));
          resObject = new ResponseObject { Message = ConstantProps.dataNotFoundText, Status = "error", Data = (ApplicableTaxOnShipmentDto)null, HttpStatusCode = HttpStatusCode.NotFound };
          return Json(resObject);
        }

        if (!ModelState.IsValid)
        {
          _logger.LogError(ConstantProps.InvalidObjectError("ApplicableTaxOnShipment"));
          resObject = new ResponseObject
          {
            Message = ConstantProps.InvalidDataModelText,
            Status = "error",
            Data = ModelState,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }
        var applicableTaxOnShipmentRepo = _mapper.Map<ApplicableTaxOnShipment>(applicableTaxOnShipment);
        await _applicableTaxOnShipmentRepository.AddApplicableTaxOnShipment(applicableTaxOnShipmentRepo);
        _logger.LogInformation(LoggingEvents.InsertItem, "ApplicableTaxOnShipment {ID} Created", applicableTaxOnShipmentRepo.ApplicableTaxOnShipmentId);
        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Adding", "ApplicableTaxOnShipment"));
        }

        var contactFromRepo2 = CreatedAtAction(nameof(GetApplicableTaxOnShipmentById), new { id = applicableTaxOnShipmentRepo.ApplicableTaxOnShipmentId }, applicableTaxOnShipmentRepo);
        var contactFromRepo = contactFromRepo2.Value;
        var data = _mapper.Map<ApplicableTaxOnShipmentDto>(contactFromRepo);
        resObject = new ResponseObject
        {
          Message = ConstantProps.SubmittedSuccessfullyText,
          Status = "success",
          Data = data,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("Create");
        _logger.LogWarning(e.Message);
        var resObject = new ResponseObject
        {
          Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
          Message2 = msg,
          Status = "error",
          HttpStatusCode = HttpStatusCode.InternalServerError
        };
        return Json(resObject);
      }
    }

    // PUT api/ApplicableTaxOnShipment/5
     [HttpPut("{id}", Name = "UpdateApplicableTaxOnShipment")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> UpdateApplicableTaxOnShipment(int id, [FromBody] ApplicableTaxOnShipmentDto applicableTaxOnShipment)
    {
      try
      {
        ResponseObject resObject;
        if (applicableTaxOnShipment == null)
        {
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = (ApplicableTaxOnShipmentDto)null,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }

        if (!ModelState.IsValid)
        {
          _logger.LogError(ConstantProps.InvalidDataModelText);
          resObject = new ResponseObject
          {
            Message = ConstantProps.InvalidDataModelText,
            Status = "error",
            Data = ModelState,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }
        var applicableTaxOnShipmentRepo = _mapper.Map<ApplicableTaxOnShipment>(applicableTaxOnShipment);
        if (!_applicableTaxOnShipmentRepository.UpdateApplicableTaxOnShipmentOk(id, applicableTaxOnShipmentRepo))
        {
          _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = id,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }

        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Updating", "ApplicableTaxOnShipment"));
        }

        var contactFromRepo2 = CreatedAtAction(nameof(GetApplicableTaxOnShipmentById), new { id = applicableTaxOnShipmentRepo.ApplicableTaxOnShipmentId }, applicableTaxOnShipmentRepo);
        var contactFromRepo = contactFromRepo2.Value;
        var data = _mapper.Map<ApplicableTaxOnShipmentDto>(contactFromRepo);
        resObject = new ResponseObject
        {
          Message = ConstantProps.SavedSuccessfullyText,
          Status = "success",
          Data = data,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
        _logger.LogWarning(e.Message);

        var resObject = new ResponseObject
        {
          Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
          Message2 = msg,
          Status = "error",
          HttpStatusCode = HttpStatusCode.InternalServerError
        };
        return Json(resObject);
      }
    }

    /// <summary>
    /// DELETE api/ApplicableTaxOnShipment/5
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpDelete("{id}", Name = "DeleteApplicableTaxOnShipment")]
    public async Task<IActionResult> DeleteApplicableTaxOnShipment(int id)
    {
      try
      {
        ResponseObject resObject;
        var entity = _applicableTaxOnShipmentRepository.GetApplicableTaxOnShipmentByIdAsNoTracking(id);
        if (entity == false)
        {
          _logger.LogError(ConstantProps.EntityWithIdNotFound(id.ToString()));
          resObject = new ResponseObject
          {
            Message = ConstantProps.dataNotFoundText,
            Status = "error",
            Data = id,
            HttpStatusCode = HttpStatusCode.BadRequest
          };
          return Json(resObject);
        }
        _applicableTaxOnShipmentRepository.DeleteApplicableTaxOnShipment(id);
        if (!await _unitOfWork.CompleteAsync())
        {
          throw new Exception(ConstantProps.FailedOnSave("Deleting", "ApplicableTaxOnShipment"));
        }
        resObject = new ResponseObject
        {
          Message = ConstantProps.DeletedSuccessfullyText,
          Status = "success",
          Data = id,
          HttpStatusCode = HttpStatusCode.OK
        };
        return Json(resObject);
      }

      catch (Exception e)
      {
        string msg = ConstantProps.InternalServerError("Delete");
        _logger.LogError(msg);
        var resObject = new ResponseObject
        {
          Message2 = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
          Message = msg,
          Status = "error",
          HttpStatusCode = HttpStatusCode.InternalServerError
        };
        return Json(resObject);
      }
    }
  }
}

