﻿using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Controllers;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.API.Helpers
{
    public static class ApiDescriptionExtensions
    {
        public static string GroupBySwaggerGroupAttribute(this ApiDescription api)
        {
            //var groupNameAttribute = (SwaggerGroupAttribute)api.ControllerAttributes().SingleOrDefault(attribute => attribute is SwaggerGroupAttribute);
            var groupNameAttribute = (SwaggerGroupAttribute)Swashbuckle.AspNetCore.SwaggerGen.ApiDescriptionExtensions.CustomAttributes(api).SingleOrDefault(attribute => attribute is SwaggerGroupAttribute);

            // ------
            // Lifted from ApiDescriptionExtensions
            var actionDescriptor = api.GetProperty<ControllerActionDescriptor>();

            if (actionDescriptor == null)
            {
                actionDescriptor = api.ActionDescriptor as ControllerActionDescriptor;
                api.SetProperty(actionDescriptor);
            }
            // ------

            return groupNameAttribute != null ? groupNameAttribute.GroupName+" > "+ actionDescriptor?.ControllerName : actionDescriptor?.ControllerName;
        }
    }
}
