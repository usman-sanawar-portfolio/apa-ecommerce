using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MailKit.Security;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Newsletter;
using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using System;

namespace DRSAPA.Microservice.API.Services
{
    public class MailService : IMailService
    {
        private readonly MailSettings _mailSettings;
        ISubscriberRepository _subscriberRepository;
        INewsletterSentRepository _newsletterSentRepository;
        INewsletterRepository _newsletterRepository;
        IConfiguration _configuration;
        IWebHostEnvironment _env;
        public MailService(IOptions<MailSettings> mailSettings, IConfiguration configuration, INewsletterSentRepository newsletterSentRepository, INewsletterRepository newsletterRepository,

        ISubscriberRepository subscriberRepository, IWebHostEnvironment env)
        {
            _mailSettings = mailSettings.Value;
            _configuration = configuration;
            _newsletterSentRepository = newsletterSentRepository;
            _newsletterRepository = newsletterRepository;
            _subscriberRepository = subscriberRepository;
            _env = env;
        }

        public async Task SendEmailAsync(Newsletter mailRequest)
        {
            NewsletterSent newsLetterSent = new NewsletterSent();
            var subscriber = await _subscriberRepository.GetAllSubscribers();

            foreach (var subs in subscriber)
            {
                if (subs.IsActive == true)
                {
                    var email = new MimeMessage();
                    email.Sender = MailboxAddress.Parse(_mailSettings.Mail);
                    email.To.Add(MailboxAddress.Parse(subs.Email));
                    email.Subject = mailRequest.Title;
                    var builder = new BodyBuilder();

                    builder.HtmlBody = mailRequest.Content;
                    builder.HtmlBody += "";
                    email.Body = builder.ToMessageBody();
                    using var smtp = new SmtpClient();

                    smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTlsWhenAvailable);
                    smtp.Authenticate(_mailSettings.Mail, _mailSettings.Password);
                    await smtp.SendAsync(email);
                    newsLetterSent.NewsletterId = mailRequest.NewsletterId;
                    newsLetterSent.SubscriberId = subs.SubscriberId;
                    _newsletterSentRepository.AddNewsletterSent(newsLetterSent);
                    newsLetterSent.NewsletterId = 0;
                    newsLetterSent.SubscriberId = 0;
                    await smtp.DisconnectAsync(true);
                }
            }
        }
        public async Task ReSendEmailAsync(Newsletter mailRequest)
        {
            NewsletterSent newsLetterSent = new NewsletterSent();
            ICollection<int> collection = new List<int>();
            foreach (var sendEmailAddress in mailRequest.NewsletterSent)
            {
                collection.Add(sendEmailAddress.SubscriberId);
            }

            var subscriber = await _subscriberRepository.GetAllResentSubscribers(collection);
            foreach (var subs in subscriber)
            {
                if (subs.IsActive == true)
                {
                    var email = new MimeMessage();
                    email.Sender = MailboxAddress.Parse(_mailSettings.Mail);
                    email.To.Add(MailboxAddress.Parse(subs.Email));
                    email.Subject = mailRequest.Title;
                    var builder = new BodyBuilder();

                    builder.HtmlBody = mailRequest.Content;
                    email.Body = builder.ToMessageBody();
                    using var smtp = new SmtpClient();

                    smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTlsWhenAvailable);
                    smtp.Authenticate(_mailSettings.Mail, _mailSettings.Password);
                    await smtp.SendAsync(email);
                    newsLetterSent.NewsletterId = mailRequest.NewsletterId;
                    newsLetterSent.SubscriberId = subs.SubscriberId;
                    var newsletterCheck = _newsletterSentRepository.GetNewsletterSentByIdAsNoTracking(newsLetterSent.NewsletterId, newsLetterSent.SubscriberId);
                    if (newsletterCheck != true)
                    {
                        _newsletterSentRepository.AddNewsletterSent(newsLetterSent);

                    }
                    else
                    {
                        _newsletterRepository.UpdateResendNewsletterOk(mailRequest.NewsletterId, mailRequest);

                    }
                    newsLetterSent.NewsletterId = 0;
                    newsLetterSent.SubscriberId = 0;
                    await smtp.DisconnectAsync(true);
                }
            }
        }

        public async Task SendUserConfirmationEmail(string Name, string ToEmailAddress, string VerificationCode, bool isSiteUser)
        {
            if (!string.IsNullOrEmpty(ToEmailAddress))
            {

                string body = string.Empty;
                string contentRootPath = _env.WebRootPath + "\\Uploads\\EmailTemplate";
                using (StreamReader reader = new StreamReader(contentRootPath + "\\UserConfirmationEmail.html"))
                {
                    body = reader.ReadToEnd();
                }

                var email = new MimeMessage();
                email.Sender = MailboxAddress.Parse(_mailSettings.Mail);
                email.To.Add(MailboxAddress.Parse(ToEmailAddress));
                email.Subject = "Account Confirmation Email";
                var builder = new BodyBuilder();
                body = body.Replace("{ClientName}", Name);
                if (isSiteUser == false)
                {
                    body = body.Replace("{Link}", _configuration["UserActivation:Link"] + VerificationCode);
                }
                else
                {
                    body = body.Replace("{Link}", _configuration["CustomerUserActivation:Link"] + VerificationCode);
                }
                builder.HtmlBody = body;

                email.Body = builder.ToMessageBody();
                using var smtp = new SmtpClient();
                smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTlsWhenAvailable);
                smtp.Authenticate(_mailSettings.Mail, _mailSettings.Password);
                await smtp.SendAsync(email);
                await smtp.DisconnectAsync(true);
            }
        }
        public async Task SendSaleOrderConfirmationEmail(SaleOrder saleOrder, string CustomerEmail)
       {
            if (!string.IsNullOrEmpty(CustomerEmail))
            {

                string body = string.Empty;
                string contentRootPath = _env.WebRootPath + "\\Uploads\\EmailTemplate";
                using (StreamReader reader = new StreamReader(contentRootPath + "\\SaleOrderConfirmationEmail.html"))
                {
                    body = reader.ReadToEnd();
                }
                var email = new MimeMessage();
                email.Sender = MailboxAddress.Parse(_mailSettings.Mail);
                email.To.Add(MailboxAddress.Parse(CustomerEmail));
                email.Subject = "Sale Order Confirmation Email";
                var builder = new BodyBuilder();
                if (saleOrder.Customers != null)
                {
                    body = body.Replace("{ClientName}", saleOrder.Customers.FirstName + " " + saleOrder.Customers.LastName);
                }
                string SaleOrderlinesDetails = "";
                body = body.Replace("{SaleOrderNumber}", saleOrder.SaleIdentifier);
                foreach (var v in saleOrder.SaleOrderLines)
                {
                    SaleOrderlinesDetails += "Product :" + v.Product.ProductName + "/n";
                    foreach (var o in v.SaleOrderLineProductOptions)
                    {
                        SaleOrderlinesDetails += "Selected Model :" + o.OptionModel;
                        foreach (var oc in o.SaleOrderLineProductOptionCombinations)
                        {
                            SaleOrderlinesDetails += "Selected Option Name :" + oc.OptionValue.Name; ;
                        }
                    }
                    SaleOrderlinesDetails += "Quantity :" + v.Quantity + "/n";
                    SaleOrderlinesDetails += "Discount Percentage :" + v.DiscountPercentage + "/n";
                    foreach (var i in v.SaleOrderLineTaxRates)
                    {
                        SaleOrderlinesDetails += "Applied taxes :" + i.SaleOrderLineTaxRateName + " - " + i.SaleOrderTaxRate + " % /n";
                    }
          SaleOrderlinesDetails += "Line Total : " + v.LineTotal + "/n";
             }

        string textBody = " <table border=" + 1 + " cellpadding=" + 0 + " cellspacing=" + 0 + " width = " + 100 +"%"+ "><tr bgcolor='#4da6ff'><td><b>Product Name</b></td><td> <b> Quantity </b> </td><td> <b> Tax </b> </td> <td> <b> Discount </b> </td> <td> <b> Price </b> </td></tr>";



        foreach (var item in saleOrder.SaleOrderLines)
        {
          string ProductOptions = "";

          foreach (var o in item.SaleOrderLineProductOptions)
          {
            ProductOptions += "Selected Model :" + o.OptionModel +" </br>";
            foreach (var oc in o.SaleOrderLineProductOptionCombinations)
            {
              ProductOptions += "Selected Option Name :" + oc.OptionValue.Name + " </br>"; 
            }
          }
         


          decimal totalTax = 0;
          string Appliedtax = "";
          int i = 1;
          foreach (var v in item.SaleOrderLineTaxRates)
          {
            Appliedtax += i++  +": "+ v.SaleOrderLineTaxRateName + " - " + Convert.ToInt32(v.SaleOrderTaxRate) + " % </br>";
            var taxs = v.SaleOrderTaxRate;
            totalTax += taxs;
          }
          if (ProductOptions == "")
          {
            textBody += "<tr><td>" + item.Product.ProductName + "</td><td> " + item.Quantity + "</td><td> " + Appliedtax + "</td><td> " + Convert.ToInt32(item.DiscountPercentage) + "%" + "</td><td> " + item.LineTotal + "</td> </tr> ";

          }
          else
          {
            textBody += "<tr><td>" + item.Product.ProductName + "(" + ProductOptions + ")" + "</td><td> " + item.Quantity + "</td><td> " + /*Convert.ToInt32(totalTax)*/ Appliedtax + "</td><td> " + Convert.ToInt32(item.DiscountPercentage) + "%" + "</td><td> " + item.LineTotal + "</td> </tr> ";


          }
        }
        textBody += "</table>";
        body = body.Replace("{data}", textBody);

        SaleOrderlinesDetails += "Grand Total : " + saleOrder.SaleOrderAmountWithTaxAndDiscount + "/n";
                body = body.Replace("{GrandTotal}", saleOrder.SaleOrderAmountWithTaxAndDiscount.ToString("0.00"));
                body = body.Replace("{OrderDetails}", SaleOrderlinesDetails);

                builder.HtmlBody = body;
                email.Body = builder.ToMessageBody();
                using var smtp = new SmtpClient();
                smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTlsWhenAvailable);
                smtp.Authenticate(_mailSettings.Mail, _mailSettings.Password);
                await smtp.SendAsync(email);
                await smtp.DisconnectAsync(true);
            }
        }
        public async Task ForgetPasswordEmail(string resetToken, string emailAddress)
        {
            if (!string.IsNullOrEmpty(emailAddress))
            {

                string body = string.Empty;
                string contentRootPath = _env.WebRootPath + "\\Uploads\\EmailTemplate";
                using (StreamReader reader = new StreamReader(contentRootPath + "\\ForgetPasswordEmail2.html"))
                {
                    body = reader.ReadToEnd();
                }

                var email = new MimeMessage();
                email.Sender = MailboxAddress.Parse(_mailSettings.Mail);
                email.To.Add(MailboxAddress.Parse(emailAddress));
                email.Subject = "Reset Password Email";
                var builder = new BodyBuilder();
                body = body.Replace("{ClientName}", emailAddress);

                body = body.Replace("{Link}", _configuration["ForgetPassword:Link"] + "?" + resetToken);

                builder.HtmlBody = body;

                email.Body = builder.ToMessageBody();
                using var smtp = new SmtpClient();
                smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTlsWhenAvailable);
                smtp.Authenticate(_mailSettings.Mail, _mailSettings.Password);
                await smtp.SendAsync(email);
                await smtp.DisconnectAsync(true);
            }
        }

        public async Task PasswordChangedByAdmin(string Name, string emailAddress)
        {
            if (!string.IsNullOrEmpty(emailAddress))
            {

                string body = string.Empty;
                string contentRootPath = _env.WebRootPath + "\\Uploads\\EmailTemplate";
                using (StreamReader reader = new StreamReader(contentRootPath + "\\PasswordChangedEmail.html"))
                {
                    body = reader.ReadToEnd();
                }

                var email = new MimeMessage();
                email.Sender = MailboxAddress.Parse(_mailSettings.Mail);
                email.To.Add(MailboxAddress.Parse(emailAddress));
                email.Subject = "Account Confirmation Email";
                var builder = new BodyBuilder();
                body = body.Replace("{ClientName}", Name);

                builder.HtmlBody = body;

                email.Body = builder.ToMessageBody();
                using var smtp = new SmtpClient();
                smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTlsWhenAvailable);
                smtp.Authenticate(_mailSettings.Mail, _mailSettings.Password);
                await smtp.SendAsync(email);
                await smtp.DisconnectAsync(true);
            }
        }



    }
}
