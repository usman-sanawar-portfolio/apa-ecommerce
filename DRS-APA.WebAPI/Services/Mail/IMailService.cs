using DRS.APA.Microserivce.Domain.Core.Domain.Newsletter;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRSAPA.Microservice.API.Services
{
    public interface IMailService
    {
        Task SendEmailAsync(Newsletter mailRequest);
        Task ReSendEmailAsync(Newsletter mailRequest);
        Task SendUserConfirmationEmail(string Name, string ToEmailAddress, string VerificationCode,bool isSiteUser);
        Task SendSaleOrderConfirmationEmail(SaleOrder saleOrder, string CustomerEmail);
        Task Forget
            Email(string resetToken, string emailAddress);
        Task PasswordChangedByAdmin(string Name, string emailAddress);
    }
}
