using AutoMapper;
using DRS.APA.Domain.Core.Domain.JWT;
using DRS.APA.Domain.Core.Domain.MasterData;
using DRS.APA.Domain.Core.Dtos.MasterData;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.Microserivce.Domain.Core.Domain.MasterData;
using DRS.APA.Microserivce.Domain.Core.Domain.Newsletter;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using DRS.APA.Microserivce.Domain.Core.Domain.Shipment;
using DRS.APA.Microserivce.Domain.Core.Domain.Website;
using DRS.APA.Microserivce.Domain.Core.Dtos.MasterData;
using DRS.APA.Microserivce.Domain.Core.Dtos.Newsletter;
using DRS.APA.Microserivce.Domain.Core.Dtos.Orders;
using DRS.APA.Microserivce.Domain.Core.Dtos.Reports;
using DRS.APA.Microserivce.Domain.Core.Dtos.Shared;
using DRS.APA.Microserivce.Domain.Core.Dtos.Shipment;
using DRS.APA.Microserivce.Domain.Core.Dtos.Website;
using DRS.APAAPA.Microserivce.Domain.Core.Domain.Website;
//using DRS.APA.Domain.Core.Domain.AccountAggregate;

namespace CRM.Microservice.API.Services.Mapping
{
    public class CommonMappingProfile : Profile
    {
        public CommonMappingProfile()
        {
            CreateMap<ProductCategory, ProductCategoryDto>()
                .ForMember(c => c.ParentCategoryName, o => o.MapFrom(c => c.ParentCategory.Name));
            CreateMap<ProductCategoryDto, ProductCategory>();

            CreateMap<DeliveryMethod, DeliveryMethodDto>();
            CreateMap<DeliveryMethodDto, DeliveryMethod>();

            CreateMap<Product, ProductDto>()
               .ForMember(c => c.StoreName, o => o.MapFrom(c => c.Store.StoreName))
              .ForMember(c => c.WeightUnitName, o => o.MapFrom(c => c.WeightUnit.WeightUnit))
             .ForMember(c => c.LengthUnitName, o => o.MapFrom(c => c.LengthUnit.LengthUnit))
             .ForMember(c => c.StockStatusName, o => o.MapFrom(c => c.StockStatus.Name))
             .ForMember(c => c.ManufacturerName, o => o.MapFrom(x => x.Manufacturer.ManufacturerName))
             .ForMember(c => c.TotalReviewsCount, o => o.MapFrom(x => x.Reviews.Count));
            CreateMap<ProductDto, Product>();


            CreateMap<Product, ProductShortDto>();
            CreateMap<ProductShortDto, Product>();

            CreateMap<Product, KeyValueDto>();
            CreateMap<KeyValueDto, Product>();


            CreateMap<Product, ProductKeyValueDto>();
            CreateMap<ProductKeyValueDto, Product>();

            CreateMap<ProductPhoto, ProductPhotoDto>()
                .ForMember(c => c.ProductName, o => o.MapFrom(c => c.Product.ProductName));
            CreateMap<ProductPhotoDto, ProductPhoto>();

            CreateMap<ProductCategoriesJunction, ProductCategoriesJunctionDto>()
                .ForMember(x => x.ProductName, x => x.MapFrom(c => c.Product.ProductName))
                .ForMember(x => x.CategoryCode, x => x.MapFrom(c => c.ProductCategory.Code));
            CreateMap<ProductCategoriesJunctionDto, ProductCategoriesJunction>();

            CreateMap<Product, RelatedProductsDto>()
                .ForMember(x => x.ManufacturerName, x => x.MapFrom(x => x.Manufacturer.ManufacturerName));
            CreateMap<RelatedProductsDto, Product>();

            CreateMap<ProductOptions, ProductOptionsDto>()
           .ForMember(x => x.ProductName, x => x.MapFrom(x => x.Product.ProductName)); 

            CreateMap<ProductOptionsDto, ProductOptions>();

            CreateMap<DiscountProduct, DiscountProductDto>()
                .ForMember(x => x.ProductName, x => x.MapFrom(x => x.Product.ProductName))
                .ForMember(x => x.DiscountName, x => x.MapFrom(x => x.Discount.Name));
            CreateMap<DiscountProductDto, DiscountProduct>();

            //CreateMap<Special, SpecialDto>();
            //CreateMap<SpecialDto, Special>();

            //CreateMap<RewardPoints, RewardPointsDto>()
            //    .ForMember(x => x.ProductName, x => x.MapFrom(x => x.Product.ProductName));
            //CreateMap<RewardPointsDto, RewardPoints>();

            //CreateMap<RewardPointCustomerGroup, RewardPointCustomerGroupDto>()
            //    .ForMember(x => x.CustomerGroupName, x => x.MapFrom(x => x.CustomerGroup.CustomerGroupName));            
            //CreateMap<RewardPointCustomerGroupDto, RewardPointCustomerGroup>();


            CreateMap<Collection, CollectionDto>();
            CreateMap<CollectionDto, Collection>();

            CreateMap<CollectionProduct, CollectionProductDto>()
            .ForMember(c => c.CollectionName, o => o.MapFrom(c => c.Collection.Name));
            //.ForMember(c => c.ProductName, o => o.MapFrom(c => c.Product.Title));
            CreateMap<CollectionProductDto, CollectionProduct>();

            CreateMap<Discount, DiscountDto>();
            CreateMap<DiscountDto, Discount>();

            CreateMap<DiscountCustomerGroup, DiscountCustomerGroupDto>()
            .ForMember(c => c.DiscountName, o => o.MapFrom(c => c.Discount.Name))
            .ForMember(c => c.CustomerGroupName, o => o.MapFrom(c => c.CustomerGroup.CustomerGroupName));
            CreateMap<DiscountCustomerGroupDto, DiscountCustomerGroup>();

            CreateMap<Customer, CustomerDto>()
                .ForMember(c => c.CustomerGroupName, o => o.MapFrom(c => c.CustomerGroup.CustomerGroupName));
            CreateMap<CustomerDto, Customer>();

            CreateMap<CustomerGroup, CustomerGroupDto>();
            CreateMap<CustomerGroupDto, CustomerGroup>();

            CreateMap<Country, CountryDto>();
            //.ForMember(c => c.GeoZonesName, o => o.MapFrom(c => c.GeoZones.Name));
            CreateMap<CountryDto, Country>();

            CreateMap<Zone, ZoneDto>()
                .ForMember(c => c.CountryName, o => o.MapFrom(c => c.Country.CountryName));
            CreateMap<ZoneDto, Zone>();

            CreateMap<GeoZones, GeoZonesDto>();
            CreateMap<GeoZonesDto, GeoZones>();

            CreateMap<ZoneToGeoZone, ZoneToGeoZoneDto>()
                .ForMember(x => x.CountryName, o => o.MapFrom(c => c.Country.CountryName))
                .ForMember(x => x.ZoneName, o => o.MapFrom(c => c.Zone.ZoneName))
                .ForMember(x => x.GeoZonesName, o => o.MapFrom(c => c.GeoZones.Name));
            CreateMap<ZoneToGeoZoneDto, ZoneToGeoZone>();
                

            CreateMap<Length, LengthDto>();
            CreateMap<LengthDto, Length>();

            CreateMap<Weight, WeightDto>();
            CreateMap<WeightDto, Weight>();

            CreateMap<Manufacturer, ManufacturerDto>();
            CreateMap<ManufacturerDto, Manufacturer>();

            CreateMap<StockStatus, StockStatusDto>();
            CreateMap<StockStatusDto, StockStatus>();

            CreateMap<TaxRate, TaxRateDto>()
                .ForMember(c => c.TaxClassName, o => o.MapFrom(c => c.TaxClass.TaxClassTitle))
                .ForMember(c => c.GeoZonesName, o => o.MapFrom(c => c.GeoZones.Name));
            CreateMap<TaxRateDto, TaxRate>();

            CreateMap<TaxClass, TaxClassDto>();
            CreateMap<TaxClassDto, TaxClass>();

            CreateMap<Information, InformationDto>();
            CreateMap<InformationDto, Information>();

            CreateMap<Store, StoreDto>();
            CreateMap<StoreDto, Store>();

            CreateMap<TaxRatesCustomerGroups, TaxRatesCustomerGroupsDto>()
                .ForMember(c => c.TaxRateName, o => o.MapFrom(c => c.TaxRate.TaxRateName))
                .ForMember(c => c.CustomerGroupName, o => o.MapFrom(c => c.CustomerGroup.CustomerGroupName));
            CreateMap<TaxRatesCustomerGroupsDto, TaxRatesCustomerGroups>();

            CreateMap<OrderStatus, OrderStatusDto>();
            CreateMap<OrderStatusDto, OrderStatus>();

            CreateMap<ReturnAction, ReturnActionDto>();
            CreateMap<ReturnActionDto, ReturnAction>();

            CreateMap<ReturnReason, ReturnReasonDto>();
            CreateMap<ReturnReasonDto, ReturnReason>();

            CreateMap<ReturnStatus, ReturnStatusDto>();
            CreateMap<Option, OptionDto>()
                .ForMember(c => c.OptionTypeName, o => o.MapFrom(c => c.OptionType.OptionTypeName));
            CreateMap<OptionDto, Option>();
            CreateMap<ReturnStatusDto, ReturnStatus>();


            CreateMap<OptionType, OptionTypeDto>();
            CreateMap<OptionTypeDto, OptionType>();

            CreateMap<OptionValue, OptionValueDto>()
                .ForMember(c => c.OptionName, o => o.MapFrom(c => c.Option.OptionName))
                .ForMember(c => c.OptionTypeId, o => o.MapFrom(c => c.Option.OptionTypeId))
                .ForMember(c => c.OptionTypeName, o => o.MapFrom(c => c.Option.OptionType.OptionTypeName));
            CreateMap<OptionValueDto, OptionValue>();

            CreateMap<WebPage, WebPageDto>();
            CreateMap<WebPageDto, WebPage>();

            CreateMap<WebMenu, WebMenuDto>()
                .ForMember(c => c.WebPageTitle, o => o.MapFrom(c => c.WebPage.WebPageTitle));
            CreateMap<WebMenuDto, WebMenu>();

            CreateMap<WebSubMenu, WebSubMenuDto>()
                .ForMember(c => c.WebPageTitle, o => o.MapFrom(c => c.WebPage.WebPageTitle));
            //.ForMember(c => c.Slug, o => o.MapFrom(c => c.WebPage.Slug));
            CreateMap<WebSubMenuDto, WebSubMenu>();

            CreateMap<WebComponent, WebComponentDto>();
            CreateMap<WebComponentDto, WebComponent>();

            CreateMap<WebPageComponent, WebPageComponentDto>()
                .ForMember(c => c.WebPageTitle, o => o.MapFrom(c => c.WebPage.WebPageTitle))
                .ForMember(c => c.WebComponentTitle, o => o.MapFrom(c => c.WebComponent.Title));
            CreateMap<WebPageComponentDto, WebPageComponent>();

            CreateMap<WebCard, WebCardDto>()
                .ForMember(x => x.WebComponentTitle, c => c.MapFrom(x => x.WebComponent.Title));
            CreateMap<WebCardDto, WebCard>();

            CreateMap<WebPhoto, WebPhotoDto>()
                .ForMember(x => x.WebComponentTitle, c => c.MapFrom(x => x.WebComponent.Title));
            CreateMap<WebPhotoDto, WebPhoto>();

            CreateMap<WebVideo, WebVideoDto>()
              .ForMember(x => x.WebComponentTitle, c => c.MapFrom(x => x.WebComponent.Title));
            CreateMap<WebVideoDto, WebVideo>();

            CreateMap<WebDoc, WebDocDto>()
              .ForMember(x => x.WebComponentTitle, c => c.MapFrom(x => x.WebComponent.Title));
            CreateMap<WebDocDto, WebDoc>();

            CreateMap<WebHtml, WebHtmlDto>()
              .ForMember(x => x.WebComponentTitle, c => c.MapFrom(x => x.WebComponent.Title));
            CreateMap<WebHtmlDto, WebHtml>();

            CreateMap<WebMenu, WebMenuDto>()
                .ForMember(x => x.WebPageTitle, c => c.MapFrom(x => x.WebPage.WebPageTitle))
                .ForMember(x => x.Slug, c => c.MapFrom(x => x.WebPage.Slug));
            CreateMap<WebMenuDto, WebMenu>();

            CreateMap<WebSubMenu, WebSubMenuDto>()
                .ForMember(x => x.WebMenuTitle, c => c.MapFrom(x => x.WebMenu.WebMenuTitle))
                .ForMember(x => x.WebPageTitle, c => c.MapFrom(x => x.WebPage.WebPageTitle))
                .ForMember(x => x.Slug, c => c.MapFrom(x => x.WebPage.Slug));
            CreateMap<WebSubMenuDto, WebSubMenu>();

       CreateMap<ProductOptionCombination, ProductOptionCombinationDto>()
    .ForMember(x => x.OptionName, c => c.MapFrom(x => x.Option.OptionName))
    .ForMember(x => x.OptionValueName, c => c.MapFrom(x => x.OptionValue.Name))
    .ForMember(x => x.OptionTypeId, c => c.MapFrom(x => x.Option.OptionTypeId))
    .ForMember(x => x.OptionTypeName, c => c.MapFrom(x => x.Option.OptionType.OptionTypeName));
      CreateMap<ProductOptionCombinationDto, ProductOptionCombination>();
      CreateMap<ProductOptionCombinationDto, ProductOptionCombination>();

            CreateMap<Review, ReviewDto>()
          .ForMember(x => x.ProductName, c => c.MapFrom(x => x.Product.ProductName))
          .ForMember(x => x.CustomerName, c => c.MapFrom(x => string.Format("{0} {1}", x.Customer.FirstName, x.Customer.LastName)));
            CreateMap<ReviewDto, Review>();

            CreateMap<FileLibrary, FileLibraryDto>();
            CreateMap<FileLibraryDto, FileLibrary>();

            CreateMap<Location, LocationDto>();
            CreateMap<LocationDto, Location>();

            CreateMap<Organization, OrganizationDto>();
            CreateMap<OrganizationDto, Organization>();

            CreateMap<SaleOrder, SaleOrderDto>()
                .ForMember(x => x.OrderStatusCode, x => x.MapFrom(x => x.OrderStatus.Code))
                .ForMember(x => x.CustomerName, x => x.MapFrom(x => x.Customers.FirstName))
                .ForMember(x => x.OrderStatusName, x => x.MapFrom(x => x.OrderStatus.Name));
            CreateMap<SaleOrderDto, SaleOrder>();

            CreateMap<SaleOrderLines, SaleOrderLinesDto>()
                 .ForMember(x => x.SaleIdentifier, c => c.MapFrom(x => x.SaleOrder.SaleIdentifier))
                .ForMember(x => x.ProductName, c => c.MapFrom(x => x.Product.ProductName));
            CreateMap<SaleOrderLinesDto, SaleOrderLines>();

            CreateMap<SaleOrderAddress, SaleOrderAddressDto>()
                   .ForMember(x => x.SaleIdentifier, c => c.MapFrom(x => x.SaleOrder.SaleIdentifier));
            CreateMap<SaleOrderAddressDto, SaleOrderAddress>();

            CreateMap<HomePageCollection, HomePageCollectionDto>()
                .ForMember(x => x.ProductCategoryName, x => x.MapFrom(x => x.ProductCategory.Name))
                .ForMember(x => x.ProductCategoriesJunctionForHome, x => x.MapFrom(x => x.ProductCategory.ProductCategoriesJunction));
            CreateMap<HomePageCollectionDto, HomePageCollection>();

            CreateMap<ProductCategoriesJunction, ProductCategoriesJunctionForHomeDto>();

            CreateMap<Product, ProductForHomePageCollectionDto>();

            CreateMap<WebCarousal, WebCarousalDto>();
            CreateMap<WebCarousalDto, WebCarousal>();

            CreateMap<WebCarousalPhotos, WebCarousalPhotosDto>()
                .ForMember(x => x.PageSlug, x => x.MapFrom(x => x.WebCarousal.PageSlug));
            CreateMap<WebCarousalPhotosDto, WebCarousalPhotos>();

            CreateMap<WebBanner, WebBannerDto>();
            CreateMap<WebBannerDto, WebBanner>();
            CreateMap<CustomerAddress, CustomerAddressDto>()
                 .ForMember(x => x.CustomerName, c => c.MapFrom(x => x.Customer.FirstName));
            CreateMap<CustomerAddressDto, CustomerAddress>();

            CreateMap<SaleOrderLineProductOptions, SaleOrderLineProductOptionsDto>()
                .ForMember(x => x.ProductName, x => x.MapFrom(x => x.SaleOrderLines.Product.ProductName));
            CreateMap<SaleOrderLineProductOptionsDto, SaleOrderLineProductOptions>();

            CreateMap<SaleOrderLineProductOptionCombinations, SaleOrderLineProductOptionCombinationsDto>()
                .ForMember(x => x.OptionName, c => c.MapFrom(x => x.Option.OptionName))
                .ForMember(x => x.OptionValueName, c => c.MapFrom(x => x.OptionValue.Name))
                .ForMember(x => x.OptionTypeId, c => c.MapFrom(x => x.Option.OptionTypeId))
                .ForMember(x => x.OptionTypeName, c => c.MapFrom(x => x.Option.OptionType.OptionTypeName));
            CreateMap<SaleOrderLineProductOptionCombinationsDto, SaleOrderLineProductOptionCombinations>();

            CreateMap<Orders, OrderDto>()
                .ForMember(x => x.OrderStatusCode, x => x.MapFrom(x => x.OrderStatus.Code))
                .ForMember(x => x.CustomerName, x => x.MapFrom(x => x.Customers.FirstName))
               .ForMember(x => x.OrderStatusName, x => x.MapFrom(x => x.OrderStatus.Name));
            CreateMap<OrderDto, Orders>();
            CreateMap<OrderLines, OrderLinesDto>()
                     .ForMember(x => x.OrderIdentifier, c => c.MapFrom(x => x.Orders.OrderIdentifier))
                    .ForMember(x => x.ProductName, c => c.MapFrom(x => x.Product.ProductName));
            CreateMap<OrderLinesDto, OrderLines>();
            CreateMap<OrderAddress, OrderAddressDto>()
                   .ForMember(x => x.OrderIdentifier, c => c.MapFrom(x => x.Orders.OrderIdentifier));
            CreateMap<OrderAddressDto, OrderAddress>();
            CreateMap<OrderLineProductOptions, OrderLineProductOptionsDto>()
            .ForMember(x => x.ProductName, x => x.MapFrom(x => x.OrderLines.Product.ProductName));
            CreateMap<OrderLineProductOptionsDto, OrderLineProductOptions>();

            CreateMap<OrderLineProductOptionCombinations, OrderLineProductOptionCombinationsDto>()
                .ForMember(x => x.OptionName, c => c.MapFrom(x => x.Option.OptionName))
                .ForMember(x => x.OptionValueName, c => c.MapFrom(x => x.OptionValue.Name))
                .ForMember(x => x.OptionTypeId, c => c.MapFrom(x => x.Option.OptionTypeId))
                .ForMember(x => x.OptionTypeName, c => c.MapFrom(x => x.Option.OptionType.OptionTypeName));
            CreateMap<OrderLineProductOptionCombinationsDto, OrderLineProductOptionCombinations>();

            CreateMap<SaleOrderLineTaxRate, SaleOrderLineTaxRateDto>();
            CreateMap<SaleOrderLineTaxRateDto, SaleOrderLineTaxRate>();

            CreateMap<OrderLineTaxRate, OrderLineTaxRateDto>();
            CreateMap<OrderLineTaxRateDto, OrderLineTaxRate>();
            CreateMap<Attachment, AttachmentDto>()
          .ForMember(x => x.ProductName, c => c.MapFrom(x => x.Product.ProductName));
            CreateMap<AttachmentDto, Attachment>();

            CreateMap<Orders, SaleOrder>()
             .ForMember(x => x.SaleIdentifier, c => c.MapFrom(x => x.OrderIdentifier))
             .ForMember(x => x.IsOnlineOrder, c => c.MapFrom(x => x.IsOnlineOrder))
             .ForMember(x => x.IsPaymentOnline, c => c.MapFrom(x => x.IsPaymentOnline))
             .ForMember(x => x.OnlinePaymentId, c => c.MapFrom(x => x.OnlinePaymentId))
             .ForMember(x => x.SaleOrderDate, c => c.MapFrom(x => x.OrderDate))
             .ForMember(x => x.OrderDueDate, c => c.MapFrom(x => x.OrderDueDate))
             .ForMember(x => x.IsCancelled, c => c.MapFrom(x => x.IsCancelled))
             .ForMember(x => x.CancelReason, c => c.MapFrom(x => x.CancelReason))
             .ForMember(x => x.SaleOrderAmountWithTaxAndDiscount, c => c.MapFrom(x => x.OrderAmountWithTaxAndDiscount))
             .ForMember(x => x.SaleOrderNotes, c => c.MapFrom(x => x.OrderNotes))
             .ForMember(x => x.CustomerId, c => c.MapFrom(x => x.CustomerId))
             .ForMember(x => x.Customers, c => c.MapFrom(x => x.Customers))
             .ForMember(x => x.SaleOrderAddress, c => c.MapFrom(x => x.OrderAddress))
             .ForMember(x => x.SaleOrderLines, c => c.MapFrom(x => x.OrderLines))
             .ForMember(x => x.OrderId, c => c.MapFrom(x => x.OrderId));
            CreateMap<SaleOrder, Orders>();

            CreateMap<OrderAddress, SaleOrderAddress>();
            CreateMap<SaleOrderAddress, OrderAddress>();


            CreateMap<OrderLines, SaleOrderLines>()
           .ForMember(x => x.ProductId, c => c.MapFrom(x => x.ProductId))
           .ForMember(x => x.Quantity, c => c.MapFrom(x => x.Quantity))
           .ForMember(x => x.LineTotal, c => c.MapFrom(x => x.LineTotal))
           .ForMember(x => x.UnitPrice, c => c.MapFrom(x => x.UnitPrice))
           .ForMember(x => x.SaleOrderLineProductOptions, c => c.MapFrom(x => x.OrderLineProductOptions))
           .ForMember(x => x.SaleOrderLineTaxRates, c => c.MapFrom(x => x.OrderLineTaxRates));
            CreateMap<SaleOrderLines, OrderLines>();

            CreateMap<OrderLineProductOptions, SaleOrderLineProductOptions>()
              .ForMember(x => x.SaleOrderLineProductOptionCombinations, c => c.MapFrom(x => x.OrderLineProductOptionCombinations));
            CreateMap<SaleOrderLineProductOptions, OrderLineProductOptions>();

            CreateMap<OrderLineProductOptionCombinations, SaleOrderLineProductOptionCombinations>()
              .ForMember(x => x.OptionValueId, c => c.MapFrom(x => x.OptionValueId))
              .ForMember(x => x.OptionId, c => c.MapFrom(x => x.OptionId))
              ;
            CreateMap<SaleOrderLineProductOptionCombinations, OrderLineProductOptionCombinations>();

            CreateMap<OrderLineTaxRate, SaleOrderLineTaxRate>()
           .ForMember(x => x.SaleOrderLineTaxRateName, c => c.MapFrom(x => x.OrderLineTaxRateName))
           .ForMember(x => x.SaleOrderTaxRate, c => c.MapFrom(x => x.OrderTaxRate))
           .ForMember(x => x.SaleOrderLineTaxRateCode, c => c.MapFrom(x => x.OrderLineTaxRateCode));
            CreateMap<SaleOrderLineTaxRate, OrderLineTaxRate>();

            CreateMap<BarriersPerRack, BarriersPerRackDto>();
            CreateMap<BarriersPerRackDto, BarriersPerRack>();

            CreateMap<UK_BarrierDeliveryPrices, UK_BarrierDeliveryPricesDto>();
            CreateMap<UK_BarrierDeliveryPricesDto, UK_BarrierDeliveryPrices>();

            CreateMap<UK_ExpressDataBagDelivery, UK_ExpressDataBagDeliveryDto>();
            CreateMap<UK_ExpressDataBagDeliveryDto, UK_ExpressDataBagDelivery>();

            CreateMap<UK_FreeDeliveryPrices, UK_FreeDeliveryPricesDto>();
            CreateMap<UK_FreeDeliveryPricesDto, UK_FreeDeliveryPrices>();

            CreateMap<UK_ParcelDelivery, UK_ParcelDeliveryDto>();
            CreateMap<UK_ParcelDeliveryDto, UK_ParcelDelivery>();

            CreateMap<UK_SpecialDeliveryPrices, UK_SpecialDeliveryPricesDto>();
            CreateMap<UK_SpecialDeliveryPricesDto, UK_SpecialDeliveryPrices>();

            CreateMap<Blog, BlogDto>()
              .ForMember(x => x.BlogCategoryName, c => c.MapFrom(x => x.BlogCategory.CategoryName))
              .ForMember(x => x.CustomerName, c => c.MapFrom(x => string.Format("{0} {1}", x.Customer.FirstName, x.Customer.LastName)));
      CreateMap<BlogDto, Blog>();

            CreateMap<BlogCategory, BlogCategoryDto>();
            CreateMap<BlogCategoryDto, BlogCategory>();

            CreateMap<BlogComment, BlogCommentDto>()
              .ForMember(x => x.BlogTitle, c => c.MapFrom(x => x.Blog.BlogTitle))
             .ForMember(x => x.CustomerName, c => c.MapFrom(x => string.Format("{0} {1}", x.Customer.FirstName, x.Customer.LastName)));
      CreateMap<BlogCommentDto, BlogComment>();

            CreateMap<PricePerRack, PricePerRackDto>();
            CreateMap<PricePerRackDto, PricePerRack>();

            CreateMap<EU_CourierCharges, EU_CourierChargesDto>()
           .ForMember(x => x.GeoZonesName, c => c.MapFrom(x => x.GeoZones.Name));
            CreateMap<EU_CourierChargesDto, EU_CourierCharges>();

            //JWT Tables
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();

            CreateMap<Permission, PermissionDto>();
            CreateMap<PermissionDto, Permission>();

            CreateMap<UserPermission, UserPermissionDto>()
               .ForMember(c => c.UserName, o => o.MapFrom(c => c.user.Name))
               .ForMember(c => c.PermissionTitle, o => o.MapFrom(c => c.permission.PermissionTitle));
            CreateMap<UserPermissionDto, UserPermission>();

        CreateMap<Role, RoleDto>();
            CreateMap<RoleDto, Role>();

            CreateMap<RolePermission, RolePermissionDto>()
        .ForMember(c => c.RoleTitle, o => o.MapFrom(c => c.Role.RoleTitle)) 
               .ForMember(c => c.PermissionTitle, o => o.MapFrom(c => c.permission.PermissionTitle));
            CreateMap<RolePermissionDto, RolePermission>();

            CreateMap<SaleOrder, SaleOrderReportDto>()
                      .ForMember(x => x.CustomerName, c => c.MapFrom(x => x.Customers.FirstName));
      CreateMap<SaleOrderReportDto, SaleOrder>();

            CreateMap<Newsletter, NewsletterDto>();
            CreateMap<NewsletterDto, Newsletter>();

            CreateMap<NewsletterSent, NewsletterSentDto>()
                .ForMember(x => x.NewsletterTitle, x => x.MapFrom(x => x.Newsletter.Title))
                .ForMember(x => x.SubscriberEmail, x => x.MapFrom(x => x.Subscriber.Email));
            CreateMap<NewsletterSentDto, NewsletterSent>();

            CreateMap<Subscriber, SubscriberDto>();
            CreateMap<SubscriberDto, Subscriber>();

           CreateMap<ApplicableTaxOnShipment, ApplicableTaxOnShipmentDto>()
                .ForMember(x => x.TaxClassTitle, x => x.MapFrom(x => x.TaxClass.TaxClassTitle));
            CreateMap<ApplicableTaxOnShipmentDto, ApplicableTaxOnShipment>();

        }
    }
}
