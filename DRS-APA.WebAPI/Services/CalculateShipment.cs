﻿using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Microserivce.Domain.Core.Dtos.Orders;
using DRS.APA.Microserivce.Domain.Core.Dtos.Shipment;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DRS.APA.Microservice.API.Services
{
    public class CalculateShipment : Controller
    {
        IProductCategoryRepository _ProductCategoryRepository;
        private readonly IProductRepository _productRepository;

        public CalculateShipment(IProductCategoryRepository ProductCategoryRepository,
            IProductRepository productRepository)
        {
            _ProductCategoryRepository = ProductCategoryRepository;
            _productRepository = productRepository;

        }
        public CalculateShipmentDto Calculate(CalculateShipmentDto ShipmentDto, UK_AllShipmentDto allShipments)
        {
            try
            {
                OrderDto order = ShipmentDto.order;

                //Shipment calculations :starts
                var AllShipments = allShipments;
                UK_ParcelDeliveryDto ParcelDelivery = null;
                if (AllShipments.UK_ParcelDelivery != null)
                {
                    foreach (var p in AllShipments.UK_ParcelDelivery)
                    {
                        if (p.UK_ParcelDeliveryId == order.UK_DeliveryDurationId)
                        {
                            ParcelDelivery = p;
                            order.UK_DeliveryDurationTitle = p.Duration;
                            break;
                        }
                    }
                }
                else
                {
                    ParcelDelivery = new UK_ParcelDeliveryDto();
                }
                //check if product is retrofit/operator/barrier
                order.UK_DeliveryDurationId = (int)ParcelDelivery.UK_ParcelDeliveryId;
                bool IsParcelDelivery = false;
                bool IsBarrierDelivery = false;
                foreach (var v in order.OrderLines)
                {
                    var product = _productRepository.GetProductByIdWithLessJoins(v.ProductId).Result;

                    if (product?.UKSpecialDeliveryPrices == true)
                    {
                        //Product is either retro fit or operator; will be checked based on category
                        //Get Product Category on ProductId
                        var ProductCategories = _ProductCategoryRepository.GetProductCategoriesOnProductId(v.ProductId).Result;
                        bool IsRetrofit = false;
                        bool IsOperator = false;
                        decimal RetrofitShipmentCost = 0;
                        decimal OperatorShipmentCost = 0;
                        if (ProductCategories != null)
                        {
                            foreach (var pc in ProductCategories)
                            {
                                if (pc.Code == "ret")
                                {
                                    //this is retrofit, apply shipment prices
                                    IsRetrofit = true;
                                    IsOperator = false;
                                    break;
                                }
                                else if (pc.Code == "op")
                                {
                                    //this is operator, apply operator prices
                                    IsOperator = true;
                                    IsRetrofit = false;
                                    break;
                                }
                            }
                            if (AllShipments.UK_SpecialDeliveryPrices != null)
                            {
                                var specialPrice = AllShipments.UK_SpecialDeliveryPrices
                                    .Where(x => x.IsActive != false)
                                    .OrderByDescending(x => x.UK_SpecialDeliveryPricesId)
                                    .FirstOrDefault();
                                RetrofitShipmentCost = specialPrice.RetrofitDeliveryCost;
                                OperatorShipmentCost = specialPrice.OperatorDeliveryCost;
                            }
                            if (IsRetrofit == true)
                            {
                                v.AppliedDeliveryPrice = RetrofitShipmentCost * v.Quantity;
                                order.TotalRetrofitPrices = order.TotalRetrofitPrices + v.AppliedDeliveryPrice;
                                order.TotalRetrofits = order.TotalRetrofits + v.Quantity;
                            }
                            else if (IsOperator == true)
                            {
                                v.AppliedDeliveryPrice = OperatorShipmentCost * v.Quantity;
                                order.TotalOperatorPrice = order.TotalOperatorPrice + v.AppliedDeliveryPrice;
                                order.TotalOperators = order.TotalOperators + v.Quantity;
                            }
                        }

                        if (product.UKFreeDeliverPrices == true)
                        {
                            v.IsEligibleForFreeDelivery = true;
                        }
                        else
                        {
                            v.IsEligibleForFreeDelivery = false;
                        }

                        v.DeliveryServiceType = "special";
                    }
                    else if (product?.UKBarrierDeliveryPrices == true)
                    {
                        //product is barrier; i.e. barrier prices will be applicable + rack Price ( Barrier Calculations for per Rack )
                        decimal BarrierShipmentPrice = 0;
                        var CheckBarrierShipmentPrice = AllShipments.UK_BarrierDeliveryPrices
                            .Where(x => x.Qty == v.Quantity)
                            .FirstOrDefault();
                        if (CheckBarrierShipmentPrice == null)
                        {

                            var GetOtherBarrierShipmentPrice = AllShipments.UK_BarrierDeliveryPrices
                                .Where(x => x.Qty <= v.Quantity).OrderByDescending(x => x.Qty)
                                .FirstOrDefault();
                            int CalcQty = v.Quantity - GetOtherBarrierShipmentPrice.Qty;

                            if (GetOtherBarrierShipmentPrice != null)
                            {
                                //BarrierShipmentPrice = GetOtherBarrierShipmentPrice.Price;
                                BarrierShipmentPrice = GetOtherBarrierShipmentPrice.Price + (GetOtherBarrierShipmentPrice.PerAdditionalQtyPrice * CalcQty);
                            }
                            else
                            {
                                BarrierShipmentPrice = 0;
                            }
                        }
                        else
                        {
                            BarrierShipmentPrice = CheckBarrierShipmentPrice.Price;
                        }
                        if (product.UKFreeDeliverPrices == true)
                        {
                            v.IsEligibleForFreeDelivery = true;
                        }
                        else
                        {
                            v.IsEligibleForFreeDelivery = false;
                        }
                        //v.AppliedDeliveryPrice = v.Quantity * BarrierShipmentPrice;
                        v.AppliedDeliveryPrice = BarrierShipmentPrice;
                        v.DeliveryServiceType = "barrier";

                        //order.TotalBarrierPrice += v.AppliedDeliveryPrice;
                        order.TotalBarriers += v.Quantity;
                        IsBarrierDelivery = true;
                    }

                    else if (product?.UKParcelDelivery == true)
                    {
                        IsParcelDelivery = true;
                        //order.UK_DeliveryDurationId = 3;
                        //Will be checked on Product Weight 0-5 & 5-15 rule & UK_ParcelDeliveryId selected by client from front end
                        if (product.UKFreeDeliverPrices == true)
                        {
                            v.IsEligibleForFreeDelivery = true;
                        }
                        else
                        {
                            v.IsEligibleForFreeDelivery = false;
                        }
                        if (Convert.ToDouble(product.Weight) <= ParcelDelivery.CustomKgMax)
                        {
                            v.AppliedDeliveryPrice = ParcelDelivery.PricePerCustomKg * v.Quantity;
                        }
                        else if (Convert.ToDouble(product.Weight) <= ParcelDelivery.CustomKgMax2)
                        {
                            v.AppliedDeliveryPrice = ParcelDelivery.PricePerCustomKg2 * v.Quantity;
                        }
                        else if (Convert.ToDouble(product.Weight) > ParcelDelivery.CustomKgMax2)
                        {
                            //calculate price with additional per Kg
                            v.AppliedDeliveryPrice = ParcelDelivery.PricePerCustomKg2 * v.Quantity;
                        }
                        v.DeliveryServiceType = "parcel";
                        order.TotalParcelWeight = order.TotalParcelWeight + (Convert.ToDouble(product.Weight) * v.Quantity);
                    }
                }
                //calculate total weight in shipment
                if (IsParcelDelivery == true)
                {
                    if (order.TotalParcelWeight <= ParcelDelivery.CustomKgMax)
                    {
                        order.TotalParcelPrice = ParcelDelivery.PricePerCustomKg;
                    }
                    else if (order.TotalParcelWeight > ParcelDelivery.CustomKgMax && order.TotalParcelWeight <= ParcelDelivery.CustomKgMax2)
                    {
                        order.TotalParcelPrice = ParcelDelivery.PricePerCustomKg2;
                    }
                    else if (order.TotalParcelWeight > ParcelDelivery.CustomKgMax2)
                    {
                        order.TotalParcelPrice = ParcelDelivery.PricePerCustomKg2;
                        double AdditionalWeight = order.TotalParcelWeight - ParcelDelivery.CustomKgMax2;
                        double AdditionalPrice = AdditionalWeight * Convert.ToDouble(ParcelDelivery.AdditionalPerKg);
                        order.TotalParcelPrice = order.TotalParcelPrice + Convert.ToDecimal(AdditionalPrice);
                    }
                }
                //Method to calculate barriers and allocate racks ( Barriers per Rack formula from DB )
                if (IsBarrierDelivery == true)
                {
                    var GetBarriersPerRack = AllShipments.BarriersPerRack.Where(x => x.IsActive != false).FirstOrDefault();
                    if (GetBarriersPerRack != null)
                    {
                        int BarriersPerRack = GetBarriersPerRack.BarrierPerRackQty;
                        decimal PricePerRack = GetBarriersPerRack.PricePerRack;
                        double RacksUsed = (double)order.TotalBarriers / BarriersPerRack;// =1.0 | 1.16 |2.40
                        int RacksUsedInteger = (int)RacksUsed;//1|1|2
                        //if (RacksUsed != RacksUsedInteger)
                        if (RacksUsedInteger != RacksUsed)
                        {
                            RacksUsedInteger = RacksUsedInteger + 1;
                        }
                        order.NumberOfRacksUsed = order.NumberOfRacksUsed + RacksUsedInteger;
                        order.TotalRackPrice = order.TotalRackPrice + (order.NumberOfRacksUsed * PricePerRack);

                        //calculate barrier price from db thresh hold
                        decimal BarrierShipmentPrice = 0;
                        var CheckBarrierShipmentPrice = AllShipments.UK_BarrierDeliveryPrices
                            .Where(x => x.Qty == order.TotalBarriers)
                            .FirstOrDefault();
                        if (CheckBarrierShipmentPrice == null)
                        {
                            var GetOtherBarrierShipmentPrice = AllShipments.UK_BarrierDeliveryPrices
                                .Where(x => x.Qty <= order.TotalBarriers).OrderByDescending(x => x.Qty)
                                .FirstOrDefault();
                            int CalcQty = order.TotalBarriers - GetOtherBarrierShipmentPrice.Qty;

                            if (GetOtherBarrierShipmentPrice != null)
                            {
                                //BarrierShipmentPrice = GetOtherBarrierShipmentPrice.Price;
                                BarrierShipmentPrice = GetOtherBarrierShipmentPrice.Price + (GetOtherBarrierShipmentPrice.PerAdditionalQtyPrice * CalcQty);
                            }
                            else
                            {
                                BarrierShipmentPrice = 0;
                            }
                        }
                        else
                        {
                            BarrierShipmentPrice = CheckBarrierShipmentPrice.Price;
                        }
                        order.TotalBarrierPrice = BarrierShipmentPrice;
                        //
                    }
                }
                //calculate free delivery price based on Selected Items IsEligible for Free with FREE condition from db
                var FreeDelivery = AllShipments.UK_FreeDeliveryPrices.Where(x => x.IsActive == true).LastOrDefault();
                decimal FreeDeliveryAmount = 0;
                if (FreeDelivery != null)
                {
                    if (FreeDelivery.FreeOrderStartsFrom > 0)
                    {
                        decimal TotalLinePriceEligibleForFreeDelivery = order.OrderAmountWithTaxAndDiscountFreeEligible;
                        
                        if (TotalLinePriceEligibleForFreeDelivery >= FreeDelivery.FreeOrderStartsFrom)
                        {
                            

                            //Add a boolean to check order eligible for free delivery in Orders
                            //Get here all lines with Not Eligible Items, put them in temp object having
                            //1.Operators 2.Retrofits 3. Barriers 4.Parcels
                            //Apply Formulas and calculate prices for shipment
                            //Put them in an object and save in Order objects

                            order.IsOrderEligibleForFreeDelivery = true;
                            IsParcelDelivery = false;
                            IsBarrierDelivery = false;
                            foreach (var v in order.OrderLines)
                            {
                                var product = _productRepository.GetProductByIdWithLessJoins(v.ProductId).Result;
                                //Checks if product Not eligible for FREE delivery i.e. UKFreeDeliverPrices not true (empty or false )
                                if (product?.UKFreeDeliverPrices != true)
                                {
                                    if (product?.UKSpecialDeliveryPrices == true)
                                    {
                                        //Product is either retro fit or operator; will be checked based on category
                                        //Get Product Category on ProductId
                                        var ProductCategories = _ProductCategoryRepository.GetProductCategoriesOnProductId(v.ProductId).Result;
                                        bool IsRetrofit = false;
                                        bool IsOperator = false;
                                        decimal RetrofitShipmentCost = 0;
                                        decimal OperatorShipmentCost = 0;
                                        if (ProductCategories != null)
                                        {
                                            foreach (var pc in ProductCategories)
                                            {
                                                if (pc.Code == "ret")
                                                {
                                                    //this is retrofit, apply shipment prices
                                                    IsRetrofit = true;
                                                    IsOperator = false;
                                                    break;
                                                }
                                                else if (pc.Code == "op")
                                                {
                                                    //this is operator, apply operator prices
                                                    IsOperator = true;
                                                    IsRetrofit = false;
                                                    break;
                                                }
                                            }
                                            if (AllShipments.UK_SpecialDeliveryPrices != null)
                                            {
                                                var specialPrice = AllShipments.UK_SpecialDeliveryPrices
                                                    .Where(x => x.IsActive != false)
                                                    .OrderByDescending(x => x.UK_SpecialDeliveryPricesId)
                                                    .FirstOrDefault();
                                                RetrofitShipmentCost = specialPrice.RetrofitDeliveryCost;
                                                OperatorShipmentCost = specialPrice.OperatorDeliveryCost;
                                            }
                                            if (IsRetrofit == true)
                                            {
                                                
                                                order.TotalRetrofitPricesNotEligible = order.TotalRetrofitPricesNotEligible + (RetrofitShipmentCost * v.Quantity);
                                                order.TotalRetrofitsNotEligible = order.TotalRetrofitsNotEligible + v.Quantity;
                                            }
                                            else if (IsOperator == true)
                                            {
                                                order.TotalOperatorPricesNotEligible = order.TotalOperatorPricesNotEligible + (OperatorShipmentCost * v.Quantity);
                                                order.TotalOperatorsNotEligible = order.TotalOperatorsNotEligible + v.Quantity;
                                            }
                                        }
                                    }
                                    else if (product?.UKBarrierDeliveryPrices == true)
                                    {
                                        //product is barrier; i.e. barrier prices will be applicable + rack Price ( Barrier Calculations for per Rack )
                                        decimal BarrierShipmentPrice = 0;
                                        var CheckBarrierShipmentPrice = AllShipments.UK_BarrierDeliveryPrices
                                            .Where(x => x.Qty == v.Quantity)
                                            .FirstOrDefault();
                                        if (CheckBarrierShipmentPrice == null)
                                        {
                                            var GetOtherBarrierShipmentPrice = AllShipments.UK_BarrierDeliveryPrices
                                                .Where(x => x.Qty <= v.Quantity).OrderByDescending(x => x.Qty)
                                                .FirstOrDefault();
                                            int CalcQty = v.Quantity - GetOtherBarrierShipmentPrice.Qty;

                                            if (GetOtherBarrierShipmentPrice != null)
                                            {
                                                BarrierShipmentPrice = GetOtherBarrierShipmentPrice.Price + (GetOtherBarrierShipmentPrice.PerAdditionalQtyPrice * CalcQty);

                                            }
                                            else
                                            {
                                                BarrierShipmentPrice = 0;
                                            }
                                        }
                                        else
                                        {
                                            BarrierShipmentPrice = CheckBarrierShipmentPrice.Price;
                                        }
                                        order.TotalBarrierPriceNotEligible += (v.Quantity * BarrierShipmentPrice);
                                        order.TotalBarriersNotEligible += v.Quantity;
                                        IsBarrierDelivery = true;
                                    }
                                    else if (product?.UKParcelDelivery == true)
                                    {
                                        IsParcelDelivery = true;
                                        order.ParcelWeightNotEligible = order.ParcelWeightNotEligible + (Convert.ToDouble(product.Weight) * v.Quantity);
                                    }
                                    //
                                }
                            }
                            //
                            if (IsParcelDelivery == true)
                            {
                                if (order.ParcelWeightNotEligible <= ParcelDelivery.CustomKgMax)
                                {
                                    order.ParcelPriceNotEligible = ParcelDelivery.PricePerCustomKg;
                                }
                                else if (order.ParcelWeightNotEligible > ParcelDelivery.CustomKgMax && order.ParcelWeightNotEligible <= ParcelDelivery.CustomKgMax2)
                                {
                                    order.ParcelPriceNotEligible = ParcelDelivery.PricePerCustomKg2;
                                }
                                else if (order.ParcelWeightNotEligible > ParcelDelivery.CustomKgMax2)
                                {
                                    order.ParcelPriceNotEligible = ParcelDelivery.PricePerCustomKg2;
                                    double AdditionalWeight = order.ParcelWeightNotEligible - ParcelDelivery.CustomKgMax2;
                                    double AdditionalPrice = AdditionalWeight * Convert.ToDouble(ParcelDelivery.AdditionalPerKg);
                                    order.ParcelPriceNotEligible = order.ParcelPriceNotEligible + Convert.ToDecimal(AdditionalPrice);
                                }
                            }
                            //Method to calculate barriers and allocate racks ( Barriers per Rack formula from DB )
                            if (IsBarrierDelivery == true)
                            {
                                var GetBarriersPerRack = AllShipments.BarriersPerRack.Where(x => x.IsActive != false).FirstOrDefault();
                                if (GetBarriersPerRack != null)
                                {
                                    order.NumberOfRacksNotEligible = order.NumberOfRacksUsed;
                                    order.TotalRackPriceNotEligible = order.TotalRackPrice;

                                    //calculate barrier price from db thresh hold
                                    decimal BarrierShipmentPrice = 0;
                                    var CheckBarrierShipmentPrice = AllShipments.UK_BarrierDeliveryPrices
                                        .Where(x => x.Qty == order.TotalBarriersNotEligible)
                                        .FirstOrDefault();
                                    if (CheckBarrierShipmentPrice == null)
                                    {
                                        var GetOtherBarrierShipmentPrice = AllShipments.UK_BarrierDeliveryPrices
                                            .Where(x => x.Qty <= order.TotalBarriersNotEligible).OrderByDescending(x => x.Qty)
                                            .FirstOrDefault();
                                        int CalcQty = order.TotalBarriersNotEligible - GetOtherBarrierShipmentPrice.Qty;

                                        if (GetOtherBarrierShipmentPrice != null)
                                        {
                                            BarrierShipmentPrice = GetOtherBarrierShipmentPrice.Price + (GetOtherBarrierShipmentPrice.PerAdditionalQtyPrice * CalcQty);
                                        }
                                        else
                                        {
                                            BarrierShipmentPrice = 0;
                                        }
                                    }
                                    else
                                    {
                                        BarrierShipmentPrice = CheckBarrierShipmentPrice.Price;
                                    }
                                    order.TotalBarrierPriceNotEligible = BarrierShipmentPrice;
                                }
                            }
                        }
                    }
                }
                order.FreeDeliveryEligiblePrice = FreeDeliveryAmount;

                //Shipment calculations :ends
                return ShipmentDto;
            }
            catch (Exception)
            {
                ShipmentDto.error = "System Exception";
                return ShipmentDto;
            }
        }
    }
}
