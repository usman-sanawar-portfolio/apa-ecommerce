using AutoMapper;
using DRS.APA.Core.Repositories;
using DRS.APA.Domain.Core.Repositories.MasterData;
using DRS.APA.Domain.Core.Repositories.Order;
using DRS.APA.Domain.Core.Shared;
using DRS.APA.Microserivce.Domain.Core.Domain.Orders;
using DRS.APA.Microserivce.Domain.Core.Dtos.Orders;
using DRS.APA.Microservice.API.Controllers.Common;
using DRSAPA.Microservice.API.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Stripe;
using System;
using System.Net;
using System.Threading.Tasks;

namespace DRS.APA.API.Services
{
    public class ConversionOrderToSaleOrder : Controller
    {

        private readonly IOrderRepository _orderRepository;
        private readonly ISaleOrderRepository _saleorderRepository;
        
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        protected readonly LinkGenerator _linkGenerator;
        IMailService _mailService;
    private IConfiguration _configuration;
    public ConversionOrderToSaleOrder(IOrderRepository orderRepository, ISaleOrderRepository saleorderRepository, IReviewRepository reviewRepository,
        IUnitOfWork unitOfWork, IMapper mapper, ILogger<ConversionOrderToSaleOrder> logger, LinkGenerator linkGenerator, IConfiguration Configuration,
        IMailService mailService)
        {
            _orderRepository = orderRepository;
            _saleorderRepository = saleorderRepository;
            _mapper = mapper;
            _logger = logger;
            _linkGenerator = linkGenerator;
            _mailService = mailService;
      _configuration = Configuration;
    }

        public async Task<IActionResult> ConvertOrderToSaleOrder(string orderIdentifier, string sessionId, string paymentIntentId)
        {

            try
            {
                ResponseObject resObject;
                bool session = false;
                if (!string.IsNullOrEmpty(sessionId))
                {
                    session = _orderRepository.CheckONOrderSessionId(sessionId, paymentIntentId);
                }
                if (session != true)
                {
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = (OrderDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }
                if (sessionId == null)
                {
                    resObject = new ResponseObject
                    {
                        Message = ConstantProps.dataNotFoundText,
                        Status = "error",
                        Data = (OrderAddressDto)null,
                        HttpStatusCode = HttpStatusCode.BadRequest
                    };
                    return Json(resObject);
                }

                _orderRepository.UpdateOrderStatus(orderIdentifier);
                var OrderdataRepo = await _orderRepository.GetOrderByidentifier(orderIdentifier);

                var data = _mapper.Map<SaleOrder>(OrderdataRepo);
                _saleorderRepository.AddSaleOrderForOrder(data);
                _orderRepository.UpdateOrderStatusAfterPaymentRecieve(orderIdentifier);
                _saleorderRepository.DeductQuantityAfterSaleOrder(orderIdentifier);

                resObject = new ResponseObject
                {
                    Message = ConstantProps.SavedSuccessfullyText,
                    Status = "success",
                    Data = data,
                    HttpStatusCode = HttpStatusCode.OK
                };
                return Json(resObject);
            }
            catch (Exception e)
            {
                string msg = ConstantProps.UnableToProcessPutText + " " + BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem);
                _logger.LogWarning(e.Message);

                var resObject = new ResponseObject
                {
                    Message = BuiltMessages.BuiltMessage(e, LoggingEvents.GetItem),
                    Message2 = msg,
                    Status = "error",
                    HttpStatusCode = HttpStatusCode.InternalServerError
                };
                return Json(resObject);
            }
            finally
            {
                var saleOrder = await _saleorderRepository.GetSaleOrderByIdentifier(orderIdentifier);
                string CustomerEmail = saleOrder.Customers.Email;
                
                await _mailService.SendSaleOrderConfirmationEmail(saleOrder, CustomerEmail);
            }
        }
        public async Task<Coupon> AddCouppon(decimal amount)
        {

            try
            {

                var optionCoupon = new CouponCreateOptions
                {
                    //PercentOff = Convert.ToInt64(amount),
                    PercentOff = amount,
                    Currency = "gbp",
                    Duration = "once",
                };
                var optionservice = new CouponService();
                optionservice.Create(optionCoupon);

        StripeConfiguration.ApiKey = _configuration["StripeApiKey"];
 
                var optionsCoupon = new CouponListOptions
                {
                    Limit = 1
                };
                var serviceCoupon = new CouponService();
                StripeList<Coupon> coupons = serviceCoupon.List(
                  optionsCoupon
                );
                var res = coupons.Data;
                var res1 = coupons.StripeResponse;
                var res2 = coupons.Object;
                Coupon couponsObj = new Coupon();
                string couponId = "";
                foreach (var d in res)
                {
                    couponId = d.Id;
                    couponsObj.Id = d.Id;
                    couponsObj.AmountOff = d.AmountOff;
                    couponsObj.PercentOff = d.PercentOff;
                    couponsObj.Currency = d.Currency;
                    couponsObj.Duration = d.Duration;
                    couponsObj.Livemode = d.Livemode;
                    couponsObj.Name = "Discounts";
                }
                var xx = JsonConvert.DeserializeObject<Coupon>(res1.Content);
                var res4 = coupons.Object;
                return couponsObj;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
