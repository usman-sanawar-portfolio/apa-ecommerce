﻿using System;

namespace DRS.APA.API.Filters
{
    public class MyAPIException : Exception
    {
        public MyAPIException()
        { }

        public MyAPIException(string message)
            : base(message)
        { }

        public MyAPIException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
