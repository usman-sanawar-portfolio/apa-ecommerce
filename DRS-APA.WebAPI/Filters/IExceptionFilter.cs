﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace DRS.APA.API.Filters
{
    public interface IExceptionFilter : IFilterMetadata
    {
        void OnException(ExceptionContext context);
    }
}
